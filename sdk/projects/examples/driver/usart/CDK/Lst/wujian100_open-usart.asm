
.//Obj/wujian100_open-usart.elf:     file format elf32-littleriscv


Disassembly of section .text:

00000000 <Reset_Handler>:
    .globl  Reset_Handler
    .type   Reset_Handler, %function
Reset_Handler:
.option push
.option norelax
    la      gp, __global_pointer$
       0:	20000197          	auipc	gp,0x20000
       4:	67418193          	addi	gp,gp,1652 # 20000674 <__global_pointer$>
.option pop
    la      a0, Default_Handler
       8:	00000517          	auipc	a0,0x0
       c:	1b850513          	addi	a0,a0,440 # 1c0 <Default_Handler>
    ori     a0, a0, 3
      10:	00356513          	ori	a0,a0,3
    csrw    mtvec, a0
      14:	30551073          	csrw	mtvec,a0

    la      a0, __Vectors
      18:	98c18513          	addi	a0,gp,-1652 # 20000000 <__Vectors>
    csrw    mtvt, a0
      1c:	30751073          	csrw	mtvt,a0

    la      sp, g_top_irqstack
      20:	20001117          	auipc	sp,0x20001
      24:	65410113          	addi	sp,sp,1620 # 20001674 <g_top_irqstack>

    /* Load data section */
    la      a0, __erodata
      28:	00007517          	auipc	a0,0x7
      2c:	90050513          	addi	a0,a0,-1792 # 6928 <__ctor_end__>
    la      a1, __data_start__
      30:	98c18593          	addi	a1,gp,-1652 # 20000000 <__Vectors>
    la      a2, __data_end__
      34:	00018613          	mv	a2,gp
    bgeu    a1, a2, 2f
      38:	00c5fa63          	bgeu	a1,a2,4c <Reset_Handler+0x4c>
1:
    lw      t0, (a0)
      3c:	00052283          	lw	t0,0(a0)
    sw      t0, (a1)
      40:	0055a023          	sw	t0,0(a1)
    addi    a0, a0, 4
      44:	0511                	addi	a0,a0,4
    addi    a1, a1, 4
      46:	0591                	addi	a1,a1,4
    bltu    a1, a2, 1b
      48:	fec5eae3          	bltu	a1,a2,3c <Reset_Handler+0x3c>
2:

    /* Clear bss section */
    la      a0, __bss_start__
      4c:	00018513          	mv	a0,gp
    la      a1, __bss_end__
      50:	20002597          	auipc	a1,0x20002
      54:	e3058593          	addi	a1,a1,-464 # 20001e80 <__bss_end__>
    bgeu    a0, a1, 2f
      58:	00b57763          	bgeu	a0,a1,66 <Reset_Handler+0x66>
1:
    sw      zero, (a0)
      5c:	00052023          	sw	zero,0(a0)
    addi    a0, a0, 4
      60:	0511                	addi	a0,a0,4
    bltu    a0, a1, 1b
      62:	feb56de3          	bltu	a0,a1,5c <Reset_Handler+0x5c>
2:

#ifndef __NO_SYSTEM_INIT
    jal     SystemInit
      66:	30d030ef          	jal	ra,3b72 <SystemInit>
#endif

#ifndef __NO_BOARD_INIT
    jal     board_init
      6a:	419020ef          	jal	ra,2c82 <board_init>
#endif

    jal     main
      6e:	79c040ef          	jal	ra,480a <main>

00000072 <__exit>:

    .size   Reset_Handler, . - Reset_Handler

__exit:
    j      __exit
      72:	a001                	j	72 <__exit>
	...

00000080 <Default_IRQHandler>:
    .align  2
    .global Default_IRQHandler
    .weak   Default_IRQHandler
    .type   Default_IRQHandler, %function
Default_IRQHandler:
    addi    sp, sp, -48
      80:	7179                	addi	sp,sp,-48
    sw      t0, 4(sp)
      82:	c216                	sw	t0,4(sp)
    sw      t1, 8(sp)
      84:	c41a                	sw	t1,8(sp)
    csrr    t0, mepc
      86:	341022f3          	csrr	t0,mepc
    csrr    t1, mcause
      8a:	34202373          	csrr	t1,mcause
    sw      t1, 40(sp)
      8e:	d41a                	sw	t1,40(sp)
    sw      t0, 44(sp)
      90:	d616                	sw	t0,44(sp)
    csrs    mstatus, 8
      92:	30046073          	csrsi	mstatus,8

    sw      ra, 0(sp)
      96:	c006                	sw	ra,0(sp)
    sw      t2, 12(sp)
      98:	c61e                	sw	t2,12(sp)
    sw      a0, 16(sp)
      9a:	c82a                	sw	a0,16(sp)
    sw      a1, 20(sp)
      9c:	ca2e                	sw	a1,20(sp)
    sw      a2, 24(sp)
      9e:	cc32                	sw	a2,24(sp)
    sw      a3, 28(sp)
      a0:	ce36                	sw	a3,28(sp)
    sw      a4, 32(sp)
      a2:	d03a                	sw	a4,32(sp)
    sw      a5, 36(sp)
      a4:	d23e                	sw	a5,36(sp)

    andi    t1, t1, 0x3FF
      a6:	3ff37313          	andi	t1,t1,1023
    slli    t1, t1, 2
      aa:	030a                	slli	t1,t1,0x2

    la      t0, g_irqvector
      ac:	20002297          	auipc	t0,0x20002
      b0:	bc428293          	addi	t0,t0,-1084 # 20001c70 <g_irqvector>
    add     t0, t0, t1
      b4:	929a                	add	t0,t0,t1
    lw      t2, (t0)
      b6:	0002a383          	lw	t2,0(t0)
    jalr    t2
      ba:	9382                	jalr	t2

    csrc    mstatus, 8
      bc:	30047073          	csrci	mstatus,8

    lw      a1, 40(sp)
      c0:	55a2                	lw	a1,40(sp)
    andi    a0, a1, 0x3FF
      c2:	3ff5f513          	andi	a0,a1,1023

    /* clear pending */
    li      a2, 0xE000E100
      c6:	e000e637          	lui	a2,0xe000e
      ca:	10060613          	addi	a2,a2,256 # e000e100 <__heap_end+0xbffde100>
    add     a2, a2, a0
      ce:	962a                	add	a2,a2,a0
    lb      a3, 0(a2)
      d0:	00060683          	lb	a3,0(a2)
    li      a4, 1
      d4:	4705                	li	a4,1
    not     a4, a4
      d6:	fff74713          	not	a4,a4
    and     a5, a4, a3
      da:	00d777b3          	and	a5,a4,a3
    sb      a5, 0(a2)
      de:	00f60023          	sb	a5,0(a2)

    li      t0, MSTATUS_PRV1
      e2:	000022b7          	lui	t0,0x2
      e6:	88028293          	addi	t0,t0,-1920 # 1880 <__muldf3+0x260>
    csrs    mstatus, t0
      ea:	3002a073          	csrs	mstatus,t0
    csrw    mcause, a1
      ee:	34259073          	csrw	mcause,a1
    lw      t0, 44(sp)
      f2:	52b2                	lw	t0,44(sp)
    csrw    mepc, t0
      f4:	34129073          	csrw	mepc,t0
    lw      ra, 0(sp)
      f8:	4082                	lw	ra,0(sp)
    lw      t0, 4(sp)
      fa:	4292                	lw	t0,4(sp)
    lw      t1, 8(sp)
      fc:	4322                	lw	t1,8(sp)
    lw      t2, 12(sp)
      fe:	43b2                	lw	t2,12(sp)
    lw      a0, 16(sp)
     100:	4542                	lw	a0,16(sp)
    lw      a1, 20(sp)
     102:	45d2                	lw	a1,20(sp)
    lw      a2, 24(sp)
     104:	4662                	lw	a2,24(sp)
    lw      a3, 28(sp)
     106:	46f2                	lw	a3,28(sp)
    lw      a4, 32(sp)
     108:	5702                	lw	a4,32(sp)
    lw      a5, 36(sp)
     10a:	5792                	lw	a5,36(sp)

    addi    sp, sp, 48
     10c:	6145                	addi	sp,sp,48
    mret
     10e:	30200073          	mret
     112:	0001                	nop

00000114 <trap>:
    .align  2
    .global trap
    .type   trap, %function
trap:
    /* Check for interrupt */
    addi    sp, sp, -4
     114:	1171                	addi	sp,sp,-4
    sw      t0, 0x0(sp)
     116:	c016                	sw	t0,0(sp)
    csrr    t0, mcause
     118:	342022f3          	csrr	t0,mcause

    blt     t0, x0, .Lirq
     11c:	0602c463          	bltz	t0,184 <trap+0x70>

    addi    sp, sp, 4
     120:	0111                	addi	sp,sp,4

    la      t0, g_trap_sp
     122:	20001297          	auipc	t0,0x20001
     126:	75228293          	addi	t0,t0,1874 # 20001874 <g_trap_sp>
    addi    t0, t0, -68
     12a:	fbc28293          	addi	t0,t0,-68
    sw      x1, 0(t0)
     12e:	0012a023          	sw	ra,0(t0)
    sw      x2, 4(t0)
     132:	0022a223          	sw	sp,4(t0)
    sw      x3, 8(t0)
     136:	0032a423          	sw	gp,8(t0)
    sw      x4, 12(t0)
     13a:	0042a623          	sw	tp,12(t0)
    sw      x6, 20(t0)
     13e:	0062aa23          	sw	t1,20(t0)
    sw      x7, 24(t0)
     142:	0072ac23          	sw	t2,24(t0)
    sw      x8, 28(t0)
     146:	0082ae23          	sw	s0,28(t0)
    sw      x9, 32(t0)
     14a:	0292a023          	sw	s1,32(t0)
    sw      x10, 36(t0)
     14e:	02a2a223          	sw	a0,36(t0)
    sw      x11, 40(t0)
     152:	02b2a423          	sw	a1,40(t0)
    sw      x12, 44(t0)
     156:	02c2a623          	sw	a2,44(t0)
    sw      x13, 48(t0)
     15a:	02d2a823          	sw	a3,48(t0)
    sw      x14, 52(t0)
     15e:	02e2aa23          	sw	a4,52(t0)
    sw      x15, 56(t0)
     162:	02f2ac23          	sw	a5,56(t0)
    csrr    a0, mepc
     166:	34102573          	csrr	a0,mepc
    sw      a0, 60(t0)
     16a:	02a2ae23          	sw	a0,60(t0)
    csrr    a0, mstatus
     16e:	30002573          	csrr	a0,mstatus
    sw      a0, 64(t0)
     172:	04a2a023          	sw	a0,64(t0)

    mv      a0, t0
     176:	8516                	mv	a0,t0
    lw      t0, -4(sp)
     178:	ffc12283          	lw	t0,-4(sp)
    mv      sp, a0
     17c:	812a                	mv	sp,a0
    sw      t0, 16(sp)
     17e:	c816                	sw	t0,16(sp)

    jal     trap_c
     180:	251030ef          	jal	ra,3bd0 <trap_c>


.Lirq:
    lw      t0, 0x0(sp)
     184:	4282                	lw	t0,0(sp)
    addi    sp, sp, 4
     186:	0111                	addi	sp,sp,4
    j       Default_IRQHandler
     188:	ef9ff06f          	j	80 <Default_IRQHandler>
     18c:	00000013          	nop
     190:	00000013          	nop
     194:	00000013          	nop
     198:	00000013          	nop
     19c:	00000013          	nop
     1a0:	00000013          	nop
     1a4:	00000013          	nop
     1a8:	00000013          	nop
     1ac:	00000013          	nop
     1b0:	00000013          	nop
     1b4:	00000013          	nop
     1b8:	00000013          	nop
     1bc:	00000013          	nop

000001c0 <Default_Handler>:
    .align  6
    .weak   Default_Handler
    .global Default_Handler
    .type   Default_Handler, %function
Default_Handler:
    j      trap
     1c0:	bf91                	j	114 <trap>
	...

000001f4 <__udivdi3>:
     1f4:	1161                	addi	sp,sp,-8
     1f6:	c222                	sw	s0,4(sp)
     1f8:	c026                	sw	s1,0(sp)
     1fa:	87ae                	mv	a5,a1
     1fc:	20069d63          	bnez	a3,416 <__min_heap_size+0x216>
     200:	83b6                	mv	t2,a3
     202:	8332                	mv	t1,a2
     204:	82aa                	mv	t0,a0
     206:	00005697          	auipc	a3,0x5
     20a:	58a68693          	addi	a3,a3,1418 # 5790 <__clz_tab>
     20e:	0ac5fd63          	bgeu	a1,a2,2c8 <__min_heap_size+0xc8>
     212:	6741                	lui	a4,0x10
     214:	0ae67363          	bgeu	a2,a4,2ba <__min_heap_size+0xba>
     218:	0ff00713          	li	a4,255
     21c:	00c73733          	sltu	a4,a4,a2
     220:	070e                	slli	a4,a4,0x3
     222:	00e653b3          	srl	t2,a2,a4
     226:	969e                	add	a3,a3,t2
     228:	0006c683          	lbu	a3,0(a3)
     22c:	9736                	add	a4,a4,a3
     22e:	02000693          	li	a3,32
     232:	8e99                	sub	a3,a3,a4
     234:	ca91                	beqz	a3,248 <__min_heap_size+0x48>
     236:	00d795b3          	sll	a1,a5,a3
     23a:	00e55733          	srl	a4,a0,a4
     23e:	00d61333          	sll	t1,a2,a3
     242:	8dd9                	or	a1,a1,a4
     244:	00d512b3          	sll	t0,a0,a3
     248:	01035513          	srli	a0,t1,0x10
     24c:	02a5f733          	remu	a4,a1,a0
     250:	01031613          	slli	a2,t1,0x10
     254:	8241                	srli	a2,a2,0x10
     256:	0102d693          	srli	a3,t0,0x10
     25a:	02a5d5b3          	divu	a1,a1,a0
     25e:	0742                	slli	a4,a4,0x10
     260:	8ed9                	or	a3,a3,a4
     262:	02b607b3          	mul	a5,a2,a1
     266:	872e                	mv	a4,a1
     268:	00f6fc63          	bgeu	a3,a5,280 <__min_heap_size+0x80>
     26c:	969a                	add	a3,a3,t1
     26e:	fff58713          	addi	a4,a1,-1
     272:	0066e763          	bltu	a3,t1,280 <__min_heap_size+0x80>
     276:	00f6f563          	bgeu	a3,a5,280 <__min_heap_size+0x80>
     27a:	ffe58713          	addi	a4,a1,-2
     27e:	969a                	add	a3,a3,t1
     280:	8e9d                	sub	a3,a3,a5
     282:	02a6f7b3          	remu	a5,a3,a0
     286:	02c2                	slli	t0,t0,0x10
     288:	0102d293          	srli	t0,t0,0x10
     28c:	02a6d6b3          	divu	a3,a3,a0
     290:	07c2                	slli	a5,a5,0x10
     292:	0057e2b3          	or	t0,a5,t0
     296:	02d60633          	mul	a2,a2,a3
     29a:	8536                	mv	a0,a3
     29c:	00c2fb63          	bgeu	t0,a2,2b2 <__min_heap_size+0xb2>
     2a0:	929a                	add	t0,t0,t1
     2a2:	fff68513          	addi	a0,a3,-1
     2a6:	0062e663          	bltu	t0,t1,2b2 <__min_heap_size+0xb2>
     2aa:	00c2f463          	bgeu	t0,a2,2b2 <__min_heap_size+0xb2>
     2ae:	ffe68513          	addi	a0,a3,-2
     2b2:	0742                	slli	a4,a4,0x10
     2b4:	8f49                	or	a4,a4,a0
     2b6:	4581                	li	a1,0
     2b8:	a85d                	j	36e <__min_heap_size+0x16e>
     2ba:	010003b7          	lui	t2,0x1000
     2be:	4741                	li	a4,16
     2c0:	f67661e3          	bltu	a2,t2,222 <__min_heap_size+0x22>
     2c4:	4761                	li	a4,24
     2c6:	bfb1                	j	222 <__min_heap_size+0x22>
     2c8:	e601                	bnez	a2,2d0 <__min_heap_size+0xd0>
     2ca:	4705                	li	a4,1
     2cc:	02c75333          	divu	t1,a4,a2
     2d0:	6741                	lui	a4,0x10
     2d2:	0ae37363          	bgeu	t1,a4,378 <__min_heap_size+0x178>
     2d6:	0ff00713          	li	a4,255
     2da:	00677363          	bgeu	a4,t1,2e0 <__min_heap_size+0xe0>
     2de:	43a1                	li	t2,8
     2e0:	00735733          	srl	a4,t1,t2
     2e4:	96ba                	add	a3,a3,a4
     2e6:	0006c703          	lbu	a4,0(a3)
     2ea:	971e                	add	a4,a4,t2
     2ec:	02000393          	li	t2,32
     2f0:	40e383b3          	sub	t2,t2,a4
     2f4:	08039963          	bnez	t2,386 <__min_heap_size+0x186>
     2f8:	406787b3          	sub	a5,a5,t1
     2fc:	4585                	li	a1,1
     2fe:	01035413          	srli	s0,t1,0x10
     302:	01031393          	slli	t2,t1,0x10
     306:	0103d393          	srli	t2,t2,0x10
     30a:	0102d693          	srli	a3,t0,0x10
     30e:	0287f733          	remu	a4,a5,s0
     312:	0287d7b3          	divu	a5,a5,s0
     316:	0742                	slli	a4,a4,0x10
     318:	8ed9                	or	a3,a3,a4
     31a:	02f38633          	mul	a2,t2,a5
     31e:	873e                	mv	a4,a5
     320:	00c6fc63          	bgeu	a3,a2,338 <__min_heap_size+0x138>
     324:	969a                	add	a3,a3,t1
     326:	fff78713          	addi	a4,a5,-1
     32a:	0066e763          	bltu	a3,t1,338 <__min_heap_size+0x138>
     32e:	00c6f563          	bgeu	a3,a2,338 <__min_heap_size+0x138>
     332:	ffe78713          	addi	a4,a5,-2
     336:	969a                	add	a3,a3,t1
     338:	8e91                	sub	a3,a3,a2
     33a:	0286f7b3          	remu	a5,a3,s0
     33e:	02c2                	slli	t0,t0,0x10
     340:	0102d293          	srli	t0,t0,0x10
     344:	0286d6b3          	divu	a3,a3,s0
     348:	07c2                	slli	a5,a5,0x10
     34a:	0057e2b3          	or	t0,a5,t0
     34e:	02d383b3          	mul	t2,t2,a3
     352:	8536                	mv	a0,a3
     354:	0072fb63          	bgeu	t0,t2,36a <__min_heap_size+0x16a>
     358:	929a                	add	t0,t0,t1
     35a:	fff68513          	addi	a0,a3,-1
     35e:	0062e663          	bltu	t0,t1,36a <__min_heap_size+0x16a>
     362:	0072f463          	bgeu	t0,t2,36a <__min_heap_size+0x16a>
     366:	ffe68513          	addi	a0,a3,-2
     36a:	0742                	slli	a4,a4,0x10
     36c:	8f49                	or	a4,a4,a0
     36e:	853a                	mv	a0,a4
     370:	4412                	lw	s0,4(sp)
     372:	4482                	lw	s1,0(sp)
     374:	0121                	addi	sp,sp,8
     376:	8082                	ret
     378:	01000737          	lui	a4,0x1000
     37c:	43c1                	li	t2,16
     37e:	f6e361e3          	bltu	t1,a4,2e0 <__min_heap_size+0xe0>
     382:	43e1                	li	t2,24
     384:	bfb1                	j	2e0 <__min_heap_size+0xe0>
     386:	00731333          	sll	t1,t1,t2
     38a:	00e7d6b3          	srl	a3,a5,a4
     38e:	01035593          	srli	a1,t1,0x10
     392:	007797b3          	sll	a5,a5,t2
     396:	007512b3          	sll	t0,a0,t2
     39a:	02b6d3b3          	divu	t2,a3,a1
     39e:	00e55733          	srl	a4,a0,a4
     3a2:	00f76633          	or	a2,a4,a5
     3a6:	01031793          	slli	a5,t1,0x10
     3aa:	83c1                	srli	a5,a5,0x10
     3ac:	01065513          	srli	a0,a2,0x10
     3b0:	02b6f733          	remu	a4,a3,a1
     3b4:	027786b3          	mul	a3,a5,t2
     3b8:	0742                	slli	a4,a4,0x10
     3ba:	8f49                	or	a4,a4,a0
     3bc:	851e                	mv	a0,t2
     3be:	00d77c63          	bgeu	a4,a3,3d6 <__min_heap_size+0x1d6>
     3c2:	971a                	add	a4,a4,t1
     3c4:	fff38513          	addi	a0,t2,-1 # ffffff <__ctor_end__+0xff96d7>
     3c8:	00676763          	bltu	a4,t1,3d6 <__min_heap_size+0x1d6>
     3cc:	00d77563          	bgeu	a4,a3,3d6 <__min_heap_size+0x1d6>
     3d0:	ffe38513          	addi	a0,t2,-2
     3d4:	971a                	add	a4,a4,t1
     3d6:	40d706b3          	sub	a3,a4,a3
     3da:	02b6f733          	remu	a4,a3,a1
     3de:	02b6d6b3          	divu	a3,a3,a1
     3e2:	0742                	slli	a4,a4,0x10
     3e4:	02d783b3          	mul	t2,a5,a3
     3e8:	01061793          	slli	a5,a2,0x10
     3ec:	83c1                	srli	a5,a5,0x10
     3ee:	8fd9                	or	a5,a5,a4
     3f0:	8736                	mv	a4,a3
     3f2:	0077fc63          	bgeu	a5,t2,40a <__min_heap_size+0x20a>
     3f6:	979a                	add	a5,a5,t1
     3f8:	fff68713          	addi	a4,a3,-1
     3fc:	0067e763          	bltu	a5,t1,40a <__min_heap_size+0x20a>
     400:	0077f563          	bgeu	a5,t2,40a <__min_heap_size+0x20a>
     404:	ffe68713          	addi	a4,a3,-2
     408:	979a                	add	a5,a5,t1
     40a:	01051593          	slli	a1,a0,0x10
     40e:	407787b3          	sub	a5,a5,t2
     412:	8dd9                	or	a1,a1,a4
     414:	b5ed                	j	2fe <__min_heap_size+0xfe>
     416:	14d5e063          	bltu	a1,a3,556 <__min_heap_size+0x356>
     41a:	6741                	lui	a4,0x10
     41c:	02e6fd63          	bgeu	a3,a4,456 <__min_heap_size+0x256>
     420:	0ff00713          	li	a4,255
     424:	00d73733          	sltu	a4,a4,a3
     428:	070e                	slli	a4,a4,0x3
     42a:	00e6d333          	srl	t1,a3,a4
     42e:	00005597          	auipc	a1,0x5
     432:	36258593          	addi	a1,a1,866 # 5790 <__clz_tab>
     436:	959a                	add	a1,a1,t1
     438:	0005c583          	lbu	a1,0(a1)
     43c:	972e                	add	a4,a4,a1
     43e:	02000593          	li	a1,32
     442:	8d99                	sub	a1,a1,a4
     444:	e185                	bnez	a1,464 <__min_heap_size+0x264>
     446:	4705                	li	a4,1
     448:	f2f6e3e3          	bltu	a3,a5,36e <__min_heap_size+0x16e>
     44c:	00c53633          	sltu	a2,a0,a2
     450:	00164713          	xori	a4,a2,1
     454:	bf29                	j	36e <__min_heap_size+0x16e>
     456:	010005b7          	lui	a1,0x1000
     45a:	4741                	li	a4,16
     45c:	fcb6e7e3          	bltu	a3,a1,42a <__min_heap_size+0x22a>
     460:	4761                	li	a4,24
     462:	b7e1                	j	42a <__min_heap_size+0x22a>
     464:	00e65333          	srl	t1,a2,a4
     468:	00b696b3          	sll	a3,a3,a1
     46c:	00d366b3          	or	a3,t1,a3
     470:	00e7d3b3          	srl	t2,a5,a4
     474:	0106d413          	srli	s0,a3,0x10
     478:	0283f2b3          	remu	t0,t2,s0
     47c:	00e55733          	srl	a4,a0,a4
     480:	00b797b3          	sll	a5,a5,a1
     484:	8fd9                	or	a5,a5,a4
     486:	01069713          	slli	a4,a3,0x10
     48a:	8341                	srli	a4,a4,0x10
     48c:	0107d313          	srli	t1,a5,0x10
     490:	00b61633          	sll	a2,a2,a1
     494:	0283d3b3          	divu	t2,t2,s0
     498:	02c2                	slli	t0,t0,0x10
     49a:	0062e2b3          	or	t0,t0,t1
     49e:	027704b3          	mul	s1,a4,t2
     4a2:	831e                	mv	t1,t2
     4a4:	0092fc63          	bgeu	t0,s1,4bc <__min_heap_size+0x2bc>
     4a8:	92b6                	add	t0,t0,a3
     4aa:	fff38313          	addi	t1,t2,-1
     4ae:	00d2e763          	bltu	t0,a3,4bc <__min_heap_size+0x2bc>
     4b2:	0092f563          	bgeu	t0,s1,4bc <__min_heap_size+0x2bc>
     4b6:	ffe38313          	addi	t1,t2,-2
     4ba:	92b6                	add	t0,t0,a3
     4bc:	409282b3          	sub	t0,t0,s1
     4c0:	0282f3b3          	remu	t2,t0,s0
     4c4:	07c2                	slli	a5,a5,0x10
     4c6:	83c1                	srli	a5,a5,0x10
     4c8:	0282d2b3          	divu	t0,t0,s0
     4cc:	03c2                	slli	t2,t2,0x10
     4ce:	00f3e7b3          	or	a5,t2,a5
     4d2:	02570733          	mul	a4,a4,t0
     4d6:	8396                	mv	t2,t0
     4d8:	00e7fc63          	bgeu	a5,a4,4f0 <__min_heap_size+0x2f0>
     4dc:	97b6                	add	a5,a5,a3
     4de:	fff28393          	addi	t2,t0,-1
     4e2:	00d7e763          	bltu	a5,a3,4f0 <__min_heap_size+0x2f0>
     4e6:	00e7f563          	bgeu	a5,a4,4f0 <__min_heap_size+0x2f0>
     4ea:	ffe28393          	addi	t2,t0,-2
     4ee:	97b6                	add	a5,a5,a3
     4f0:	8f99                	sub	a5,a5,a4
     4f2:	01031713          	slli	a4,t1,0x10
     4f6:	00776733          	or	a4,a4,t2
     4fa:	63c1                	lui	t2,0x10
     4fc:	fff38313          	addi	t1,t2,-1 # ffff <__ctor_end__+0x96d7>
     500:	006776b3          	and	a3,a4,t1
     504:	01075413          	srli	s0,a4,0x10
     508:	00667333          	and	t1,a2,t1
     50c:	8241                	srli	a2,a2,0x10
     50e:	026682b3          	mul	t0,a3,t1
     512:	02640333          	mul	t1,s0,t1
     516:	02c40433          	mul	s0,s0,a2
     51a:	02c68633          	mul	a2,a3,a2
     51e:	0102d693          	srli	a3,t0,0x10
     522:	961a                	add	a2,a2,t1
     524:	96b2                	add	a3,a3,a2
     526:	0066f363          	bgeu	a3,t1,52c <__min_heap_size+0x32c>
     52a:	941e                	add	s0,s0,t2
     52c:	0106d613          	srli	a2,a3,0x10
     530:	9432                	add	s0,s0,a2
     532:	0287e063          	bltu	a5,s0,552 <__min_heap_size+0x352>
     536:	d88790e3          	bne	a5,s0,2b6 <__min_heap_size+0xb6>
     53a:	67c1                	lui	a5,0x10
     53c:	17fd                	addi	a5,a5,-1
     53e:	8efd                	and	a3,a3,a5
     540:	06c2                	slli	a3,a3,0x10
     542:	00f2f2b3          	and	t0,t0,a5
     546:	00b51533          	sll	a0,a0,a1
     54a:	9696                	add	a3,a3,t0
     54c:	4581                	li	a1,0
     54e:	e2d570e3          	bgeu	a0,a3,36e <__min_heap_size+0x16e>
     552:	177d                	addi	a4,a4,-1
     554:	b38d                	j	2b6 <__min_heap_size+0xb6>
     556:	4581                	li	a1,0
     558:	4701                	li	a4,0
     55a:	bd11                	j	36e <__min_heap_size+0x16e>

0000055c <__umoddi3>:
     55c:	1151                	addi	sp,sp,-12
     55e:	c422                	sw	s0,8(sp)
     560:	c226                	sw	s1,4(sp)
     562:	87aa                	mv	a5,a0
     564:	832e                	mv	t1,a1
     566:	1c069b63          	bnez	a3,73c <__umoddi3+0x1e0>
     56a:	8736                	mv	a4,a3
     56c:	8432                	mv	s0,a2
     56e:	00005697          	auipc	a3,0x5
     572:	22268693          	addi	a3,a3,546 # 5790 <__clz_tab>
     576:	0ac5fb63          	bgeu	a1,a2,62c <__umoddi3+0xd0>
     57a:	62c1                	lui	t0,0x10
     57c:	0a567163          	bgeu	a2,t0,61e <__umoddi3+0xc2>
     580:	0ff00293          	li	t0,255
     584:	00c2f363          	bgeu	t0,a2,58a <__umoddi3+0x2e>
     588:	4721                	li	a4,8
     58a:	00e652b3          	srl	t0,a2,a4
     58e:	9696                	add	a3,a3,t0
     590:	0006c283          	lbu	t0,0(a3)
     594:	02000393          	li	t2,32
     598:	9716                	add	a4,a4,t0
     59a:	40e383b3          	sub	t2,t2,a4
     59e:	00038c63          	beqz	t2,5b6 <__umoddi3+0x5a>
     5a2:	007595b3          	sll	a1,a1,t2
     5a6:	00e55733          	srl	a4,a0,a4
     5aa:	00761433          	sll	s0,a2,t2
     5ae:	00b76333          	or	t1,a4,a1
     5b2:	007517b3          	sll	a5,a0,t2
     5b6:	01045613          	srli	a2,s0,0x10
     5ba:	02c376b3          	remu	a3,t1,a2
     5be:	01041513          	slli	a0,s0,0x10
     5c2:	8141                	srli	a0,a0,0x10
     5c4:	0107d713          	srli	a4,a5,0x10
     5c8:	02c35333          	divu	t1,t1,a2
     5cc:	06c2                	slli	a3,a3,0x10
     5ce:	8f55                	or	a4,a4,a3
     5d0:	02650333          	mul	t1,a0,t1
     5d4:	00677863          	bgeu	a4,t1,5e4 <__umoddi3+0x88>
     5d8:	9722                	add	a4,a4,s0
     5da:	00876563          	bltu	a4,s0,5e4 <__umoddi3+0x88>
     5de:	00677363          	bgeu	a4,t1,5e4 <__umoddi3+0x88>
     5e2:	9722                	add	a4,a4,s0
     5e4:	40670733          	sub	a4,a4,t1
     5e8:	02c776b3          	remu	a3,a4,a2
     5ec:	07c2                	slli	a5,a5,0x10
     5ee:	83c1                	srli	a5,a5,0x10
     5f0:	02c75733          	divu	a4,a4,a2
     5f4:	02e50733          	mul	a4,a0,a4
     5f8:	01069513          	slli	a0,a3,0x10
     5fc:	8fc9                	or	a5,a5,a0
     5fe:	00e7f863          	bgeu	a5,a4,60e <__umoddi3+0xb2>
     602:	97a2                	add	a5,a5,s0
     604:	0087e563          	bltu	a5,s0,60e <__umoddi3+0xb2>
     608:	00e7f363          	bgeu	a5,a4,60e <__umoddi3+0xb2>
     60c:	97a2                	add	a5,a5,s0
     60e:	8f99                	sub	a5,a5,a4
     610:	0077d533          	srl	a0,a5,t2
     614:	4581                	li	a1,0
     616:	4422                	lw	s0,8(sp)
     618:	4492                	lw	s1,4(sp)
     61a:	0131                	addi	sp,sp,12
     61c:	8082                	ret
     61e:	010002b7          	lui	t0,0x1000
     622:	4741                	li	a4,16
     624:	f65663e3          	bltu	a2,t0,58a <__umoddi3+0x2e>
     628:	4761                	li	a4,24
     62a:	b785                	j	58a <__umoddi3+0x2e>
     62c:	e601                	bnez	a2,634 <__umoddi3+0xd8>
     62e:	4605                	li	a2,1
     630:	02865433          	divu	s0,a2,s0
     634:	6641                	lui	a2,0x10
     636:	08c47263          	bgeu	s0,a2,6ba <__umoddi3+0x15e>
     63a:	0ff00613          	li	a2,255
     63e:	00867363          	bgeu	a2,s0,644 <__umoddi3+0xe8>
     642:	4721                	li	a4,8
     644:	00e45633          	srl	a2,s0,a4
     648:	96b2                	add	a3,a3,a2
     64a:	0006c283          	lbu	t0,0(a3)
     64e:	02000393          	li	t2,32
     652:	9716                	add	a4,a4,t0
     654:	40e383b3          	sub	t2,t2,a4
     658:	06039863          	bnez	t2,6c8 <__umoddi3+0x16c>
     65c:	8d81                	sub	a1,a1,s0
     65e:	01045693          	srli	a3,s0,0x10
     662:	01041513          	slli	a0,s0,0x10
     666:	8141                	srli	a0,a0,0x10
     668:	0107d613          	srli	a2,a5,0x10
     66c:	02d5f733          	remu	a4,a1,a3
     670:	02d5d5b3          	divu	a1,a1,a3
     674:	0742                	slli	a4,a4,0x10
     676:	8f51                	or	a4,a4,a2
     678:	02b505b3          	mul	a1,a0,a1
     67c:	00b77863          	bgeu	a4,a1,68c <__umoddi3+0x130>
     680:	9722                	add	a4,a4,s0
     682:	00876563          	bltu	a4,s0,68c <__umoddi3+0x130>
     686:	00b77363          	bgeu	a4,a1,68c <__umoddi3+0x130>
     68a:	9722                	add	a4,a4,s0
     68c:	40b705b3          	sub	a1,a4,a1
     690:	02d5f733          	remu	a4,a1,a3
     694:	07c2                	slli	a5,a5,0x10
     696:	83c1                	srli	a5,a5,0x10
     698:	02d5d5b3          	divu	a1,a1,a3
     69c:	02b505b3          	mul	a1,a0,a1
     6a0:	01071513          	slli	a0,a4,0x10
     6a4:	8fc9                	or	a5,a5,a0
     6a6:	00b7f863          	bgeu	a5,a1,6b6 <__umoddi3+0x15a>
     6aa:	97a2                	add	a5,a5,s0
     6ac:	0087e563          	bltu	a5,s0,6b6 <__umoddi3+0x15a>
     6b0:	00b7f363          	bgeu	a5,a1,6b6 <__umoddi3+0x15a>
     6b4:	97a2                	add	a5,a5,s0
     6b6:	8f8d                	sub	a5,a5,a1
     6b8:	bfa1                	j	610 <__umoddi3+0xb4>
     6ba:	01000637          	lui	a2,0x1000
     6be:	4741                	li	a4,16
     6c0:	f8c462e3          	bltu	s0,a2,644 <__umoddi3+0xe8>
     6c4:	4761                	li	a4,24
     6c6:	bfbd                	j	644 <__umoddi3+0xe8>
     6c8:	00741433          	sll	s0,s0,t2
     6cc:	00e5d6b3          	srl	a3,a1,a4
     6d0:	01045613          	srli	a2,s0,0x10
     6d4:	00e55733          	srl	a4,a0,a4
     6d8:	007595b3          	sll	a1,a1,t2
     6dc:	00b762b3          	or	t0,a4,a1
     6e0:	02c6f733          	remu	a4,a3,a2
     6e4:	01041593          	slli	a1,s0,0x10
     6e8:	81c1                	srli	a1,a1,0x10
     6ea:	007517b3          	sll	a5,a0,t2
     6ee:	0102d513          	srli	a0,t0,0x10
     6f2:	02c6d6b3          	divu	a3,a3,a2
     6f6:	0742                	slli	a4,a4,0x10
     6f8:	8f49                	or	a4,a4,a0
     6fa:	02d586b3          	mul	a3,a1,a3
     6fe:	00d77863          	bgeu	a4,a3,70e <__umoddi3+0x1b2>
     702:	9722                	add	a4,a4,s0
     704:	00876563          	bltu	a4,s0,70e <__umoddi3+0x1b2>
     708:	00d77363          	bgeu	a4,a3,70e <__umoddi3+0x1b2>
     70c:	9722                	add	a4,a4,s0
     70e:	40d706b3          	sub	a3,a4,a3
     712:	02c6f733          	remu	a4,a3,a2
     716:	02c6d6b3          	divu	a3,a3,a2
     71a:	0742                	slli	a4,a4,0x10
     71c:	02d586b3          	mul	a3,a1,a3
     720:	01029593          	slli	a1,t0,0x10
     724:	81c1                	srli	a1,a1,0x10
     726:	8dd9                	or	a1,a1,a4
     728:	00d5f863          	bgeu	a1,a3,738 <__umoddi3+0x1dc>
     72c:	95a2                	add	a1,a1,s0
     72e:	0085e563          	bltu	a1,s0,738 <__umoddi3+0x1dc>
     732:	00d5f363          	bgeu	a1,a3,738 <__umoddi3+0x1dc>
     736:	95a2                	add	a1,a1,s0
     738:	8d95                	sub	a1,a1,a3
     73a:	b715                	j	65e <__umoddi3+0x102>
     73c:	ecd5ede3          	bltu	a1,a3,616 <__umoddi3+0xba>
     740:	6741                	lui	a4,0x10
     742:	04e6f563          	bgeu	a3,a4,78c <__umoddi3+0x230>
     746:	0ff00393          	li	t2,255
     74a:	00d3b733          	sltu	a4,t2,a3
     74e:	070e                	slli	a4,a4,0x3
     750:	00e6d3b3          	srl	t2,a3,a4
     754:	00005297          	auipc	t0,0x5
     758:	03c28293          	addi	t0,t0,60 # 5790 <__clz_tab>
     75c:	929e                	add	t0,t0,t2
     75e:	0002c383          	lbu	t2,0(t0)
     762:	02000293          	li	t0,32
     766:	93ba                	add	t2,t2,a4
     768:	407282b3          	sub	t0,t0,t2
     76c:	02029763          	bnez	t0,79a <__umoddi3+0x23e>
     770:	00b6e463          	bltu	a3,a1,778 <__umoddi3+0x21c>
     774:	00c56963          	bltu	a0,a2,786 <__umoddi3+0x22a>
     778:	40c507b3          	sub	a5,a0,a2
     77c:	8d95                	sub	a1,a1,a3
     77e:	00f53533          	sltu	a0,a0,a5
     782:	40a58333          	sub	t1,a1,a0
     786:	853e                	mv	a0,a5
     788:	859a                	mv	a1,t1
     78a:	b571                	j	616 <__umoddi3+0xba>
     78c:	010002b7          	lui	t0,0x1000
     790:	4741                	li	a4,16
     792:	fa56efe3          	bltu	a3,t0,750 <__umoddi3+0x1f4>
     796:	4761                	li	a4,24
     798:	bf65                	j	750 <__umoddi3+0x1f4>
     79a:	007657b3          	srl	a5,a2,t2
     79e:	005696b3          	sll	a3,a3,t0
     7a2:	8edd                	or	a3,a3,a5
     7a4:	0075d4b3          	srl	s1,a1,t2
     7a8:	0106d413          	srli	s0,a3,0x10
     7ac:	00755733          	srl	a4,a0,t2
     7b0:	005517b3          	sll	a5,a0,t0
     7b4:	0284d533          	divu	a0,s1,s0
     7b8:	c03e                	sw	a5,0(sp)
     7ba:	005595b3          	sll	a1,a1,t0
     7be:	8dd9                	or	a1,a1,a4
     7c0:	01069713          	slli	a4,a3,0x10
     7c4:	8341                	srli	a4,a4,0x10
     7c6:	00561633          	sll	a2,a2,t0
     7ca:	0284f7b3          	remu	a5,s1,s0
     7ce:	02a704b3          	mul	s1,a4,a0
     7d2:	01079313          	slli	t1,a5,0x10
     7d6:	0105d793          	srli	a5,a1,0x10
     7da:	00f367b3          	or	a5,t1,a5
     7de:	832a                	mv	t1,a0
     7e0:	0097fc63          	bgeu	a5,s1,7f8 <__umoddi3+0x29c>
     7e4:	97b6                	add	a5,a5,a3
     7e6:	fff50313          	addi	t1,a0,-1
     7ea:	00d7e763          	bltu	a5,a3,7f8 <__umoddi3+0x29c>
     7ee:	0097f563          	bgeu	a5,s1,7f8 <__umoddi3+0x29c>
     7f2:	ffe50313          	addi	t1,a0,-2
     7f6:	97b6                	add	a5,a5,a3
     7f8:	8f85                	sub	a5,a5,s1
     7fa:	0287f533          	remu	a0,a5,s0
     7fe:	05c2                	slli	a1,a1,0x10
     800:	81c1                	srli	a1,a1,0x10
     802:	0287d433          	divu	s0,a5,s0
     806:	0542                	slli	a0,a0,0x10
     808:	8dc9                	or	a1,a1,a0
     80a:	02870733          	mul	a4,a4,s0
     80e:	87a2                	mv	a5,s0
     810:	00e5fc63          	bgeu	a1,a4,828 <__umoddi3+0x2cc>
     814:	95b6                	add	a1,a1,a3
     816:	fff40793          	addi	a5,s0,-1
     81a:	00d5e763          	bltu	a1,a3,828 <__umoddi3+0x2cc>
     81e:	00e5f563          	bgeu	a1,a4,828 <__umoddi3+0x2cc>
     822:	ffe40793          	addi	a5,s0,-2
     826:	95b6                	add	a1,a1,a3
     828:	0342                	slli	t1,t1,0x10
     82a:	6441                	lui	s0,0x10
     82c:	00f36333          	or	t1,t1,a5
     830:	40e58733          	sub	a4,a1,a4
     834:	fff40593          	addi	a1,s0,-1 # ffff <__ctor_end__+0x96d7>
     838:	00b377b3          	and	a5,t1,a1
     83c:	01065493          	srli	s1,a2,0x10
     840:	01035313          	srli	t1,t1,0x10
     844:	8df1                	and	a1,a1,a2
     846:	02b78533          	mul	a0,a5,a1
     84a:	02b305b3          	mul	a1,t1,a1
     84e:	029787b3          	mul	a5,a5,s1
     852:	02930333          	mul	t1,t1,s1
     856:	97ae                	add	a5,a5,a1
     858:	01055493          	srli	s1,a0,0x10
     85c:	97a6                	add	a5,a5,s1
     85e:	00b7f363          	bgeu	a5,a1,864 <__umoddi3+0x308>
     862:	9322                	add	t1,t1,s0
     864:	0107d593          	srli	a1,a5,0x10
     868:	932e                	add	t1,t1,a1
     86a:	65c1                	lui	a1,0x10
     86c:	15fd                	addi	a1,a1,-1
     86e:	8fed                	and	a5,a5,a1
     870:	07c2                	slli	a5,a5,0x10
     872:	8d6d                	and	a0,a0,a1
     874:	953e                	add	a0,a0,a5
     876:	00676763          	bltu	a4,t1,884 <__umoddi3+0x328>
     87a:	00671d63          	bne	a4,t1,894 <__umoddi3+0x338>
     87e:	4782                	lw	a5,0(sp)
     880:	00a7fa63          	bgeu	a5,a0,894 <__umoddi3+0x338>
     884:	40c50633          	sub	a2,a0,a2
     888:	00c53533          	sltu	a0,a0,a2
     88c:	96aa                	add	a3,a3,a0
     88e:	40d30333          	sub	t1,t1,a3
     892:	8532                	mv	a0,a2
     894:	4782                	lw	a5,0(sp)
     896:	40670333          	sub	t1,a4,t1
     89a:	40a78533          	sub	a0,a5,a0
     89e:	00a7b5b3          	sltu	a1,a5,a0
     8a2:	40b305b3          	sub	a1,t1,a1
     8a6:	007597b3          	sll	a5,a1,t2
     8aa:	00555533          	srl	a0,a0,t0
     8ae:	8d5d                	or	a0,a0,a5
     8b0:	0055d5b3          	srl	a1,a1,t0
     8b4:	b38d                	j	616 <__umoddi3+0xba>

000008b6 <__adddf3>:
     8b6:	001002b7          	lui	t0,0x100
     8ba:	12fd                	addi	t0,t0,-1
     8bc:	1131                	addi	sp,sp,-20
     8be:	00b2f333          	and	t1,t0,a1
     8c2:	030e                	slli	t1,t1,0x3
     8c4:	01d55793          	srli	a5,a0,0x1d
     8c8:	c622                	sw	s0,12(sp)
     8ca:	c426                	sw	s1,8(sp)
     8cc:	0145d413          	srli	s0,a1,0x14
     8d0:	01f5d493          	srli	s1,a1,0x1f
     8d4:	00d2f5b3          	and	a1,t0,a3
     8d8:	0146d293          	srli	t0,a3,0x14
     8dc:	7ff47413          	andi	s0,s0,2047
     8e0:	0067e7b3          	or	a5,a5,t1
     8e4:	7ff2f293          	andi	t0,t0,2047
     8e8:	01d65313          	srli	t1,a2,0x1d
     8ec:	058e                	slli	a1,a1,0x3
     8ee:	c806                	sw	ra,16(sp)
     8f0:	82fd                	srli	a3,a3,0x1f
     8f2:	00351713          	slli	a4,a0,0x3
     8f6:	00b36333          	or	t1,t1,a1
     8fa:	060e                	slli	a2,a2,0x3
     8fc:	40540533          	sub	a0,s0,t0
     900:	22d49663          	bne	s1,a3,b2c <__adddf3+0x276>
     904:	0ea05163          	blez	a0,9e6 <__adddf3+0x130>
     908:	02029863          	bnez	t0,938 <__adddf3+0x82>
     90c:	00c366b3          	or	a3,t1,a2
     910:	56068c63          	beqz	a3,e88 <__adddf3+0x5d2>
     914:	fff50593          	addi	a1,a0,-1
     918:	e989                	bnez	a1,92a <__adddf3+0x74>
     91a:	963a                	add	a2,a2,a4
     91c:	00e63733          	sltu	a4,a2,a4
     920:	979a                	add	a5,a5,t1
     922:	97ba                	add	a5,a5,a4
     924:	8732                	mv	a4,a2
     926:	4405                	li	s0,1
     928:	a8b1                	j	984 <__adddf3+0xce>
     92a:	7ff00693          	li	a3,2047
     92e:	00d51e63          	bne	a0,a3,94a <__adddf3+0x94>
     932:	7ff00413          	li	s0,2047
     936:	aa61                	j	ace <__adddf3+0x218>
     938:	7ff00693          	li	a3,2047
     93c:	18d40963          	beq	s0,a3,ace <__adddf3+0x218>
     940:	008006b7          	lui	a3,0x800
     944:	00d36333          	or	t1,t1,a3
     948:	85aa                	mv	a1,a0
     94a:	03800693          	li	a3,56
     94e:	08b6c763          	blt	a3,a1,9dc <__adddf3+0x126>
     952:	46fd                	li	a3,31
     954:	04b6cf63          	blt	a3,a1,9b2 <__adddf3+0xfc>
     958:	02000513          	li	a0,32
     95c:	8d0d                	sub	a0,a0,a1
     95e:	00a316b3          	sll	a3,t1,a0
     962:	00b652b3          	srl	t0,a2,a1
     966:	00a61633          	sll	a2,a2,a0
     96a:	0056e6b3          	or	a3,a3,t0
     96e:	00c03633          	snez	a2,a2
     972:	8e55                	or	a2,a2,a3
     974:	00b35333          	srl	t1,t1,a1
     978:	963a                	add	a2,a2,a4
     97a:	933e                	add	t1,t1,a5
     97c:	00e637b3          	sltu	a5,a2,a4
     980:	979a                	add	a5,a5,t1
     982:	8732                	mv	a4,a2
     984:	00879693          	slli	a3,a5,0x8
     988:	1406d363          	bgez	a3,ace <__adddf3+0x218>
     98c:	0405                	addi	s0,s0,1
     98e:	7ff00693          	li	a3,2047
     992:	48d40c63          	beq	s0,a3,e2a <__adddf3+0x574>
     996:	ff800637          	lui	a2,0xff800
     99a:	167d                	addi	a2,a2,-1
     99c:	8e7d                	and	a2,a2,a5
     99e:	00175693          	srli	a3,a4,0x1
     9a2:	8b05                	andi	a4,a4,1
     9a4:	01f61793          	slli	a5,a2,0x1f
     9a8:	8f55                	or	a4,a4,a3
     9aa:	8f5d                	or	a4,a4,a5
     9ac:	00165793          	srli	a5,a2,0x1
     9b0:	aa39                	j	ace <__adddf3+0x218>
     9b2:	fe058693          	addi	a3,a1,-32 # ffe0 <__ctor_end__+0x96b8>
     9b6:	02000293          	li	t0,32
     9ba:	00d356b3          	srl	a3,t1,a3
     9be:	4501                	li	a0,0
     9c0:	00558863          	beq	a1,t0,9d0 <__adddf3+0x11a>
     9c4:	04000513          	li	a0,64
     9c8:	40b505b3          	sub	a1,a0,a1
     9cc:	00b31533          	sll	a0,t1,a1
     9d0:	8e49                	or	a2,a2,a0
     9d2:	00c03633          	snez	a2,a2
     9d6:	8e55                	or	a2,a2,a3
     9d8:	4301                	li	t1,0
     9da:	bf79                	j	978 <__adddf3+0xc2>
     9dc:	00c36633          	or	a2,t1,a2
     9e0:	00c03633          	snez	a2,a2
     9e4:	bfd5                	j	9d8 <__adddf3+0x122>
     9e6:	c945                	beqz	a0,a96 <__adddf3+0x1e0>
     9e8:	408285b3          	sub	a1,t0,s0
     9ec:	e40d                	bnez	s0,a16 <__adddf3+0x160>
     9ee:	00e7e6b3          	or	a3,a5,a4
     9f2:	42068463          	beqz	a3,e1a <__adddf3+0x564>
     9f6:	fff58693          	addi	a3,a1,-1
     9fa:	e699                	bnez	a3,a08 <__adddf3+0x152>
     9fc:	9732                	add	a4,a4,a2
     9fe:	979a                	add	a5,a5,t1
     a00:	00c73633          	sltu	a2,a4,a2
     a04:	97b2                	add	a5,a5,a2
     a06:	b705                	j	926 <__adddf3+0x70>
     a08:	7ff00513          	li	a0,2047
     a0c:	00a59d63          	bne	a1,a0,a26 <__adddf3+0x170>
     a10:	879a                	mv	a5,t1
     a12:	8732                	mv	a4,a2
     a14:	bf39                	j	932 <__adddf3+0x7c>
     a16:	7ff00693          	li	a3,2047
     a1a:	fed28be3          	beq	t0,a3,a10 <__adddf3+0x15a>
     a1e:	008006b7          	lui	a3,0x800
     a22:	8fd5                	or	a5,a5,a3
     a24:	86ae                	mv	a3,a1
     a26:	03800593          	li	a1,56
     a2a:	06d5c263          	blt	a1,a3,a8e <__adddf3+0x1d8>
     a2e:	45fd                	li	a1,31
     a30:	02d5ca63          	blt	a1,a3,a64 <__adddf3+0x1ae>
     a34:	02000513          	li	a0,32
     a38:	8d15                	sub	a0,a0,a3
     a3a:	00d753b3          	srl	t2,a4,a3
     a3e:	00a795b3          	sll	a1,a5,a0
     a42:	00a71733          	sll	a4,a4,a0
     a46:	0075e5b3          	or	a1,a1,t2
     a4a:	00e03733          	snez	a4,a4
     a4e:	8f4d                	or	a4,a4,a1
     a50:	00d7d6b3          	srl	a3,a5,a3
     a54:	9732                	add	a4,a4,a2
     a56:	006687b3          	add	a5,a3,t1
     a5a:	00c73633          	sltu	a2,a4,a2
     a5e:	97b2                	add	a5,a5,a2
     a60:	8416                	mv	s0,t0
     a62:	b70d                	j	984 <__adddf3+0xce>
     a64:	fe068593          	addi	a1,a3,-32 # 7fffe0 <__ctor_end__+0x7f96b8>
     a68:	02000393          	li	t2,32
     a6c:	00b7d5b3          	srl	a1,a5,a1
     a70:	4501                	li	a0,0
     a72:	00768863          	beq	a3,t2,a82 <__adddf3+0x1cc>
     a76:	04000513          	li	a0,64
     a7a:	40d506b3          	sub	a3,a0,a3
     a7e:	00d79533          	sll	a0,a5,a3
     a82:	8f49                	or	a4,a4,a0
     a84:	00e03733          	snez	a4,a4
     a88:	8f4d                	or	a4,a4,a1
     a8a:	4681                	li	a3,0
     a8c:	b7e1                	j	a54 <__adddf3+0x19e>
     a8e:	8f5d                	or	a4,a4,a5
     a90:	00e03733          	snez	a4,a4
     a94:	bfdd                	j	a8a <__adddf3+0x1d4>
     a96:	00140593          	addi	a1,s0,1
     a9a:	7fe5f693          	andi	a3,a1,2046
     a9e:	e6a5                	bnez	a3,b06 <__adddf3+0x250>
     aa0:	00e7e6b3          	or	a3,a5,a4
     aa4:	e429                	bnez	s0,aee <__adddf3+0x238>
     aa6:	36068d63          	beqz	a3,e20 <__adddf3+0x56a>
     aaa:	00c366b3          	or	a3,t1,a2
     aae:	c285                	beqz	a3,ace <__adddf3+0x218>
     ab0:	963a                	add	a2,a2,a4
     ab2:	00e63733          	sltu	a4,a2,a4
     ab6:	979a                	add	a5,a5,t1
     ab8:	97ba                	add	a5,a5,a4
     aba:	00879693          	slli	a3,a5,0x8
     abe:	8732                	mv	a4,a2
     ac0:	0006d763          	bgez	a3,ace <__adddf3+0x218>
     ac4:	ff8006b7          	lui	a3,0xff800
     ac8:	16fd                	addi	a3,a3,-1
     aca:	8ff5                	and	a5,a5,a3
     acc:	4405                	li	s0,1
     ace:	00777693          	andi	a3,a4,7
     ad2:	34068e63          	beqz	a3,e2e <__adddf3+0x578>
     ad6:	00f77693          	andi	a3,a4,15
     ada:	4611                	li	a2,4
     adc:	34c68963          	beq	a3,a2,e2e <__adddf3+0x578>
     ae0:	00470693          	addi	a3,a4,4 # 10004 <__ctor_end__+0x96dc>
     ae4:	00e6b733          	sltu	a4,a3,a4
     ae8:	97ba                	add	a5,a5,a4
     aea:	8736                	mv	a4,a3
     aec:	a689                	j	e2e <__adddf3+0x578>
     aee:	d28d                	beqz	a3,a10 <__adddf3+0x15a>
     af0:	00c36333          	or	t1,t1,a2
     af4:	e2030fe3          	beqz	t1,932 <__adddf3+0x7c>
     af8:	4481                	li	s1,0
     afa:	004007b7          	lui	a5,0x400
     afe:	4701                	li	a4,0
     b00:	7ff00413          	li	s0,2047
     b04:	a62d                	j	e2e <__adddf3+0x578>
     b06:	7ff00693          	li	a3,2047
     b0a:	30d58e63          	beq	a1,a3,e26 <__adddf3+0x570>
     b0e:	963a                	add	a2,a2,a4
     b10:	00e63733          	sltu	a4,a2,a4
     b14:	979a                	add	a5,a5,t1
     b16:	00e786b3          	add	a3,a5,a4
     b1a:	01f69793          	slli	a5,a3,0x1f
     b1e:	8205                	srli	a2,a2,0x1
     b20:	00c7e733          	or	a4,a5,a2
     b24:	0016d793          	srli	a5,a3,0x1
     b28:	842e                	mv	s0,a1
     b2a:	b755                	j	ace <__adddf3+0x218>
     b2c:	0ca05763          	blez	a0,bfa <__adddf3+0x344>
     b30:	08029163          	bnez	t0,bb2 <__adddf3+0x2fc>
     b34:	00c366b3          	or	a3,t1,a2
     b38:	34068863          	beqz	a3,e88 <__adddf3+0x5d2>
     b3c:	fff50593          	addi	a1,a0,-1
     b40:	e999                	bnez	a1,b56 <__adddf3+0x2a0>
     b42:	40c70633          	sub	a2,a4,a2
     b46:	00c73733          	sltu	a4,a4,a2
     b4a:	406787b3          	sub	a5,a5,t1
     b4e:	8f99                	sub	a5,a5,a4
     b50:	8732                	mv	a4,a2
     b52:	4405                	li	s0,1
     b54:	a0a9                	j	b9e <__adddf3+0x2e8>
     b56:	7ff00693          	li	a3,2047
     b5a:	dcd50ce3          	beq	a0,a3,932 <__adddf3+0x7c>
     b5e:	03800693          	li	a3,56
     b62:	08b6c763          	blt	a3,a1,bf0 <__adddf3+0x33a>
     b66:	46fd                	li	a3,31
     b68:	04b6cf63          	blt	a3,a1,bc6 <__adddf3+0x310>
     b6c:	02000513          	li	a0,32
     b70:	8d0d                	sub	a0,a0,a1
     b72:	00a316b3          	sll	a3,t1,a0
     b76:	00b652b3          	srl	t0,a2,a1
     b7a:	00a61633          	sll	a2,a2,a0
     b7e:	0056e6b3          	or	a3,a3,t0
     b82:	00c03633          	snez	a2,a2
     b86:	8e55                	or	a2,a2,a3
     b88:	00b35333          	srl	t1,t1,a1
     b8c:	40c70633          	sub	a2,a4,a2
     b90:	40678333          	sub	t1,a5,t1
     b94:	00c737b3          	sltu	a5,a4,a2
     b98:	40f307b3          	sub	a5,t1,a5
     b9c:	8732                	mv	a4,a2
     b9e:	00879693          	slli	a3,a5,0x8
     ba2:	f206d6e3          	bgez	a3,ace <__adddf3+0x218>
     ba6:	008005b7          	lui	a1,0x800
     baa:	15fd                	addi	a1,a1,-1
     bac:	8dfd                	and	a1,a1,a5
     bae:	82ba                	mv	t0,a4
     bb0:	a24d                	j	d52 <__adddf3+0x49c>
     bb2:	7ff00693          	li	a3,2047
     bb6:	f0d40ce3          	beq	s0,a3,ace <__adddf3+0x218>
     bba:	008006b7          	lui	a3,0x800
     bbe:	00d36333          	or	t1,t1,a3
     bc2:	85aa                	mv	a1,a0
     bc4:	bf69                	j	b5e <__adddf3+0x2a8>
     bc6:	fe058693          	addi	a3,a1,-32 # 7fffe0 <__ctor_end__+0x7f96b8>
     bca:	02000293          	li	t0,32
     bce:	00d356b3          	srl	a3,t1,a3
     bd2:	4501                	li	a0,0
     bd4:	00558863          	beq	a1,t0,be4 <__adddf3+0x32e>
     bd8:	04000513          	li	a0,64
     bdc:	40b505b3          	sub	a1,a0,a1
     be0:	00b31533          	sll	a0,t1,a1
     be4:	8e49                	or	a2,a2,a0
     be6:	00c03633          	snez	a2,a2
     bea:	8e55                	or	a2,a2,a3
     bec:	4301                	li	t1,0
     bee:	bf79                	j	b8c <__adddf3+0x2d6>
     bf0:	00c36633          	or	a2,t1,a2
     bf4:	00c03633          	snez	a2,a2
     bf8:	bfd5                	j	bec <__adddf3+0x336>
     bfa:	c161                	beqz	a0,cba <__adddf3+0x404>
     bfc:	40828533          	sub	a0,t0,s0
     c00:	e815                	bnez	s0,c34 <__adddf3+0x37e>
     c02:	00e7e5b3          	or	a1,a5,a4
     c06:	28058363          	beqz	a1,e8c <__adddf3+0x5d6>
     c0a:	fff50593          	addi	a1,a0,-1
     c0e:	e991                	bnez	a1,c22 <__adddf3+0x36c>
     c10:	40e60733          	sub	a4,a2,a4
     c14:	40f307b3          	sub	a5,t1,a5
     c18:	00e63633          	sltu	a2,a2,a4
     c1c:	8f91                	sub	a5,a5,a2
     c1e:	84b6                	mv	s1,a3
     c20:	bf0d                	j	b52 <__adddf3+0x29c>
     c22:	7ff00393          	li	t2,2047
     c26:	00751f63          	bne	a0,t2,c44 <__adddf3+0x38e>
     c2a:	879a                	mv	a5,t1
     c2c:	8732                	mv	a4,a2
     c2e:	7ff00413          	li	s0,2047
     c32:	a05d                	j	cd8 <__adddf3+0x422>
     c34:	7ff00593          	li	a1,2047
     c38:	feb289e3          	beq	t0,a1,c2a <__adddf3+0x374>
     c3c:	008005b7          	lui	a1,0x800
     c40:	8fcd                	or	a5,a5,a1
     c42:	85aa                	mv	a1,a0
     c44:	03800513          	li	a0,56
     c48:	06b54563          	blt	a0,a1,cb2 <__adddf3+0x3fc>
     c4c:	457d                	li	a0,31
     c4e:	02b54c63          	blt	a0,a1,c86 <__adddf3+0x3d0>
     c52:	02000393          	li	t2,32
     c56:	40b383b3          	sub	t2,t2,a1
     c5a:	00b75433          	srl	s0,a4,a1
     c5e:	00779533          	sll	a0,a5,t2
     c62:	00771733          	sll	a4,a4,t2
     c66:	8d41                	or	a0,a0,s0
     c68:	00e03733          	snez	a4,a4
     c6c:	8f49                	or	a4,a4,a0
     c6e:	00b7d5b3          	srl	a1,a5,a1
     c72:	40e60733          	sub	a4,a2,a4
     c76:	40b307b3          	sub	a5,t1,a1
     c7a:	00e63633          	sltu	a2,a2,a4
     c7e:	8f91                	sub	a5,a5,a2
     c80:	8416                	mv	s0,t0
     c82:	84b6                	mv	s1,a3
     c84:	bf29                	j	b9e <__adddf3+0x2e8>
     c86:	fe058513          	addi	a0,a1,-32 # 7fffe0 <__ctor_end__+0x7f96b8>
     c8a:	02000413          	li	s0,32
     c8e:	00a7d533          	srl	a0,a5,a0
     c92:	4381                	li	t2,0
     c94:	00858863          	beq	a1,s0,ca4 <__adddf3+0x3ee>
     c98:	04000393          	li	t2,64
     c9c:	40b385b3          	sub	a1,t2,a1
     ca0:	00b793b3          	sll	t2,a5,a1
     ca4:	00e3e733          	or	a4,t2,a4
     ca8:	00e03733          	snez	a4,a4
     cac:	8f49                	or	a4,a4,a0
     cae:	4581                	li	a1,0
     cb0:	b7c9                	j	c72 <__adddf3+0x3bc>
     cb2:	8f5d                	or	a4,a4,a5
     cb4:	00e03733          	snez	a4,a4
     cb8:	bfdd                	j	cae <__adddf3+0x3f8>
     cba:	00140593          	addi	a1,s0,1
     cbe:	7fe5f593          	andi	a1,a1,2046
     cc2:	e5a5                	bnez	a1,d2a <__adddf3+0x474>
     cc4:	00e7e533          	or	a0,a5,a4
     cc8:	00c365b3          	or	a1,t1,a2
     ccc:	e429                	bnez	s0,d16 <__adddf3+0x460>
     cce:	e519                	bnez	a0,cdc <__adddf3+0x426>
     cd0:	1c058263          	beqz	a1,e94 <__adddf3+0x5de>
     cd4:	879a                	mv	a5,t1
     cd6:	8732                	mv	a4,a2
     cd8:	84b6                	mv	s1,a3
     cda:	bbd5                	j	ace <__adddf3+0x218>
     cdc:	de0589e3          	beqz	a1,ace <__adddf3+0x218>
     ce0:	40c70533          	sub	a0,a4,a2
     ce4:	00a732b3          	sltu	t0,a4,a0
     ce8:	406785b3          	sub	a1,a5,t1
     cec:	405585b3          	sub	a1,a1,t0
     cf0:	00859293          	slli	t0,a1,0x8
     cf4:	0002da63          	bgez	t0,d08 <__adddf3+0x452>
     cf8:	40e60733          	sub	a4,a2,a4
     cfc:	40f307b3          	sub	a5,t1,a5
     d00:	00e63633          	sltu	a2,a2,a4
     d04:	8f91                	sub	a5,a5,a2
     d06:	bfc9                	j	cd8 <__adddf3+0x422>
     d08:	00b56733          	or	a4,a0,a1
     d0c:	18070863          	beqz	a4,e9c <__adddf3+0x5e6>
     d10:	87ae                	mv	a5,a1
     d12:	872a                	mv	a4,a0
     d14:	bb6d                	j	ace <__adddf3+0x218>
     d16:	e519                	bnez	a0,d24 <__adddf3+0x46e>
     d18:	18058463          	beqz	a1,ea0 <__adddf3+0x5ea>
     d1c:	879a                	mv	a5,t1
     d1e:	8732                	mv	a4,a2
     d20:	84b6                	mv	s1,a3
     d22:	b901                	j	932 <__adddf3+0x7c>
     d24:	c00587e3          	beqz	a1,932 <__adddf3+0x7c>
     d28:	bbc1                	j	af8 <__adddf3+0x242>
     d2a:	40c702b3          	sub	t0,a4,a2
     d2e:	00573533          	sltu	a0,a4,t0
     d32:	406785b3          	sub	a1,a5,t1
     d36:	8d89                	sub	a1,a1,a0
     d38:	00859513          	slli	a0,a1,0x8
     d3c:	06055d63          	bgez	a0,db6 <__adddf3+0x500>
     d40:	40e602b3          	sub	t0,a2,a4
     d44:	40f307b3          	sub	a5,t1,a5
     d48:	00563633          	sltu	a2,a2,t0
     d4c:	40c785b3          	sub	a1,a5,a2
     d50:	84b6                	mv	s1,a3
     d52:	c9a5                	beqz	a1,dc2 <__adddf3+0x50c>
     d54:	852e                	mv	a0,a1
     d56:	c216                	sw	t0,4(sp)
     d58:	c02e                	sw	a1,0(sp)
     d5a:	446010ef          	jal	ra,21a0 <__clzsi2>
     d5e:	4582                	lw	a1,0(sp)
     d60:	4292                	lw	t0,4(sp)
     d62:	ff850693          	addi	a3,a0,-8
     d66:	47fd                	li	a5,31
     d68:	06d7c763          	blt	a5,a3,dd6 <__adddf3+0x520>
     d6c:	02000793          	li	a5,32
     d70:	8f95                	sub	a5,a5,a3
     d72:	00d595b3          	sll	a1,a1,a3
     d76:	00f2d7b3          	srl	a5,t0,a5
     d7a:	8ddd                	or	a1,a1,a5
     d7c:	00d29733          	sll	a4,t0,a3
     d80:	0886c763          	blt	a3,s0,e0e <__adddf3+0x558>
     d84:	40868433          	sub	s0,a3,s0
     d88:	00140613          	addi	a2,s0,1
     d8c:	47fd                	li	a5,31
     d8e:	04c7ca63          	blt	a5,a2,de2 <__adddf3+0x52c>
     d92:	02000513          	li	a0,32
     d96:	8d11                	sub	a0,a0,a2
     d98:	00c757b3          	srl	a5,a4,a2
     d9c:	00a596b3          	sll	a3,a1,a0
     da0:	8edd                	or	a3,a3,a5
     da2:	00a717b3          	sll	a5,a4,a0
     da6:	00f037b3          	snez	a5,a5
     daa:	00f6e733          	or	a4,a3,a5
     dae:	00c5d7b3          	srl	a5,a1,a2
     db2:	4401                	li	s0,0
     db4:	bb29                	j	ace <__adddf3+0x218>
     db6:	00b2e733          	or	a4,t0,a1
     dba:	ff41                	bnez	a4,d52 <__adddf3+0x49c>
     dbc:	4781                	li	a5,0
     dbe:	4401                	li	s0,0
     dc0:	a8e1                	j	e98 <__adddf3+0x5e2>
     dc2:	8516                	mv	a0,t0
     dc4:	c22e                	sw	a1,4(sp)
     dc6:	c016                	sw	t0,0(sp)
     dc8:	3d8010ef          	jal	ra,21a0 <__clzsi2>
     dcc:	02050513          	addi	a0,a0,32
     dd0:	4592                	lw	a1,4(sp)
     dd2:	4282                	lw	t0,0(sp)
     dd4:	b779                	j	d62 <__adddf3+0x4ac>
     dd6:	fd850593          	addi	a1,a0,-40
     dda:	00b295b3          	sll	a1,t0,a1
     dde:	4701                	li	a4,0
     de0:	b745                	j	d80 <__adddf3+0x4ca>
     de2:	1405                	addi	s0,s0,-31
     de4:	02000793          	li	a5,32
     de8:	0085d433          	srl	s0,a1,s0
     dec:	4281                	li	t0,0
     dee:	00f60863          	beq	a2,a5,dfe <__adddf3+0x548>
     df2:	04000293          	li	t0,64
     df6:	40c282b3          	sub	t0,t0,a2
     dfa:	005592b3          	sll	t0,a1,t0
     dfe:	005767b3          	or	a5,a4,t0
     e02:	00f037b3          	snez	a5,a5
     e06:	00f46733          	or	a4,s0,a5
     e0a:	4781                	li	a5,0
     e0c:	b75d                	j	db2 <__adddf3+0x4fc>
     e0e:	ff8007b7          	lui	a5,0xff800
     e12:	17fd                	addi	a5,a5,-1
     e14:	8c15                	sub	s0,s0,a3
     e16:	8fed                	and	a5,a5,a1
     e18:	b95d                	j	ace <__adddf3+0x218>
     e1a:	879a                	mv	a5,t1
     e1c:	8732                	mv	a4,a2
     e1e:	b329                	j	b28 <__adddf3+0x272>
     e20:	879a                	mv	a5,t1
     e22:	8732                	mv	a4,a2
     e24:	b16d                	j	ace <__adddf3+0x218>
     e26:	7ff00413          	li	s0,2047
     e2a:	4781                	li	a5,0
     e2c:	4701                	li	a4,0
     e2e:	00879693          	slli	a3,a5,0x8
     e32:	0006db63          	bgez	a3,e48 <__adddf3+0x592>
     e36:	0405                	addi	s0,s0,1
     e38:	7ff00693          	li	a3,2047
     e3c:	06d40763          	beq	s0,a3,eaa <__adddf3+0x5f4>
     e40:	ff8006b7          	lui	a3,0xff800
     e44:	16fd                	addi	a3,a3,-1
     e46:	8ff5                	and	a5,a5,a3
     e48:	01d79513          	slli	a0,a5,0x1d
     e4c:	830d                	srli	a4,a4,0x3
     e4e:	7ff00693          	li	a3,2047
     e52:	8f49                	or	a4,a4,a0
     e54:	838d                	srli	a5,a5,0x3
     e56:	00d41963          	bne	s0,a3,e68 <__adddf3+0x5b2>
     e5a:	8f5d                	or	a4,a4,a5
     e5c:	4781                	li	a5,0
     e5e:	c709                	beqz	a4,e68 <__adddf3+0x5b2>
     e60:	000807b7          	lui	a5,0x80
     e64:	4701                	li	a4,0
     e66:	4481                	li	s1,0
     e68:	07b2                	slli	a5,a5,0xc
     e6a:	7ff47413          	andi	s0,s0,2047
     e6e:	0452                	slli	s0,s0,0x14
     e70:	83b1                	srli	a5,a5,0xc
     e72:	8fc1                	or	a5,a5,s0
     e74:	40c2                	lw	ra,16(sp)
     e76:	4432                	lw	s0,12(sp)
     e78:	04fe                	slli	s1,s1,0x1f
     e7a:	0097e6b3          	or	a3,a5,s1
     e7e:	853a                	mv	a0,a4
     e80:	44a2                	lw	s1,8(sp)
     e82:	85b6                	mv	a1,a3
     e84:	0151                	addi	sp,sp,20
     e86:	8082                	ret
     e88:	842a                	mv	s0,a0
     e8a:	b191                	j	ace <__adddf3+0x218>
     e8c:	879a                	mv	a5,t1
     e8e:	8732                	mv	a4,a2
     e90:	842a                	mv	s0,a0
     e92:	b599                	j	cd8 <__adddf3+0x422>
     e94:	4781                	li	a5,0
     e96:	4701                	li	a4,0
     e98:	4481                	li	s1,0
     e9a:	bf51                	j	e2e <__adddf3+0x578>
     e9c:	4781                	li	a5,0
     e9e:	bfed                	j	e98 <__adddf3+0x5e2>
     ea0:	4701                	li	a4,0
     ea2:	4481                	li	s1,0
     ea4:	004007b7          	lui	a5,0x400
     ea8:	b9a1                	j	b00 <__adddf3+0x24a>
     eaa:	4781                	li	a5,0
     eac:	4701                	li	a4,0
     eae:	bf69                	j	e48 <__adddf3+0x592>

00000eb0 <__divdf3>:
     eb0:	fdc10113          	addi	sp,sp,-36
     eb4:	872a                	mv	a4,a0
     eb6:	832a                	mv	t1,a0
     eb8:	01f5d793          	srli	a5,a1,0x1f
     ebc:	0145d513          	srli	a0,a1,0x14
     ec0:	cc26                	sw	s1,24(sp)
     ec2:	d006                	sw	ra,32(sp)
     ec4:	00c59493          	slli	s1,a1,0xc
     ec8:	ce22                	sw	s0,28(sp)
     eca:	7ff57513          	andi	a0,a0,2047
     ece:	c43e                	sw	a5,8(sp)
     ed0:	80b1                	srli	s1,s1,0xc
     ed2:	c941                	beqz	a0,f62 <__divdf3+0xb2>
     ed4:	7ff00593          	li	a1,2047
     ed8:	0eb50763          	beq	a0,a1,fc6 <__divdf3+0x116>
     edc:	00349793          	slli	a5,s1,0x3
     ee0:	01d75413          	srli	s0,a4,0x1d
     ee4:	8c5d                	or	s0,s0,a5
     ee6:	008007b7          	lui	a5,0x800
     eea:	8c5d                	or	s0,s0,a5
     eec:	00371313          	slli	t1,a4,0x3
     ef0:	c0150493          	addi	s1,a0,-1023
     ef4:	4381                	li	t2,0
     ef6:	0146d513          	srli	a0,a3,0x14
     efa:	01f6d713          	srli	a4,a3,0x1f
     efe:	00c69293          	slli	t0,a3,0xc
     f02:	7ff57513          	andi	a0,a0,2047
     f06:	c63a                	sw	a4,12(sp)
     f08:	87b2                	mv	a5,a2
     f0a:	00c2d293          	srli	t0,t0,0xc
     f0e:	cd69                	beqz	a0,fe8 <__divdf3+0x138>
     f10:	7ff00713          	li	a4,2047
     f14:	14e50463          	beq	a0,a4,105c <__divdf3+0x1ac>
     f18:	01d65713          	srli	a4,a2,0x1d
     f1c:	028e                	slli	t0,t0,0x3
     f1e:	005762b3          	or	t0,a4,t0
     f22:	00800737          	lui	a4,0x800
     f26:	00e2e733          	or	a4,t0,a4
     f2a:	00361793          	slli	a5,a2,0x3
     f2e:	c0150513          	addi	a0,a0,-1023
     f32:	4681                	li	a3,0
     f34:	45b2                	lw	a1,12(sp)
     f36:	4622                	lw	a2,8(sp)
     f38:	8e2d                	xor	a2,a2,a1
     f3a:	c032                	sw	a2,0(sp)
     f3c:	40a48633          	sub	a2,s1,a0
     f40:	c232                	sw	a2,4(sp)
     f42:	00239613          	slli	a2,t2,0x2
     f46:	8e55                	or	a2,a2,a3
     f48:	167d                	addi	a2,a2,-1
     f4a:	45b9                	li	a1,14
     f4c:	12c5e963          	bltu	a1,a2,107e <__divdf3+0x1ce>
     f50:	00004597          	auipc	a1,0x4
     f54:	7c858593          	addi	a1,a1,1992 # 5718 <__srodata+0x18>
     f58:	060a                	slli	a2,a2,0x2
     f5a:	962e                	add	a2,a2,a1
     f5c:	4210                	lw	a2,0(a2)
     f5e:	95b2                	add	a1,a1,a2
     f60:	8582                	jr	a1
     f62:	00e4e433          	or	s0,s1,a4
     f66:	c825                	beqz	s0,fd6 <__divdf3+0x126>
     f68:	c636                	sw	a3,12(sp)
     f6a:	c232                	sw	a2,4(sp)
     f6c:	cc8d                	beqz	s1,fa6 <__divdf3+0xf6>
     f6e:	8526                	mv	a0,s1
     f70:	c03a                	sw	a4,0(sp)
     f72:	22e010ef          	jal	ra,21a0 <__clzsi2>
     f76:	4702                	lw	a4,0(sp)
     f78:	4612                	lw	a2,4(sp)
     f7a:	46b2                	lw	a3,12(sp)
     f7c:	ff550593          	addi	a1,a0,-11
     f80:	4371                	li	t1,28
     f82:	02b34c63          	blt	t1,a1,fba <__divdf3+0x10a>
     f86:	4475                	li	s0,29
     f88:	ff850313          	addi	t1,a0,-8
     f8c:	8c0d                	sub	s0,s0,a1
     f8e:	006497b3          	sll	a5,s1,t1
     f92:	00875433          	srl	s0,a4,s0
     f96:	8c5d                	or	s0,s0,a5
     f98:	00671333          	sll	t1,a4,t1
     f9c:	c0d00593          	li	a1,-1011
     fa0:	40a584b3          	sub	s1,a1,a0
     fa4:	bf81                	j	ef4 <__divdf3+0x44>
     fa6:	853a                	mv	a0,a4
     fa8:	c03a                	sw	a4,0(sp)
     faa:	1f6010ef          	jal	ra,21a0 <__clzsi2>
     fae:	02050513          	addi	a0,a0,32
     fb2:	46b2                	lw	a3,12(sp)
     fb4:	4612                	lw	a2,4(sp)
     fb6:	4702                	lw	a4,0(sp)
     fb8:	b7d1                	j	f7c <__divdf3+0xcc>
     fba:	fd850413          	addi	s0,a0,-40
     fbe:	00871433          	sll	s0,a4,s0
     fc2:	4301                	li	t1,0
     fc4:	bfe1                	j	f9c <__divdf3+0xec>
     fc6:	00e4e433          	or	s0,s1,a4
     fca:	c811                	beqz	s0,fde <__divdf3+0x12e>
     fcc:	8426                	mv	s0,s1
     fce:	438d                	li	t2,3
     fd0:	7ff00493          	li	s1,2047
     fd4:	b70d                	j	ef6 <__divdf3+0x46>
     fd6:	4301                	li	t1,0
     fd8:	4481                	li	s1,0
     fda:	4385                	li	t2,1
     fdc:	bf29                	j	ef6 <__divdf3+0x46>
     fde:	4301                	li	t1,0
     fe0:	7ff00493          	li	s1,2047
     fe4:	4389                	li	t2,2
     fe6:	bf01                	j	ef6 <__divdf3+0x46>
     fe8:	00c2e733          	or	a4,t0,a2
     fec:	c341                	beqz	a4,106c <__divdf3+0x1bc>
     fee:	04028363          	beqz	t0,1034 <__divdf3+0x184>
     ff2:	8516                	mv	a0,t0
     ff4:	ca32                	sw	a2,20(sp)
     ff6:	c81e                	sw	t2,16(sp)
     ff8:	c21a                	sw	t1,4(sp)
     ffa:	c016                	sw	t0,0(sp)
     ffc:	1a4010ef          	jal	ra,21a0 <__clzsi2>
    1000:	4282                	lw	t0,0(sp)
    1002:	4312                	lw	t1,4(sp)
    1004:	43c2                	lw	t2,16(sp)
    1006:	4652                	lw	a2,20(sp)
    1008:	ff550693          	addi	a3,a0,-11
    100c:	47f1                	li	a5,28
    100e:	04d7c163          	blt	a5,a3,1050 <__divdf3+0x1a0>
    1012:	4775                	li	a4,29
    1014:	ff850793          	addi	a5,a0,-8
    1018:	8f15                	sub	a4,a4,a3
    101a:	00f292b3          	sll	t0,t0,a5
    101e:	00e65733          	srl	a4,a2,a4
    1022:	00576733          	or	a4,a4,t0
    1026:	00f617b3          	sll	a5,a2,a5
    102a:	c0d00693          	li	a3,-1011
    102e:	40a68533          	sub	a0,a3,a0
    1032:	b701                	j	f32 <__divdf3+0x82>
    1034:	8532                	mv	a0,a2
    1036:	ca16                	sw	t0,20(sp)
    1038:	c81e                	sw	t2,16(sp)
    103a:	c21a                	sw	t1,4(sp)
    103c:	c032                	sw	a2,0(sp)
    103e:	162010ef          	jal	ra,21a0 <__clzsi2>
    1042:	02050513          	addi	a0,a0,32
    1046:	42d2                	lw	t0,20(sp)
    1048:	43c2                	lw	t2,16(sp)
    104a:	4312                	lw	t1,4(sp)
    104c:	4602                	lw	a2,0(sp)
    104e:	bf6d                	j	1008 <__divdf3+0x158>
    1050:	fd850293          	addi	t0,a0,-40
    1054:	00561733          	sll	a4,a2,t0
    1058:	4781                	li	a5,0
    105a:	bfc1                	j	102a <__divdf3+0x17a>
    105c:	00c2e733          	or	a4,t0,a2
    1060:	cb11                	beqz	a4,1074 <__divdf3+0x1c4>
    1062:	8716                	mv	a4,t0
    1064:	7ff00513          	li	a0,2047
    1068:	468d                	li	a3,3
    106a:	b5e9                	j	f34 <__divdf3+0x84>
    106c:	4781                	li	a5,0
    106e:	4501                	li	a0,0
    1070:	4685                	li	a3,1
    1072:	b5c9                	j	f34 <__divdf3+0x84>
    1074:	4781                	li	a5,0
    1076:	7ff00513          	li	a0,2047
    107a:	4689                	li	a3,2
    107c:	bd65                	j	f34 <__divdf3+0x84>
    107e:	00876663          	bltu	a4,s0,108a <__divdf3+0x1da>
    1082:	2ce41563          	bne	s0,a4,134c <__divdf3+0x49c>
    1086:	2cf36363          	bltu	t1,a5,134c <__divdf3+0x49c>
    108a:	01f41593          	slli	a1,s0,0x1f
    108e:	00135693          	srli	a3,t1,0x1
    1092:	01f31613          	slli	a2,t1,0x1f
    1096:	8005                	srli	s0,s0,0x1
    1098:	00d5e333          	or	t1,a1,a3
    109c:	00871293          	slli	t0,a4,0x8
    10a0:	0187d713          	srli	a4,a5,0x18
    10a4:	005762b3          	or	t0,a4,t0
    10a8:	0102d493          	srli	s1,t0,0x10
    10ac:	02945733          	divu	a4,s0,s1
    10b0:	00879693          	slli	a3,a5,0x8
    10b4:	01029793          	slli	a5,t0,0x10
    10b8:	83c1                	srli	a5,a5,0x10
    10ba:	c43e                	sw	a5,8(sp)
    10bc:	01035593          	srli	a1,t1,0x10
    10c0:	02947433          	remu	s0,s0,s1
    10c4:	02e787b3          	mul	a5,a5,a4
    10c8:	0442                	slli	s0,s0,0x10
    10ca:	8c4d                	or	s0,s0,a1
    10cc:	85ba                	mv	a1,a4
    10ce:	00f47c63          	bgeu	s0,a5,10e6 <__divdf3+0x236>
    10d2:	9416                	add	s0,s0,t0
    10d4:	fff70593          	addi	a1,a4,-1 # 7fffff <__ctor_end__+0x7f96d7>
    10d8:	00546763          	bltu	s0,t0,10e6 <__divdf3+0x236>
    10dc:	00f47563          	bgeu	s0,a5,10e6 <__divdf3+0x236>
    10e0:	ffe70593          	addi	a1,a4,-2
    10e4:	9416                	add	s0,s0,t0
    10e6:	40f407b3          	sub	a5,s0,a5
    10ea:	0297d533          	divu	a0,a5,s1
    10ee:	01029713          	slli	a4,t0,0x10
    10f2:	8341                	srli	a4,a4,0x10
    10f4:	0342                	slli	t1,t1,0x10
    10f6:	01035313          	srli	t1,t1,0x10
    10fa:	0297f7b3          	remu	a5,a5,s1
    10fe:	83aa                	mv	t2,a0
    1100:	02a70733          	mul	a4,a4,a0
    1104:	07c2                	slli	a5,a5,0x10
    1106:	00f36333          	or	t1,t1,a5
    110a:	00e37c63          	bgeu	t1,a4,1122 <__divdf3+0x272>
    110e:	9316                	add	t1,t1,t0
    1110:	fff50393          	addi	t2,a0,-1
    1114:	00536763          	bltu	t1,t0,1122 <__divdf3+0x272>
    1118:	00e37563          	bgeu	t1,a4,1122 <__divdf3+0x272>
    111c:	ffe50393          	addi	t2,a0,-2
    1120:	9316                	add	t1,t1,t0
    1122:	05c2                	slli	a1,a1,0x10
    1124:	6441                	lui	s0,0x10
    1126:	0075e5b3          	or	a1,a1,t2
    112a:	fff40793          	addi	a5,s0,-1 # ffff <__ctor_end__+0x96d7>
    112e:	00f5f533          	and	a0,a1,a5
    1132:	40e30333          	sub	t1,t1,a4
    1136:	8ff5                	and	a5,a5,a3
    1138:	0105d713          	srli	a4,a1,0x10
    113c:	02f503b3          	mul	t2,a0,a5
    1140:	c43e                	sw	a5,8(sp)
    1142:	02f707b3          	mul	a5,a4,a5
    1146:	c63e                	sw	a5,12(sp)
    1148:	0106d793          	srli	a5,a3,0x10
    114c:	02a78533          	mul	a0,a5,a0
    1150:	02f70733          	mul	a4,a4,a5
    1154:	47b2                	lw	a5,12(sp)
    1156:	953e                	add	a0,a0,a5
    1158:	0103d793          	srli	a5,t2,0x10
    115c:	97aa                	add	a5,a5,a0
    115e:	4532                	lw	a0,12(sp)
    1160:	00a7f363          	bgeu	a5,a0,1166 <__divdf3+0x2b6>
    1164:	9722                	add	a4,a4,s0
    1166:	0107d413          	srli	s0,a5,0x10
    116a:	943a                	add	s0,s0,a4
    116c:	6741                	lui	a4,0x10
    116e:	177d                	addi	a4,a4,-1
    1170:	8ff9                	and	a5,a5,a4
    1172:	07c2                	slli	a5,a5,0x10
    1174:	00e3f3b3          	and	t2,t2,a4
    1178:	93be                	add	t2,t2,a5
    117a:	00836763          	bltu	t1,s0,1188 <__divdf3+0x2d8>
    117e:	872e                	mv	a4,a1
    1180:	02831e63          	bne	t1,s0,11bc <__divdf3+0x30c>
    1184:	02767c63          	bgeu	a2,t2,11bc <__divdf3+0x30c>
    1188:	9636                	add	a2,a2,a3
    118a:	00d637b3          	sltu	a5,a2,a3
    118e:	9796                	add	a5,a5,t0
    1190:	933e                	add	t1,t1,a5
    1192:	fff58713          	addi	a4,a1,-1
    1196:	0062e663          	bltu	t0,t1,11a2 <__divdf3+0x2f2>
    119a:	02629163          	bne	t0,t1,11bc <__divdf3+0x30c>
    119e:	00d66f63          	bltu	a2,a3,11bc <__divdf3+0x30c>
    11a2:	00836663          	bltu	t1,s0,11ae <__divdf3+0x2fe>
    11a6:	00641b63          	bne	s0,t1,11bc <__divdf3+0x30c>
    11aa:	00767963          	bgeu	a2,t2,11bc <__divdf3+0x30c>
    11ae:	9636                	add	a2,a2,a3
    11b0:	00d637b3          	sltu	a5,a2,a3
    11b4:	9796                	add	a5,a5,t0
    11b6:	ffe58713          	addi	a4,a1,-2
    11ba:	933e                	add	t1,t1,a5
    11bc:	407603b3          	sub	t2,a2,t2
    11c0:	40830333          	sub	t1,t1,s0
    11c4:	00763633          	sltu	a2,a2,t2
    11c8:	40c30633          	sub	a2,t1,a2
    11cc:	57fd                	li	a5,-1
    11ce:	10c28263          	beq	t0,a2,12d2 <__divdf3+0x422>
    11d2:	02965533          	divu	a0,a2,s1
    11d6:	01029793          	slli	a5,t0,0x10
    11da:	83c1                	srli	a5,a5,0x10
    11dc:	0103d593          	srli	a1,t2,0x10
    11e0:	02967633          	remu	a2,a2,s1
    11e4:	02a787b3          	mul	a5,a5,a0
    11e8:	0642                	slli	a2,a2,0x10
    11ea:	8e4d                	or	a2,a2,a1
    11ec:	85aa                	mv	a1,a0
    11ee:	00f67c63          	bgeu	a2,a5,1206 <__divdf3+0x356>
    11f2:	9616                	add	a2,a2,t0
    11f4:	fff50593          	addi	a1,a0,-1
    11f8:	00566763          	bltu	a2,t0,1206 <__divdf3+0x356>
    11fc:	00f67563          	bgeu	a2,a5,1206 <__divdf3+0x356>
    1200:	ffe50593          	addi	a1,a0,-2
    1204:	9616                	add	a2,a2,t0
    1206:	8e1d                	sub	a2,a2,a5
    1208:	02965333          	divu	t1,a2,s1
    120c:	01029793          	slli	a5,t0,0x10
    1210:	83c1                	srli	a5,a5,0x10
    1212:	03c2                	slli	t2,t2,0x10
    1214:	0103d393          	srli	t2,t2,0x10
    1218:	02967633          	remu	a2,a2,s1
    121c:	841a                	mv	s0,t1
    121e:	02678533          	mul	a0,a5,t1
    1222:	01061793          	slli	a5,a2,0x10
    1226:	00f3e633          	or	a2,t2,a5
    122a:	00a67c63          	bgeu	a2,a0,1242 <__divdf3+0x392>
    122e:	9616                	add	a2,a2,t0
    1230:	fff30413          	addi	s0,t1,-1
    1234:	00566763          	bltu	a2,t0,1242 <__divdf3+0x392>
    1238:	00a67563          	bgeu	a2,a0,1242 <__divdf3+0x392>
    123c:	ffe30413          	addi	s0,t1,-2
    1240:	9616                	add	a2,a2,t0
    1242:	01059793          	slli	a5,a1,0x10
    1246:	8fc1                	or	a5,a5,s0
    1248:	4422                	lw	s0,8(sp)
    124a:	8e09                	sub	a2,a2,a0
    124c:	4522                	lw	a0,8(sp)
    124e:	0107d393          	srli	t2,a5,0x10
    1252:	01079593          	slli	a1,a5,0x10
    1256:	02838333          	mul	t1,t2,s0
    125a:	81c1                	srli	a1,a1,0x10
    125c:	0106d413          	srli	s0,a3,0x10
    1260:	02a58533          	mul	a0,a1,a0
    1264:	02b405b3          	mul	a1,s0,a1
    1268:	027403b3          	mul	t2,s0,t2
    126c:	959a                	add	a1,a1,t1
    126e:	01055413          	srli	s0,a0,0x10
    1272:	95a2                	add	a1,a1,s0
    1274:	0065f463          	bgeu	a1,t1,127c <__divdf3+0x3cc>
    1278:	6341                	lui	t1,0x10
    127a:	939a                	add	t2,t2,t1
    127c:	0105d313          	srli	t1,a1,0x10
    1280:	939a                	add	t2,t2,t1
    1282:	6341                	lui	t1,0x10
    1284:	137d                	addi	t1,t1,-1
    1286:	0065f5b3          	and	a1,a1,t1
    128a:	05c2                	slli	a1,a1,0x10
    128c:	00657533          	and	a0,a0,t1
    1290:	952e                	add	a0,a0,a1
    1292:	00766563          	bltu	a2,t2,129c <__divdf3+0x3ec>
    1296:	1a761263          	bne	a2,t2,143a <__divdf3+0x58a>
    129a:	cd05                	beqz	a0,12d2 <__divdf3+0x422>
    129c:	9616                	add	a2,a2,t0
    129e:	fff78593          	addi	a1,a5,-1 # 7fffff <__ctor_end__+0x7f96d7>
    12a2:	02566163          	bltu	a2,t0,12c4 <__divdf3+0x414>
    12a6:	00766663          	bltu	a2,t2,12b2 <__divdf3+0x402>
    12aa:	18761763          	bne	a2,t2,1438 <__divdf3+0x588>
    12ae:	00a6fd63          	bgeu	a3,a0,12c8 <__divdf3+0x418>
    12b2:	ffe78593          	addi	a1,a5,-2
    12b6:	00169793          	slli	a5,a3,0x1
    12ba:	00d7b6b3          	sltu	a3,a5,a3
    12be:	92b6                	add	t0,t0,a3
    12c0:	9616                	add	a2,a2,t0
    12c2:	86be                	mv	a3,a5
    12c4:	00761563          	bne	a2,t2,12ce <__divdf3+0x41e>
    12c8:	87ae                	mv	a5,a1
    12ca:	00d50463          	beq	a0,a3,12d2 <__divdf3+0x422>
    12ce:	0015e793          	ori	a5,a1,1
    12d2:	4692                	lw	a3,4(sp)
    12d4:	3ff68613          	addi	a2,a3,1023 # ff8003ff <__heap_end+0xdf7d03ff>
    12d8:	0ac05963          	blez	a2,138a <__divdf3+0x4da>
    12dc:	0077f693          	andi	a3,a5,7
    12e0:	ce81                	beqz	a3,12f8 <__divdf3+0x448>
    12e2:	00f7f693          	andi	a3,a5,15
    12e6:	4591                	li	a1,4
    12e8:	00b68863          	beq	a3,a1,12f8 <__divdf3+0x448>
    12ec:	00478693          	addi	a3,a5,4
    12f0:	00f6b7b3          	sltu	a5,a3,a5
    12f4:	973e                	add	a4,a4,a5
    12f6:	87b6                	mv	a5,a3
    12f8:	00771693          	slli	a3,a4,0x7
    12fc:	0006d963          	bgez	a3,130e <__divdf3+0x45e>
    1300:	ff0006b7          	lui	a3,0xff000
    1304:	16fd                	addi	a3,a3,-1
    1306:	8f75                	and	a4,a4,a3
    1308:	4692                	lw	a3,4(sp)
    130a:	40068613          	addi	a2,a3,1024 # ff000400 <__heap_end+0xdefd0400>
    130e:	7fe00693          	li	a3,2046
    1312:	10c6ca63          	blt	a3,a2,1426 <__divdf3+0x576>
    1316:	838d                	srli	a5,a5,0x3
    1318:	01d71693          	slli	a3,a4,0x1d
    131c:	8fd5                	or	a5,a5,a3
    131e:	00375293          	srli	t0,a4,0x3
    1322:	4702                	lw	a4,0(sp)
    1324:	7ff67693          	andi	a3,a2,2047
    1328:	02b2                	slli	t0,t0,0xc
    132a:	5082                	lw	ra,32(sp)
    132c:	4472                	lw	s0,28(sp)
    132e:	06d2                	slli	a3,a3,0x14
    1330:	00c2d293          	srli	t0,t0,0xc
    1334:	01f71613          	slli	a2,a4,0x1f
    1338:	00d2e2b3          	or	t0,t0,a3
    133c:	00c2e733          	or	a4,t0,a2
    1340:	44e2                	lw	s1,24(sp)
    1342:	853e                	mv	a0,a5
    1344:	85ba                	mv	a1,a4
    1346:	02410113          	addi	sp,sp,36
    134a:	8082                	ret
    134c:	4692                	lw	a3,4(sp)
    134e:	4601                	li	a2,0
    1350:	16fd                	addi	a3,a3,-1
    1352:	c236                	sw	a3,4(sp)
    1354:	b3a1                	j	109c <__divdf3+0x1ec>
    1356:	47a2                	lw	a5,8(sp)
    1358:	8722                	mv	a4,s0
    135a:	869e                	mv	a3,t2
    135c:	c03e                	sw	a5,0(sp)
    135e:	879a                	mv	a5,t1
    1360:	4609                	li	a2,2
    1362:	0cc68263          	beq	a3,a2,1426 <__divdf3+0x576>
    1366:	460d                	li	a2,3
    1368:	0ac68863          	beq	a3,a2,1418 <__divdf3+0x568>
    136c:	4605                	li	a2,1
    136e:	f6c692e3          	bne	a3,a2,12d2 <__divdf3+0x422>
    1372:	4281                	li	t0,0
    1374:	4781                	li	a5,0
    1376:	a89d                	j	13ec <__divdf3+0x53c>
    1378:	4632                	lw	a2,12(sp)
    137a:	c032                	sw	a2,0(sp)
    137c:	b7d5                	j	1360 <__divdf3+0x4b0>
    137e:	00080737          	lui	a4,0x80
    1382:	4781                	li	a5,0
    1384:	c002                	sw	zero,0(sp)
    1386:	468d                	li	a3,3
    1388:	bfe1                	j	1360 <__divdf3+0x4b0>
    138a:	4285                	li	t0,1
    138c:	40c282b3          	sub	t0,t0,a2
    1390:	03800693          	li	a3,56
    1394:	fc56cfe3          	blt	a3,t0,1372 <__divdf3+0x4c2>
    1398:	46fd                	li	a3,31
    139a:	0456cb63          	blt	a3,t0,13f0 <__divdf3+0x540>
    139e:	4692                	lw	a3,4(sp)
    13a0:	0057d633          	srl	a2,a5,t0
    13a4:	005752b3          	srl	t0,a4,t0
    13a8:	41e68593          	addi	a1,a3,1054
    13ac:	00b716b3          	sll	a3,a4,a1
    13b0:	00b797b3          	sll	a5,a5,a1
    13b4:	8ed1                	or	a3,a3,a2
    13b6:	00f037b3          	snez	a5,a5
    13ba:	8fd5                	or	a5,a5,a3
    13bc:	0077f713          	andi	a4,a5,7
    13c0:	cf01                	beqz	a4,13d8 <__divdf3+0x528>
    13c2:	00f7f713          	andi	a4,a5,15
    13c6:	4691                	li	a3,4
    13c8:	00d70863          	beq	a4,a3,13d8 <__divdf3+0x528>
    13cc:	00478713          	addi	a4,a5,4
    13d0:	00f737b3          	sltu	a5,a4,a5
    13d4:	92be                	add	t0,t0,a5
    13d6:	87ba                	mv	a5,a4
    13d8:	00829713          	slli	a4,t0,0x8
    13dc:	04074a63          	bltz	a4,1430 <__divdf3+0x580>
    13e0:	01d29713          	slli	a4,t0,0x1d
    13e4:	838d                	srli	a5,a5,0x3
    13e6:	8fd9                	or	a5,a5,a4
    13e8:	0032d293          	srli	t0,t0,0x3
    13ec:	4601                	li	a2,0
    13ee:	bf15                	j	1322 <__divdf3+0x472>
    13f0:	5685                	li	a3,-31
    13f2:	8e91                	sub	a3,a3,a2
    13f4:	02000613          	li	a2,32
    13f8:	00d756b3          	srl	a3,a4,a3
    13fc:	4581                	li	a1,0
    13fe:	00c28763          	beq	t0,a2,140c <__divdf3+0x55c>
    1402:	4612                	lw	a2,4(sp)
    1404:	43e60593          	addi	a1,a2,1086 # ff80043e <__heap_end+0xdf7d043e>
    1408:	00b715b3          	sll	a1,a4,a1
    140c:	8fcd                	or	a5,a5,a1
    140e:	00f037b3          	snez	a5,a5
    1412:	8fd5                	or	a5,a5,a3
    1414:	4281                	li	t0,0
    1416:	b75d                	j	13bc <__divdf3+0x50c>
    1418:	000802b7          	lui	t0,0x80
    141c:	4781                	li	a5,0
    141e:	7ff00613          	li	a2,2047
    1422:	c002                	sw	zero,0(sp)
    1424:	bdfd                	j	1322 <__divdf3+0x472>
    1426:	4281                	li	t0,0
    1428:	4781                	li	a5,0
    142a:	7ff00613          	li	a2,2047
    142e:	bdd5                	j	1322 <__divdf3+0x472>
    1430:	4281                	li	t0,0
    1432:	4781                	li	a5,0
    1434:	4605                	li	a2,1
    1436:	b5f5                	j	1322 <__divdf3+0x472>
    1438:	87ae                	mv	a5,a1
    143a:	85be                	mv	a1,a5
    143c:	bd49                	j	12ce <__divdf3+0x41e>

0000143e <__eqdf2>:
    143e:	001007b7          	lui	a5,0x100
    1442:	17fd                	addi	a5,a5,-1
    1444:	1151                	addi	sp,sp,-12
    1446:	00b7f333          	and	t1,a5,a1
    144a:	0145d713          	srli	a4,a1,0x14
    144e:	81fd                	srli	a1,a1,0x1f
    1450:	c422                	sw	s0,8(sp)
    1452:	c226                	sw	s1,4(sp)
    1454:	82aa                	mv	t0,a0
    1456:	842a                	mv	s0,a0
    1458:	c02e                	sw	a1,0(sp)
    145a:	7ff77713          	andi	a4,a4,2047
    145e:	0146d593          	srli	a1,a3,0x14
    1462:	7ff00513          	li	a0,2047
    1466:	8ff5                	and	a5,a5,a3
    1468:	84b2                	mv	s1,a2
    146a:	7ff5f593          	andi	a1,a1,2047
    146e:	82fd                	srli	a3,a3,0x1f
    1470:	00a71a63          	bne	a4,a0,1484 <__eqdf2+0x46>
    1474:	005363b3          	or	t2,t1,t0
    1478:	4505                	li	a0,1
    147a:	02039963          	bnez	t2,14ac <__eqdf2+0x6e>
    147e:	02e59763          	bne	a1,a4,14ac <__eqdf2+0x6e>
    1482:	a019                	j	1488 <__eqdf2+0x4a>
    1484:	00a59563          	bne	a1,a0,148e <__eqdf2+0x50>
    1488:	8e5d                	or	a2,a2,a5
    148a:	4505                	li	a0,1
    148c:	e205                	bnez	a2,14ac <__eqdf2+0x6e>
    148e:	4505                	li	a0,1
    1490:	00b71e63          	bne	a4,a1,14ac <__eqdf2+0x6e>
    1494:	00f31c63          	bne	t1,a5,14ac <__eqdf2+0x6e>
    1498:	00941a63          	bne	s0,s1,14ac <__eqdf2+0x6e>
    149c:	4782                	lw	a5,0(sp)
    149e:	00d78b63          	beq	a5,a3,14b4 <__eqdf2+0x76>
    14a2:	e709                	bnez	a4,14ac <__eqdf2+0x6e>
    14a4:	00536533          	or	a0,t1,t0
    14a8:	00a03533          	snez	a0,a0
    14ac:	4422                	lw	s0,8(sp)
    14ae:	4492                	lw	s1,4(sp)
    14b0:	0131                	addi	sp,sp,12
    14b2:	8082                	ret
    14b4:	4501                	li	a0,0
    14b6:	bfdd                	j	14ac <__eqdf2+0x6e>

000014b8 <__gedf2>:
    14b8:	001007b7          	lui	a5,0x100
    14bc:	17fd                	addi	a5,a5,-1
    14be:	1151                	addi	sp,sp,-12
    14c0:	00b7f2b3          	and	t0,a5,a1
    14c4:	0145d313          	srli	t1,a1,0x14
    14c8:	0146d713          	srli	a4,a3,0x14
    14cc:	8ff5                	and	a5,a5,a3
    14ce:	82fd                	srli	a3,a3,0x1f
    14d0:	c422                	sw	s0,8(sp)
    14d2:	c226                	sw	s1,4(sp)
    14d4:	c036                	sw	a3,0(sp)
    14d6:	7ff37313          	andi	t1,t1,2047
    14da:	7ff00693          	li	a3,2047
    14de:	83aa                	mv	t2,a0
    14e0:	842a                	mv	s0,a0
    14e2:	81fd                	srli	a1,a1,0x1f
    14e4:	84b2                	mv	s1,a2
    14e6:	7ff77713          	andi	a4,a4,2047
    14ea:	00d31a63          	bne	t1,a3,14fe <__gedf2+0x46>
    14ee:	00a2e6b3          	or	a3,t0,a0
    14f2:	5579                	li	a0,-2
    14f4:	c6bd                	beqz	a3,1562 <__gedf2+0xaa>
    14f6:	4422                	lw	s0,8(sp)
    14f8:	4492                	lw	s1,4(sp)
    14fa:	0131                	addi	sp,sp,12
    14fc:	8082                	ret
    14fe:	00d71663          	bne	a4,a3,150a <__gedf2+0x52>
    1502:	00c7e6b3          	or	a3,a5,a2
    1506:	5579                	li	a0,-2
    1508:	f6fd                	bnez	a3,14f6 <__gedf2+0x3e>
    150a:	04031e63          	bnez	t1,1566 <__gedf2+0xae>
    150e:	0072e533          	or	a0,t0,t2
    1512:	00153513          	seqz	a0,a0
    1516:	e319                	bnez	a4,151c <__gedf2+0x64>
    1518:	8e5d                	or	a2,a2,a5
    151a:	ce15                	beqz	a2,1556 <__gedf2+0x9e>
    151c:	e519                	bnez	a0,152a <__gedf2+0x72>
    151e:	4682                	lw	a3,0(sp)
    1520:	00d58963          	beq	a1,a3,1532 <__gedf2+0x7a>
    1524:	4505                	li	a0,1
    1526:	d9e1                	beqz	a1,14f6 <__gedf2+0x3e>
    1528:	a019                	j	152e <__gedf2+0x76>
    152a:	4782                	lw	a5,0(sp)
    152c:	f7e9                	bnez	a5,14f6 <__gedf2+0x3e>
    152e:	557d                	li	a0,-1
    1530:	b7d9                	j	14f6 <__gedf2+0x3e>
    1532:	fe6749e3          	blt	a4,t1,1524 <__gedf2+0x6c>
    1536:	00e35663          	bge	t1,a4,1542 <__gedf2+0x8a>
    153a:	557d                	li	a0,-1
    153c:	ddcd                	beqz	a1,14f6 <__gedf2+0x3e>
    153e:	4505                	li	a0,1
    1540:	bf5d                	j	14f6 <__gedf2+0x3e>
    1542:	fe57e1e3          	bltu	a5,t0,1524 <__gedf2+0x6c>
    1546:	00f29b63          	bne	t0,a5,155c <__gedf2+0xa4>
    154a:	fc84ede3          	bltu	s1,s0,1524 <__gedf2+0x6c>
    154e:	4501                	li	a0,0
    1550:	fa9473e3          	bgeu	s0,s1,14f6 <__gedf2+0x3e>
    1554:	b7dd                	j	153a <__gedf2+0x82>
    1556:	d579                	beqz	a0,1524 <__gedf2+0x6c>
    1558:	4501                	li	a0,0
    155a:	bf71                	j	14f6 <__gedf2+0x3e>
    155c:	fcf2efe3          	bltu	t0,a5,153a <__gedf2+0x82>
    1560:	bfe5                	j	1558 <__gedf2+0xa0>
    1562:	fa6700e3          	beq	a4,t1,1502 <__gedf2+0x4a>
    1566:	ff45                	bnez	a4,151e <__gedf2+0x66>
    1568:	4501                	li	a0,0
    156a:	b77d                	j	1518 <__gedf2+0x60>

0000156c <__ledf2>:
    156c:	001007b7          	lui	a5,0x100
    1570:	17fd                	addi	a5,a5,-1
    1572:	1151                	addi	sp,sp,-12
    1574:	00b7f2b3          	and	t0,a5,a1
    1578:	0145d313          	srli	t1,a1,0x14
    157c:	0146d713          	srli	a4,a3,0x14
    1580:	8ff5                	and	a5,a5,a3
    1582:	82fd                	srli	a3,a3,0x1f
    1584:	c422                	sw	s0,8(sp)
    1586:	c226                	sw	s1,4(sp)
    1588:	c036                	sw	a3,0(sp)
    158a:	7ff37313          	andi	t1,t1,2047
    158e:	7ff00693          	li	a3,2047
    1592:	83aa                	mv	t2,a0
    1594:	842a                	mv	s0,a0
    1596:	81fd                	srli	a1,a1,0x1f
    1598:	84b2                	mv	s1,a2
    159a:	7ff77713          	andi	a4,a4,2047
    159e:	00d31a63          	bne	t1,a3,15b2 <__ledf2+0x46>
    15a2:	00a2e6b3          	or	a3,t0,a0
    15a6:	4509                	li	a0,2
    15a8:	c6bd                	beqz	a3,1616 <__ledf2+0xaa>
    15aa:	4422                	lw	s0,8(sp)
    15ac:	4492                	lw	s1,4(sp)
    15ae:	0131                	addi	sp,sp,12
    15b0:	8082                	ret
    15b2:	00d71663          	bne	a4,a3,15be <__ledf2+0x52>
    15b6:	00c7e6b3          	or	a3,a5,a2
    15ba:	4509                	li	a0,2
    15bc:	f6fd                	bnez	a3,15aa <__ledf2+0x3e>
    15be:	04031e63          	bnez	t1,161a <__ledf2+0xae>
    15c2:	0072e533          	or	a0,t0,t2
    15c6:	00153513          	seqz	a0,a0
    15ca:	e319                	bnez	a4,15d0 <__ledf2+0x64>
    15cc:	8e5d                	or	a2,a2,a5
    15ce:	ce15                	beqz	a2,160a <__ledf2+0x9e>
    15d0:	e519                	bnez	a0,15de <__ledf2+0x72>
    15d2:	4682                	lw	a3,0(sp)
    15d4:	00d58963          	beq	a1,a3,15e6 <__ledf2+0x7a>
    15d8:	4505                	li	a0,1
    15da:	d9e1                	beqz	a1,15aa <__ledf2+0x3e>
    15dc:	a019                	j	15e2 <__ledf2+0x76>
    15de:	4782                	lw	a5,0(sp)
    15e0:	f7e9                	bnez	a5,15aa <__ledf2+0x3e>
    15e2:	557d                	li	a0,-1
    15e4:	b7d9                	j	15aa <__ledf2+0x3e>
    15e6:	fe6749e3          	blt	a4,t1,15d8 <__ledf2+0x6c>
    15ea:	00e35663          	bge	t1,a4,15f6 <__ledf2+0x8a>
    15ee:	557d                	li	a0,-1
    15f0:	ddcd                	beqz	a1,15aa <__ledf2+0x3e>
    15f2:	4505                	li	a0,1
    15f4:	bf5d                	j	15aa <__ledf2+0x3e>
    15f6:	fe57e1e3          	bltu	a5,t0,15d8 <__ledf2+0x6c>
    15fa:	00f29b63          	bne	t0,a5,1610 <__ledf2+0xa4>
    15fe:	fc84ede3          	bltu	s1,s0,15d8 <__ledf2+0x6c>
    1602:	4501                	li	a0,0
    1604:	fa9473e3          	bgeu	s0,s1,15aa <__ledf2+0x3e>
    1608:	b7dd                	j	15ee <__ledf2+0x82>
    160a:	d579                	beqz	a0,15d8 <__ledf2+0x6c>
    160c:	4501                	li	a0,0
    160e:	bf71                	j	15aa <__ledf2+0x3e>
    1610:	fcf2efe3          	bltu	t0,a5,15ee <__ledf2+0x82>
    1614:	bfe5                	j	160c <__ledf2+0xa0>
    1616:	fa6700e3          	beq	a4,t1,15b6 <__ledf2+0x4a>
    161a:	ff45                	bnez	a4,15d2 <__ledf2+0x66>
    161c:	4501                	li	a0,0
    161e:	b77d                	j	15cc <__ledf2+0x60>

00001620 <__muldf3>:
    1620:	fd810113          	addi	sp,sp,-40
    1624:	872a                	mv	a4,a0
    1626:	832a                	mv	t1,a0
    1628:	01f5d793          	srli	a5,a1,0x1f
    162c:	0145d513          	srli	a0,a1,0x14
    1630:	d022                	sw	s0,32(sp)
    1632:	ce26                	sw	s1,28(sp)
    1634:	d206                	sw	ra,36(sp)
    1636:	00c59493          	slli	s1,a1,0xc
    163a:	7ff57513          	andi	a0,a0,2047
    163e:	c43e                	sw	a5,8(sp)
    1640:	8436                	mv	s0,a3
    1642:	80b1                	srli	s1,s1,0xc
    1644:	c941                	beqz	a0,16d4 <__muldf3+0xb4>
    1646:	7ff00693          	li	a3,2047
    164a:	0ed50563          	beq	a0,a3,1734 <__muldf3+0x114>
    164e:	01d75693          	srli	a3,a4,0x1d
    1652:	00349793          	slli	a5,s1,0x3
    1656:	8fd5                	or	a5,a5,a3
    1658:	008006b7          	lui	a3,0x800
    165c:	00d7e3b3          	or	t2,a5,a3
    1660:	00371313          	slli	t1,a4,0x3
    1664:	c0150593          	addi	a1,a0,-1023
    1668:	4481                	li	s1,0
    166a:	01445513          	srli	a0,s0,0x14
    166e:	00c41293          	slli	t0,s0,0xc
    1672:	7ff57513          	andi	a0,a0,2047
    1676:	8732                	mv	a4,a2
    1678:	00c2d293          	srli	t0,t0,0xc
    167c:	807d                	srli	s0,s0,0x1f
    167e:	cd69                	beqz	a0,1758 <__muldf3+0x138>
    1680:	7ff00793          	li	a5,2047
    1684:	14f50863          	beq	a0,a5,17d4 <__muldf3+0x1b4>
    1688:	01d65793          	srli	a5,a2,0x1d
    168c:	028e                	slli	t0,t0,0x3
    168e:	0057e2b3          	or	t0,a5,t0
    1692:	008007b7          	lui	a5,0x800
    1696:	00f2e7b3          	or	a5,t0,a5
    169a:	00361713          	slli	a4,a2,0x3
    169e:	c0150513          	addi	a0,a0,-1023
    16a2:	4681                	li	a3,0
    16a4:	4622                	lw	a2,8(sp)
    16a6:	8e21                	xor	a2,a2,s0
    16a8:	c032                	sw	a2,0(sp)
    16aa:	00a58633          	add	a2,a1,a0
    16ae:	c632                	sw	a2,12(sp)
    16b0:	0605                	addi	a2,a2,1
    16b2:	c232                	sw	a2,4(sp)
    16b4:	00249613          	slli	a2,s1,0x2
    16b8:	8e55                	or	a2,a2,a3
    16ba:	167d                	addi	a2,a2,-1
    16bc:	45b9                	li	a1,14
    16be:	12c5ec63          	bltu	a1,a2,17f6 <__muldf3+0x1d6>
    16c2:	00004597          	auipc	a1,0x4
    16c6:	09258593          	addi	a1,a1,146 # 5754 <__srodata+0x54>
    16ca:	060a                	slli	a2,a2,0x2
    16cc:	962e                	add	a2,a2,a1
    16ce:	4210                	lw	a2,0(a2)
    16d0:	95b2                	add	a1,a1,a2
    16d2:	8582                	jr	a1
    16d4:	00e4e3b3          	or	t2,s1,a4
    16d8:	06038763          	beqz	t2,1746 <__muldf3+0x126>
    16dc:	c232                	sw	a2,4(sp)
    16de:	cc85                	beqz	s1,1716 <__muldf3+0xf6>
    16e0:	8526                	mv	a0,s1
    16e2:	c03a                	sw	a4,0(sp)
    16e4:	2bd000ef          	jal	ra,21a0 <__clzsi2>
    16e8:	4702                	lw	a4,0(sp)
    16ea:	4612                	lw	a2,4(sp)
    16ec:	ff550593          	addi	a1,a0,-11
    16f0:	46f1                	li	a3,28
    16f2:	02b6cb63          	blt	a3,a1,1728 <__muldf3+0x108>
    16f6:	46f5                	li	a3,29
    16f8:	ff850313          	addi	t1,a0,-8
    16fc:	8e8d                	sub	a3,a3,a1
    16fe:	006497b3          	sll	a5,s1,t1
    1702:	00d756b3          	srl	a3,a4,a3
    1706:	00f6e3b3          	or	t2,a3,a5
    170a:	00671333          	sll	t1,a4,t1
    170e:	c0d00593          	li	a1,-1011
    1712:	8d89                	sub	a1,a1,a0
    1714:	bf91                	j	1668 <__muldf3+0x48>
    1716:	853a                	mv	a0,a4
    1718:	c03a                	sw	a4,0(sp)
    171a:	287000ef          	jal	ra,21a0 <__clzsi2>
    171e:	02050513          	addi	a0,a0,32
    1722:	4612                	lw	a2,4(sp)
    1724:	4702                	lw	a4,0(sp)
    1726:	b7d9                	j	16ec <__muldf3+0xcc>
    1728:	fd850793          	addi	a5,a0,-40
    172c:	00f713b3          	sll	t2,a4,a5
    1730:	4301                	li	t1,0
    1732:	bff1                	j	170e <__muldf3+0xee>
    1734:	00e4e3b3          	or	t2,s1,a4
    1738:	00038b63          	beqz	t2,174e <__muldf3+0x12e>
    173c:	83a6                	mv	t2,s1
    173e:	7ff00593          	li	a1,2047
    1742:	448d                	li	s1,3
    1744:	b71d                	j	166a <__muldf3+0x4a>
    1746:	4301                	li	t1,0
    1748:	4581                	li	a1,0
    174a:	4485                	li	s1,1
    174c:	bf39                	j	166a <__muldf3+0x4a>
    174e:	4301                	li	t1,0
    1750:	7ff00593          	li	a1,2047
    1754:	4489                	li	s1,2
    1756:	bf11                	j	166a <__muldf3+0x4a>
    1758:	00c2e7b3          	or	a5,t0,a2
    175c:	c7c1                	beqz	a5,17e4 <__muldf3+0x1c4>
    175e:	04028563          	beqz	t0,17a8 <__muldf3+0x188>
    1762:	8516                	mv	a0,t0
    1764:	ca32                	sw	a2,20(sp)
    1766:	c81a                	sw	t1,16(sp)
    1768:	c61e                	sw	t2,12(sp)
    176a:	c22e                	sw	a1,4(sp)
    176c:	c016                	sw	t0,0(sp)
    176e:	233000ef          	jal	ra,21a0 <__clzsi2>
    1772:	4282                	lw	t0,0(sp)
    1774:	4592                	lw	a1,4(sp)
    1776:	43b2                	lw	t2,12(sp)
    1778:	4342                	lw	t1,16(sp)
    177a:	4652                	lw	a2,20(sp)
    177c:	ff550693          	addi	a3,a0,-11
    1780:	47f1                	li	a5,28
    1782:	04d7c363          	blt	a5,a3,17c8 <__muldf3+0x1a8>
    1786:	47f5                	li	a5,29
    1788:	ff850713          	addi	a4,a0,-8
    178c:	8f95                	sub	a5,a5,a3
    178e:	00e292b3          	sll	t0,t0,a4
    1792:	00f657b3          	srl	a5,a2,a5
    1796:	0057e7b3          	or	a5,a5,t0
    179a:	00e61733          	sll	a4,a2,a4
    179e:	c0d00693          	li	a3,-1011
    17a2:	40a68533          	sub	a0,a3,a0
    17a6:	bdf5                	j	16a2 <__muldf3+0x82>
    17a8:	8532                	mv	a0,a2
    17aa:	ca16                	sw	t0,20(sp)
    17ac:	c81a                	sw	t1,16(sp)
    17ae:	c61e                	sw	t2,12(sp)
    17b0:	c22e                	sw	a1,4(sp)
    17b2:	c032                	sw	a2,0(sp)
    17b4:	1ed000ef          	jal	ra,21a0 <__clzsi2>
    17b8:	02050513          	addi	a0,a0,32
    17bc:	42d2                	lw	t0,20(sp)
    17be:	4342                	lw	t1,16(sp)
    17c0:	43b2                	lw	t2,12(sp)
    17c2:	4592                	lw	a1,4(sp)
    17c4:	4602                	lw	a2,0(sp)
    17c6:	bf5d                	j	177c <__muldf3+0x15c>
    17c8:	fd850293          	addi	t0,a0,-40
    17cc:	005617b3          	sll	a5,a2,t0
    17d0:	4701                	li	a4,0
    17d2:	b7f1                	j	179e <__muldf3+0x17e>
    17d4:	00c2e7b3          	or	a5,t0,a2
    17d8:	cb91                	beqz	a5,17ec <__muldf3+0x1cc>
    17da:	8796                	mv	a5,t0
    17dc:	7ff00513          	li	a0,2047
    17e0:	468d                	li	a3,3
    17e2:	b5c9                	j	16a4 <__muldf3+0x84>
    17e4:	4701                	li	a4,0
    17e6:	4501                	li	a0,0
    17e8:	4685                	li	a3,1
    17ea:	bd6d                	j	16a4 <__muldf3+0x84>
    17ec:	4701                	li	a4,0
    17ee:	7ff00513          	li	a0,2047
    17f2:	4689                	li	a3,2
    17f4:	bd45                	j	16a4 <__muldf3+0x84>
    17f6:	01035613          	srli	a2,t1,0x10
    17fa:	01075493          	srli	s1,a4,0x10
    17fe:	66c1                	lui	a3,0x10
    1800:	fff68293          	addi	t0,a3,-1 # ffff <__ctor_end__+0x96d7>
    1804:	029606b3          	mul	a3,a2,s1
    1808:	00537333          	and	t1,t1,t0
    180c:	00577733          	and	a4,a4,t0
    1810:	02e60433          	mul	s0,a2,a4
    1814:	c436                	sw	a3,8(sp)
    1816:	026486b3          	mul	a3,s1,t1
    181a:	026705b3          	mul	a1,a4,t1
    181e:	96a2                	add	a3,a3,s0
    1820:	8536                	mv	a0,a3
    1822:	0105d693          	srli	a3,a1,0x10
    1826:	96aa                	add	a3,a3,a0
    1828:	0086f763          	bgeu	a3,s0,1836 <__muldf3+0x216>
    182c:	02960433          	mul	s0,a2,s1
    1830:	6541                	lui	a0,0x10
    1832:	9522                	add	a0,a0,s0
    1834:	c42a                	sw	a0,8(sp)
    1836:	0106d513          	srli	a0,a3,0x10
    183a:	0056f6b3          	and	a3,a3,t0
    183e:	0055f5b3          	and	a1,a1,t0
    1842:	06c2                	slli	a3,a3,0x10
    1844:	96ae                	add	a3,a3,a1
    1846:	0057f2b3          	and	t0,a5,t0
    184a:	0107d593          	srli	a1,a5,0x10
    184e:	ca36                	sw	a3,20(sp)
    1850:	026287b3          	mul	a5,t0,t1
    1854:	025606b3          	mul	a3,a2,t0
    1858:	0107d413          	srli	s0,a5,0x10
    185c:	02658333          	mul	t1,a1,t1
    1860:	9336                	add	t1,t1,a3
    1862:	9322                	add	t1,t1,s0
    1864:	02b60633          	mul	a2,a2,a1
    1868:	00d37463          	bgeu	t1,a3,1870 <__muldf3+0x250>
    186c:	66c1                	lui	a3,0x10
    186e:	9636                	add	a2,a2,a3
    1870:	01035413          	srli	s0,t1,0x10
    1874:	00c406b3          	add	a3,s0,a2
    1878:	c836                	sw	a3,16(sp)
    187a:	66c1                	lui	a3,0x10
    187c:	fff68613          	addi	a2,a3,-1 # ffff <__ctor_end__+0x96d7>
    1880:	00c37333          	and	t1,t1,a2
    1884:	8ff1                	and	a5,a5,a2
    1886:	0342                	slli	t1,t1,0x10
    1888:	933e                	add	t1,t1,a5
    188a:	006507b3          	add	a5,a0,t1
    188e:	00c3f633          	and	a2,t2,a2
    1892:	0103d513          	srli	a0,t2,0x10
    1896:	cc3e                	sw	a5,24(sp)
    1898:	02a483b3          	mul	t2,s1,a0
    189c:	02c707b3          	mul	a5,a4,a2
    18a0:	02c484b3          	mul	s1,s1,a2
    18a4:	0107d413          	srli	s0,a5,0x10
    18a8:	02e50733          	mul	a4,a0,a4
    18ac:	94ba                	add	s1,s1,a4
    18ae:	94a2                	add	s1,s1,s0
    18b0:	00e4f363          	bgeu	s1,a4,18b6 <__muldf3+0x296>
    18b4:	93b6                	add	t2,t2,a3
    18b6:	02c28433          	mul	s0,t0,a2
    18ba:	0104d713          	srli	a4,s1,0x10
    18be:	93ba                	add	t2,t2,a4
    18c0:	6741                	lui	a4,0x10
    18c2:	fff70693          	addi	a3,a4,-1 # ffff <__ctor_end__+0x96d7>
    18c6:	8cf5                	and	s1,s1,a3
    18c8:	04c2                	slli	s1,s1,0x10
    18ca:	8ff5                	and	a5,a5,a3
    18cc:	94be                	add	s1,s1,a5
    18ce:	025502b3          	mul	t0,a0,t0
    18d2:	02c58633          	mul	a2,a1,a2
    18d6:	02a58533          	mul	a0,a1,a0
    18da:	9616                	add	a2,a2,t0
    18dc:	01045593          	srli	a1,s0,0x10
    18e0:	95b2                	add	a1,a1,a2
    18e2:	0055f363          	bgeu	a1,t0,18e8 <__muldf3+0x2c8>
    18e6:	953a                	add	a0,a0,a4
    18e8:	47a2                	lw	a5,8(sp)
    18ea:	4762                	lw	a4,24(sp)
    18ec:	00e786b3          	add	a3,a5,a4
    18f0:	67c1                	lui	a5,0x10
    18f2:	17fd                	addi	a5,a5,-1
    18f4:	00f5f633          	and	a2,a1,a5
    18f8:	8c7d                	and	s0,s0,a5
    18fa:	47c2                	lw	a5,16(sp)
    18fc:	0642                	slli	a2,a2,0x10
    18fe:	9622                	add	a2,a2,s0
    1900:	0066b333          	sltu	t1,a3,t1
    1904:	963e                	add	a2,a2,a5
    1906:	4742                	lw	a4,16(sp)
    1908:	96a6                	add	a3,a3,s1
    190a:	00660433          	add	s0,a2,t1
    190e:	0096b7b3          	sltu	a5,a3,s1
    1912:	007402b3          	add	t0,s0,t2
    1916:	00f284b3          	add	s1,t0,a5
    191a:	00e63633          	sltu	a2,a2,a4
    191e:	00643333          	sltu	t1,s0,t1
    1922:	00f4b7b3          	sltu	a5,s1,a5
    1926:	00666633          	or	a2,a2,t1
    192a:	81c1                	srli	a1,a1,0x10
    192c:	0072b2b3          	sltu	t0,t0,t2
    1930:	962e                	add	a2,a2,a1
    1932:	00f2e2b3          	or	t0,t0,a5
    1936:	92b2                	add	t0,t0,a2
    1938:	4652                	lw	a2,20(sp)
    193a:	00969713          	slli	a4,a3,0x9
    193e:	9516                	add	a0,a0,t0
    1940:	0174d793          	srli	a5,s1,0x17
    1944:	00951293          	slli	t0,a0,0x9
    1948:	8f51                	or	a4,a4,a2
    194a:	82dd                	srli	a3,a3,0x17
    194c:	00f2e7b3          	or	a5,t0,a5
    1950:	00e03733          	snez	a4,a4
    1954:	8f55                	or	a4,a4,a3
    1956:	04a6                	slli	s1,s1,0x9
    1958:	00779693          	slli	a3,a5,0x7
    195c:	8f45                	or	a4,a4,s1
    195e:	0a06d963          	bgez	a3,1a10 <__muldf3+0x3f0>
    1962:	00175693          	srli	a3,a4,0x1
    1966:	8b05                	andi	a4,a4,1
    1968:	8f55                	or	a4,a4,a3
    196a:	01f79693          	slli	a3,a5,0x1f
    196e:	8f55                	or	a4,a4,a3
    1970:	8385                	srli	a5,a5,0x1
    1972:	4692                	lw	a3,4(sp)
    1974:	3ff68613          	addi	a2,a3,1023
    1978:	08c05f63          	blez	a2,1a16 <__muldf3+0x3f6>
    197c:	00777693          	andi	a3,a4,7
    1980:	ce81                	beqz	a3,1998 <__muldf3+0x378>
    1982:	00f77693          	andi	a3,a4,15
    1986:	4591                	li	a1,4
    1988:	00b68863          	beq	a3,a1,1998 <__muldf3+0x378>
    198c:	00470693          	addi	a3,a4,4
    1990:	00e6b733          	sltu	a4,a3,a4
    1994:	97ba                	add	a5,a5,a4
    1996:	8736                	mv	a4,a3
    1998:	00779693          	slli	a3,a5,0x7
    199c:	0006d963          	bgez	a3,19ae <__muldf3+0x38e>
    19a0:	ff0006b7          	lui	a3,0xff000
    19a4:	16fd                	addi	a3,a3,-1
    19a6:	8ff5                	and	a5,a5,a3
    19a8:	4692                	lw	a3,4(sp)
    19aa:	40068613          	addi	a2,a3,1024 # ff000400 <__heap_end+0xdefd0400>
    19ae:	7fe00693          	li	a3,2046
    19b2:	10c6c063          	blt	a3,a2,1ab2 <__muldf3+0x492>
    19b6:	830d                	srli	a4,a4,0x3
    19b8:	01d79693          	slli	a3,a5,0x1d
    19bc:	8f55                	or	a4,a4,a3
    19be:	0037d293          	srli	t0,a5,0x3
    19c2:	4782                	lw	a5,0(sp)
    19c4:	02b2                	slli	t0,t0,0xc
    19c6:	7ff67693          	andi	a3,a2,2047
    19ca:	06d2                	slli	a3,a3,0x14
    19cc:	5092                	lw	ra,36(sp)
    19ce:	5402                	lw	s0,32(sp)
    19d0:	00c2d293          	srli	t0,t0,0xc
    19d4:	07fe                	slli	a5,a5,0x1f
    19d6:	00d2e2b3          	or	t0,t0,a3
    19da:	00f2e6b3          	or	a3,t0,a5
    19de:	44f2                	lw	s1,28(sp)
    19e0:	853a                	mv	a0,a4
    19e2:	85b6                	mv	a1,a3
    19e4:	02810113          	addi	sp,sp,40
    19e8:	8082                	ret
    19ea:	47a2                	lw	a5,8(sp)
    19ec:	c03e                	sw	a5,0(sp)
    19ee:	879e                	mv	a5,t2
    19f0:	871a                	mv	a4,t1
    19f2:	86a6                	mv	a3,s1
    19f4:	4609                	li	a2,2
    19f6:	0ac68e63          	beq	a3,a2,1ab2 <__muldf3+0x492>
    19fa:	460d                	li	a2,3
    19fc:	0ac68463          	beq	a3,a2,1aa4 <__muldf3+0x484>
    1a00:	4605                	li	a2,1
    1a02:	f6c698e3          	bne	a3,a2,1972 <__muldf3+0x352>
    1a06:	4281                	li	t0,0
    1a08:	4701                	li	a4,0
    1a0a:	a0bd                	j	1a78 <__muldf3+0x458>
    1a0c:	c022                	sw	s0,0(sp)
    1a0e:	b7dd                	j	19f4 <__muldf3+0x3d4>
    1a10:	46b2                	lw	a3,12(sp)
    1a12:	c236                	sw	a3,4(sp)
    1a14:	bfb9                	j	1972 <__muldf3+0x352>
    1a16:	4285                	li	t0,1
    1a18:	40c282b3          	sub	t0,t0,a2
    1a1c:	03800693          	li	a3,56
    1a20:	fe56c3e3          	blt	a3,t0,1a06 <__muldf3+0x3e6>
    1a24:	46fd                	li	a3,31
    1a26:	0456cb63          	blt	a3,t0,1a7c <__muldf3+0x45c>
    1a2a:	4692                	lw	a3,4(sp)
    1a2c:	005755b3          	srl	a1,a4,t0
    1a30:	0057d2b3          	srl	t0,a5,t0
    1a34:	41e68613          	addi	a2,a3,1054
    1a38:	00c796b3          	sll	a3,a5,a2
    1a3c:	00c71733          	sll	a4,a4,a2
    1a40:	8ecd                	or	a3,a3,a1
    1a42:	00e03733          	snez	a4,a4
    1a46:	8f55                	or	a4,a4,a3
    1a48:	00777793          	andi	a5,a4,7
    1a4c:	cf81                	beqz	a5,1a64 <__muldf3+0x444>
    1a4e:	00f77793          	andi	a5,a4,15
    1a52:	4691                	li	a3,4
    1a54:	00d78863          	beq	a5,a3,1a64 <__muldf3+0x444>
    1a58:	00470793          	addi	a5,a4,4
    1a5c:	00e7b733          	sltu	a4,a5,a4
    1a60:	92ba                	add	t0,t0,a4
    1a62:	873e                	mv	a4,a5
    1a64:	00829793          	slli	a5,t0,0x8
    1a68:	0407ca63          	bltz	a5,1abc <__muldf3+0x49c>
    1a6c:	01d29793          	slli	a5,t0,0x1d
    1a70:	830d                	srli	a4,a4,0x3
    1a72:	8f5d                	or	a4,a4,a5
    1a74:	0032d293          	srli	t0,t0,0x3
    1a78:	4601                	li	a2,0
    1a7a:	b7a1                	j	19c2 <__muldf3+0x3a2>
    1a7c:	5685                	li	a3,-31
    1a7e:	8e91                	sub	a3,a3,a2
    1a80:	02000593          	li	a1,32
    1a84:	00d7d6b3          	srl	a3,a5,a3
    1a88:	4601                	li	a2,0
    1a8a:	00b28763          	beq	t0,a1,1a98 <__muldf3+0x478>
    1a8e:	4612                	lw	a2,4(sp)
    1a90:	43e60613          	addi	a2,a2,1086
    1a94:	00c79633          	sll	a2,a5,a2
    1a98:	8f51                	or	a4,a4,a2
    1a9a:	00e03733          	snez	a4,a4
    1a9e:	8f55                	or	a4,a4,a3
    1aa0:	4281                	li	t0,0
    1aa2:	b75d                	j	1a48 <__muldf3+0x428>
    1aa4:	000802b7          	lui	t0,0x80
    1aa8:	4701                	li	a4,0
    1aaa:	7ff00613          	li	a2,2047
    1aae:	c002                	sw	zero,0(sp)
    1ab0:	bf09                	j	19c2 <__muldf3+0x3a2>
    1ab2:	4281                	li	t0,0
    1ab4:	4701                	li	a4,0
    1ab6:	7ff00613          	li	a2,2047
    1aba:	b721                	j	19c2 <__muldf3+0x3a2>
    1abc:	4281                	li	t0,0
    1abe:	4701                	li	a4,0
    1ac0:	4605                	li	a2,1
    1ac2:	b701                	j	19c2 <__muldf3+0x3a2>

00001ac4 <__subdf3>:
    1ac4:	001002b7          	lui	t0,0x100
    1ac8:	12fd                	addi	t0,t0,-1
    1aca:	1131                	addi	sp,sp,-20
    1acc:	00b2f333          	and	t1,t0,a1
    1ad0:	030e                	slli	t1,t1,0x3
    1ad2:	c622                	sw	s0,12(sp)
    1ad4:	c426                	sw	s1,8(sp)
    1ad6:	0145d413          	srli	s0,a1,0x14
    1ada:	01f5d493          	srli	s1,a1,0x1f
    1ade:	01d55793          	srli	a5,a0,0x1d
    1ae2:	00d2f5b3          	and	a1,t0,a3
    1ae6:	0067e7b3          	or	a5,a5,t1
    1aea:	0146d293          	srli	t0,a3,0x14
    1aee:	01d65313          	srli	t1,a2,0x1d
    1af2:	058e                	slli	a1,a1,0x3
    1af4:	00b365b3          	or	a1,t1,a1
    1af8:	c806                	sw	ra,16(sp)
    1afa:	00361313          	slli	t1,a2,0x3
    1afe:	7ff2f293          	andi	t0,t0,2047
    1b02:	7ff00613          	li	a2,2047
    1b06:	7ff47413          	andi	s0,s0,2047
    1b0a:	00351713          	slli	a4,a0,0x3
    1b0e:	82fd                	srli	a3,a3,0x1f
    1b10:	00c29563          	bne	t0,a2,1b1a <__subdf3+0x56>
    1b14:	0065e633          	or	a2,a1,t1
    1b18:	e219                	bnez	a2,1b1e <__subdf3+0x5a>
    1b1a:	0016c693          	xori	a3,a3,1
    1b1e:	40540533          	sub	a0,s0,t0
    1b22:	22969563          	bne	a3,s1,1d4c <__subdf3+0x288>
    1b26:	0ea05e63          	blez	a0,1c22 <__subdf3+0x15e>
    1b2a:	04029463          	bnez	t0,1b72 <__subdf3+0xae>
    1b2e:	0065e6b3          	or	a3,a1,t1
    1b32:	c285                	beqz	a3,1b52 <__subdf3+0x8e>
    1b34:	fff40513          	addi	a0,s0,-1
    1b38:	e909                	bnez	a0,1b4a <__subdf3+0x86>
    1b3a:	933a                	add	t1,t1,a4
    1b3c:	00e33733          	sltu	a4,t1,a4
    1b40:	97ae                	add	a5,a5,a1
    1b42:	97ba                	add	a5,a5,a4
    1b44:	871a                	mv	a4,t1
    1b46:	4405                	li	s0,1
    1b48:	a895                	j	1bbc <__subdf3+0xf8>
    1b4a:	7ff00693          	li	a3,2047
    1b4e:	02d41963          	bne	s0,a3,1b80 <__subdf3+0xbc>
    1b52:	00777693          	andi	a3,a4,7
    1b56:	4e068f63          	beqz	a3,2054 <__subdf3+0x590>
    1b5a:	00f77693          	andi	a3,a4,15
    1b5e:	4611                	li	a2,4
    1b60:	4ec68a63          	beq	a3,a2,2054 <__subdf3+0x590>
    1b64:	00470693          	addi	a3,a4,4
    1b68:	00e6b733          	sltu	a4,a3,a4
    1b6c:	97ba                	add	a5,a5,a4
    1b6e:	8736                	mv	a4,a3
    1b70:	a1d5                	j	2054 <__subdf3+0x590>
    1b72:	7ff00693          	li	a3,2047
    1b76:	fcd40ee3          	beq	s0,a3,1b52 <__subdf3+0x8e>
    1b7a:	008006b7          	lui	a3,0x800
    1b7e:	8dd5                	or	a1,a1,a3
    1b80:	03800693          	li	a3,56
    1b84:	08a6ca63          	blt	a3,a0,1c18 <__subdf3+0x154>
    1b88:	46fd                	li	a3,31
    1b8a:	06a6c063          	blt	a3,a0,1bea <__subdf3+0x126>
    1b8e:	02000613          	li	a2,32
    1b92:	8e09                	sub	a2,a2,a0
    1b94:	00c596b3          	sll	a3,a1,a2
    1b98:	00a352b3          	srl	t0,t1,a0
    1b9c:	00c31333          	sll	t1,t1,a2
    1ba0:	0056e6b3          	or	a3,a3,t0
    1ba4:	00603333          	snez	t1,t1
    1ba8:	0066e333          	or	t1,a3,t1
    1bac:	00a5d5b3          	srl	a1,a1,a0
    1bb0:	933a                	add	t1,t1,a4
    1bb2:	95be                	add	a1,a1,a5
    1bb4:	00e337b3          	sltu	a5,t1,a4
    1bb8:	97ae                	add	a5,a5,a1
    1bba:	871a                	mv	a4,t1
    1bbc:	00879693          	slli	a3,a5,0x8
    1bc0:	f806d9e3          	bgez	a3,1b52 <__subdf3+0x8e>
    1bc4:	0405                	addi	s0,s0,1
    1bc6:	7ff00693          	li	a3,2047
    1bca:	48d40363          	beq	s0,a3,2050 <__subdf3+0x58c>
    1bce:	ff800637          	lui	a2,0xff800
    1bd2:	167d                	addi	a2,a2,-1
    1bd4:	8e7d                	and	a2,a2,a5
    1bd6:	00175693          	srli	a3,a4,0x1
    1bda:	8b05                	andi	a4,a4,1
    1bdc:	01f61793          	slli	a5,a2,0x1f
    1be0:	8f55                	or	a4,a4,a3
    1be2:	8f5d                	or	a4,a4,a5
    1be4:	00165793          	srli	a5,a2,0x1
    1be8:	b7ad                	j	1b52 <__subdf3+0x8e>
    1bea:	fe050693          	addi	a3,a0,-32 # ffe0 <__ctor_end__+0x96b8>
    1bee:	02000293          	li	t0,32
    1bf2:	00d5d6b3          	srl	a3,a1,a3
    1bf6:	4601                	li	a2,0
    1bf8:	00550863          	beq	a0,t0,1c08 <__subdf3+0x144>
    1bfc:	04000613          	li	a2,64
    1c00:	40a60533          	sub	a0,a2,a0
    1c04:	00a59633          	sll	a2,a1,a0
    1c08:	00666333          	or	t1,a2,t1
    1c0c:	00603333          	snez	t1,t1
    1c10:	0066e333          	or	t1,a3,t1
    1c14:	4581                	li	a1,0
    1c16:	bf69                	j	1bb0 <__subdf3+0xec>
    1c18:	0065e333          	or	t1,a1,t1
    1c1c:	00603333          	snez	t1,t1
    1c20:	bfd5                	j	1c14 <__subdf3+0x150>
    1c22:	c945                	beqz	a0,1cd2 <__subdf3+0x20e>
    1c24:	40828633          	sub	a2,t0,s0
    1c28:	e40d                	bnez	s0,1c52 <__subdf3+0x18e>
    1c2a:	00e7e6b3          	or	a3,a5,a4
    1c2e:	40068863          	beqz	a3,203e <__subdf3+0x57a>
    1c32:	fff60693          	addi	a3,a2,-1 # ff7fffff <__heap_end+0xdf7cffff>
    1c36:	e699                	bnez	a3,1c44 <__subdf3+0x180>
    1c38:	971a                	add	a4,a4,t1
    1c3a:	97ae                	add	a5,a5,a1
    1c3c:	00673333          	sltu	t1,a4,t1
    1c40:	979a                	add	a5,a5,t1
    1c42:	b711                	j	1b46 <__subdf3+0x82>
    1c44:	7ff00513          	li	a0,2047
    1c48:	00a61d63          	bne	a2,a0,1c62 <__subdf3+0x19e>
    1c4c:	87ae                	mv	a5,a1
    1c4e:	871a                	mv	a4,t1
    1c50:	ace5                	j	1f48 <__subdf3+0x484>
    1c52:	7ff00693          	li	a3,2047
    1c56:	fed28be3          	beq	t0,a3,1c4c <__subdf3+0x188>
    1c5a:	008006b7          	lui	a3,0x800
    1c5e:	8fd5                	or	a5,a5,a3
    1c60:	86b2                	mv	a3,a2
    1c62:	03800613          	li	a2,56
    1c66:	06d64263          	blt	a2,a3,1cca <__subdf3+0x206>
    1c6a:	467d                	li	a2,31
    1c6c:	02d64a63          	blt	a2,a3,1ca0 <__subdf3+0x1dc>
    1c70:	02000513          	li	a0,32
    1c74:	8d15                	sub	a0,a0,a3
    1c76:	00d753b3          	srl	t2,a4,a3
    1c7a:	00a79633          	sll	a2,a5,a0
    1c7e:	00a71733          	sll	a4,a4,a0
    1c82:	00766633          	or	a2,a2,t2
    1c86:	00e03733          	snez	a4,a4
    1c8a:	8f51                	or	a4,a4,a2
    1c8c:	00d7d6b3          	srl	a3,a5,a3
    1c90:	971a                	add	a4,a4,t1
    1c92:	00b687b3          	add	a5,a3,a1
    1c96:	00673333          	sltu	t1,a4,t1
    1c9a:	979a                	add	a5,a5,t1
    1c9c:	8416                	mv	s0,t0
    1c9e:	bf39                	j	1bbc <__subdf3+0xf8>
    1ca0:	fe068613          	addi	a2,a3,-32 # 7fffe0 <__ctor_end__+0x7f96b8>
    1ca4:	02000393          	li	t2,32
    1ca8:	00c7d633          	srl	a2,a5,a2
    1cac:	4501                	li	a0,0
    1cae:	00768863          	beq	a3,t2,1cbe <__subdf3+0x1fa>
    1cb2:	04000513          	li	a0,64
    1cb6:	40d506b3          	sub	a3,a0,a3
    1cba:	00d79533          	sll	a0,a5,a3
    1cbe:	8f49                	or	a4,a4,a0
    1cc0:	00e03733          	snez	a4,a4
    1cc4:	8f51                	or	a4,a4,a2
    1cc6:	4681                	li	a3,0
    1cc8:	b7e1                	j	1c90 <__subdf3+0x1cc>
    1cca:	8f5d                	or	a4,a4,a5
    1ccc:	00e03733          	snez	a4,a4
    1cd0:	bfdd                	j	1cc6 <__subdf3+0x202>
    1cd2:	00140693          	addi	a3,s0,1
    1cd6:	7fe6f613          	andi	a2,a3,2046
    1cda:	e631                	bnez	a2,1d26 <__subdf3+0x262>
    1cdc:	00e7e6b3          	or	a3,a5,a4
    1ce0:	e41d                	bnez	s0,1d0e <__subdf3+0x24a>
    1ce2:	36068263          	beqz	a3,2046 <__subdf3+0x582>
    1ce6:	0065e6b3          	or	a3,a1,t1
    1cea:	e60684e3          	beqz	a3,1b52 <__subdf3+0x8e>
    1cee:	933a                	add	t1,t1,a4
    1cf0:	00e33733          	sltu	a4,t1,a4
    1cf4:	97ae                	add	a5,a5,a1
    1cf6:	97ba                	add	a5,a5,a4
    1cf8:	00879693          	slli	a3,a5,0x8
    1cfc:	871a                	mv	a4,t1
    1cfe:	e406dae3          	bgez	a3,1b52 <__subdf3+0x8e>
    1d02:	ff8006b7          	lui	a3,0xff800
    1d06:	16fd                	addi	a3,a3,-1
    1d08:	8ff5                	and	a5,a5,a3
    1d0a:	4405                	li	s0,1
    1d0c:	b599                	j	1b52 <__subdf3+0x8e>
    1d0e:	de9d                	beqz	a3,1c4c <__subdf3+0x188>
    1d10:	0065e333          	or	t1,a1,t1
    1d14:	22030a63          	beqz	t1,1f48 <__subdf3+0x484>
    1d18:	4481                	li	s1,0
    1d1a:	004007b7          	lui	a5,0x400
    1d1e:	4701                	li	a4,0
    1d20:	7ff00413          	li	s0,2047
    1d24:	ae05                	j	2054 <__subdf3+0x590>
    1d26:	7ff00613          	li	a2,2047
    1d2a:	32c68163          	beq	a3,a2,204c <__subdf3+0x588>
    1d2e:	933a                	add	t1,t1,a4
    1d30:	95be                	add	a1,a1,a5
    1d32:	00e337b3          	sltu	a5,t1,a4
    1d36:	95be                	add	a1,a1,a5
    1d38:	01f59793          	slli	a5,a1,0x1f
    1d3c:	00135313          	srli	t1,t1,0x1
    1d40:	0067e733          	or	a4,a5,t1
    1d44:	8436                	mv	s0,a3
    1d46:	0015d793          	srli	a5,a1,0x1
    1d4a:	b521                	j	1b52 <__subdf3+0x8e>
    1d4c:	0ca05763          	blez	a0,1e1a <__subdf3+0x356>
    1d50:	08029163          	bnez	t0,1dd2 <__subdf3+0x30e>
    1d54:	0065e6b3          	or	a3,a1,t1
    1d58:	de068de3          	beqz	a3,1b52 <__subdf3+0x8e>
    1d5c:	fff40513          	addi	a0,s0,-1
    1d60:	e911                	bnez	a0,1d74 <__subdf3+0x2b0>
    1d62:	40670333          	sub	t1,a4,t1
    1d66:	00673733          	sltu	a4,a4,t1
    1d6a:	8f8d                	sub	a5,a5,a1
    1d6c:	8f99                	sub	a5,a5,a4
    1d6e:	871a                	mv	a4,t1
    1d70:	4405                	li	s0,1
    1d72:	a0b1                	j	1dbe <__subdf3+0x2fa>
    1d74:	7ff00693          	li	a3,2047
    1d78:	dcd40de3          	beq	s0,a3,1b52 <__subdf3+0x8e>
    1d7c:	03800693          	li	a3,56
    1d80:	08a6c863          	blt	a3,a0,1e10 <__subdf3+0x34c>
    1d84:	46fd                	li	a3,31
    1d86:	04a6ce63          	blt	a3,a0,1de2 <__subdf3+0x31e>
    1d8a:	02000613          	li	a2,32
    1d8e:	8e09                	sub	a2,a2,a0
    1d90:	00c596b3          	sll	a3,a1,a2
    1d94:	00a352b3          	srl	t0,t1,a0
    1d98:	00c31333          	sll	t1,t1,a2
    1d9c:	0056e6b3          	or	a3,a3,t0
    1da0:	00603333          	snez	t1,t1
    1da4:	0066e333          	or	t1,a3,t1
    1da8:	00a5d5b3          	srl	a1,a1,a0
    1dac:	40670333          	sub	t1,a4,t1
    1db0:	40b785b3          	sub	a1,a5,a1
    1db4:	006737b3          	sltu	a5,a4,t1
    1db8:	40f587b3          	sub	a5,a1,a5
    1dbc:	871a                	mv	a4,t1
    1dbe:	00879693          	slli	a3,a5,0x8
    1dc2:	d806d8e3          	bgez	a3,1b52 <__subdf3+0x8e>
    1dc6:	00800637          	lui	a2,0x800
    1dca:	167d                	addi	a2,a2,-1
    1dcc:	8e7d                	and	a2,a2,a5
    1dce:	82ba                	mv	t0,a4
    1dd0:	a26d                	j	1f7a <__subdf3+0x4b6>
    1dd2:	7ff00693          	li	a3,2047
    1dd6:	d6d40ee3          	beq	s0,a3,1b52 <__subdf3+0x8e>
    1dda:	008006b7          	lui	a3,0x800
    1dde:	8dd5                	or	a1,a1,a3
    1de0:	bf71                	j	1d7c <__subdf3+0x2b8>
    1de2:	fe050693          	addi	a3,a0,-32
    1de6:	02000293          	li	t0,32
    1dea:	00d5d6b3          	srl	a3,a1,a3
    1dee:	4601                	li	a2,0
    1df0:	00550863          	beq	a0,t0,1e00 <__subdf3+0x33c>
    1df4:	04000613          	li	a2,64
    1df8:	40a60533          	sub	a0,a2,a0
    1dfc:	00a59633          	sll	a2,a1,a0
    1e00:	00666333          	or	t1,a2,t1
    1e04:	00603333          	snez	t1,t1
    1e08:	0066e333          	or	t1,a3,t1
    1e0c:	4581                	li	a1,0
    1e0e:	bf79                	j	1dac <__subdf3+0x2e8>
    1e10:	0065e333          	or	t1,a1,t1
    1e14:	00603333          	snez	t1,t1
    1e18:	bfd5                	j	1e0c <__subdf3+0x348>
    1e1a:	c171                	beqz	a0,1ede <__subdf3+0x41a>
    1e1c:	40828533          	sub	a0,t0,s0
    1e20:	e81d                	bnez	s0,1e56 <__subdf3+0x392>
    1e22:	00e7e633          	or	a2,a5,a4
    1e26:	28060463          	beqz	a2,20ae <__subdf3+0x5ea>
    1e2a:	fff50613          	addi	a2,a0,-1
    1e2e:	ea19                	bnez	a2,1e44 <__subdf3+0x380>
    1e30:	40e30733          	sub	a4,t1,a4
    1e34:	40f587b3          	sub	a5,a1,a5
    1e38:	00e33333          	sltu	t1,t1,a4
    1e3c:	406787b3          	sub	a5,a5,t1
    1e40:	84b6                	mv	s1,a3
    1e42:	b73d                	j	1d70 <__subdf3+0x2ac>
    1e44:	7ff00393          	li	t2,2047
    1e48:	00751f63          	bne	a0,t2,1e66 <__subdf3+0x3a2>
    1e4c:	87ae                	mv	a5,a1
    1e4e:	871a                	mv	a4,t1
    1e50:	7ff00413          	li	s0,2047
    1e54:	a065                	j	1efc <__subdf3+0x438>
    1e56:	7ff00613          	li	a2,2047
    1e5a:	fec289e3          	beq	t0,a2,1e4c <__subdf3+0x388>
    1e5e:	00800637          	lui	a2,0x800
    1e62:	8fd1                	or	a5,a5,a2
    1e64:	862a                	mv	a2,a0
    1e66:	03800513          	li	a0,56
    1e6a:	06c54663          	blt	a0,a2,1ed6 <__subdf3+0x412>
    1e6e:	457d                	li	a0,31
    1e70:	02c54d63          	blt	a0,a2,1eaa <__subdf3+0x3e6>
    1e74:	02000393          	li	t2,32
    1e78:	40c383b3          	sub	t2,t2,a2
    1e7c:	00c75433          	srl	s0,a4,a2
    1e80:	00779533          	sll	a0,a5,t2
    1e84:	00771733          	sll	a4,a4,t2
    1e88:	8d41                	or	a0,a0,s0
    1e8a:	00e03733          	snez	a4,a4
    1e8e:	8f49                	or	a4,a4,a0
    1e90:	00c7d633          	srl	a2,a5,a2
    1e94:	40e30733          	sub	a4,t1,a4
    1e98:	40c587b3          	sub	a5,a1,a2
    1e9c:	00e33333          	sltu	t1,t1,a4
    1ea0:	406787b3          	sub	a5,a5,t1
    1ea4:	8416                	mv	s0,t0
    1ea6:	84b6                	mv	s1,a3
    1ea8:	bf19                	j	1dbe <__subdf3+0x2fa>
    1eaa:	fe060513          	addi	a0,a2,-32 # 7fffe0 <__ctor_end__+0x7f96b8>
    1eae:	02000413          	li	s0,32
    1eb2:	00a7d533          	srl	a0,a5,a0
    1eb6:	4381                	li	t2,0
    1eb8:	00860863          	beq	a2,s0,1ec8 <__subdf3+0x404>
    1ebc:	04000393          	li	t2,64
    1ec0:	40c38633          	sub	a2,t2,a2
    1ec4:	00c793b3          	sll	t2,a5,a2
    1ec8:	00e3e733          	or	a4,t2,a4
    1ecc:	00e03733          	snez	a4,a4
    1ed0:	8f49                	or	a4,a4,a0
    1ed2:	4601                	li	a2,0
    1ed4:	b7c1                	j	1e94 <__subdf3+0x3d0>
    1ed6:	8f5d                	or	a4,a4,a5
    1ed8:	00e03733          	snez	a4,a4
    1edc:	bfdd                	j	1ed2 <__subdf3+0x40e>
    1ede:	00140613          	addi	a2,s0,1
    1ee2:	7fe67613          	andi	a2,a2,2046
    1ee6:	e635                	bnez	a2,1f52 <__subdf3+0x48e>
    1ee8:	00e7e533          	or	a0,a5,a4
    1eec:	0065e633          	or	a2,a1,t1
    1ef0:	e431                	bnez	s0,1f3c <__subdf3+0x478>
    1ef2:	e519                	bnez	a0,1f00 <__subdf3+0x43c>
    1ef4:	1c060163          	beqz	a2,20b6 <__subdf3+0x5f2>
    1ef8:	87ae                	mv	a5,a1
    1efa:	871a                	mv	a4,t1
    1efc:	84b6                	mv	s1,a3
    1efe:	b991                	j	1b52 <__subdf3+0x8e>
    1f00:	c40609e3          	beqz	a2,1b52 <__subdf3+0x8e>
    1f04:	40670533          	sub	a0,a4,t1
    1f08:	00a732b3          	sltu	t0,a4,a0
    1f0c:	40b78633          	sub	a2,a5,a1
    1f10:	40560633          	sub	a2,a2,t0
    1f14:	00861293          	slli	t0,a2,0x8
    1f18:	0002db63          	bgez	t0,1f2e <__subdf3+0x46a>
    1f1c:	40e30733          	sub	a4,t1,a4
    1f20:	40f587b3          	sub	a5,a1,a5
    1f24:	00e33333          	sltu	t1,t1,a4
    1f28:	406787b3          	sub	a5,a5,t1
    1f2c:	bfc1                	j	1efc <__subdf3+0x438>
    1f2e:	00c56733          	or	a4,a0,a2
    1f32:	18070663          	beqz	a4,20be <__subdf3+0x5fa>
    1f36:	87b2                	mv	a5,a2
    1f38:	872a                	mv	a4,a0
    1f3a:	b921                	j	1b52 <__subdf3+0x8e>
    1f3c:	e909                	bnez	a0,1f4e <__subdf3+0x48a>
    1f3e:	18060263          	beqz	a2,20c2 <__subdf3+0x5fe>
    1f42:	87ae                	mv	a5,a1
    1f44:	871a                	mv	a4,t1
    1f46:	84b6                	mv	s1,a3
    1f48:	7ff00413          	li	s0,2047
    1f4c:	b119                	j	1b52 <__subdf3+0x8e>
    1f4e:	de6d                	beqz	a2,1f48 <__subdf3+0x484>
    1f50:	b3e1                	j	1d18 <__subdf3+0x254>
    1f52:	406702b3          	sub	t0,a4,t1
    1f56:	00573533          	sltu	a0,a4,t0
    1f5a:	40b78633          	sub	a2,a5,a1
    1f5e:	8e09                	sub	a2,a2,a0
    1f60:	00861513          	slli	a0,a2,0x8
    1f64:	06055c63          	bgez	a0,1fdc <__subdf3+0x518>
    1f68:	40e302b3          	sub	t0,t1,a4
    1f6c:	40f587b3          	sub	a5,a1,a5
    1f70:	00533333          	sltu	t1,t1,t0
    1f74:	40678633          	sub	a2,a5,t1
    1f78:	84b6                	mv	s1,a3
    1f7a:	c63d                	beqz	a2,1fe8 <__subdf3+0x524>
    1f7c:	8532                	mv	a0,a2
    1f7e:	c216                	sw	t0,4(sp)
    1f80:	c032                	sw	a2,0(sp)
    1f82:	2c39                	jal	21a0 <__clzsi2>
    1f84:	4602                	lw	a2,0(sp)
    1f86:	4292                	lw	t0,4(sp)
    1f88:	ff850693          	addi	a3,a0,-8
    1f8c:	47fd                	li	a5,31
    1f8e:	06d7c663          	blt	a5,a3,1ffa <__subdf3+0x536>
    1f92:	02000793          	li	a5,32
    1f96:	8f95                	sub	a5,a5,a3
    1f98:	00d61633          	sll	a2,a2,a3
    1f9c:	00f2d7b3          	srl	a5,t0,a5
    1fa0:	8e5d                	or	a2,a2,a5
    1fa2:	00d29733          	sll	a4,t0,a3
    1fa6:	0886c663          	blt	a3,s0,2032 <__subdf3+0x56e>
    1faa:	40868433          	sub	s0,a3,s0
    1fae:	00140593          	addi	a1,s0,1
    1fb2:	47fd                	li	a5,31
    1fb4:	04b7c963          	blt	a5,a1,2006 <__subdf3+0x542>
    1fb8:	02000513          	li	a0,32
    1fbc:	8d0d                	sub	a0,a0,a1
    1fbe:	00b757b3          	srl	a5,a4,a1
    1fc2:	00a616b3          	sll	a3,a2,a0
    1fc6:	8edd                	or	a3,a3,a5
    1fc8:	00a717b3          	sll	a5,a4,a0
    1fcc:	00f037b3          	snez	a5,a5
    1fd0:	00f6e733          	or	a4,a3,a5
    1fd4:	00b657b3          	srl	a5,a2,a1
    1fd8:	4401                	li	s0,0
    1fda:	bea5                	j	1b52 <__subdf3+0x8e>
    1fdc:	00c2e733          	or	a4,t0,a2
    1fe0:	ff49                	bnez	a4,1f7a <__subdf3+0x4b6>
    1fe2:	4781                	li	a5,0
    1fe4:	4401                	li	s0,0
    1fe6:	a8d1                	j	20ba <__subdf3+0x5f6>
    1fe8:	8516                	mv	a0,t0
    1fea:	c232                	sw	a2,4(sp)
    1fec:	c016                	sw	t0,0(sp)
    1fee:	2a4d                	jal	21a0 <__clzsi2>
    1ff0:	02050513          	addi	a0,a0,32
    1ff4:	4612                	lw	a2,4(sp)
    1ff6:	4282                	lw	t0,0(sp)
    1ff8:	bf41                	j	1f88 <__subdf3+0x4c4>
    1ffa:	fd850613          	addi	a2,a0,-40
    1ffe:	00c29633          	sll	a2,t0,a2
    2002:	4701                	li	a4,0
    2004:	b74d                	j	1fa6 <__subdf3+0x4e2>
    2006:	1405                	addi	s0,s0,-31
    2008:	02000793          	li	a5,32
    200c:	00865433          	srl	s0,a2,s0
    2010:	4281                	li	t0,0
    2012:	00f58863          	beq	a1,a5,2022 <__subdf3+0x55e>
    2016:	04000293          	li	t0,64
    201a:	40b282b3          	sub	t0,t0,a1
    201e:	005612b3          	sll	t0,a2,t0
    2022:	005767b3          	or	a5,a4,t0
    2026:	00f037b3          	snez	a5,a5
    202a:	00f46733          	or	a4,s0,a5
    202e:	4781                	li	a5,0
    2030:	b765                	j	1fd8 <__subdf3+0x514>
    2032:	ff8007b7          	lui	a5,0xff800
    2036:	17fd                	addi	a5,a5,-1
    2038:	8c15                	sub	s0,s0,a3
    203a:	8ff1                	and	a5,a5,a2
    203c:	be19                	j	1b52 <__subdf3+0x8e>
    203e:	87ae                	mv	a5,a1
    2040:	871a                	mv	a4,t1
    2042:	8432                	mv	s0,a2
    2044:	b639                	j	1b52 <__subdf3+0x8e>
    2046:	87ae                	mv	a5,a1
    2048:	871a                	mv	a4,t1
    204a:	b621                	j	1b52 <__subdf3+0x8e>
    204c:	7ff00413          	li	s0,2047
    2050:	4781                	li	a5,0
    2052:	4701                	li	a4,0
    2054:	00879693          	slli	a3,a5,0x8
    2058:	0006db63          	bgez	a3,206e <__subdf3+0x5aa>
    205c:	0405                	addi	s0,s0,1
    205e:	7ff00693          	li	a3,2047
    2062:	06d40563          	beq	s0,a3,20cc <__subdf3+0x608>
    2066:	ff8006b7          	lui	a3,0xff800
    206a:	16fd                	addi	a3,a3,-1
    206c:	8ff5                	and	a5,a5,a3
    206e:	01d79513          	slli	a0,a5,0x1d
    2072:	830d                	srli	a4,a4,0x3
    2074:	7ff00693          	li	a3,2047
    2078:	8f49                	or	a4,a4,a0
    207a:	838d                	srli	a5,a5,0x3
    207c:	00d41963          	bne	s0,a3,208e <__subdf3+0x5ca>
    2080:	8f5d                	or	a4,a4,a5
    2082:	4781                	li	a5,0
    2084:	c709                	beqz	a4,208e <__subdf3+0x5ca>
    2086:	000807b7          	lui	a5,0x80
    208a:	4701                	li	a4,0
    208c:	4481                	li	s1,0
    208e:	07b2                	slli	a5,a5,0xc
    2090:	7ff47413          	andi	s0,s0,2047
    2094:	0452                	slli	s0,s0,0x14
    2096:	83b1                	srli	a5,a5,0xc
    2098:	8fc1                	or	a5,a5,s0
    209a:	40c2                	lw	ra,16(sp)
    209c:	4432                	lw	s0,12(sp)
    209e:	04fe                	slli	s1,s1,0x1f
    20a0:	0097e6b3          	or	a3,a5,s1
    20a4:	853a                	mv	a0,a4
    20a6:	44a2                	lw	s1,8(sp)
    20a8:	85b6                	mv	a1,a3
    20aa:	0151                	addi	sp,sp,20
    20ac:	8082                	ret
    20ae:	87ae                	mv	a5,a1
    20b0:	871a                	mv	a4,t1
    20b2:	842a                	mv	s0,a0
    20b4:	b5a1                	j	1efc <__subdf3+0x438>
    20b6:	4781                	li	a5,0
    20b8:	4701                	li	a4,0
    20ba:	4481                	li	s1,0
    20bc:	bf61                	j	2054 <__subdf3+0x590>
    20be:	4781                	li	a5,0
    20c0:	bfed                	j	20ba <__subdf3+0x5f6>
    20c2:	4701                	li	a4,0
    20c4:	4481                	li	s1,0
    20c6:	004007b7          	lui	a5,0x400
    20ca:	b999                	j	1d20 <__subdf3+0x25c>
    20cc:	4781                	li	a5,0
    20ce:	4701                	li	a4,0
    20d0:	bf79                	j	206e <__subdf3+0x5aa>

000020d2 <__fixdfsi>:
    20d2:	0145d713          	srli	a4,a1,0x14
    20d6:	001006b7          	lui	a3,0x100
    20da:	fff68793          	addi	a5,a3,-1 # fffff <__ctor_end__+0xf96d7>
    20de:	7ff77713          	andi	a4,a4,2047
    20e2:	3fe00613          	li	a2,1022
    20e6:	8fed                	and	a5,a5,a1
    20e8:	81fd                	srli	a1,a1,0x1f
    20ea:	04e65463          	bge	a2,a4,2132 <__fixdfsi+0x60>
    20ee:	41d00613          	li	a2,1053
    20f2:	00e65863          	bge	a2,a4,2102 <__fixdfsi+0x30>
    20f6:	80000537          	lui	a0,0x80000
    20fa:	fff54513          	not	a0,a0
    20fe:	952e                	add	a0,a0,a1
    2100:	8082                	ret
    2102:	8fd5                	or	a5,a5,a3
    2104:	43300693          	li	a3,1075
    2108:	8e99                	sub	a3,a3,a4
    210a:	467d                	li	a2,31
    210c:	00d64d63          	blt	a2,a3,2126 <__fixdfsi+0x54>
    2110:	bed70713          	addi	a4,a4,-1043
    2114:	00e797b3          	sll	a5,a5,a4
    2118:	00d55533          	srl	a0,a0,a3
    211c:	8d5d                	or	a0,a0,a5
    211e:	c999                	beqz	a1,2134 <__fixdfsi+0x62>
    2120:	40a00533          	neg	a0,a0
    2124:	8082                	ret
    2126:	41300513          	li	a0,1043
    212a:	8d19                	sub	a0,a0,a4
    212c:	00a7d533          	srl	a0,a5,a0
    2130:	b7fd                	j	211e <__fixdfsi+0x4c>
    2132:	4501                	li	a0,0
    2134:	8082                	ret

00002136 <__floatsidf>:
    2136:	1151                	addi	sp,sp,-12
    2138:	c406                	sw	ra,8(sp)
    213a:	c222                	sw	s0,4(sp)
    213c:	c026                	sw	s1,0(sp)
    213e:	cd21                	beqz	a0,2196 <__floatsidf+0x60>
    2140:	41f55793          	srai	a5,a0,0x1f
    2144:	00a7c433          	xor	s0,a5,a0
    2148:	8c1d                	sub	s0,s0,a5
    214a:	01f55493          	srli	s1,a0,0x1f
    214e:	8522                	mv	a0,s0
    2150:	2881                	jal	21a0 <__clzsi2>
    2152:	41e00713          	li	a4,1054
    2156:	47a9                	li	a5,10
    2158:	8f09                	sub	a4,a4,a0
    215a:	02a7c963          	blt	a5,a0,218c <__floatsidf+0x56>
    215e:	47ad                	li	a5,11
    2160:	8f89                	sub	a5,a5,a0
    2162:	0555                	addi	a0,a0,21
    2164:	00f457b3          	srl	a5,s0,a5
    2168:	00a41433          	sll	s0,s0,a0
    216c:	07b2                	slli	a5,a5,0xc
    216e:	7ff77713          	andi	a4,a4,2047
    2172:	0752                	slli	a4,a4,0x14
    2174:	8522                	mv	a0,s0
    2176:	40a2                	lw	ra,8(sp)
    2178:	4412                	lw	s0,4(sp)
    217a:	83b1                	srli	a5,a5,0xc
    217c:	04fe                	slli	s1,s1,0x1f
    217e:	8fd9                	or	a5,a5,a4
    2180:	0097e733          	or	a4,a5,s1
    2184:	85ba                	mv	a1,a4
    2186:	4482                	lw	s1,0(sp)
    2188:	0131                	addi	sp,sp,12
    218a:	8082                	ret
    218c:	1555                	addi	a0,a0,-11
    218e:	00a417b3          	sll	a5,s0,a0
    2192:	4401                	li	s0,0
    2194:	bfe1                	j	216c <__floatsidf+0x36>
    2196:	4781                	li	a5,0
    2198:	4401                	li	s0,0
    219a:	4701                	li	a4,0
    219c:	4481                	li	s1,0
    219e:	b7f9                	j	216c <__floatsidf+0x36>

000021a0 <__clzsi2>:
    21a0:	67c1                	lui	a5,0x10
    21a2:	02f57663          	bgeu	a0,a5,21ce <__clzsi2+0x2e>
    21a6:	0ff00793          	li	a5,255
    21aa:	00a7b7b3          	sltu	a5,a5,a0
    21ae:	078e                	slli	a5,a5,0x3
    21b0:	02000713          	li	a4,32
    21b4:	8f1d                	sub	a4,a4,a5
    21b6:	00f557b3          	srl	a5,a0,a5
    21ba:	00003517          	auipc	a0,0x3
    21be:	5d650513          	addi	a0,a0,1494 # 5790 <__clz_tab>
    21c2:	97aa                	add	a5,a5,a0
    21c4:	0007c503          	lbu	a0,0(a5) # 10000 <__ctor_end__+0x96d8>
    21c8:	40a70533          	sub	a0,a4,a0
    21cc:	8082                	ret
    21ce:	01000737          	lui	a4,0x1000
    21d2:	47c1                	li	a5,16
    21d4:	fce56ee3          	bltu	a0,a4,21b0 <__clzsi2+0x10>
    21d8:	47e1                	li	a5,24
    21da:	bfd9                	j	21b0 <__clzsi2+0x10>

000021dc <memcpy>:
    21dc:	00a5c7b3          	xor	a5,a1,a0
    21e0:	8b8d                	andi	a5,a5,3
    21e2:	00c50733          	add	a4,a0,a2
    21e6:	e781                	bnez	a5,21ee <memcpy+0x12>
    21e8:	478d                	li	a5,3
    21ea:	02c7e963          	bltu	a5,a2,221c <memcpy+0x40>
    21ee:	87aa                	mv	a5,a0
    21f0:	0ce57163          	bgeu	a0,a4,22b2 <memcpy+0xd6>
    21f4:	0005c683          	lbu	a3,0(a1)
    21f8:	0785                	addi	a5,a5,1
    21fa:	0585                	addi	a1,a1,1
    21fc:	fed78fa3          	sb	a3,-1(a5)
    2200:	fee7eae3          	bltu	a5,a4,21f4 <memcpy+0x18>
    2204:	8082                	ret
    2206:	0005c683          	lbu	a3,0(a1)
    220a:	0785                	addi	a5,a5,1
    220c:	0585                	addi	a1,a1,1
    220e:	fed78fa3          	sb	a3,-1(a5)
    2212:	fee7eae3          	bltu	a5,a4,2206 <memcpy+0x2a>
    2216:	4402                	lw	s0,0(sp)
    2218:	0111                	addi	sp,sp,4
    221a:	8082                	ret
    221c:	00357693          	andi	a3,a0,3
    2220:	87aa                	mv	a5,a0
    2222:	ca91                	beqz	a3,2236 <memcpy+0x5a>
    2224:	0005c683          	lbu	a3,0(a1)
    2228:	0785                	addi	a5,a5,1
    222a:	0585                	addi	a1,a1,1
    222c:	fed78fa3          	sb	a3,-1(a5)
    2230:	0037f693          	andi	a3,a5,3
    2234:	b7fd                	j	2222 <memcpy+0x46>
    2236:	ffc77693          	andi	a3,a4,-4
    223a:	fe068613          	addi	a2,a3,-32
    223e:	06c7f563          	bgeu	a5,a2,22a8 <memcpy+0xcc>
    2242:	1171                	addi	sp,sp,-4
    2244:	c022                	sw	s0,0(sp)
    2246:	49c0                	lw	s0,20(a1)
    2248:	0005a303          	lw	t1,0(a1)
    224c:	0085a383          	lw	t2,8(a1)
    2250:	cbc0                	sw	s0,20(a5)
    2252:	4d80                	lw	s0,24(a1)
    2254:	0067a023          	sw	t1,0(a5)
    2258:	0045a303          	lw	t1,4(a1)
    225c:	cf80                	sw	s0,24(a5)
    225e:	4dc0                	lw	s0,28(a1)
    2260:	0067a223          	sw	t1,4(a5)
    2264:	00c5a283          	lw	t0,12(a1)
    2268:	0105a303          	lw	t1,16(a1)
    226c:	02458593          	addi	a1,a1,36
    2270:	cfc0                	sw	s0,28(a5)
    2272:	ffc5a403          	lw	s0,-4(a1)
    2276:	0077a423          	sw	t2,8(a5)
    227a:	0057a623          	sw	t0,12(a5)
    227e:	0067a823          	sw	t1,16(a5)
    2282:	02478793          	addi	a5,a5,36
    2286:	fe87ae23          	sw	s0,-4(a5)
    228a:	fac7eee3          	bltu	a5,a2,2246 <memcpy+0x6a>
    228e:	f8d7f2e3          	bgeu	a5,a3,2212 <memcpy+0x36>
    2292:	4190                	lw	a2,0(a1)
    2294:	0791                	addi	a5,a5,4
    2296:	0591                	addi	a1,a1,4
    2298:	fec7ae23          	sw	a2,-4(a5)
    229c:	bfcd                	j	228e <memcpy+0xb2>
    229e:	4190                	lw	a2,0(a1)
    22a0:	0791                	addi	a5,a5,4
    22a2:	0591                	addi	a1,a1,4
    22a4:	fec7ae23          	sw	a2,-4(a5)
    22a8:	fed7ebe3          	bltu	a5,a3,229e <memcpy+0xc2>
    22ac:	f4e7e4e3          	bltu	a5,a4,21f4 <memcpy+0x18>
    22b0:	8082                	ret
    22b2:	8082                	ret
	...

000022b6 <memmove>:
    22b6:	04a5f363          	bgeu	a1,a0,22fc <memmove+0x46>
    22ba:	00c586b3          	add	a3,a1,a2
    22be:	02d57f63          	bgeu	a0,a3,22fc <memmove+0x46>
    22c2:	fff64593          	not	a1,a2
    22c6:	4781                	li	a5,0
    22c8:	17fd                	addi	a5,a5,-1
    22ca:	00f59363          	bne	a1,a5,22d0 <memmove+0x1a>
    22ce:	8082                	ret
    22d0:	00f68733          	add	a4,a3,a5
    22d4:	00074303          	lbu	t1,0(a4) # 1000000 <__ctor_end__+0xff96d8>
    22d8:	00c78733          	add	a4,a5,a2
    22dc:	972a                	add	a4,a4,a0
    22de:	00670023          	sb	t1,0(a4)
    22e2:	b7dd                	j	22c8 <memmove+0x12>
    22e4:	00f58733          	add	a4,a1,a5
    22e8:	00074683          	lbu	a3,0(a4)
    22ec:	00f50733          	add	a4,a0,a5
    22f0:	0785                	addi	a5,a5,1
    22f2:	00d70023          	sb	a3,0(a4)
    22f6:	fef617e3          	bne	a2,a5,22e4 <memmove+0x2e>
    22fa:	8082                	ret
    22fc:	4781                	li	a5,0
    22fe:	bfe5                	j	22f6 <memmove+0x40>

00002300 <memset>:
    2300:	433d                	li	t1,15
    2302:	872a                	mv	a4,a0
    2304:	02c37363          	bgeu	t1,a2,232a <memset+0x2a>
    2308:	00f77793          	andi	a5,a4,15
    230c:	efbd                	bnez	a5,238a <memset+0x8a>
    230e:	e5ad                	bnez	a1,2378 <memset+0x78>
    2310:	ff067693          	andi	a3,a2,-16
    2314:	8a3d                	andi	a2,a2,15
    2316:	96ba                	add	a3,a3,a4
    2318:	c30c                	sw	a1,0(a4)
    231a:	c34c                	sw	a1,4(a4)
    231c:	c70c                	sw	a1,8(a4)
    231e:	c74c                	sw	a1,12(a4)
    2320:	0741                	addi	a4,a4,16
    2322:	fed76be3          	bltu	a4,a3,2318 <memset+0x18>
    2326:	e211                	bnez	a2,232a <memset+0x2a>
    2328:	8082                	ret
    232a:	40c306b3          	sub	a3,t1,a2
    232e:	068a                	slli	a3,a3,0x2
    2330:	00000297          	auipc	t0,0x0
    2334:	9696                	add	a3,a3,t0
    2336:	00a68067          	jr	10(a3)
    233a:	00b70723          	sb	a1,14(a4)
    233e:	00b706a3          	sb	a1,13(a4)
    2342:	00b70623          	sb	a1,12(a4)
    2346:	00b705a3          	sb	a1,11(a4)
    234a:	00b70523          	sb	a1,10(a4)
    234e:	00b704a3          	sb	a1,9(a4)
    2352:	00b70423          	sb	a1,8(a4)
    2356:	00b703a3          	sb	a1,7(a4)
    235a:	00b70323          	sb	a1,6(a4)
    235e:	00b702a3          	sb	a1,5(a4)
    2362:	00b70223          	sb	a1,4(a4)
    2366:	00b701a3          	sb	a1,3(a4)
    236a:	00b70123          	sb	a1,2(a4)
    236e:	00b700a3          	sb	a1,1(a4)
    2372:	00b70023          	sb	a1,0(a4)
    2376:	8082                	ret
    2378:	0ff5f593          	andi	a1,a1,255
    237c:	00859693          	slli	a3,a1,0x8
    2380:	8dd5                	or	a1,a1,a3
    2382:	01059693          	slli	a3,a1,0x10
    2386:	8dd5                	or	a1,a1,a3
    2388:	b761                	j	2310 <memset+0x10>
    238a:	00279693          	slli	a3,a5,0x2
    238e:	00000297          	auipc	t0,0x0
    2392:	9696                	add	a3,a3,t0
    2394:	8286                	mv	t0,ra
    2396:	fa8680e7          	jalr	-88(a3)
    239a:	8096                	mv	ra,t0
    239c:	17c1                	addi	a5,a5,-16
    239e:	8f1d                	sub	a4,a4,a5
    23a0:	963e                	add	a2,a2,a5
    23a2:	f8c374e3          	bgeu	t1,a2,232a <memset+0x2a>
    23a6:	b7a5                	j	230e <memset+0xe>
	...

000023aa <strchr>:
    23aa:	0ff5f593          	andi	a1,a1,255
    23ae:	00054783          	lbu	a5,0(a0)
    23b2:	c791                	beqz	a5,23be <strchr+0x14>
    23b4:	00b79363          	bne	a5,a1,23ba <strchr+0x10>
    23b8:	8082                	ret
    23ba:	0505                	addi	a0,a0,1
    23bc:	bfcd                	j	23ae <strchr+0x4>
    23be:	c191                	beqz	a1,23c2 <strchr+0x18>
    23c0:	4501                	li	a0,0
    23c2:	8082                	ret

000023c4 <strcpy>:
    23c4:	87aa                	mv	a5,a0
    23c6:	0005c703          	lbu	a4,0(a1)
    23ca:	0785                	addi	a5,a5,1
    23cc:	0585                	addi	a1,a1,1
    23ce:	fee78fa3          	sb	a4,-1(a5)
    23d2:	fb75                	bnez	a4,23c6 <strcpy+0x2>
    23d4:	8082                	ret

000023d6 <_strerror_r>:
    23d6:	87ae                	mv	a5,a1
    23d8:	08e00713          	li	a4,142
    23dc:	85b2                	mv	a1,a2
    23de:	32f76763          	bltu	a4,a5,270c <_strerror_r+0x336>
    23e2:	00003317          	auipc	t1,0x3
    23e6:	4ae30313          	addi	t1,t1,1198 # 5890 <__clz_tab+0x100>
    23ea:	00279613          	slli	a2,a5,0x2
    23ee:	961a                	add	a2,a2,t1
    23f0:	4218                	lw	a4,0(a2)
    23f2:	971a                	add	a4,a4,t1
    23f4:	8702                	jr	a4
    23f6:	00004517          	auipc	a0,0x4
    23fa:	e2250513          	addi	a0,a0,-478 # 6218 <pad_line+0x20>
    23fe:	8082                	ret
    2400:	00004517          	auipc	a0,0x4
    2404:	e2450513          	addi	a0,a0,-476 # 6224 <pad_line+0x2c>
    2408:	8082                	ret
    240a:	00004517          	auipc	a0,0x4
    240e:	e3650513          	addi	a0,a0,-458 # 6240 <pad_line+0x48>
    2412:	8082                	ret
    2414:	00004517          	auipc	a0,0x4
    2418:	e3c50513          	addi	a0,a0,-452 # 6250 <pad_line+0x58>
    241c:	8082                	ret
    241e:	00004517          	auipc	a0,0x4
    2422:	e4a50513          	addi	a0,a0,-438 # 6268 <pad_line+0x70>
    2426:	8082                	ret
    2428:	00004517          	auipc	a0,0x4
    242c:	e4c50513          	addi	a0,a0,-436 # 6274 <pad_line+0x7c>
    2430:	8082                	ret
    2432:	00004517          	auipc	a0,0x4
    2436:	e5e50513          	addi	a0,a0,-418 # 6290 <pad_line+0x98>
    243a:	8082                	ret
    243c:	00004517          	auipc	a0,0x4
    2440:	e6850513          	addi	a0,a0,-408 # 62a4 <pad_line+0xac>
    2444:	8082                	ret
    2446:	00004517          	auipc	a0,0x4
    244a:	e7250513          	addi	a0,a0,-398 # 62b8 <pad_line+0xc0>
    244e:	8082                	ret
    2450:	00004517          	auipc	a0,0x4
    2454:	e8450513          	addi	a0,a0,-380 # 62d4 <pad_line+0xdc>
    2458:	8082                	ret
    245a:	00004517          	auipc	a0,0x4
    245e:	e8a50513          	addi	a0,a0,-374 # 62e4 <pad_line+0xec>
    2462:	8082                	ret
    2464:	00004517          	auipc	a0,0x4
    2468:	e8c50513          	addi	a0,a0,-372 # 62f0 <pad_line+0xf8>
    246c:	8082                	ret
    246e:	00004517          	auipc	a0,0x4
    2472:	ea250513          	addi	a0,a0,-350 # 6310 <pad_line+0x118>
    2476:	8082                	ret
    2478:	00004517          	auipc	a0,0x4
    247c:	eac50513          	addi	a0,a0,-340 # 6324 <pad_line+0x12c>
    2480:	8082                	ret
    2482:	00004517          	auipc	a0,0x4
    2486:	eb650513          	addi	a0,a0,-330 # 6338 <pad_line+0x140>
    248a:	8082                	ret
    248c:	00004517          	auipc	a0,0x4
    2490:	ec050513          	addi	a0,a0,-320 # 634c <pad_line+0x154>
    2494:	8082                	ret
    2496:	00004517          	auipc	a0,0x4
    249a:	ec250513          	addi	a0,a0,-318 # 6358 <pad_line+0x160>
    249e:	8082                	ret
    24a0:	00004517          	auipc	a0,0x4
    24a4:	ed050513          	addi	a0,a0,-304 # 6370 <pad_line+0x178>
    24a8:	8082                	ret
    24aa:	00004517          	auipc	a0,0x4
    24ae:	ed250513          	addi	a0,a0,-302 # 637c <pad_line+0x184>
    24b2:	8082                	ret
    24b4:	00004517          	auipc	a0,0x4
    24b8:	edc50513          	addi	a0,a0,-292 # 6390 <pad_line+0x198>
    24bc:	8082                	ret
    24be:	00004517          	auipc	a0,0x4
    24c2:	ee250513          	addi	a0,a0,-286 # 63a0 <pad_line+0x1a8>
    24c6:	8082                	ret
    24c8:	00004517          	auipc	a0,0x4
    24cc:	ee850513          	addi	a0,a0,-280 # 63b0 <pad_line+0x1b8>
    24d0:	8082                	ret
    24d2:	00004517          	auipc	a0,0x4
    24d6:	eee50513          	addi	a0,a0,-274 # 63c0 <pad_line+0x1c8>
    24da:	8082                	ret
    24dc:	00004517          	auipc	a0,0x4
    24e0:	f0450513          	addi	a0,a0,-252 # 63e0 <pad_line+0x1e8>
    24e4:	8082                	ret
    24e6:	00004517          	auipc	a0,0x4
    24ea:	f0a50513          	addi	a0,a0,-246 # 63f0 <pad_line+0x1f8>
    24ee:	8082                	ret
    24f0:	00004517          	auipc	a0,0x4
    24f4:	f1450513          	addi	a0,a0,-236 # 6404 <pad_line+0x20c>
    24f8:	8082                	ret
    24fa:	00004517          	auipc	a0,0x4
    24fe:	f2e50513          	addi	a0,a0,-210 # 6428 <pad_line+0x230>
    2502:	8082                	ret
    2504:	00004517          	auipc	a0,0x4
    2508:	f4450513          	addi	a0,a0,-188 # 6448 <pad_line+0x250>
    250c:	8082                	ret
    250e:	00004517          	auipc	a0,0x4
    2512:	f5a50513          	addi	a0,a0,-166 # 6468 <pad_line+0x270>
    2516:	8082                	ret
    2518:	00004517          	auipc	a0,0x4
    251c:	f7050513          	addi	a0,a0,-144 # 6488 <pad_line+0x290>
    2520:	8082                	ret
    2522:	00004517          	auipc	a0,0x4
    2526:	f7e50513          	addi	a0,a0,-130 # 64a0 <pad_line+0x2a8>
    252a:	8082                	ret
    252c:	00004517          	auipc	a0,0x4
    2530:	f8450513          	addi	a0,a0,-124 # 64b0 <pad_line+0x2b8>
    2534:	8082                	ret
    2536:	00004517          	auipc	a0,0x4
    253a:	f8a50513          	addi	a0,a0,-118 # 64c0 <pad_line+0x2c8>
    253e:	8082                	ret
    2540:	00004517          	auipc	a0,0x4
    2544:	f9450513          	addi	a0,a0,-108 # 64d4 <pad_line+0x2dc>
    2548:	8082                	ret
    254a:	00004517          	auipc	a0,0x4
    254e:	fa250513          	addi	a0,a0,-94 # 64ec <pad_line+0x2f4>
    2552:	8082                	ret
    2554:	00004517          	auipc	a0,0x4
    2558:	fa850513          	addi	a0,a0,-88 # 64fc <pad_line+0x304>
    255c:	8082                	ret
    255e:	00004517          	auipc	a0,0x4
    2562:	fae50513          	addi	a0,a0,-82 # 650c <pad_line+0x314>
    2566:	8082                	ret
    2568:	00004517          	auipc	a0,0x4
    256c:	fbc50513          	addi	a0,a0,-68 # 6524 <pad_line+0x32c>
    2570:	8082                	ret
    2572:	00004517          	auipc	a0,0x4
    2576:	fc250513          	addi	a0,a0,-62 # 6534 <pad_line+0x33c>
    257a:	8082                	ret
    257c:	00004517          	auipc	a0,0x4
    2580:	fc450513          	addi	a0,a0,-60 # 6540 <pad_line+0x348>
    2584:	8082                	ret
    2586:	00004517          	auipc	a0,0x4
    258a:	fea50513          	addi	a0,a0,-22 # 6570 <pad_line+0x378>
    258e:	8082                	ret
    2590:	00004517          	auipc	a0,0x4
    2594:	ff450513          	addi	a0,a0,-12 # 6584 <pad_line+0x38c>
    2598:	8082                	ret
    259a:	00004517          	auipc	a0,0x4
    259e:	00650513          	addi	a0,a0,6 # 65a0 <pad_line+0x3a8>
    25a2:	8082                	ret
    25a4:	00004517          	auipc	a0,0x4
    25a8:	01050513          	addi	a0,a0,16 # 65b4 <pad_line+0x3bc>
    25ac:	8082                	ret
    25ae:	00004517          	auipc	a0,0x4
    25b2:	01e50513          	addi	a0,a0,30 # 65cc <pad_line+0x3d4>
    25b6:	8082                	ret
    25b8:	00004517          	auipc	a0,0x4
    25bc:	02050513          	addi	a0,a0,32 # 65d8 <pad_line+0x3e0>
    25c0:	8082                	ret
    25c2:	00004517          	auipc	a0,0x4
    25c6:	02e50513          	addi	a0,a0,46 # 65f0 <pad_line+0x3f8>
    25ca:	8082                	ret
    25cc:	00004517          	auipc	a0,0x4
    25d0:	02c50513          	addi	a0,a0,44 # 65f8 <pad_line+0x400>
    25d4:	8082                	ret
    25d6:	00004517          	auipc	a0,0x4
    25da:	03250513          	addi	a0,a0,50 # 6608 <pad_line+0x410>
    25de:	8082                	ret
    25e0:	00004517          	auipc	a0,0x4
    25e4:	04050513          	addi	a0,a0,64 # 6620 <pad_line+0x428>
    25e8:	8082                	ret
    25ea:	00004517          	auipc	a0,0x4
    25ee:	04a50513          	addi	a0,a0,74 # 6634 <pad_line+0x43c>
    25f2:	8082                	ret
    25f4:	00004517          	auipc	a0,0x4
    25f8:	05850513          	addi	a0,a0,88 # 664c <pad_line+0x454>
    25fc:	8082                	ret
    25fe:	00004517          	auipc	a0,0x4
    2602:	05e50513          	addi	a0,a0,94 # 665c <pad_line+0x464>
    2606:	8082                	ret
    2608:	00004517          	auipc	a0,0x4
    260c:	06850513          	addi	a0,a0,104 # 6670 <pad_line+0x478>
    2610:	8082                	ret
    2612:	00004517          	auipc	a0,0x4
    2616:	07250513          	addi	a0,a0,114 # 6684 <pad_line+0x48c>
    261a:	8082                	ret
    261c:	00004517          	auipc	a0,0x4
    2620:	07450513          	addi	a0,a0,116 # 6690 <pad_line+0x498>
    2624:	8082                	ret
    2626:	00004517          	auipc	a0,0x4
    262a:	08650513          	addi	a0,a0,134 # 66ac <pad_line+0x4b4>
    262e:	8082                	ret
    2630:	00004517          	auipc	a0,0x4
    2634:	09050513          	addi	a0,a0,144 # 66c0 <pad_line+0x4c8>
    2638:	8082                	ret
    263a:	00004517          	auipc	a0,0x4
    263e:	0a250513          	addi	a0,a0,162 # 66dc <pad_line+0x4e4>
    2642:	8082                	ret
    2644:	00004517          	auipc	a0,0x4
    2648:	0b050513          	addi	a0,a0,176 # 66f4 <pad_line+0x4fc>
    264c:	8082                	ret
    264e:	00004517          	auipc	a0,0x4
    2652:	0c250513          	addi	a0,a0,194 # 6710 <pad_line+0x518>
    2656:	8082                	ret
    2658:	00004517          	auipc	a0,0x4
    265c:	0c050513          	addi	a0,a0,192 # 6718 <pad_line+0x520>
    2660:	8082                	ret
    2662:	00004517          	auipc	a0,0x4
    2666:	0e650513          	addi	a0,a0,230 # 6748 <pad_line+0x550>
    266a:	8082                	ret
    266c:	00004517          	auipc	a0,0x4
    2670:	0fc50513          	addi	a0,a0,252 # 6768 <pad_line+0x570>
    2674:	8082                	ret
    2676:	00004517          	auipc	a0,0x4
    267a:	11250513          	addi	a0,a0,274 # 6788 <pad_line+0x590>
    267e:	8082                	ret
    2680:	00004517          	auipc	a0,0x4
    2684:	12050513          	addi	a0,a0,288 # 67a0 <pad_line+0x5a8>
    2688:	8082                	ret
    268a:	00004517          	auipc	a0,0x4
    268e:	12a50513          	addi	a0,a0,298 # 67b4 <pad_line+0x5bc>
    2692:	8082                	ret
    2694:	00004517          	auipc	a0,0x4
    2698:	13c50513          	addi	a0,a0,316 # 67d0 <pad_line+0x5d8>
    269c:	8082                	ret
    269e:	00004517          	auipc	a0,0x4
    26a2:	14a50513          	addi	a0,a0,330 # 67e8 <pad_line+0x5f0>
    26a6:	8082                	ret
    26a8:	00004517          	auipc	a0,0x4
    26ac:	15850513          	addi	a0,a0,344 # 6800 <pad_line+0x608>
    26b0:	8082                	ret
    26b2:	00004517          	auipc	a0,0x4
    26b6:	17250513          	addi	a0,a0,370 # 6824 <pad_line+0x62c>
    26ba:	8082                	ret
    26bc:	00004517          	auipc	a0,0x4
    26c0:	18050513          	addi	a0,a0,384 # 683c <pad_line+0x644>
    26c4:	8082                	ret
    26c6:	00004517          	auipc	a0,0x4
    26ca:	19250513          	addi	a0,a0,402 # 6858 <pad_line+0x660>
    26ce:	8082                	ret
    26d0:	00004517          	auipc	a0,0x4
    26d4:	19c50513          	addi	a0,a0,412 # 686c <pad_line+0x674>
    26d8:	8082                	ret
    26da:	00004517          	auipc	a0,0x4
    26de:	1aa50513          	addi	a0,a0,426 # 6884 <pad_line+0x68c>
    26e2:	8082                	ret
    26e4:	00004517          	auipc	a0,0x4
    26e8:	1b450513          	addi	a0,a0,436 # 6898 <pad_line+0x6a0>
    26ec:	8082                	ret
    26ee:	00004517          	auipc	a0,0x4
    26f2:	1ce50513          	addi	a0,a0,462 # 68bc <pad_line+0x6c4>
    26f6:	8082                	ret
    26f8:	00004517          	auipc	a0,0x4
    26fc:	1ec50513          	addi	a0,a0,492 # 68e4 <pad_line+0x6ec>
    2700:	8082                	ret
    2702:	00004517          	auipc	a0,0x4
    2706:	1f650513          	addi	a0,a0,502 # 68f8 <pad_line+0x700>
    270a:	8082                	ret
    270c:	1151                	addi	sp,sp,-12
    270e:	c406                	sw	ra,8(sp)
    2710:	e291                	bnez	a3,2714 <_strerror_r+0x33e>
    2712:	86aa                	mv	a3,a0
    2714:	8636                	mv	a2,a3
    2716:	853e                	mv	a0,a5
    2718:	2e25                	jal	2a50 <_user_strerror>
    271a:	e509                	bnez	a0,2724 <_strerror_r+0x34e>
    271c:	00004517          	auipc	a0,0x4
    2720:	99050513          	addi	a0,a0,-1648 # 60ac <sg_usi_config+0x41c>
    2724:	40a2                	lw	ra,8(sp)
    2726:	0131                	addi	sp,sp,12
    2728:	8082                	ret
    272a:	00004517          	auipc	a0,0x4
    272e:	1e650513          	addi	a0,a0,486 # 6910 <pad_line+0x718>
    2732:	8082                	ret

00002734 <strerror>:
    2734:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2738:	85aa                	mv	a1,a0
    273a:	4388                	lw	a0,0(a5)
    273c:	4681                	li	a3,0
    273e:	4601                	li	a2,0
    2740:	b959                	j	23d6 <_strerror_r>

00002742 <strerror_l>:
    2742:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2746:	85aa                	mv	a1,a0
    2748:	4388                	lw	a0,0(a5)
    274a:	4681                	li	a3,0
    274c:	4601                	li	a2,0
    274e:	b161                	j	23d6 <_strerror_r>

00002750 <strlen>:
    2750:	87aa                	mv	a5,a0
    2752:	0785                	addi	a5,a5,1
    2754:	fff7c703          	lbu	a4,-1(a5)
    2758:	ff6d                	bnez	a4,2752 <strlen+0x2>
    275a:	40a78533          	sub	a0,a5,a0
    275e:	157d                	addi	a0,a0,-1
    2760:	8082                	ret

00002762 <_strtol_l.isra.0>:
    2762:	fdc10113          	addi	sp,sp,-36
    2766:	cc26                	sw	s1,24(sp)
    2768:	d006                	sw	ra,32(sp)
    276a:	ce22                	sw	s0,28(sp)
    276c:	c42a                	sw	a0,8(sp)
    276e:	84ae                	mv	s1,a1
    2770:	00148413          	addi	s0,s1,1
    2774:	fff44783          	lbu	a5,-1(s0)
    2778:	853a                	mv	a0,a4
    277a:	ca36                	sw	a3,20(sp)
    277c:	c832                	sw	a2,16(sp)
    277e:	c62e                	sw	a1,12(sp)
    2780:	c23e                	sw	a5,4(sp)
    2782:	c03a                	sw	a4,0(sp)
    2784:	2e1d                	jal	2aba <__locale_ctype_ptr_l>
    2786:	4792                	lw	a5,4(sp)
    2788:	4702                	lw	a4,0(sp)
    278a:	45b2                	lw	a1,12(sp)
    278c:	953e                	add	a0,a0,a5
    278e:	00154503          	lbu	a0,1(a0)
    2792:	4642                	lw	a2,16(sp)
    2794:	46d2                	lw	a3,20(sp)
    2796:	8921                	andi	a0,a0,8
    2798:	ed51                	bnez	a0,2834 <_strtol_l.isra.0+0xd2>
    279a:	02d00713          	li	a4,45
    279e:	08e79d63          	bne	a5,a4,2838 <_strtol_l.isra.0+0xd6>
    27a2:	00044783          	lbu	a5,0(s0)
    27a6:	4385                	li	t2,1
    27a8:	00248413          	addi	s0,s1,2
    27ac:	cae5                	beqz	a3,289c <_strtol_l.isra.0+0x13a>
    27ae:	4741                	li	a4,16
    27b0:	02e69263          	bne	a3,a4,27d4 <_strtol_l.isra.0+0x72>
    27b4:	03000713          	li	a4,48
    27b8:	00e79e63          	bne	a5,a4,27d4 <_strtol_l.isra.0+0x72>
    27bc:	00044783          	lbu	a5,0(s0)
    27c0:	05800713          	li	a4,88
    27c4:	0df7f793          	andi	a5,a5,223
    27c8:	0ce79563          	bne	a5,a4,2892 <_strtol_l.isra.0+0x130>
    27cc:	00144783          	lbu	a5,1(s0)
    27d0:	46c1                	li	a3,16
    27d2:	0409                	addi	s0,s0,2
    27d4:	800002b7          	lui	t0,0x80000
    27d8:	00039463          	bnez	t2,27e0 <_strtol_l.isra.0+0x7e>
    27dc:	fff2c293          	not	t0,t0
    27e0:	02d2f733          	remu	a4,t0,a3
    27e4:	4301                	li	t1,0
    27e6:	4501                	li	a0,0
    27e8:	c23a                	sw	a4,4(sp)
    27ea:	02d2d733          	divu	a4,t0,a3
    27ee:	c03a                	sw	a4,0(sp)
    27f0:	fd078713          	addi	a4,a5,-48
    27f4:	44a5                	li	s1,9
    27f6:	00e4f963          	bgeu	s1,a4,2808 <_strtol_l.isra.0+0xa6>
    27fa:	fbf78713          	addi	a4,a5,-65
    27fe:	44e5                	li	s1,25
    2800:	04e4e763          	bltu	s1,a4,284e <_strtol_l.isra.0+0xec>
    2804:	fc978713          	addi	a4,a5,-55
    2808:	04d75b63          	bge	a4,a3,285e <_strtol_l.isra.0+0xfc>
    280c:	57fd                	li	a5,-1
    280e:	00f30f63          	beq	t1,a5,282c <_strtol_l.isra.0+0xca>
    2812:	4782                	lw	a5,0(sp)
    2814:	537d                	li	t1,-1
    2816:	00a7eb63          	bltu	a5,a0,282c <_strtol_l.isra.0+0xca>
    281a:	00a79563          	bne	a5,a0,2824 <_strtol_l.isra.0+0xc2>
    281e:	4792                	lw	a5,4(sp)
    2820:	00e7c663          	blt	a5,a4,282c <_strtol_l.isra.0+0xca>
    2824:	02a68533          	mul	a0,a3,a0
    2828:	4305                	li	t1,1
    282a:	953a                	add	a0,a0,a4
    282c:	0405                	addi	s0,s0,1
    282e:	fff44783          	lbu	a5,-1(s0)
    2832:	bf7d                	j	27f0 <_strtol_l.isra.0+0x8e>
    2834:	84a2                	mv	s1,s0
    2836:	bf2d                	j	2770 <_strtol_l.isra.0+0xe>
    2838:	02b00713          	li	a4,43
    283c:	00e78463          	beq	a5,a4,2844 <_strtol_l.isra.0+0xe2>
    2840:	4381                	li	t2,0
    2842:	b7ad                	j	27ac <_strtol_l.isra.0+0x4a>
    2844:	00044783          	lbu	a5,0(s0)
    2848:	00248413          	addi	s0,s1,2
    284c:	bfd5                	j	2840 <_strtol_l.isra.0+0xde>
    284e:	f9f78713          	addi	a4,a5,-97
    2852:	44e5                	li	s1,25
    2854:	00e4e563          	bltu	s1,a4,285e <_strtol_l.isra.0+0xfc>
    2858:	fa978713          	addi	a4,a5,-87
    285c:	b775                	j	2808 <_strtol_l.isra.0+0xa6>
    285e:	57fd                	li	a5,-1
    2860:	00f31e63          	bne	t1,a5,287c <_strtol_l.isra.0+0x11a>
    2864:	4722                	lw	a4,8(sp)
    2866:	02200793          	li	a5,34
    286a:	8516                	mv	a0,t0
    286c:	c31c                	sw	a5,0(a4)
    286e:	ee11                	bnez	a2,288a <_strtol_l.isra.0+0x128>
    2870:	5082                	lw	ra,32(sp)
    2872:	4472                	lw	s0,28(sp)
    2874:	44e2                	lw	s1,24(sp)
    2876:	02410113          	addi	sp,sp,36
    287a:	8082                	ret
    287c:	00038463          	beqz	t2,2884 <_strtol_l.isra.0+0x122>
    2880:	40a00533          	neg	a0,a0
    2884:	d675                	beqz	a2,2870 <_strtol_l.isra.0+0x10e>
    2886:	00030463          	beqz	t1,288e <_strtol_l.isra.0+0x12c>
    288a:	fff40593          	addi	a1,s0,-1
    288e:	c20c                	sw	a1,0(a2)
    2890:	b7c5                	j	2870 <_strtol_l.isra.0+0x10e>
    2892:	03000793          	li	a5,48
    2896:	fe9d                	bnez	a3,27d4 <_strtol_l.isra.0+0x72>
    2898:	46a1                	li	a3,8
    289a:	bf2d                	j	27d4 <_strtol_l.isra.0+0x72>
    289c:	03000713          	li	a4,48
    28a0:	f0e78ee3          	beq	a5,a4,27bc <_strtol_l.isra.0+0x5a>
    28a4:	46a9                	li	a3,10
    28a6:	b73d                	j	27d4 <_strtol_l.isra.0+0x72>

000028a8 <_strtol_r>:
    28a8:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    28ac:	439c                	lw	a5,0(a5)
    28ae:	5bd8                	lw	a4,52(a5)
    28b0:	e319                	bnez	a4,28b6 <_strtol_r+0xe>
    28b2:	e7c18713          	addi	a4,gp,-388 # 200004f0 <__global_locale>
    28b6:	b575                	j	2762 <_strtol_l.isra.0>

000028b8 <strtol_l>:
    28b8:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    28bc:	8736                	mv	a4,a3
    28be:	86b2                	mv	a3,a2
    28c0:	862e                	mv	a2,a1
    28c2:	85aa                	mv	a1,a0
    28c4:	4388                	lw	a0,0(a5)
    28c6:	bd71                	j	2762 <_strtol_l.isra.0>

000028c8 <strtol>:
    28c8:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    28cc:	439c                	lw	a5,0(a5)
    28ce:	86b2                	mv	a3,a2
    28d0:	5bd8                	lw	a4,52(a5)
    28d2:	e319                	bnez	a4,28d8 <strtol+0x10>
    28d4:	e7c18713          	addi	a4,gp,-388 # 200004f0 <__global_locale>
    28d8:	862e                	mv	a2,a1
    28da:	85aa                	mv	a1,a0
    28dc:	853e                	mv	a0,a5
    28de:	b551                	j	2762 <_strtol_l.isra.0>

000028e0 <_strtoul_l.isra.0>:
    28e0:	fdc10113          	addi	sp,sp,-36
    28e4:	cc26                	sw	s1,24(sp)
    28e6:	d006                	sw	ra,32(sp)
    28e8:	ce22                	sw	s0,28(sp)
    28ea:	c22a                	sw	a0,4(sp)
    28ec:	84ae                	mv	s1,a1
    28ee:	00148413          	addi	s0,s1,1
    28f2:	fff44783          	lbu	a5,-1(s0)
    28f6:	853a                	mv	a0,a4
    28f8:	ca36                	sw	a3,20(sp)
    28fa:	c832                	sw	a2,16(sp)
    28fc:	c62e                	sw	a1,12(sp)
    28fe:	c43e                	sw	a5,8(sp)
    2900:	c03a                	sw	a4,0(sp)
    2902:	2a65                	jal	2aba <__locale_ctype_ptr_l>
    2904:	47a2                	lw	a5,8(sp)
    2906:	4702                	lw	a4,0(sp)
    2908:	45b2                	lw	a1,12(sp)
    290a:	953e                	add	a0,a0,a5
    290c:	00154503          	lbu	a0,1(a0)
    2910:	4642                	lw	a2,16(sp)
    2912:	46d2                	lw	a3,20(sp)
    2914:	8921                	andi	a0,a0,8
    2916:	e551                	bnez	a0,29a2 <_strtoul_l.isra.0+0xc2>
    2918:	02d00713          	li	a4,45
    291c:	08e79563          	bne	a5,a4,29a6 <_strtoul_l.isra.0+0xc6>
    2920:	00044783          	lbu	a5,0(s0)
    2924:	4285                	li	t0,1
    2926:	00248413          	addi	s0,s1,2
    292a:	c2ed                	beqz	a3,2a0c <_strtoul_l.isra.0+0x12c>
    292c:	4741                	li	a4,16
    292e:	02e69263          	bne	a3,a4,2952 <_strtoul_l.isra.0+0x72>
    2932:	03000713          	li	a4,48
    2936:	00e79e63          	bne	a5,a4,2952 <_strtoul_l.isra.0+0x72>
    293a:	00044783          	lbu	a5,0(s0)
    293e:	05800713          	li	a4,88
    2942:	0df7f793          	andi	a5,a5,223
    2946:	0ae79e63          	bne	a5,a4,2a02 <_strtoul_l.isra.0+0x122>
    294a:	00144783          	lbu	a5,1(s0)
    294e:	46c1                	li	a3,16
    2950:	0409                	addi	s0,s0,2
    2952:	577d                	li	a4,-1
    2954:	02d754b3          	divu	s1,a4,a3
    2958:	4301                	li	t1,0
    295a:	4501                	li	a0,0
    295c:	02d77733          	remu	a4,a4,a3
    2960:	c03a                	sw	a4,0(sp)
    2962:	fd078713          	addi	a4,a5,-48
    2966:	43a5                	li	t2,9
    2968:	00e3f963          	bgeu	t2,a4,297a <_strtoul_l.isra.0+0x9a>
    296c:	fbf78713          	addi	a4,a5,-65
    2970:	43e5                	li	t2,25
    2972:	04e3e563          	bltu	t2,a4,29bc <_strtoul_l.isra.0+0xdc>
    2976:	fc978713          	addi	a4,a5,-55
    297a:	04d75b63          	bge	a4,a3,29d0 <_strtoul_l.isra.0+0xf0>
    297e:	04034763          	bltz	t1,29cc <_strtoul_l.isra.0+0xec>
    2982:	537d                	li	t1,-1
    2984:	00a4eb63          	bltu	s1,a0,299a <_strtoul_l.isra.0+0xba>
    2988:	00a49563          	bne	s1,a0,2992 <_strtoul_l.isra.0+0xb2>
    298c:	4782                	lw	a5,0(sp)
    298e:	00e7c663          	blt	a5,a4,299a <_strtoul_l.isra.0+0xba>
    2992:	02a68533          	mul	a0,a3,a0
    2996:	4305                	li	t1,1
    2998:	953a                	add	a0,a0,a4
    299a:	0405                	addi	s0,s0,1
    299c:	fff44783          	lbu	a5,-1(s0)
    29a0:	b7c9                	j	2962 <_strtoul_l.isra.0+0x82>
    29a2:	84a2                	mv	s1,s0
    29a4:	b7a9                	j	28ee <_strtoul_l.isra.0+0xe>
    29a6:	02b00713          	li	a4,43
    29aa:	00e78463          	beq	a5,a4,29b2 <_strtoul_l.isra.0+0xd2>
    29ae:	4281                	li	t0,0
    29b0:	bfad                	j	292a <_strtoul_l.isra.0+0x4a>
    29b2:	00044783          	lbu	a5,0(s0)
    29b6:	00248413          	addi	s0,s1,2
    29ba:	bfd5                	j	29ae <_strtoul_l.isra.0+0xce>
    29bc:	f9f78713          	addi	a4,a5,-97
    29c0:	43e5                	li	t2,25
    29c2:	00e3e763          	bltu	t2,a4,29d0 <_strtoul_l.isra.0+0xf0>
    29c6:	fa978713          	addi	a4,a5,-87
    29ca:	bf45                	j	297a <_strtoul_l.isra.0+0x9a>
    29cc:	537d                	li	t1,-1
    29ce:	b7f1                	j	299a <_strtoul_l.isra.0+0xba>
    29d0:	00035e63          	bgez	t1,29ec <_strtoul_l.isra.0+0x10c>
    29d4:	4712                	lw	a4,4(sp)
    29d6:	02200793          	li	a5,34
    29da:	557d                	li	a0,-1
    29dc:	c31c                	sw	a5,0(a4)
    29de:	ee11                	bnez	a2,29fa <_strtoul_l.isra.0+0x11a>
    29e0:	5082                	lw	ra,32(sp)
    29e2:	4472                	lw	s0,28(sp)
    29e4:	44e2                	lw	s1,24(sp)
    29e6:	02410113          	addi	sp,sp,36
    29ea:	8082                	ret
    29ec:	00028463          	beqz	t0,29f4 <_strtoul_l.isra.0+0x114>
    29f0:	40a00533          	neg	a0,a0
    29f4:	d675                	beqz	a2,29e0 <_strtoul_l.isra.0+0x100>
    29f6:	00030463          	beqz	t1,29fe <_strtoul_l.isra.0+0x11e>
    29fa:	fff40593          	addi	a1,s0,-1
    29fe:	c20c                	sw	a1,0(a2)
    2a00:	b7c5                	j	29e0 <_strtoul_l.isra.0+0x100>
    2a02:	03000793          	li	a5,48
    2a06:	f6b1                	bnez	a3,2952 <_strtoul_l.isra.0+0x72>
    2a08:	46a1                	li	a3,8
    2a0a:	b7a1                	j	2952 <_strtoul_l.isra.0+0x72>
    2a0c:	03000713          	li	a4,48
    2a10:	f2e785e3          	beq	a5,a4,293a <_strtoul_l.isra.0+0x5a>
    2a14:	46a9                	li	a3,10
    2a16:	bf35                	j	2952 <_strtoul_l.isra.0+0x72>

00002a18 <_strtoul_r>:
    2a18:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2a1c:	439c                	lw	a5,0(a5)
    2a1e:	5bd8                	lw	a4,52(a5)
    2a20:	e319                	bnez	a4,2a26 <_strtoul_r+0xe>
    2a22:	e7c18713          	addi	a4,gp,-388 # 200004f0 <__global_locale>
    2a26:	bd6d                	j	28e0 <_strtoul_l.isra.0>

00002a28 <strtoul_l>:
    2a28:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2a2c:	8736                	mv	a4,a3
    2a2e:	86b2                	mv	a3,a2
    2a30:	862e                	mv	a2,a1
    2a32:	85aa                	mv	a1,a0
    2a34:	4388                	lw	a0,0(a5)
    2a36:	b56d                	j	28e0 <_strtoul_l.isra.0>

00002a38 <strtoul>:
    2a38:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2a3c:	439c                	lw	a5,0(a5)
    2a3e:	86b2                	mv	a3,a2
    2a40:	5bd8                	lw	a4,52(a5)
    2a42:	e319                	bnez	a4,2a48 <strtoul+0x10>
    2a44:	e7c18713          	addi	a4,gp,-388 # 200004f0 <__global_locale>
    2a48:	862e                	mv	a2,a1
    2a4a:	85aa                	mv	a1,a0
    2a4c:	853e                	mv	a0,a5
    2a4e:	bd49                	j	28e0 <_strtoul_l.isra.0>

00002a50 <_user_strerror>:
    2a50:	4501                	li	a0,0
    2a52:	8082                	ret

00002a54 <_setlocale_r>:
    2a54:	ee11                	bnez	a2,2a70 <_setlocale_r+0x1c>
    2a56:	00004517          	auipc	a0,0x4
    2a5a:	ec250513          	addi	a0,a0,-318 # 6918 <pad_line+0x720>
    2a5e:	8082                	ret
    2a60:	00004517          	auipc	a0,0x4
    2a64:	eb850513          	addi	a0,a0,-328 # 6918 <pad_line+0x720>
    2a68:	40a2                	lw	ra,8(sp)
    2a6a:	4412                	lw	s0,4(sp)
    2a6c:	0131                	addi	sp,sp,12
    2a6e:	8082                	ret
    2a70:	1151                	addi	sp,sp,-12
    2a72:	00004597          	auipc	a1,0x4
    2a76:	eaa58593          	addi	a1,a1,-342 # 691c <pad_line+0x724>
    2a7a:	8532                	mv	a0,a2
    2a7c:	c222                	sw	s0,4(sp)
    2a7e:	c406                	sw	ra,8(sp)
    2a80:	8432                	mv	s0,a2
    2a82:	207d                	jal	2b30 <strcmp>
    2a84:	dd71                	beqz	a0,2a60 <_setlocale_r+0xc>
    2a86:	00004597          	auipc	a1,0x4
    2a8a:	e9258593          	addi	a1,a1,-366 # 6918 <pad_line+0x720>
    2a8e:	8522                	mv	a0,s0
    2a90:	2045                	jal	2b30 <strcmp>
    2a92:	d579                	beqz	a0,2a60 <_setlocale_r+0xc>
    2a94:	00003597          	auipc	a1,0x3
    2a98:	61858593          	addi	a1,a1,1560 # 60ac <sg_usi_config+0x41c>
    2a9c:	8522                	mv	a0,s0
    2a9e:	2849                	jal	2b30 <strcmp>
    2aa0:	d161                	beqz	a0,2a60 <_setlocale_r+0xc>
    2aa2:	4501                	li	a0,0
    2aa4:	b7d1                	j	2a68 <_setlocale_r+0x14>

00002aa6 <__locale_mb_cur_max>:
    2aa6:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2aaa:	439c                	lw	a5,0(a5)
    2aac:	5bdc                	lw	a5,52(a5)
    2aae:	e399                	bnez	a5,2ab4 <__locale_mb_cur_max+0xe>
    2ab0:	e7c18793          	addi	a5,gp,-388 # 200004f0 <__global_locale>
    2ab4:	1287c503          	lbu	a0,296(a5)
    2ab8:	8082                	ret

00002aba <__locale_ctype_ptr_l>:
    2aba:	0ec52503          	lw	a0,236(a0)
    2abe:	8082                	ret

00002ac0 <__locale_ctype_ptr>:
    2ac0:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2ac4:	439c                	lw	a5,0(a5)
    2ac6:	5bdc                	lw	a5,52(a5)
    2ac8:	e399                	bnez	a5,2ace <__locale_ctype_ptr+0xe>
    2aca:	e7c18793          	addi	a5,gp,-388 # 200004f0 <__global_locale>
    2ace:	0ec7a503          	lw	a0,236(a5)
    2ad2:	8082                	ret

00002ad4 <setlocale>:
    2ad4:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2ad8:	862e                	mv	a2,a1
    2ada:	85aa                	mv	a1,a0
    2adc:	4388                	lw	a0,0(a5)
    2ade:	bf9d                	j	2a54 <_setlocale_r>

00002ae0 <_mbtowc_r>:
    2ae0:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2ae4:	439c                	lw	a5,0(a5)
    2ae6:	5bdc                	lw	a5,52(a5)
    2ae8:	e399                	bnez	a5,2aee <_mbtowc_r+0xe>
    2aea:	e7c18793          	addi	a5,gp,-388 # 200004f0 <__global_locale>
    2aee:	0e47a303          	lw	t1,228(a5)
    2af2:	8302                	jr	t1

00002af4 <__ascii_mbtowc>:
    2af4:	e185                	bnez	a1,2b14 <__ascii_mbtowc+0x20>
    2af6:	1171                	addi	sp,sp,-4
    2af8:	858a                	mv	a1,sp
    2afa:	4501                	li	a0,0
    2afc:	ca11                	beqz	a2,2b10 <__ascii_mbtowc+0x1c>
    2afe:	5579                	li	a0,-2
    2b00:	ca81                	beqz	a3,2b10 <__ascii_mbtowc+0x1c>
    2b02:	00064783          	lbu	a5,0(a2)
    2b06:	c19c                	sw	a5,0(a1)
    2b08:	00064503          	lbu	a0,0(a2)
    2b0c:	00a03533          	snez	a0,a0
    2b10:	0111                	addi	sp,sp,4
    2b12:	8082                	ret
    2b14:	4501                	li	a0,0
    2b16:	ca19                	beqz	a2,2b2c <__ascii_mbtowc+0x38>
    2b18:	5579                	li	a0,-2
    2b1a:	ca89                	beqz	a3,2b2c <__ascii_mbtowc+0x38>
    2b1c:	00064783          	lbu	a5,0(a2)
    2b20:	c19c                	sw	a5,0(a1)
    2b22:	00064503          	lbu	a0,0(a2)
    2b26:	00a03533          	snez	a0,a0
    2b2a:	8082                	ret
    2b2c:	8082                	ret
	...

00002b30 <strcmp>:
    2b30:	00b56733          	or	a4,a0,a1
    2b34:	53fd                	li	t2,-1
    2b36:	8b0d                	andi	a4,a4,3
    2b38:	e779                	bnez	a4,2c06 <strcmp+0xd6>
    2b3a:	7f7f87b7          	lui	a5,0x7f7f8
    2b3e:	f7f78793          	addi	a5,a5,-129 # 7f7f7f7f <__heap_end+0x5f7c7f7f>
    2b42:	4110                	lw	a2,0(a0)
    2b44:	4194                	lw	a3,0(a1)
    2b46:	00f672b3          	and	t0,a2,a5
    2b4a:	00f66333          	or	t1,a2,a5
    2b4e:	92be                	add	t0,t0,a5
    2b50:	0062e2b3          	or	t0,t0,t1
    2b54:	0c729763          	bne	t0,t2,2c22 <strcmp+0xf2>
    2b58:	06d61863          	bne	a2,a3,2bc8 <strcmp+0x98>
    2b5c:	4150                	lw	a2,4(a0)
    2b5e:	41d4                	lw	a3,4(a1)
    2b60:	00f672b3          	and	t0,a2,a5
    2b64:	00f66333          	or	t1,a2,a5
    2b68:	92be                	add	t0,t0,a5
    2b6a:	0062e2b3          	or	t0,t0,t1
    2b6e:	0a729863          	bne	t0,t2,2c1e <strcmp+0xee>
    2b72:	04d61b63          	bne	a2,a3,2bc8 <strcmp+0x98>
    2b76:	4510                	lw	a2,8(a0)
    2b78:	4594                	lw	a3,8(a1)
    2b7a:	00f672b3          	and	t0,a2,a5
    2b7e:	00f66333          	or	t1,a2,a5
    2b82:	92be                	add	t0,t0,a5
    2b84:	0062e2b3          	or	t0,t0,t1
    2b88:	0a729163          	bne	t0,t2,2c2a <strcmp+0xfa>
    2b8c:	02d61e63          	bne	a2,a3,2bc8 <strcmp+0x98>
    2b90:	4550                	lw	a2,12(a0)
    2b92:	45d4                	lw	a3,12(a1)
    2b94:	00f672b3          	and	t0,a2,a5
    2b98:	00f66333          	or	t1,a2,a5
    2b9c:	92be                	add	t0,t0,a5
    2b9e:	0062e2b3          	or	t0,t0,t1
    2ba2:	08729a63          	bne	t0,t2,2c36 <strcmp+0x106>
    2ba6:	02d61163          	bne	a2,a3,2bc8 <strcmp+0x98>
    2baa:	4910                	lw	a2,16(a0)
    2bac:	4994                	lw	a3,16(a1)
    2bae:	00f672b3          	and	t0,a2,a5
    2bb2:	00f66333          	or	t1,a2,a5
    2bb6:	92be                	add	t0,t0,a5
    2bb8:	0062e2b3          	or	t0,t0,t1
    2bbc:	08729363          	bne	t0,t2,2c42 <strcmp+0x112>
    2bc0:	0551                	addi	a0,a0,20
    2bc2:	05d1                	addi	a1,a1,20
    2bc4:	f6d60fe3          	beq	a2,a3,2b42 <strcmp+0x12>
    2bc8:	01061713          	slli	a4,a2,0x10
    2bcc:	01069793          	slli	a5,a3,0x10
    2bd0:	00f71c63          	bne	a4,a5,2be8 <strcmp+0xb8>
    2bd4:	01065713          	srli	a4,a2,0x10
    2bd8:	0106d793          	srli	a5,a3,0x10
    2bdc:	40f70533          	sub	a0,a4,a5
    2be0:	0ff57593          	andi	a1,a0,255
    2be4:	e991                	bnez	a1,2bf8 <strcmp+0xc8>
    2be6:	8082                	ret
    2be8:	8341                	srli	a4,a4,0x10
    2bea:	83c1                	srli	a5,a5,0x10
    2bec:	40f70533          	sub	a0,a4,a5
    2bf0:	0ff57593          	andi	a1,a0,255
    2bf4:	e191                	bnez	a1,2bf8 <strcmp+0xc8>
    2bf6:	8082                	ret
    2bf8:	0ff77713          	andi	a4,a4,255
    2bfc:	0ff7f793          	andi	a5,a5,255
    2c00:	40f70533          	sub	a0,a4,a5
    2c04:	8082                	ret
    2c06:	00054603          	lbu	a2,0(a0)
    2c0a:	0005c683          	lbu	a3,0(a1)
    2c0e:	0505                	addi	a0,a0,1
    2c10:	0585                	addi	a1,a1,1
    2c12:	00d61363          	bne	a2,a3,2c18 <strcmp+0xe8>
    2c16:	fa65                	bnez	a2,2c06 <strcmp+0xd6>
    2c18:	40d60533          	sub	a0,a2,a3
    2c1c:	8082                	ret
    2c1e:	0511                	addi	a0,a0,4
    2c20:	0591                	addi	a1,a1,4
    2c22:	fed612e3          	bne	a2,a3,2c06 <strcmp+0xd6>
    2c26:	4501                	li	a0,0
    2c28:	8082                	ret
    2c2a:	0521                	addi	a0,a0,8
    2c2c:	05a1                	addi	a1,a1,8
    2c2e:	fcd61ce3          	bne	a2,a3,2c06 <strcmp+0xd6>
    2c32:	4501                	li	a0,0
    2c34:	8082                	ret
    2c36:	0531                	addi	a0,a0,12
    2c38:	05b1                	addi	a1,a1,12
    2c3a:	fcd616e3          	bne	a2,a3,2c06 <strcmp+0xd6>
    2c3e:	4501                	li	a0,0
    2c40:	8082                	ret
    2c42:	0541                	addi	a0,a0,16
    2c44:	05c1                	addi	a1,a1,16
    2c46:	fcd610e3          	bne	a2,a3,2c06 <strcmp+0xd6>
    2c4a:	4501                	li	a0,0
    2c4c:	8082                	ret

00002c4e <_wctomb_r>:
    2c4e:	a4c18793          	addi	a5,gp,-1460 # 200000c0 <_impure_ptr>
    2c52:	439c                	lw	a5,0(a5)
    2c54:	5bdc                	lw	a5,52(a5)
    2c56:	e399                	bnez	a5,2c5c <_wctomb_r+0xe>
    2c58:	e7c18793          	addi	a5,gp,-388 # 200004f0 <__global_locale>
    2c5c:	0e07a303          	lw	t1,224(a5)
    2c60:	8302                	jr	t1

00002c62 <__ascii_wctomb>:
    2c62:	cd91                	beqz	a1,2c7e <__ascii_wctomb+0x1c>
    2c64:	0ff00793          	li	a5,255
    2c68:	00c7f763          	bgeu	a5,a2,2c76 <__ascii_wctomb+0x14>
    2c6c:	08a00793          	li	a5,138
    2c70:	c11c                	sw	a5,0(a0)
    2c72:	557d                	li	a0,-1
    2c74:	8082                	ret
    2c76:	00c58023          	sb	a2,0(a1)
    2c7a:	4505                	li	a0,1
    2c7c:	8082                	ret
    2c7e:	4501                	li	a0,0
    2c80:	8082                	ret

00002c82 <board_init>:

extern int clock_timer_init(void);
extern int clock_timer_start(void);

void board_init(void)
{
    2c82:	1151                	addi	sp,sp,-12
    2c84:	c406                	sw	ra,8(sp)
    int32_t ret = 0;
    /* init the console*/
    clock_timer_init();
    2c86:	7e7000ef          	jal	ra,3c6c <clock_timer_init>
    clock_timer_start();
    2c8a:	06e010ef          	jal	ra,3cf8 <clock_timer_start>

    console_handle = csi_usart_initialize(CONSOLE_IDX, NULL);
    2c8e:	4581                	li	a1,0
    2c90:	4501                	li	a0,0
    2c92:	375000ef          	jal	ra,3806 <csi_usart_initialize>
    2c96:	200027b7          	lui	a5,0x20002
    ret = csi_usart_config(console_handle, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);

    if (ret < 0) {
        return;
    }
}
    2c9a:	40a2                	lw	ra,8(sp)
    console_handle = csi_usart_initialize(CONSOLE_IDX, NULL);
    2c9c:	c6a7a223          	sw	a0,-924(a5) # 20001c64 <console_handle>
    ret = csi_usart_config(console_handle, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    2ca0:	65f1                	lui	a1,0x1c
    2ca2:	478d                	li	a5,3
    2ca4:	4701                	li	a4,0
    2ca6:	4681                	li	a3,0
    2ca8:	4601                	li	a2,0
    2caa:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
}
    2cae:	0131                	addi	sp,sp,12
    ret = csi_usart_config(console_handle, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    2cb0:	35b0006f          	j	380a <csi_usart_config>

00002cb4 <drv_irq_enable>:
  \details Enable a device-specific interrupt in the VIC interrupt controller.
  \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void csi_vic_enable_irq(int32_t IRQn)
{
    CLIC->INTIE[IRQn] |= CLIC_INTIE_IE_Msk;
    2cb4:	e000e7b7          	lui	a5,0xe000e
    2cb8:	10078793          	addi	a5,a5,256 # e000e100 <__heap_end+0xbffde100>
    2cbc:	953e                	add	a0,a0,a5
    2cbe:	40054783          	lbu	a5,1024(a0)
    2cc2:	0017e793          	ori	a5,a5,1
    2cc6:	40f50023          	sb	a5,1024(a0)
#ifdef CONFIG_SYSTEM_SECURE
    csi_vic_enable_sirq(irq_num);
#else
    csi_vic_enable_irq(irq_num);
#endif
}
    2cca:	8082                	ret

00002ccc <drv_irq_disable>:
  \details Disable a device-specific interrupt in the VIC interrupt controller.
  \param [in]      IRQn  External interrupt number. Value cannot be negative.
 */
__STATIC_INLINE void csi_vic_disable_irq(int32_t IRQn)
{
    CLIC->INTIE[IRQn] &= ~CLIC_INTIE_IE_Msk;
    2ccc:	e000e7b7          	lui	a5,0xe000e
    2cd0:	10078793          	addi	a5,a5,256 # e000e100 <__heap_end+0xbffde100>
    2cd4:	953e                	add	a0,a0,a5
    2cd6:	40054783          	lbu	a5,1024(a0)
    2cda:	0fe7f793          	andi	a5,a5,254
    2cde:	40f50023          	sb	a5,1024(a0)
#ifdef CONFIG_SYSTEM_SECURE
    csi_vic_disable_sirq(irq_num);
#else
    csi_vic_disable_irq(irq_num);
#endif
}
    2ce2:	8082                	ret

00002ce4 <drv_irq_register>:
  \param[in]   irq_handler IRQ Handler.
  \return      None.
*/
void drv_irq_register(uint32_t irq_num, void *irq_handler)
{
    g_irqvector[irq_num] = irq_handler;
    2ce4:	200027b7          	lui	a5,0x20002
    2ce8:	050a                	slli	a0,a0,0x2
    2cea:	c7078793          	addi	a5,a5,-912 # 20001c70 <g_irqvector>
    2cee:	953e                	add	a0,a0,a5
    2cf0:	c10c                	sw	a1,0(a0)
}
    2cf2:	8082                	ret

00002cf4 <drv_irq_unregister>:
  \param[in]   irq_num Number of IRQ.
  \return      None.
*/
void drv_irq_unregister(uint32_t irq_num)
{
    g_irqvector[irq_num] = (void *)Default_Handler;
    2cf4:	200027b7          	lui	a5,0x20002
    2cf8:	c7078793          	addi	a5,a5,-912 # 20001c70 <g_irqvector>
    2cfc:	050a                	slli	a0,a0,0x2
    2cfe:	953e                	add	a0,a0,a5
    2d00:	000007b7          	lui	a5,0x0
    2d04:	1c078793          	addi	a5,a5,448 # 1c0 <Default_Handler>
    2d08:	c11c                	sw	a5,0(a0)
}
    2d0a:	8082                	ret

00002d0c <wj_usi_set_rxfifo_th>:

static wj_usi_priv_t usi_instance[CONFIG_USI_NUM];

void wj_usi_set_rxfifo_th(wj_usi_reg_t *addr, uint32_t length)
{
    addr->USI_INTR_CTRL &= ~USI_INTR_CTRL_TH_MODE;
    2d0c:	457c                	lw	a5,76(a0)
    2d0e:	7741                	lui	a4,0xffff0
    2d10:	177d                	addi	a4,a4,-1
    2d12:	8ff9                	and	a5,a5,a4
    2d14:	c57c                	sw	a5,76(a0)
    addr->USI_INTR_CTRL &= USI_INTR_CTRL_RXFIFO_TH;
    2d16:	457c                	lw	a5,76(a0)
    2d18:	cff7f793          	andi	a5,a5,-769
    2d1c:	c57c                	sw	a5,76(a0)

    if (length >= USI_RX_MAX_FIFO) {
    2d1e:	47bd                	li	a5,15
    2d20:	00b7f963          	bgeu	a5,a1,2d32 <wj_usi_set_rxfifo_th+0x26>
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_12 | USI_INTR_CTRL_TH_MODE;
    2d24:	457c                	lw	a5,76(a0)
    2d26:	6741                	lui	a4,0x10
    2d28:	30070713          	addi	a4,a4,768 # 10300 <__ctor_end__+0x99d8>
    } else if (length >= USI_RX_MAX_FIFO - 4) {
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_8 | USI_INTR_CTRL_TH_MODE;
    } else if (length >= 4) {
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_4 | USI_INTR_CTRL_TH_MODE;
    2d2c:	8fd9                	or	a5,a5,a4
    } else {
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_4;
    2d2e:	c57c                	sw	a5,76(a0)
    }
}
    2d30:	8082                	ret
    } else if (length >= USI_RX_MAX_FIFO - 4) {
    2d32:	47ad                	li	a5,11
    2d34:	00b7f763          	bgeu	a5,a1,2d42 <wj_usi_set_rxfifo_th+0x36>
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_8 | USI_INTR_CTRL_TH_MODE;
    2d38:	6741                	lui	a4,0x10
    2d3a:	457c                	lw	a5,76(a0)
    2d3c:	20070713          	addi	a4,a4,512 # 10200 <__ctor_end__+0x98d8>
    2d40:	b7f5                	j	2d2c <wj_usi_set_rxfifo_th+0x20>
    } else if (length >= 4) {
    2d42:	478d                	li	a5,3
    2d44:	00b7f763          	bgeu	a5,a1,2d52 <wj_usi_set_rxfifo_th+0x46>
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_4 | USI_INTR_CTRL_TH_MODE;
    2d48:	6741                	lui	a4,0x10
    2d4a:	457c                	lw	a5,76(a0)
    2d4c:	10070713          	addi	a4,a4,256 # 10100 <__ctor_end__+0x97d8>
    2d50:	bff1                	j	2d2c <wj_usi_set_rxfifo_th+0x20>
        addr->USI_INTR_CTRL |= USI_INTR_CTRL_RXFIFO_TH_4;
    2d52:	457c                	lw	a5,76(a0)
    2d54:	1007e793          	ori	a5,a5,256
    2d58:	bfd9                	j	2d2e <wj_usi_set_rxfifo_th+0x22>

00002d5a <wj_usi_irqhandler>:
//            CSI_DRIVER
//------------------------------------------
void wj_usi_irqhandler(int32_t idx)
{
    wj_usi_priv_t *usi_priv = &usi_instance[idx];
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usi_priv->base);
    2d5a:	200027b7          	lui	a5,0x20002
    2d5e:	00351713          	slli	a4,a0,0x3
    2d62:	87c78793          	addi	a5,a5,-1924 # 2000187c <usi_instance>
    2d66:	97ba                	add	a5,a5,a4
    2d68:	439c                	lw	a5,0(a5)

    switch (addr->USI_MODE_SEL & 0x3) {
    2d6a:	4705                	li	a4,1
    2d6c:	43dc                	lw	a5,4(a5)
    2d6e:	8b8d                	andi	a5,a5,3
    2d70:	00e78863          	beq	a5,a4,2d80 <wj_usi_irqhandler+0x26>
    2d74:	c789                	beqz	a5,2d7e <wj_usi_irqhandler+0x24>
    2d76:	4709                	li	a4,2
    2d78:	00e78563          	beq	a5,a4,2d82 <wj_usi_irqhandler+0x28>
    2d7c:	8082                	ret
        case USI_MODE_UART:
#ifndef  CONFIG_CHIP_PANGU
            wj_usi_usart_irqhandler(idx);
    2d7e:	af35                	j	34ba <wj_usi_usart_irqhandler>
#endif
            break;

        case USI_MODE_I2C:
            wj_usi_i2c_irqhandler(idx);
    2d80:	a8a9                	j	2dda <wj_usi_i2c_irqhandler>
            break;

        case USI_MODE_SPI:
            wj_usi_spi_irqhandler(idx);
    2d82:	ae39                	j	30a0 <wj_usi_spi_irqhandler>

00002d84 <drv_usi_initialize>:
            return;
    }
}

int32_t drv_usi_initialize(int32_t idx)
{
    2d84:	1131                	addi	sp,sp,-20
    uint32_t base = 0u;
    uint32_t irq = 0u;

    int32_t ret = target_usi_init(idx, &base, &irq);
    2d86:	0050                	addi	a2,sp,4
    2d88:	858a                	mv	a1,sp
{
    2d8a:	c622                	sw	s0,12(sp)
    2d8c:	c806                	sw	ra,16(sp)
    2d8e:	842a                	mv	s0,a0
    uint32_t base = 0u;
    2d90:	c002                	sw	zero,0(sp)
    uint32_t irq = 0u;
    2d92:	c202                	sw	zero,4(sp)
    int32_t ret = target_usi_init(idx, &base, &irq);
    2d94:	2c9000ef          	jal	ra,385c <target_usi_init>

    if (ret < 0 || ret >= CONFIG_USI_NUM) {
    2d98:	4789                	li	a5,2
    2d9a:	02a7e163          	bltu	a5,a0,2dbc <drv_usi_initialize+0x38>
        return ERR_USI(DRV_ERROR_PARAMETER);
    }

    wj_usi_priv_t *usi_priv = &usi_instance[idx];
    usi_priv->base = base;
    2d9e:	4782                	lw	a5,0(sp)
    2da0:	20002537          	lui	a0,0x20002
    2da4:	040e                	slli	s0,s0,0x3
    2da6:	87c50513          	addi	a0,a0,-1924 # 2000187c <usi_instance>
    2daa:	9522                	add	a0,a0,s0
    2dac:	c11c                	sw	a5,0(a0)
    usi_priv->irq  = irq;
    2dae:	4792                	lw	a5,4(sp)
    2db0:	c15c                	sw	a5,4(a0)

    return 0;
    2db2:	4501                	li	a0,0
}
    2db4:	40c2                	lw	ra,16(sp)
    2db6:	4432                	lw	s0,12(sp)
    2db8:	0151                	addi	sp,sp,20
    2dba:	8082                	ret
        return ERR_USI(DRV_ERROR_PARAMETER);
    2dbc:	81180537          	lui	a0,0x81180
    2dc0:	08450513          	addi	a0,a0,132 # 81180084 <__heap_end+0x61150084>
    2dc4:	bfc5                	j	2db4 <drv_usi_initialize+0x30>

00002dc6 <drv_usi_uninitialize>:

int32_t drv_usi_uninitialize(int32_t idx)
{
    if (idx < 0 || idx >= CONFIG_USI_NUM) {
    2dc6:	4789                	li	a5,2
    2dc8:	00a7f763          	bgeu	a5,a0,2dd6 <drv_usi_uninitialize+0x10>
        return ERR_USI(DRV_ERROR_PARAMETER);
    2dcc:	81180537          	lui	a0,0x81180
    2dd0:	08450513          	addi	a0,a0,132 # 81180084 <__heap_end+0x61150084>
    2dd4:	8082                	ret
    }

    return 0;
    2dd6:	4501                	li	a0,0
}
    2dd8:	8082                	ret

00002dda <wj_usi_i2c_irqhandler>:

}
void wj_usi_i2c_irqhandler(int32_t idx)
{
    wj_usi_iic_priv_t *iic_priv = &iic_instance[idx];
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(iic_priv->base);
    2dda:	03000793          	li	a5,48
    2dde:	02f507b3          	mul	a5,a0,a5
{
    2de2:	1111                	addi	sp,sp,-28
    2de4:	c826                	sw	s1,16(sp)
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(iic_priv->base);
    2de6:	200024b7          	lui	s1,0x20002
    2dea:	89448713          	addi	a4,s1,-1900 # 20001894 <iic_instance>
{
    2dee:	ca22                	sw	s0,20(sp)
    2df0:	cc06                	sw	ra,24(sp)
    2df2:	862a                	mv	a2,a0
    2df4:	89448493          	addi	s1,s1,-1900
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(iic_priv->base);
    2df8:	97ba                	add	a5,a5,a4
    2dfa:	4380                	lw	s0,0(a5)

    uint32_t intr_state = addr->USI_INTR_STA & 0x3ffff;

    if ((intr_state & USI_INT_I2CM_LOSE_ARBI) || (intr_state & USI_INT_I2C_NACK)) {
    2dfc:	6719                	lui	a4,0x6
    uint32_t intr_state = addr->USI_INTR_STA & 0x3ffff;
    2dfe:	4874                	lw	a3,84(s0)
    2e00:	00e69313          	slli	t1,a3,0xe
    if ((intr_state & USI_INT_I2CM_LOSE_ARBI) || (intr_state & USI_INT_I2C_NACK)) {
    2e04:	8f75                	and	a4,a4,a3
    uint32_t intr_state = addr->USI_INTR_STA & 0x3ffff;
    2e06:	00e35313          	srli	t1,t1,0xe
    if ((intr_state & USI_INT_I2CM_LOSE_ARBI) || (intr_state & USI_INT_I2C_NACK)) {
    2e0a:	c70d                	beqz	a4,2e34 <wj_usi_i2c_irqhandler+0x5a>
        /* If arbitration fault, it indicates either a slave device not
        * responding as expected, or other master which is not supported
        * by this SW.
        */
        iic_priv->status    = IIC_STATE_DONE;
    2e0c:	4711                	li	a4,4
    2e0e:	d798                	sw	a4,40(a5)
        addr->USI_EN = 0;
    2e10:	00042023          	sw	zero,0(s0)
        addr->USI_INTR_UNMASK = 0;
    2e14:	04042e23          	sw	zero,92(s0)
        addr->USI_INTR_EN = 0;
        addr->USI_INTR_CLR = intr_state;

        if (iic_priv->cb_event) {
    2e18:	0087a383          	lw	t2,8(a5)
        addr->USI_INTR_EN = 0;
    2e1c:	04042823          	sw	zero,80(s0)
        addr->USI_INTR_CLR = intr_state;
    2e20:	06642023          	sw	t1,96(s0)
        if (iic_priv->cb_event) {
    2e24:	02038063          	beqz	t2,2e44 <wj_usi_i2c_irqhandler+0x6a>
            if (iic_priv->cb_event) {
                iic_priv->cb_event(idx, IIC_EVENT_ARBITRATION_LOST);
            }
        }
    }
}
    2e28:	4452                	lw	s0,20(sp)
    2e2a:	40e2                	lw	ra,24(sp)
    2e2c:	44c2                	lw	s1,16(sp)
            iic_priv->cb_event(idx, IIC_EVENT_BUS_ERROR);
    2e2e:	459d                	li	a1,7
}
    2e30:	0171                	addi	sp,sp,28
            iic_priv->cb_event(idx, IIC_EVENT_BUS_ERROR);
    2e32:	8382                	jr	t2
    switch (iic_priv->status) {
    2e34:	578c                	lw	a1,40(a5)
    2e36:	872a                	mv	a4,a0
    2e38:	4505                	li	a0,1
    2e3a:	02a58963          	beq	a1,a0,2e6c <wj_usi_i2c_irqhandler+0x92>
    2e3e:	4509                	li	a0,2
    2e40:	10a58563          	beq	a1,a0,2f4a <wj_usi_i2c_irqhandler+0x170>
            if (iic_priv->cb_event) {
    2e44:	03000793          	li	a5,48
    2e48:	02f607b3          	mul	a5,a2,a5
            addr->USI_INTR_CLR = intr_state;
    2e4c:	06642023          	sw	t1,96(s0)
            addr->USI_INTR_UNMASK = 0;
    2e50:	04042e23          	sw	zero,92(s0)
            addr->USI_INTR_EN = 0;
    2e54:	04042823          	sw	zero,80(s0)
            addr->USI_EN = 0;
    2e58:	00042023          	sw	zero,0(s0)
            if (iic_priv->cb_event) {
    2e5c:	94be                	add	s1,s1,a5
    2e5e:	0084a303          	lw	t1,8(s1)
    2e62:	0c030663          	beqz	t1,2f2e <wj_usi_i2c_irqhandler+0x154>
                iic_priv->cb_event(idx, IIC_EVENT_ARBITRATION_LOST);
    2e66:	4599                	li	a1,6
    2e68:	8532                	mv	a0,a2
    2e6a:	a2d9                	j	3030 <wj_usi_i2c_irqhandler+0x256>
    if (intr_stat & USI_INT_TX_EMPTY) {
    2e6c:	0026f593          	andi	a1,a3,2
    uint8_t emptyfifo = 0;
    2e70:	4601                	li	a2,0
    if (intr_stat & USI_INT_TX_EMPTY) {
    2e72:	cd8d                	beqz	a1,2eac <wj_usi_i2c_irqhandler+0xd2>
        uint32_t remain_txfifo = iic_priv->tx_total_num - iic_priv->tx_cnt;
    2e74:	53d0                	lw	a2,36(a5)
    2e76:	4b8c                	lw	a1,16(a5)
        emptyfifo = (remain_txfifo > (USI_TX_MAX_FIFO - USI_FIFO_STA_TX_NUM(addr))) ? USI_TX_MAX_FIFO - USI_FIFO_STA_TX_NUM(addr) : remain_txfifo;
    2e78:	445c                	lw	a5,12(s0)
        uint32_t remain_txfifo = iic_priv->tx_total_num - iic_priv->tx_cnt;
    2e7a:	8d91                	sub	a1,a1,a2
        emptyfifo = (remain_txfifo > (USI_TX_MAX_FIFO - USI_FIFO_STA_TX_NUM(addr))) ? USI_TX_MAX_FIFO - USI_FIFO_STA_TX_NUM(addr) : remain_txfifo;
    2e7c:	83a1                	srli	a5,a5,0x8
    2e7e:	4641                	li	a2,16
    2e80:	8bfd                	andi	a5,a5,31
    2e82:	40f607b3          	sub	a5,a2,a5
    2e86:	0ff5f613          	andi	a2,a1,255
    2e8a:	00b7f963          	bgeu	a5,a1,2e9c <wj_usi_i2c_irqhandler+0xc2>
    2e8e:	445c                	lw	a5,12(s0)
    2e90:	4641                	li	a2,16
    2e92:	83a1                	srli	a5,a5,0x8
    2e94:	8bfd                	andi	a5,a5,31
    2e96:	8e1d                	sub	a2,a2,a5
    2e98:	0ff67613          	andi	a2,a2,255
            tx_data = (uint16_t)(*iic_priv->tx_buf);
    2e9c:	03000793          	li	a5,48
    2ea0:	02f707b3          	mul	a5,a4,a5
        for (i = 0; i < emptyfifo; i++) {
    2ea4:	4501                	li	a0,0
            tx_data = (uint16_t)(*iic_priv->tx_buf);
    2ea6:	97a6                	add	a5,a5,s1
        for (i = 0; i < emptyfifo; i++) {
    2ea8:	08c56863          	bltu	a0,a2,2f38 <wj_usi_i2c_irqhandler+0x15e>
    if (iic_priv->tx_cnt == iic_priv->tx_total_num) {
    2eac:	03000793          	li	a5,48
    2eb0:	02f707b3          	mul	a5,a4,a5
    2eb4:	97a6                	add	a5,a5,s1
    2eb6:	53cc                	lw	a1,36(a5)
    2eb8:	4b9c                	lw	a5,16(a5)
    2eba:	00f59963          	bne	a1,a5,2ecc <wj_usi_i2c_irqhandler+0xf2>
        addr->USI_I2CM_CTRL |= (1 << 1);
    2ebe:	541c                	lw	a5,40(s0)
    2ec0:	0027e793          	ori	a5,a5,2
    2ec4:	d41c                	sw	a5,40(s0)
        addr->USI_INTR_EN &= ~USI_INT_TX_EMPTY;
    2ec6:	483c                	lw	a5,80(s0)
    2ec8:	9bf5                	andi	a5,a5,-3
    2eca:	c83c                	sw	a5,80(s0)
    if (intr_stat & USI_INT_I2C_STOP) {
    2ecc:	01369793          	slli	a5,a3,0x13
    2ed0:	0207d863          	bgez	a5,2f00 <wj_usi_i2c_irqhandler+0x126>
        iic_priv->status  = IIC_STATE_SEND_DONE;
    2ed4:	03000793          	li	a5,48
    2ed8:	02f707b3          	mul	a5,a4,a5
    2edc:	4595                	li	a1,5
    2ede:	97a6                	add	a5,a5,s1
    2ee0:	d78c                	sw	a1,40(a5)
        if (iic_priv->cb_event) {
    2ee2:	479c                	lw	a5,8(a5)
        addr->USI_EN = 0;
    2ee4:	00042023          	sw	zero,0(s0)
        if (iic_priv->cb_event) {
    2ee8:	cf81                	beqz	a5,2f00 <wj_usi_i2c_irqhandler+0x126>
            iic_priv->cb_event(idx, IIC_EVENT_TRANSFER_DONE);
    2eea:	853a                	mv	a0,a4
    2eec:	4581                	li	a1,0
    2eee:	c632                	sw	a2,12(sp)
    2ef0:	c41a                	sw	t1,8(sp)
    2ef2:	c236                	sw	a3,4(sp)
    2ef4:	c03a                	sw	a4,0(sp)
    2ef6:	9782                	jalr	a5
    2ef8:	4632                	lw	a2,12(sp)
    2efa:	4322                	lw	t1,8(sp)
    2efc:	4692                	lw	a3,4(sp)
    2efe:	4702                	lw	a4,0(sp)
    iic_priv->tx_cnt += emptyfifo;
    2f00:	03000793          	li	a5,48
    2f04:	02f707b3          	mul	a5,a4,a5
    if (intr_stat & USI_INT_TX_WERR) {
    2f08:	8ac1                	andi	a3,a3,16
    iic_priv->tx_cnt += emptyfifo;
    2f0a:	94be                	add	s1,s1,a5
    2f0c:	50dc                	lw	a5,36(s1)
    2f0e:	963e                	add	a2,a2,a5
    2f10:	d0d0                	sw	a2,36(s1)
    if (intr_stat & USI_INT_TX_WERR) {
    2f12:	ce81                	beqz	a3,2f2a <wj_usi_i2c_irqhandler+0x150>
        iic_priv->status = IIC_STATE_ERROR;
    2f14:	479d                	li	a5,7
    2f16:	d49c                	sw	a5,40(s1)
        if (iic_priv->cb_event) {
    2f18:	449c                	lw	a5,8(s1)
        addr->USI_EN = 0;
    2f1a:	00042023          	sw	zero,0(s0)
        if (iic_priv->cb_event) {
    2f1e:	c791                	beqz	a5,2f2a <wj_usi_i2c_irqhandler+0x150>
            iic_priv->cb_event(idx, IIC_EVENT_BUS_ERROR);
    2f20:	459d                	li	a1,7
    2f22:	853a                	mv	a0,a4
    2f24:	c01a                	sw	t1,0(sp)
    2f26:	9782                	jalr	a5
    2f28:	4302                	lw	t1,0(sp)
            addr->USI_INTR_CLR = intr_state;
    2f2a:	06642023          	sw	t1,96(s0)
}
    2f2e:	40e2                	lw	ra,24(sp)
    2f30:	4452                	lw	s0,20(sp)
    2f32:	44c2                	lw	s1,16(sp)
    2f34:	0171                	addi	sp,sp,28
    2f36:	8082                	ret
            tx_data = (uint16_t)(*iic_priv->tx_buf);
    2f38:	4f8c                	lw	a1,24(a5)
        for (i = 0; i < emptyfifo; i++) {
    2f3a:	0505                	addi	a0,a0,1
            addr->USI_TX_RX_FIFO = tx_data;
    2f3c:	0005c283          	lbu	t0,0(a1)
            iic_priv->tx_buf++;
    2f40:	0585                	addi	a1,a1,1
            addr->USI_TX_RX_FIFO = tx_data;
    2f42:	00542423          	sw	t0,8(s0)
            iic_priv->tx_buf++;
    2f46:	cf8c                	sw	a1,24(a5)
    2f48:	b785                	j	2ea8 <wj_usi_i2c_irqhandler+0xce>
    if (intr_stat & USI_INT_RX_THOLD) {
    2f4a:	0206f613          	andi	a2,a3,32
    2f4e:	c635                	beqz	a2,2fba <wj_usi_i2c_irqhandler+0x1e0>
        addr->USI_INTR_CLR = USI_INT_RX_THOLD;
    2f50:	02000613          	li	a2,32
    2f54:	d030                	sw	a2,96(s0)
        uint32_t rx_num = USI_FIFO_STA_RX_NUM(addr);
    2f56:	444c                	lw	a1,12(s0)
        for (i = 0; i < rx_num; i++) {
    2f58:	4601                	li	a2,0
        uint32_t rx_num = USI_FIFO_STA_RX_NUM(addr);
    2f5a:	81c1                	srli	a1,a1,0x10
    2f5c:	89fd                	andi	a1,a1,31
        for (i = 0; i < rx_num; i++) {
    2f5e:	0cb66e63          	bltu	a2,a1,303a <wj_usi_i2c_irqhandler+0x260>
        uint8_t rxfifo = iic_priv->rx_clk > (USI_RX_MAX_FIFO - tx_num) ? (USI_RX_MAX_FIFO - tx_num) : 1;
    2f62:	03000793          	li	a5,48
    2f66:	02f707b3          	mul	a5,a4,a5
        uint32_t tx_num = USI_FIFO_STA_TX_NUM(addr);
    2f6a:	4450                	lw	a2,12(s0)
        uint8_t rxfifo = iic_priv->rx_clk > (USI_RX_MAX_FIFO - tx_num) ? (USI_RX_MAX_FIFO - tx_num) : 1;
    2f6c:	45c1                	li	a1,16
        uint32_t tx_num = USI_FIFO_STA_TX_NUM(addr);
    2f6e:	8221                	srli	a2,a2,0x8
    2f70:	8a7d                	andi	a2,a2,31
        uint8_t rxfifo = iic_priv->rx_clk > (USI_RX_MAX_FIFO - tx_num) ? (USI_RX_MAX_FIFO - tx_num) : 1;
    2f72:	8d91                	sub	a1,a1,a2
    2f74:	97a6                	add	a5,a5,s1
    2f76:	5388                	lw	a0,32(a5)
    2f78:	4785                	li	a5,1
    2f7a:	00a5f663          	bgeu	a1,a0,2f86 <wj_usi_i2c_irqhandler+0x1ac>
    2f7e:	47c1                	li	a5,16
    2f80:	8f91                	sub	a5,a5,a2
    2f82:	0ff7f793          	andi	a5,a5,255
        if (iic_priv->rx_clk == 0) {
    2f86:	03000613          	li	a2,48
    2f8a:	02c70633          	mul	a2,a4,a2
    2f8e:	9626                	add	a2,a2,s1
    2f90:	520c                	lw	a1,32(a2)
    2f92:	cde1                	beqz	a1,306a <wj_usi_i2c_irqhandler+0x290>
        for (i = 0; i < rxfifo; i++) {
    2f94:	4581                	li	a1,0
            addr->USI_TX_RX_FIFO = 0x100;
    2f96:	10000313          	li	t1,256
        for (i = 0; i < rxfifo; i++) {
    2f9a:	0af59f63          	bne	a1,a5,3058 <wj_usi_i2c_irqhandler+0x27e>
        wj_usi_set_rxfifo_th(addr, rxfifo);
    2f9e:	85be                	mv	a1,a5
    2fa0:	8522                	mv	a0,s0
    2fa2:	c43a                	sw	a4,8(sp)
    2fa4:	c236                	sw	a3,4(sp)
    2fa6:	c03e                	sw	a5,0(sp)
    2fa8:	3395                	jal	2d0c <wj_usi_set_rxfifo_th>
        if (rxfifo == 0) {
    2faa:	4782                	lw	a5,0(sp)
    2fac:	4692                	lw	a3,4(sp)
    2fae:	4722                	lw	a4,8(sp)
    2fb0:	e789                	bnez	a5,2fba <wj_usi_i2c_irqhandler+0x1e0>
            addr->USI_INTR_EN |= USI_INT_I2C_STOP;
    2fb2:	483c                	lw	a5,80(s0)
    2fb4:	6605                	lui	a2,0x1
    2fb6:	8fd1                	or	a5,a5,a2
    2fb8:	c83c                	sw	a5,80(s0)
    if (iic_priv->rx_cnt == iic_priv->rx_total_num) {
    2fba:	03000793          	li	a5,48
    2fbe:	02f707b3          	mul	a5,a4,a5
    2fc2:	97a6                	add	a5,a5,s1
    2fc4:	4fd0                	lw	a2,28(a5)
    2fc6:	47dc                	lw	a5,12(a5)
    2fc8:	00f61a63          	bne	a2,a5,2fdc <wj_usi_i2c_irqhandler+0x202>
        addr->USI_I2CM_CTRL |= (1 << 1);
    2fcc:	541c                	lw	a5,40(s0)
    2fce:	0027e793          	ori	a5,a5,2
    2fd2:	d41c                	sw	a5,40(s0)
        addr->USI_INTR_EN &= ~USI_INT_RX_THOLD;
    2fd4:	483c                	lw	a5,80(s0)
    2fd6:	fdf7f793          	andi	a5,a5,-33
    2fda:	c83c                	sw	a5,80(s0)
    if ((intr_stat & USI_INT_RX_WERR) || (intr_stat & USI_INT_RX_RERR)) {
    2fdc:	3006f793          	andi	a5,a3,768
    2fe0:	c39d                	beqz	a5,3006 <wj_usi_i2c_irqhandler+0x22c>
        iic_priv->status = IIC_STATE_ERROR;
    2fe2:	03000793          	li	a5,48
    2fe6:	02f707b3          	mul	a5,a4,a5
    2fea:	461d                	li	a2,7
    2fec:	97a6                	add	a5,a5,s1
    2fee:	d790                	sw	a2,40(a5)
        if (iic_priv->cb_event) {
    2ff0:	479c                	lw	a5,8(a5)
        addr->USI_EN = 0;
    2ff2:	00042023          	sw	zero,0(s0)
        if (iic_priv->cb_event) {
    2ff6:	cb81                	beqz	a5,3006 <wj_usi_i2c_irqhandler+0x22c>
            iic_priv->cb_event(idx, IIC_EVENT_BUS_ERROR);
    2ff8:	853a                	mv	a0,a4
    2ffa:	459d                	li	a1,7
    2ffc:	c236                	sw	a3,4(sp)
    2ffe:	c03a                	sw	a4,0(sp)
    3000:	9782                	jalr	a5
    3002:	4692                	lw	a3,4(sp)
    3004:	4702                	lw	a4,0(sp)
    if (intr_stat & USI_INT_I2C_STOP) {
    3006:	01369793          	slli	a5,a3,0x13
    300a:	f207d2e3          	bgez	a5,2f2e <wj_usi_i2c_irqhandler+0x154>
        addr->USI_INTR_CLR = USI_INT_I2C_STOP;
    300e:	6785                	lui	a5,0x1
    3010:	d03c                	sw	a5,96(s0)
        iic_priv->status  = IIC_STATE_RECV_DONE;
    3012:	03000793          	li	a5,48
    3016:	02f707b3          	mul	a5,a4,a5
    301a:	94be                	add	s1,s1,a5
        if (iic_priv->cb_event) {
    301c:	0084a303          	lw	t1,8(s1)
        iic_priv->status  = IIC_STATE_RECV_DONE;
    3020:	4799                	li	a5,6
    3022:	d49c                	sw	a5,40(s1)
        addr->USI_EN = 0;
    3024:	00042023          	sw	zero,0(s0)
        if (iic_priv->cb_event) {
    3028:	f00303e3          	beqz	t1,2f2e <wj_usi_i2c_irqhandler+0x154>
            iic_priv->cb_event(idx, IIC_EVENT_TRANSFER_DONE);
    302c:	4581                	li	a1,0
    302e:	853a                	mv	a0,a4
}
    3030:	4452                	lw	s0,20(sp)
    3032:	40e2                	lw	ra,24(sp)
    3034:	44c2                	lw	s1,16(sp)
    3036:	0171                	addi	sp,sp,28
                iic_priv->cb_event(idx, IIC_EVENT_ARBITRATION_LOST);
    3038:	8302                	jr	t1
            *iic_priv->rx_buf = addr->USI_TX_RX_FIFO;
    303a:	00842303          	lw	t1,8(s0)
    303e:	4bc8                	lw	a0,20(a5)
        for (i = 0; i < rx_num; i++) {
    3040:	0605                	addi	a2,a2,1
    3042:	0ff67613          	andi	a2,a2,255
            *iic_priv->rx_buf = addr->USI_TX_RX_FIFO;
    3046:	00650023          	sb	t1,0(a0)
            iic_priv->rx_buf++;
    304a:	4bc8                	lw	a0,20(a5)
    304c:	0505                	addi	a0,a0,1
    304e:	cbc8                	sw	a0,20(a5)
            iic_priv->rx_cnt++;;
    3050:	4fc8                	lw	a0,28(a5)
    3052:	0505                	addi	a0,a0,1
    3054:	cfc8                	sw	a0,28(a5)
    3056:	b721                	j	2f5e <wj_usi_i2c_irqhandler+0x184>
            addr->USI_TX_RX_FIFO = 0x100;
    3058:	00642423          	sw	t1,8(s0)
            iic_priv->rx_clk--;
    305c:	5208                	lw	a0,32(a2)
        for (i = 0; i < rxfifo; i++) {
    305e:	0585                	addi	a1,a1,1
    3060:	0ff5f593          	andi	a1,a1,255
            iic_priv->rx_clk--;
    3064:	157d                	addi	a0,a0,-1
    3066:	d208                	sw	a0,32(a2)
    3068:	bf0d                	j	2f9a <wj_usi_i2c_irqhandler+0x1c0>
            rxfifo = 0;
    306a:	4781                	li	a5,0
    306c:	bf0d                	j	2f9e <wj_usi_i2c_irqhandler+0x1c4>

0000306e <wj_spi_ss_control.isra.0>:
/**
  \brief control ss line depend on controlled Output mode.
*/
static int32_t wj_spi_ss_control(wj_usi_spi_priv_t *spi_priv, spi_ss_stat_e stat)
{
    if (spi_priv->ss_mode == SPI_SS_MASTER_HW_OUTPUT) {
    306e:	4789                	li	a5,2
    3070:	02f51663          	bne	a0,a5,309c <wj_spi_ss_control.isra.0+0x2e>
static int32_t wj_spi_ss_control(wj_usi_spi_priv_t *spi_priv, spi_ss_stat_e stat)
    3074:	1151                	addi	sp,sp,-12
    3076:	c406                	sw	ra,8(sp)
        if (stat == SPI_SS_INACTIVE) {
    3078:	ed81                	bnez	a1,3090 <wj_spi_ss_control.isra.0+0x22>
            csi_gpio_pin_write(pgpio_pin_handle, true);
    307a:	4585                	li	a1,1
        } else if (stat == SPI_SS_ACTIVE) {
            csi_gpio_pin_write(pgpio_pin_handle, false);
    307c:	200027b7          	lui	a5,0x20002
    3080:	9247a503          	lw	a0,-1756(a5) # 20001924 <pgpio_pin_handle>
    3084:	055000ef          	jal	ra,38d8 <csi_gpio_pin_write>
        } else {
            return -1;
        }
    }

    return 0;
    3088:	4501                	li	a0,0
}
    308a:	40a2                	lw	ra,8(sp)
    308c:	0131                	addi	sp,sp,12
    308e:	8082                	ret
        } else if (stat == SPI_SS_ACTIVE) {
    3090:	4785                	li	a5,1
            return -1;
    3092:	557d                	li	a0,-1
        } else if (stat == SPI_SS_ACTIVE) {
    3094:	fef59be3          	bne	a1,a5,308a <wj_spi_ss_control.isra.0+0x1c>
            csi_gpio_pin_write(pgpio_pin_handle, false);
    3098:	4581                	li	a1,0
    309a:	b7cd                	j	307c <wj_spi_ss_control.isra.0+0xe>
    return 0;
    309c:	4501                	li	a0,0
}
    309e:	8082                	ret

000030a0 <wj_usi_spi_irqhandler>:
  \brief       handler the interrupt.
  \param[in]   spi      Pointer to \ref SPI_RESOURCES
*/
void wj_usi_spi_irqhandler(int32_t idx)
{
    wj_usi_spi_priv_t *spi_priv = &spi_instance[idx];
    30a0:	04c00793          	li	a5,76
    30a4:	02f507b3          	mul	a5,a0,a5
{
    30a8:	1101                	addi	sp,sp,-32
    30aa:	ca26                	sw	s1,20(sp)
    wj_usi_spi_priv_t *spi_priv = &spi_instance[idx];
    30ac:	200024b7          	lui	s1,0x20002
    30b0:	92848613          	addi	a2,s1,-1752 # 20001928 <spi_instance>
{
    30b4:	cc22                	sw	s0,24(sp)
    30b6:	ce06                	sw	ra,28(sp)
    30b8:	86aa                	mv	a3,a0
    30ba:	92848493          	addi	s1,s1,-1752
    wj_usi_spi_priv_t *spi_priv = &spi_instance[idx];
    30be:	963e                	add	a2,a2,a5
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(spi_priv->base);
    30c0:	4200                	lw	s0,0(a2)

    uint32_t intr_state = addr->USI_INTR_STA & 0x3ffff;
    30c2:	05442303          	lw	t1,84(s0)
    30c6:	00e31793          	slli	a5,t1,0xe
    30ca:	83b9                	srli	a5,a5,0xe
    30cc:	c03e                	sw	a5,0(sp)

    /* deal with receive FIFO full interrupt */
    if (intr_state & USI_INT_SPI_STOP) {
    30ce:	00e31793          	slli	a5,t1,0xe
    30d2:	0807d363          	bgez	a5,3158 <wj_usi_spi_irqhandler+0xb8>
    uint8_t rxnum = USI_FIFO_STA_RX_NUM(addr);
    30d6:	445c                	lw	a5,12(s0)
    uint32_t length = spi_priv->recv_num;
    30d8:	4a48                	lw	a0,20(a2)
    uint8_t *pbuffer = spi_priv->recv_buf;
    30da:	4e4c                	lw	a1,28(a2)
    uint8_t rxnum = USI_FIFO_STA_RX_NUM(addr);
    30dc:	83c1                	srli	a5,a5,0x10
    uint32_t rxdata_num = (rxnum > length) ? length : rxnum;
    30de:	8bfd                	andi	a5,a5,31
    30e0:	00f57363          	bgeu	a0,a5,30e6 <wj_usi_spi_irqhandler+0x46>
    30e4:	87aa                	mv	a5,a0
    for (i = 0; i < rxdata_num; i++) {
    30e6:	00f582b3          	add	t0,a1,a5
    30ea:	10559663          	bne	a1,t0,31f6 <wj_usi_spi_irqhandler+0x156>
    length -= rxdata_num;
    30ee:	8d1d                	sub	a0,a0,a5
    if (length <= 0) {
    30f0:	10051963          	bnez	a0,3202 <wj_usi_spi_irqhandler+0x162>
        addr->USI_INTR_EN &= ~USI_INT_SPI_STOP;
    30f4:	483c                	lw	a5,80(s0)
    30f6:	7581                	lui	a1,0xfffe0
    30f8:	15fd                	addi	a1,a1,-1
    30fa:	8fed                	and	a5,a5,a1
    30fc:	c83c                	sw	a5,80(s0)
        spi_priv->status.busy = 0U;
    30fe:	04c00793          	li	a5,76
    3102:	02f687b3          	mul	a5,a3,a5
    3106:	c81a                	sw	t1,16(sp)
    3108:	c636                	sw	a3,12(sp)
        wj_spi_ss_control(spi_priv, SPI_SS_INACTIVE);
    310a:	c232                	sw	a2,4(sp)
        spi_priv->status.busy = 0U;
    310c:	97a6                	add	a5,a5,s1
    310e:	0387c583          	lbu	a1,56(a5)
        spi_priv->recv_num = 0;
    3112:	c43e                	sw	a5,8(sp)
        spi_priv->status.busy = 0U;
    3114:	99f9                	andi	a1,a1,-2
    3116:	02b78c23          	sb	a1,56(a5)
        addr->USI_EN = 0x0;
    311a:	00042023          	sw	zero,0(s0)
        addr->USI_EN = 0xf;
    311e:	45bd                	li	a1,15
    3120:	c00c                	sw	a1,0(s0)
        addr->USI_EN = 0x0;
    3122:	00042023          	sw	zero,0(s0)
        spi_priv->recv_num = 0;
    3126:	0007aa23          	sw	zero,20(a5)
        wj_spi_ss_control(spi_priv, SPI_SS_INACTIVE);
    312a:	03464503          	lbu	a0,52(a2) # 1034 <__divdf3+0x184>
    312e:	4581                	li	a1,0
    3130:	3f3d                	jal	306e <wj_spi_ss_control.isra.0>
        if (spi_priv->cb_event) {
    3132:	47a2                	lw	a5,8(sp)
    3134:	4612                	lw	a2,4(sp)
    3136:	46b2                	lw	a3,12(sp)
    3138:	00c7a383          	lw	t2,12(a5)
    313c:	4342                	lw	t1,16(sp)
    313e:	00038a63          	beqz	t2,3152 <wj_usi_spi_irqhandler+0xb2>
            spi_priv->cb_event(spi_priv->idx, SPI_EVENT_RX_COMPLETE);
    3142:	43a8                	lw	a0,64(a5)
    3144:	4589                	li	a1,2
    3146:	c432                	sw	a2,8(sp)
    3148:	c21a                	sw	t1,4(sp)
    314a:	9382                	jalr	t2
            return;
    314c:	4312                	lw	t1,4(sp)
    314e:	4622                	lw	a2,8(sp)
    3150:	46b2                	lw	a3,12(sp)
        wj_spi_intr_rx_full(spi_priv);
        addr->USI_INTR_CLR = USI_INT_SPI_STOP;
    3152:	000207b7          	lui	a5,0x20
    3156:	d03c                	sw	a5,96(s0)
    }

    /* deal with transmit FIFO empty interrupt */
    if (intr_state & USI_INT_TX_EMPTY) {
    3158:	00237313          	andi	t1,t1,2
    315c:	08030663          	beqz	t1,31e8 <wj_usi_spi_irqhandler+0x148>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(spi_priv->base);
    3160:	04c00793          	li	a5,76
    3164:	02f687b3          	mul	a5,a3,a5
    3168:	97a6                	add	a5,a5,s1
    if (spi_priv->mode == WJENUM_SPI_TXRX) {
    316a:	5b8c                	lw	a1,48(a5)
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(spi_priv->base);
    316c:	0007a303          	lw	t1,0(a5) # 20000 <__ctor_end__+0x196d8>
    if (spi_priv->mode == WJENUM_SPI_TXRX) {
    3170:	0e059e63          	bnez	a1,326c <wj_usi_spi_irqhandler+0x1cc>
        while (spi_priv->transfer_num) {
    3174:	53cc                	lw	a1,36(a5)
    3176:	eddd                	bnez	a1,3234 <wj_usi_spi_irqhandler+0x194>
        if (spi_priv->clk_num >= USI_TX_MAX_FIFO) {
    3178:	5788                	lw	a0,40(a5)
    317a:	47c1                	li	a5,16
    317c:	00a7f363          	bgeu	a5,a0,3182 <wj_usi_spi_irqhandler+0xe2>
    3180:	4541                	li	a0,16
    3182:	04c00793          	li	a5,76
    3186:	02f687b3          	mul	a5,a3,a5
    318a:	97a6                	add	a5,a5,s1
    318c:	d3c8                	sw	a0,36(a5)
        for (i = 0; i < spi_priv->transfer_num; i++) {
    318e:	0aa59f63          	bne	a1,a0,324c <wj_usi_spi_irqhandler+0x1ac>
    if (spi_priv->clk_num == 0) {
    3192:	04c00793          	li	a5,76
    3196:	02f687b3          	mul	a5,a3,a5
    319a:	97a6                	add	a5,a5,s1
    319c:	578c                	lw	a1,40(a5)
    319e:	10059463          	bnez	a1,32a6 <wj_usi_spi_irqhandler+0x206>
        addr->USI_INTR_EN &= ~USI_INT_TX_EMPTY;
    31a2:	05032583          	lw	a1,80(t1)
    31a6:	c636                	sw	a3,12(sp)
    31a8:	c41a                	sw	t1,8(sp)
    31aa:	99f5                	andi	a1,a1,-3
    31ac:	04b32823          	sw	a1,80(t1)
        spi_priv->status.busy = 0U;
    31b0:	0387c583          	lbu	a1,56(a5)
        spi_priv->send_num = 0;
    31b4:	0007a823          	sw	zero,16(a5)
    31b8:	c23e                	sw	a5,4(sp)
        spi_priv->status.busy = 0U;
    31ba:	99f9                	andi	a1,a1,-2
    31bc:	02b78c23          	sb	a1,56(a5)
        wj_spi_ss_control(spi_priv, SPI_SS_INACTIVE);
    31c0:	03464503          	lbu	a0,52(a2)
    31c4:	4581                	li	a1,0
    31c6:	3565                	jal	306e <wj_spi_ss_control.isra.0>
        if (spi_priv->mode == WJENUM_SPI_TXRX) {
    31c8:	4792                	lw	a5,4(sp)
        addr->USI_EN = 0x0;
    31ca:	4322                	lw	t1,8(sp)
        addr->USI_EN = 0xf;
    31cc:	463d                	li	a2,15
        if (spi_priv->mode == WJENUM_SPI_TXRX) {
    31ce:	5b8c                	lw	a1,48(a5)
        addr->USI_EN = 0x0;
    31d0:	00032023          	sw	zero,0(t1)
        addr->USI_EN = 0xf;
    31d4:	00c32023          	sw	a2,0(t1)
        if (spi_priv->mode == WJENUM_SPI_TXRX) {
    31d8:	46b2                	lw	a3,12(sp)
    31da:	47d0                	lw	a2,12(a5)
    31dc:	e1f1                	bnez	a1,32a0 <wj_usi_spi_irqhandler+0x200>
            if (spi_priv->cb_event) {
    31de:	c661                	beqz	a2,32a6 <wj_usi_spi_irqhandler+0x206>
                spi_priv->cb_event(spi_priv->idx, SPI_EVENT_TX_COMPLETE);
    31e0:	43a8                	lw	a0,64(a5)
    31e2:	9602                	jalr	a2
        wj_spi_intr_tx_empty(spi_priv);
        addr->USI_INTR_CLR = USI_INT_TX_EMPTY;
    31e4:	4789                	li	a5,2
    31e6:	d03c                	sw	a5,96(s0)
    }

    addr->USI_INTR_CLR = intr_state;
    31e8:	4782                	lw	a5,0(sp)
}
    31ea:	40f2                	lw	ra,28(sp)
    31ec:	44d2                	lw	s1,20(sp)
    addr->USI_INTR_CLR = intr_state;
    31ee:	d03c                	sw	a5,96(s0)
}
    31f0:	4462                	lw	s0,24(sp)
    31f2:	6105                	addi	sp,sp,32
    31f4:	8082                	ret
        *pbuffer = addr->USI_TX_RX_FIFO;
    31f6:	00842383          	lw	t2,8(s0)
        pbuffer++;
    31fa:	0585                	addi	a1,a1,1
        *pbuffer = addr->USI_TX_RX_FIFO;
    31fc:	fe758fa3          	sb	t2,-1(a1) # fffdffff <__heap_end+0xdffaffff>
        pbuffer++;
    3200:	b5ed                	j	30ea <wj_usi_spi_irqhandler+0x4a>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(spi_priv->base);
    3202:	04c00793          	li	a5,76
    3206:	02f687b3          	mul	a5,a3,a5
    320a:	97a6                	add	a5,a5,s1
    320c:	0007a283          	lw	t0,0(a5)
            addr->USI_SPI_CTRL &= ~WJ_USI_SPI_CTRL_TMODE;
    3210:	0442a383          	lw	t2,68(t0) # 80000044 <__heap_end+0x5ffd0044>
    3214:	fcf3f393          	andi	t2,t2,-49
    3218:	0472a223          	sw	t2,68(t0)
            addr->USI_SPI_CTRL |= WJ_USI_SPI_CTRL_TMODE_RX;
    321c:	0442a383          	lw	t2,68(t0)
    3220:	0203e393          	ori	t2,t2,32
    3224:	0472a223          	sw	t2,68(t0)
    spi_priv->mode = mode;
    3228:	4289                	li	t0,2
    322a:	0257a823          	sw	t0,48(a5)
        spi_priv->recv_buf = pbuffer;
    322e:	cfcc                	sw	a1,28(a5)
        spi_priv->recv_num = length;
    3230:	cbc8                	sw	a0,20(a5)
    3232:	b705                	j	3152 <wj_usi_spi_irqhandler+0xb2>
            *spi_priv->recv_buf = addr->USI_TX_RX_FIFO;
    3234:	00832503          	lw	a0,8(t1)
    3238:	4fcc                	lw	a1,28(a5)
    323a:	00a58023          	sb	a0,0(a1)
            spi_priv->recv_buf++;
    323e:	4fcc                	lw	a1,28(a5)
    3240:	0585                	addi	a1,a1,1
    3242:	cfcc                	sw	a1,28(a5)
            spi_priv->transfer_num--;
    3244:	53cc                	lw	a1,36(a5)
    3246:	15fd                	addi	a1,a1,-1
    3248:	d3cc                	sw	a1,36(a5)
    324a:	b72d                	j	3174 <wj_usi_spi_irqhandler+0xd4>
            addr->USI_TX_RX_FIFO = *spi_priv->send_buf;
    324c:	0187a283          	lw	t0,24(a5)
        for (i = 0; i < spi_priv->transfer_num; i++) {
    3250:	0585                	addi	a1,a1,1
            addr->USI_TX_RX_FIFO = *spi_priv->send_buf;
    3252:	0002c383          	lbu	t2,0(t0)
            spi_priv->send_buf++;
    3256:	0285                	addi	t0,t0,1
            addr->USI_TX_RX_FIFO = *spi_priv->send_buf;
    3258:	00732423          	sw	t2,8(t1)
            spi_priv->send_buf++;
    325c:	0057ac23          	sw	t0,24(a5)
            spi_priv->send_num--;
    3260:	0107a283          	lw	t0,16(a5)
    3264:	12fd                	addi	t0,t0,-1
    3266:	0057a823          	sw	t0,16(a5)
    326a:	b715                	j	318e <wj_usi_spi_irqhandler+0xee>
        if (spi_priv->clk_num >= USI_TX_MAX_FIFO) {
    326c:	578c                	lw	a1,40(a5)
    326e:	47c1                	li	a5,16
    3270:	00b7f363          	bgeu	a5,a1,3276 <wj_usi_spi_irqhandler+0x1d6>
    3274:	45c1                	li	a1,16
    3276:	04c00793          	li	a5,76
    327a:	02f687b3          	mul	a5,a3,a5
    327e:	4281                	li	t0,0
    3280:	97a6                	add	a5,a5,s1
    3282:	d3cc                	sw	a1,36(a5)
        for (i = 0; i < spi_priv->transfer_num; i++) {
    3284:	f05587e3          	beq	a1,t0,3192 <wj_usi_spi_irqhandler+0xf2>
            addr->USI_TX_RX_FIFO = *spi_priv->send_buf;
    3288:	4f88                	lw	a0,24(a5)
        for (i = 0; i < spi_priv->transfer_num; i++) {
    328a:	0285                	addi	t0,t0,1
            addr->USI_TX_RX_FIFO = *spi_priv->send_buf;
    328c:	00054383          	lbu	t2,0(a0)
            spi_priv->send_buf++;
    3290:	0505                	addi	a0,a0,1
            addr->USI_TX_RX_FIFO = *spi_priv->send_buf;
    3292:	00732423          	sw	t2,8(t1)
            spi_priv->send_buf++;
    3296:	cf88                	sw	a0,24(a5)
            spi_priv->send_num--;
    3298:	4b88                	lw	a0,16(a5)
    329a:	157d                	addi	a0,a0,-1
    329c:	cb88                	sw	a0,16(a5)
    329e:	b7dd                	j	3284 <wj_usi_spi_irqhandler+0x1e4>
            if (spi_priv->cb_event) {
    32a0:	c219                	beqz	a2,32a6 <wj_usi_spi_irqhandler+0x206>
                spi_priv->cb_event(spi_priv->idx, SPI_EVENT_TX_COMPLETE);
    32a2:	4585                	li	a1,1
    32a4:	bf35                	j	31e0 <wj_usi_spi_irqhandler+0x140>
    spi_priv->clk_num -= spi_priv->transfer_num;
    32a6:	04c00793          	li	a5,76
    32aa:	02f686b3          	mul	a3,a3,a5
    32ae:	94b6                	add	s1,s1,a3
    32b0:	549c                	lw	a5,40(s1)
    32b2:	50d4                	lw	a3,36(s1)
    32b4:	8f95                	sub	a5,a5,a3
    32b6:	d49c                	sw	a5,40(s1)
    32b8:	b735                	j	31e4 <wj_usi_spi_irqhandler+0x144>

000032ba <ck_usart_intr_recv_data>:
  \brief        interrupt service function for receiver data available.
  \param[in]   usart_priv usart private to operate.
*/
static void ck_usart_intr_recv_data(wj_usi_usart_priv_t *usart_priv)
{
    if ((usart_priv->rx_total_num == 0) || (usart_priv->rx_buf == NULL)) {
    32ba:	4558                	lw	a4,12(a0)
    32bc:	c319                	beqz	a4,32c2 <ck_usart_intr_recv_data+0x8>
    32be:	495c                	lw	a5,20(a0)
    32c0:	e791                	bnez	a5,32cc <ck_usart_intr_recv_data+0x12>
        usart_priv->cb_event(usart_priv->idx, USART_EVENT_RECEIVED);
    32c2:	00852303          	lw	t1,8(a0)
    32c6:	45b9                	li	a1,14
        usart_priv->rx_buf = NULL;
        usart_priv->rx_busy = 0;
        usart_priv->rx_total_num = 0;

        if (usart_priv->cb_event) {
            usart_priv->cb_event(usart_priv->idx, USART_EVENT_RECEIVE_COMPLETE);
    32c8:	5948                	lw	a0,52(a0)
    32ca:	8302                	jr	t1
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    32cc:	4110                	lw	a2,0(a0)
    uint32_t rxfifo_num = (addr->USI_FIFO_STA >> 16) & 0xf;
    32ce:	465c                	lw	a5,12(a2)
    32d0:	83c1                	srli	a5,a5,0x10
    32d2:	8bbd                	andi	a5,a5,15
    uint32_t rxdata_num = (rxfifo_num > usart_priv->rx_total_num) ? usart_priv->rx_total_num : rxfifo_num;
    32d4:	00f77363          	bgeu	a4,a5,32da <ck_usart_intr_recv_data+0x20>
    32d8:	87ba                	mv	a5,a4
    for (i = 0; i < rxdata_num; i++) {
    32da:	4701                	li	a4,0
    32dc:	02f71563          	bne	a4,a5,3306 <ck_usart_intr_recv_data+0x4c>
    if (usart_priv->rx_cnt >= usart_priv->rx_total_num) {
    32e0:	4d58                	lw	a4,28(a0)
    32e2:	455c                	lw	a5,12(a0)
    32e4:	02f76d63          	bltu	a4,a5,331e <ck_usart_intr_recv_data+0x64>
        if (usart_priv->cb_event) {
    32e8:	00852303          	lw	t1,8(a0)
        usart_priv->rx_cnt = 0;
    32ec:	00052e23          	sw	zero,28(a0)
        usart_priv->last_rx_num = usart_priv->rx_total_num;
    32f0:	d91c                	sw	a5,48(a0)
        usart_priv->rx_buf = NULL;
    32f2:	00052a23          	sw	zero,20(a0)
        usart_priv->rx_busy = 0;
    32f6:	02052423          	sw	zero,40(a0)
        usart_priv->rx_total_num = 0;
    32fa:	00052623          	sw	zero,12(a0)
        if (usart_priv->cb_event) {
    32fe:	02030063          	beqz	t1,331e <ck_usart_intr_recv_data+0x64>
            usart_priv->cb_event(usart_priv->idx, USART_EVENT_RECEIVE_COMPLETE);
    3302:	4585                	li	a1,1
    3304:	b7d1                	j	32c8 <ck_usart_intr_recv_data+0xe>
        *((uint8_t *)usart_priv->rx_buf) = addr->USI_TX_RX_FIFO;;
    3306:	460c                	lw	a1,8(a2)
    3308:	4954                	lw	a3,20(a0)
    for (i = 0; i < rxdata_num; i++) {
    330a:	0705                	addi	a4,a4,1
        *((uint8_t *)usart_priv->rx_buf) = addr->USI_TX_RX_FIFO;;
    330c:	00b68023          	sb	a1,0(a3)
        usart_priv->rx_cnt++;
    3310:	4d54                	lw	a3,28(a0)
    3312:	0685                	addi	a3,a3,1
    3314:	cd54                	sw	a3,28(a0)
        usart_priv->rx_buf++;
    3316:	4954                	lw	a3,20(a0)
    3318:	0685                	addi	a3,a3,1
    331a:	c954                	sw	a3,20(a0)
    331c:	b7c1                	j	32dc <ck_usart_intr_recv_data+0x22>
        }
    }
}
    331e:	8082                	ret

00003320 <drv_usi_usart_config_baudrate>:
{
    3320:	1141                	addi	sp,sp,-16
    3322:	c422                	sw	s0,8(sp)
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    3324:	4100                	lw	s0,0(a0)
    uint32_t divisor = ((drv_get_usi_freq(usart_priv->idx)  * 10) / baudrate) >> 4;
    3326:	5948                	lw	a0,52(a0)
{
    3328:	c606                	sw	ra,12(sp)
    addr->USI_EN = 0x0;
    332a:	00042023          	sw	zero,0(s0)
{
    332e:	c02e                	sw	a1,0(sp)
    uint32_t divisor = ((drv_get_usi_freq(usart_priv->idx)  * 10) / baudrate) >> 4;
    3330:	031000ef          	jal	ra,3b60 <drv_get_usi_freq>
    3334:	4729                	li	a4,10
    3336:	02e50533          	mul	a0,a0,a4
    333a:	4582                	lw	a1,0(sp)
    333c:	02b555b3          	divu	a1,a0,a1
    3340:	8191                	srli	a1,a1,0x4
    if ((divisor % 10) >= 5) {
    3342:	02e5d7b3          	divu	a5,a1,a4
    3346:	02e5f5b3          	remu	a1,a1,a4
    334a:	4711                	li	a4,4
    334c:	00b76363          	bltu	a4,a1,3352 <drv_usi_usart_config_baudrate+0x32>
        divisor = divisor / 10 - 1;
    3350:	17fd                	addi	a5,a5,-1
    addr->USI_CLK_DIV0 = divisor;
    3352:	c81c                	sw	a5,16(s0)
    addr->USI_EN = 0xf;
    3354:	47bd                	li	a5,15
    3356:	c01c                	sw	a5,0(s0)
}
    3358:	40b2                	lw	ra,12(sp)
    335a:	4422                	lw	s0,8(sp)
    335c:	4501                	li	a0,0
    335e:	0141                	addi	sp,sp,16
    3360:	8082                	ret

00003362 <drv_usi_usart_config_mode>:
    USART_NULL_PARAM_CHK(handle);
    3362:	cd11                	beqz	a0,337e <drv_usi_usart_config_mode+0x1c>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    3364:	411c                	lw	a5,0(a0)
        addr->USI_EN = 0xf;
    3366:	473d                	li	a4,15
    addr->USI_EN = 0x0;
    3368:	0007a023          	sw	zero,0(a5)
        addr->USI_EN = 0xf;
    336c:	c398                	sw	a4,0(a5)
    if (mode == USART_MODE_ASYNCHRONOUS) {
    336e:	e199                	bnez	a1,3374 <drv_usi_usart_config_mode+0x12>
        return 0;
    3370:	4501                	li	a0,0
    3372:	8082                	ret
    return ERR_USART(USART_ERROR_MODE);
    3374:	81020537          	lui	a0,0x81020
    3378:	08650513          	addi	a0,a0,134 # 81020086 <__heap_end+0x60ff0086>
    337c:	8082                	ret
    USART_NULL_PARAM_CHK(handle);
    337e:	81020537          	lui	a0,0x81020
    3382:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
}
    3386:	8082                	ret

00003388 <drv_usi_usart_config_parity>:
    USART_NULL_PARAM_CHK(handle);
    3388:	c939                	beqz	a0,33de <drv_usi_usart_config_parity+0x56>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    338a:	411c                	lw	a5,0(a0)
    addr->USI_EN = 0x0;
    338c:	4705                	li	a4,1
    338e:	0007a023          	sw	zero,0(a5)
    switch (parity) {
    3392:	02e58b63          	beq	a1,a4,33c8 <drv_usi_usart_config_parity+0x40>
    3396:	c989                	beqz	a1,33a8 <drv_usi_usart_config_parity+0x20>
    3398:	4709                	li	a4,2
    339a:	00e58e63          	beq	a1,a4,33b6 <drv_usi_usart_config_parity+0x2e>
            return ERR_USART(USART_ERROR_PARITY);
    339e:	81020537          	lui	a0,0x81020
    33a2:	08950513          	addi	a0,a0,137 # 81020089 <__heap_end+0x60ff0089>
    33a6:	8082                	ret
            addr->USI_UART_CTRL &= ~CTRL_PARITY_ENABLE;
    33a8:	4f98                	lw	a4,24(a5)
    33aa:	9b3d                	andi	a4,a4,-17
            addr->USI_UART_CTRL |= CTRL_PARITY_EVEN;
    33ac:	cf98                	sw	a4,24(a5)
    addr->USI_EN = 0xf;
    33ae:	473d                	li	a4,15
    33b0:	c398                	sw	a4,0(a5)
    return 0;
    33b2:	4501                	li	a0,0
    33b4:	8082                	ret
            addr->USI_UART_CTRL &= CTRL_PARITY_BITS;
    33b6:	4f98                	lw	a4,24(a5)
    33b8:	8b3d                	andi	a4,a4,15
    33ba:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_PARITY_ENABLE;
    33bc:	4f98                	lw	a4,24(a5)
    33be:	01076713          	ori	a4,a4,16
    33c2:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_PARITY_ODD;
    33c4:	4f98                	lw	a4,24(a5)
    33c6:	b7dd                	j	33ac <drv_usi_usart_config_parity+0x24>
            addr->USI_UART_CTRL &= CTRL_PARITY_BITS;
    33c8:	4f98                	lw	a4,24(a5)
    33ca:	8b3d                	andi	a4,a4,15
    33cc:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_PARITY_ENABLE;
    33ce:	4f98                	lw	a4,24(a5)
    33d0:	01076713          	ori	a4,a4,16
    33d4:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_PARITY_EVEN;
    33d6:	4f98                	lw	a4,24(a5)
    33d8:	02076713          	ori	a4,a4,32
    33dc:	bfc1                	j	33ac <drv_usi_usart_config_parity+0x24>
    USART_NULL_PARAM_CHK(handle);
    33de:	81020537          	lui	a0,0x81020
    33e2:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
}
    33e6:	8082                	ret

000033e8 <drv_usi_usart_config_stopbits>:
    USART_NULL_PARAM_CHK(handle);
    33e8:	c931                	beqz	a0,343c <drv_usi_usart_config_stopbits+0x54>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    33ea:	411c                	lw	a5,0(a0)
    addr->USI_EN = 0x0;
    33ec:	4705                	li	a4,1
    33ee:	0007a023          	sw	zero,0(a5)
    switch (stopbit) {
    33f2:	02e58d63          	beq	a1,a4,342c <drv_usi_usart_config_stopbits+0x44>
    33f6:	c989                	beqz	a1,3408 <drv_usi_usart_config_stopbits+0x20>
    33f8:	4709                	li	a4,2
    33fa:	02e58163          	beq	a1,a4,341c <drv_usi_usart_config_stopbits+0x34>
            return ERR_USART(USART_ERROR_STOP_BITS);
    33fe:	81020537          	lui	a0,0x81020
    3402:	08a50513          	addi	a0,a0,138 # 8102008a <__heap_end+0x60ff008a>
    3406:	8082                	ret
            addr->USI_UART_CTRL &= CTRL_STOP_BITS;
    3408:	4f98                	lw	a4,24(a5)
    340a:	03377713          	andi	a4,a4,51
    340e:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_STOP_1;
    3410:	4f98                	lw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_STOP_2;
    3412:	cf98                	sw	a4,24(a5)
    addr->USI_EN = 0xf;
    3414:	473d                	li	a4,15
    3416:	c398                	sw	a4,0(a5)
    return 0;
    3418:	4501                	li	a0,0
    341a:	8082                	ret
            addr->USI_UART_CTRL &= CTRL_STOP_BITS;
    341c:	4f98                	lw	a4,24(a5)
    341e:	03377713          	andi	a4,a4,51
    3422:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_STOP_1_5;
    3424:	4f98                	lw	a4,24(a5)
    3426:	00476713          	ori	a4,a4,4
    342a:	b7e5                	j	3412 <drv_usi_usart_config_stopbits+0x2a>
            addr->USI_UART_CTRL &= CTRL_STOP_BITS;
    342c:	4f98                	lw	a4,24(a5)
    342e:	03377713          	andi	a4,a4,51
    3432:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_STOP_2;
    3434:	4f98                	lw	a4,24(a5)
    3436:	00876713          	ori	a4,a4,8
    343a:	bfe1                	j	3412 <drv_usi_usart_config_stopbits+0x2a>
    USART_NULL_PARAM_CHK(handle);
    343c:	81020537          	lui	a0,0x81020
    3440:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
}
    3444:	8082                	ret

00003446 <drv_usi_usart_config_databits>:
    USART_NULL_PARAM_CHK(handle);
    3446:	c52d                	beqz	a0,34b0 <drv_usi_usart_config_databits+0x6a>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    3448:	411c                	lw	a5,0(a0)
    addr->USI_EN = 0x0;
    344a:	4705                	li	a4,1
    344c:	0007a023          	sw	zero,0(a5)
    switch (databits) {
    3450:	02e58863          	beq	a1,a4,3480 <drv_usi_usart_config_databits+0x3a>
    3454:	cd81                	beqz	a1,346c <drv_usi_usart_config_databits+0x26>
    3456:	4709                	li	a4,2
    3458:	02e58c63          	beq	a1,a4,3490 <drv_usi_usart_config_databits+0x4a>
    345c:	470d                	li	a4,3
    345e:	04e58163          	beq	a1,a4,34a0 <drv_usi_usart_config_databits+0x5a>
            return ERR_USART(USART_ERROR_DATA_BITS);
    3462:	81020537          	lui	a0,0x81020
    3466:	08850513          	addi	a0,a0,136 # 81020088 <__heap_end+0x60ff0088>
    346a:	8082                	ret
            addr->USI_UART_CTRL &= CTRL_DBIT_BITS;
    346c:	4f98                	lw	a4,24(a5)
    346e:	03c77713          	andi	a4,a4,60
    3472:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_DBIT_5;
    3474:	4f98                	lw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_DBIT_8;
    3476:	cf98                	sw	a4,24(a5)
    addr->USI_EN = 0xf;
    3478:	473d                	li	a4,15
    347a:	c398                	sw	a4,0(a5)
    return 0;
    347c:	4501                	li	a0,0
    347e:	8082                	ret
            addr->USI_UART_CTRL &= CTRL_DBIT_BITS;
    3480:	4f98                	lw	a4,24(a5)
    3482:	03c77713          	andi	a4,a4,60
    3486:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_DBIT_6;
    3488:	4f98                	lw	a4,24(a5)
    348a:	00176713          	ori	a4,a4,1
    348e:	b7e5                	j	3476 <drv_usi_usart_config_databits+0x30>
            addr->USI_UART_CTRL &= CTRL_DBIT_BITS;
    3490:	4f98                	lw	a4,24(a5)
    3492:	03c77713          	andi	a4,a4,60
    3496:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_DBIT_7;
    3498:	4f98                	lw	a4,24(a5)
    349a:	00276713          	ori	a4,a4,2
    349e:	bfe1                	j	3476 <drv_usi_usart_config_databits+0x30>
            addr->USI_UART_CTRL &= CTRL_DBIT_BITS;
    34a0:	4f98                	lw	a4,24(a5)
    34a2:	03c77713          	andi	a4,a4,60
    34a6:	cf98                	sw	a4,24(a5)
            addr->USI_UART_CTRL |= CTRL_DBIT_8;
    34a8:	4f98                	lw	a4,24(a5)
    34aa:	00376713          	ori	a4,a4,3
    34ae:	b7e1                	j	3476 <drv_usi_usart_config_databits+0x30>
    USART_NULL_PARAM_CHK(handle);
    34b0:	81020537          	lui	a0,0x81020
    34b4:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
}
    34b8:	8082                	ret

000034ba <wj_usi_usart_irqhandler>:
    }
}

void wj_usi_usart_irqhandler(int idx)
{
    wj_usi_usart_priv_t *usart_priv = &usi_usart_instance[idx];
    34ba:	03800793          	li	a5,56
    34be:	02f507b3          	mul	a5,a0,a5
    34c2:	200026b7          	lui	a3,0x20002
    34c6:	a0c68713          	addi	a4,a3,-1524 # 20001a0c <usi_usart_instance>
{
    34ca:	1101                	addi	sp,sp,-32
    34cc:	ca26                	sw	s1,20(sp)
    34ce:	cc22                	sw	s0,24(sp)
    34d0:	ce06                	sw	ra,28(sp)
    34d2:	842a                	mv	s0,a0
    wj_usi_usart_priv_t *usart_priv = &usi_usart_instance[idx];
    34d4:	97ba                	add	a5,a5,a4
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    34d6:	4384                	lw	s1,0(a5)

    uint32_t intr_state = addr->USI_INTR_STA & 0x3ffff;
    34d8:	48f8                	lw	a4,84(s1)
    34da:	c03a                	sw	a4,0(sp)
    34dc:	073a                	slli	a4,a4,0xe
    34de:	8339                	srli	a4,a4,0xe
    34e0:	c43a                	sw	a4,8(sp)

    if (intr_state & USI_INT_TX_EMPTY) {
    34e2:	4702                	lw	a4,0(sp)
    34e4:	00277613          	andi	a2,a4,2
    34e8:	a0c68713          	addi	a4,a3,-1524
    34ec:	c23a                	sw	a4,4(sp)
    34ee:	ce1d                	beqz	a2,352c <wj_usi_usart_irqhandler+0x72>
    if (usart_priv->tx_total_num == 0) {
    34f0:	4b94                	lw	a3,16(a5)
    34f2:	ca9d                	beqz	a3,3528 <wj_usi_usart_irqhandler+0x6e>
    if (usart_priv->tx_cnt >= usart_priv->tx_total_num) {
    34f4:	5390                	lw	a2,32(a5)
    34f6:	0ad66f63          	bltu	a2,a3,35b4 <wj_usi_usart_irqhandler+0xfa>
        addr->USI_INTR_EN &= (~USI_INT_TX_EMPTY);
    34fa:	48b0                	lw	a2,80(s1)
    34fc:	9a75                	andi	a2,a2,-3
    34fe:	c8b0                	sw	a2,80(s1)
        addr->USI_INTR_EN |= USI_INT_UART_STOP;
    3500:	48b0                	lw	a2,80(s1)
    3502:	40066613          	ori	a2,a2,1024
    3506:	c8b0                	sw	a2,80(s1)
        usart_priv->last_tx_num = usart_priv->tx_total_num;
    3508:	d7d4                	sw	a3,44(a5)
        if (usart_priv->cb_event) {
    350a:	4794                	lw	a3,8(a5)
        usart_priv->tx_cnt = 0;
    350c:	0207a023          	sw	zero,32(a5)
        usart_priv->tx_busy = 0;
    3510:	0207a223          	sw	zero,36(a5)
        usart_priv->tx_buf = NULL;
    3514:	0007ac23          	sw	zero,24(a5)
        usart_priv->tx_total_num = 0;
    3518:	0007a823          	sw	zero,16(a5)
        if (usart_priv->cb_event) {
    351c:	c691                	beqz	a3,3528 <wj_usi_usart_irqhandler+0x6e>
            usart_priv->cb_event(usart_priv->idx, USART_EVENT_SEND_COMPLETE);
    351e:	5bc8                	lw	a0,52(a5)
    3520:	4581                	li	a1,0
    3522:	c63e                	sw	a5,12(sp)
    3524:	9682                	jalr	a3
    3526:	47b2                	lw	a5,12(sp)
        ck_usart_intr_threshold_empty(usart_priv);
        addr->USI_INTR_CLR = USI_INT_TX_EMPTY;
    3528:	4689                	li	a3,2
    352a:	d0b4                	sw	a3,96(s1)
    }

    if (intr_state & USI_INT_RX_THOLD) {
    352c:	4702                	lw	a4,0(sp)
    352e:	02077693          	andi	a3,a4,32
    3532:	ca81                	beqz	a3,3542 <wj_usi_usart_irqhandler+0x88>
        ck_usart_intr_recv_data(usart_priv);
    3534:	853e                	mv	a0,a5
    3536:	c63e                	sw	a5,12(sp)
    3538:	3349                	jal	32ba <ck_usart_intr_recv_data>
        addr->USI_INTR_CLR = USI_INT_RX_THOLD;
    353a:	47b2                	lw	a5,12(sp)
    353c:	02000693          	li	a3,32
    3540:	d0b4                	sw	a3,96(s1)
    }

    if (intr_state & USI_INT_UART_STOP) {
    3542:	4702                	lw	a4,0(sp)
    3544:	40077693          	andi	a3,a4,1024
    3548:	c685                	beqz	a3,3570 <wj_usi_usart_irqhandler+0xb6>
        if (USI_FIFO_STA_RX_NUM(addr) > 0) {
    354a:	44d4                	lw	a3,12(s1)
    354c:	82c1                	srli	a3,a3,0x10
    354e:	8afd                	andi	a3,a3,31
    3550:	ce89                	beqz	a3,356a <wj_usi_usart_irqhandler+0xb0>
    if ((usart_priv->rx_total_num != 0) && (usart_priv->rx_buf != NULL)) {
    3552:	03800693          	li	a3,56
    3556:	02d406b3          	mul	a3,s0,a3
    355a:	4712                	lw	a4,4(sp)
    355c:	96ba                	add	a3,a3,a4
    355e:	46d0                	lw	a2,12(a3)
    3560:	ce41                	beqz	a2,35f8 <wj_usi_usart_irqhandler+0x13e>
    3562:	4ad4                	lw	a3,20(a3)
    3564:	cad1                	beqz	a3,35f8 <wj_usi_usart_irqhandler+0x13e>
        ck_usart_intr_recv_data(usart_priv);
    3566:	853e                	mv	a0,a5
    3568:	3b89                	jal	32ba <ck_usart_intr_recv_data>
            ck_usart_intr_char_timeout(usart_priv);     //receive small data
        }

        addr->USI_INTR_CLR = USI_INT_RX_THOLD;
    356a:	02000793          	li	a5,32
    356e:	d0bc                	sw	a5,96(s1)
    }

    if (intr_state & USI_INT_UART_PERR) {
    3570:	4782                	lw	a5,0(sp)
    3572:	01479713          	slli	a4,a5,0x14
    3576:	02075863          	bgez	a4,35a6 <wj_usi_usart_irqhandler+0xec>
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    357a:	03800793          	li	a5,56
    357e:	02f40433          	mul	s0,s0,a5
    3582:	4792                	lw	a5,4(sp)
    3584:	943e                	add	s0,s0,a5
    3586:	401c                	lw	a5,0(s0)
    addr->USI_EN &= ~USI_RX_FIFO_EN;
    3588:	4394                	lw	a3,0(a5)
    358a:	9add                	andi	a3,a3,-9
    358c:	c394                	sw	a3,0(a5)
    addr->USI_EN |= USI_RX_FIFO_EN;
    358e:	4394                	lw	a3,0(a5)
    3590:	0086e693          	ori	a3,a3,8
    3594:	c394                	sw	a3,0(a5)
    if (usart_priv->cb_event) {
    3596:	441c                	lw	a5,8(s0)
    3598:	c781                	beqz	a5,35a0 <wj_usi_usart_irqhandler+0xe6>
        usart_priv->cb_event(usart_priv->idx, USART_EVENT_RX_PARITY_ERROR);
    359a:	5848                	lw	a0,52(s0)
    359c:	45a5                	li	a1,9
    359e:	9782                	jalr	a5
        ck_usart_intr_recv_line(usart_priv);
        addr->USI_INTR_CLR = USI_INT_RX_THOLD;
    35a0:	02000793          	li	a5,32
    35a4:	d0bc                	sw	a5,96(s1)
    }

    addr->USI_INTR_CLR = intr_state;
    35a6:	47a2                	lw	a5,8(sp)
}
    35a8:	40f2                	lw	ra,28(sp)
    35aa:	4462                	lw	s0,24(sp)
    addr->USI_INTR_CLR = intr_state;
    35ac:	d0bc                	sw	a5,96(s1)
}
    35ae:	44d2                	lw	s1,20(sp)
    35b0:	6105                	addi	sp,sp,32
    35b2:	8082                	ret
        uint32_t remain_txdata  = usart_priv->tx_total_num - usart_priv->tx_cnt;
    35b4:	538c                	lw	a1,32(a5)
    35b6:	40b685b3          	sub	a1,a3,a1
        uint32_t txdata_num = (remain_txdata > (USI_TX_MAX_FIFO - 1)) ? (USI_TX_MAX_FIFO - 1) : remain_txdata;
    35ba:	46bd                	li	a3,15
    35bc:	00b6f363          	bgeu	a3,a1,35c2 <wj_usi_usart_irqhandler+0x108>
    35c0:	45bd                	li	a1,15
            addr->USI_TX_RX_FIFO = *((uint8_t *)usart_priv->tx_buf);
    35c2:	03800693          	li	a3,56
    35c6:	02d406b3          	mul	a3,s0,a3
    35ca:	4712                	lw	a4,4(sp)
        volatile uint32_t i = 0u;
    35cc:	c802                	sw	zero,16(sp)
        for (i = 0; i < txdata_num; i++) {
    35ce:	c802                	sw	zero,16(sp)
            addr->USI_TX_RX_FIFO = *((uint8_t *)usart_priv->tx_buf);
    35d0:	96ba                	add	a3,a3,a4
        for (i = 0; i < txdata_num; i++) {
    35d2:	4642                	lw	a2,16(sp)
    35d4:	00b66563          	bltu	a2,a1,35de <wj_usi_usart_irqhandler+0x124>
        addr->USI_INTR_CLR = USI_INT_TX_EMPTY;
    35d8:	4689                	li	a3,2
    35da:	d0b4                	sw	a3,96(s1)
    35dc:	b7b1                	j	3528 <wj_usi_usart_irqhandler+0x6e>
            addr->USI_TX_RX_FIFO = *((uint8_t *)usart_priv->tx_buf);
    35de:	4e90                	lw	a2,24(a3)
    35e0:	00064503          	lbu	a0,0(a2)
            usart_priv->tx_buf++;
    35e4:	0605                	addi	a2,a2,1
            addr->USI_TX_RX_FIFO = *((uint8_t *)usart_priv->tx_buf);
    35e6:	c488                	sw	a0,8(s1)
            usart_priv->tx_cnt++;
    35e8:	5288                	lw	a0,32(a3)
            usart_priv->tx_buf++;
    35ea:	ce90                	sw	a2,24(a3)
            usart_priv->tx_cnt++;
    35ec:	0505                	addi	a0,a0,1
    35ee:	d288                	sw	a0,32(a3)
        for (i = 0; i < txdata_num; i++) {
    35f0:	4642                	lw	a2,16(sp)
    35f2:	0605                	addi	a2,a2,1
    35f4:	c832                	sw	a2,16(sp)
    35f6:	bff1                	j	35d2 <wj_usi_usart_irqhandler+0x118>
    if (usart_priv->cb_event) {
    35f8:	03800793          	li	a5,56
    35fc:	02f407b3          	mul	a5,s0,a5
    3600:	4712                	lw	a4,4(sp)
    3602:	97ba                	add	a5,a5,a4
    3604:	4794                	lw	a3,8(a5)
    3606:	c689                	beqz	a3,3610 <wj_usi_usart_irqhandler+0x156>
        usart_priv->cb_event(usart_priv->idx, USART_EVENT_RECEIVED);
    3608:	5bc8                	lw	a0,52(a5)
    360a:	45b9                	li	a1,14
    360c:	9682                	jalr	a3
    360e:	bfb1                	j	356a <wj_usi_usart_irqhandler+0xb0>
        wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    3610:	439c                	lw	a5,0(a5)
        addr->USI_EN &= ~USI_RX_FIFO_EN;
    3612:	4394                	lw	a3,0(a5)
    3614:	9add                	andi	a3,a3,-9
    3616:	c394                	sw	a3,0(a5)
        addr->USI_EN |= USI_RX_FIFO_EN;
    3618:	4394                	lw	a3,0(a5)
    361a:	0086e693          	ori	a3,a3,8
    361e:	c394                	sw	a3,0(a5)
    3620:	b7a9                	j	356a <wj_usi_usart_irqhandler+0xb0>

00003622 <drv_usi_usart_putchar>:
  \return      error code
*/
int32_t drv_usi_usart_putchar(usart_handle_t handle, uint8_t ch)
{
    wj_usi_usart_priv_t *usart_priv = handle;
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    3622:	4118                	lw	a4,0(a0)
    //addr->USI_EN = 0xb;
    //addr->USI_EN = 0xf;
    addr->USI_TX_RX_FIFO = ch;
    3624:	c70c                	sw	a1,8(a4)

    while (!(addr->USI_FIFO_STA & 0x1));
    3626:	475c                	lw	a5,12(a4)
    3628:	8b85                	andi	a5,a5,1
    362a:	dff5                	beqz	a5,3626 <drv_usi_usart_putchar+0x4>

    return 0;
}
    362c:	4501                	li	a0,0
    362e:	8082                	ret

00003630 <drv_usi_usart_initialize>:
  \param[in]   idx usart index
  \param[in]   cb_event event call back function \ref usart_event_cb_t
  \return      return usart handle if success
*/
usart_handle_t drv_usi_usart_initialize(int32_t idx, usart_event_cb_t cb_event)
{
    3630:	1111                	addi	sp,sp,-28
    3632:	c02e                	sw	a1,0(sp)
    //initialize instace
    uint32_t base;
    uint32_t irq;
    void *handler;

    int32_t ret  = target_usi_usart_init(idx, &base, &irq, &handler);
    3634:	0074                	addi	a3,sp,12
    3636:	0030                	addi	a2,sp,8
    3638:	004c                	addi	a1,sp,4
{
    363a:	c826                	sw	s1,16(sp)
    363c:	cc06                	sw	ra,24(sp)
    363e:	ca22                	sw	s0,20(sp)
    3640:	84aa                	mv	s1,a0
    int32_t ret  = target_usi_usart_init(idx, &base, &irq, &handler);
    3642:	24b9                	jal	3890 <target_usi_usart_init>

    if (ret < 0 || ret >= CONFIG_USI_NUM) {
    3644:	4789                	li	a5,2
    3646:	00a7f963          	bgeu	a5,a0,3658 <drv_usi_usart_initialize+0x28>
        return NULL;
    364a:	4401                	li	s0,0
    addr->USI_MODE_SEL = USI_MODE_UART;
    drv_irq_register(usart_priv->irq, handler);
    drv_irq_enable(usart_priv->irq);

    return usart_priv;
}
    364c:	8522                	mv	a0,s0
    364e:	40e2                	lw	ra,24(sp)
    3650:	4452                	lw	s0,20(sp)
    3652:	44c2                	lw	s1,16(sp)
    3654:	0171                	addi	sp,sp,28
    3656:	8082                	ret
    ret = drv_usi_initialize(idx);
    3658:	8526                	mv	a0,s1
    365a:	f2aff0ef          	jal	ra,2d84 <drv_usi_initialize>
    if (ret < 0) {
    365e:	fe0546e3          	bltz	a0,364a <drv_usi_usart_initialize+0x1a>
    wj_usi_usart_priv_t *usart_priv = &usi_usart_instance[idx];
    3662:	03800413          	li	s0,56
    3666:	028487b3          	mul	a5,s1,s0
    usart_priv->irq = irq;
    366a:	4722                	lw	a4,8(sp)
    wj_usi_usart_priv_t *usart_priv = &usi_usart_instance[idx];
    366c:	20002437          	lui	s0,0x20002
    3670:	a0c40413          	addi	s0,s0,-1524 # 20001a0c <usi_usart_instance>
    wj_usi_set_rxfifo_th(addr, USI_RX_MAX_FIFO);
    3674:	45c1                	li	a1,16
    wj_usi_usart_priv_t *usart_priv = &usi_usart_instance[idx];
    3676:	943e                	add	s0,s0,a5
    usart_priv->irq = irq;
    3678:	c058                	sw	a4,4(s0)
    usart_priv->base = base;
    367a:	4792                	lw	a5,4(sp)
    usart_priv->cb_event = cb_event;
    367c:	4702                	lw	a4,0(sp)
    usart_priv->idx = idx;
    367e:	d844                	sw	s1,52(s0)
    usart_priv->base = base;
    3680:	c01c                	sw	a5,0(s0)
    usart_priv->cb_event = cb_event;
    3682:	c418                	sw	a4,8(s0)
    addr->USI_INTR_UNMASK = WJ_UART_INT_ENABLE_DEFAUL;
    3684:	6705                	lui	a4,0x1
    3686:	f9870713          	addi	a4,a4,-104 # f98 <__divdf3+0xe8>
    addr->USI_EN = 0x0;
    368a:	0007a023          	sw	zero,0(a5)
    addr->USI_INTR_UNMASK = WJ_UART_INT_ENABLE_DEFAUL;
    368e:	cff8                	sw	a4,92(a5)
    addr->USI_INTR_EN = WJ_UART_INT_ENABLE_DEFAUL;
    3690:	cbb8                	sw	a4,80(a5)
    wj_usi_set_rxfifo_th(addr, USI_RX_MAX_FIFO);
    3692:	853e                	mv	a0,a5
    3694:	c03e                	sw	a5,0(sp)
    3696:	e76ff0ef          	jal	ra,2d0c <wj_usi_set_rxfifo_th>
    addr->USI_MODE_SEL = USI_MODE_UART;
    369a:	4782                	lw	a5,0(sp)
    drv_irq_register(usart_priv->irq, handler);
    369c:	4048                	lw	a0,4(s0)
    369e:	45b2                	lw	a1,12(sp)
    addr->USI_MODE_SEL = USI_MODE_UART;
    36a0:	0007a223          	sw	zero,4(a5)
    drv_irq_register(usart_priv->irq, handler);
    36a4:	e40ff0ef          	jal	ra,2ce4 <drv_irq_register>
    drv_irq_enable(usart_priv->irq);
    36a8:	4048                	lw	a0,4(s0)
    36aa:	e0aff0ef          	jal	ra,2cb4 <drv_irq_enable>
    return usart_priv;
    36ae:	bf79                	j	364c <drv_usi_usart_initialize+0x1c>

000036b0 <drv_usi_usart_uninitialize>:
  \param[in]   handle  usart handle to operate.
  \return      error code
*/
int32_t drv_usi_usart_uninitialize(usart_handle_t handle)
{
    USART_NULL_PARAM_CHK(handle);
    36b0:	c129                	beqz	a0,36f2 <drv_usi_usart_uninitialize+0x42>
{
    36b2:	1151                	addi	sp,sp,-12
    36b4:	c222                	sw	s0,4(sp)
    36b6:	c026                	sw	s1,0(sp)
    36b8:	842a                	mv	s0,a0

    wj_usi_usart_priv_t *usart_priv = handle;
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    36ba:	4104                	lw	s1,0(a0)

    drv_irq_disable(usart_priv->irq);
    36bc:	4148                	lw	a0,4(a0)
{
    36be:	c406                	sw	ra,8(sp)
    drv_irq_disable(usart_priv->irq);
    36c0:	e0cff0ef          	jal	ra,2ccc <drv_irq_disable>
    drv_irq_unregister(usart_priv->irq);
    36c4:	4048                	lw	a0,4(s0)
    36c6:	e2eff0ef          	jal	ra,2cf4 <drv_irq_unregister>
    addr->USI_EN = 0;
    usart_priv->cb_event   = NULL;
    int32_t  ret = drv_usi_uninitialize(usart_priv->idx);
    36ca:	5848                	lw	a0,52(s0)
    addr->USI_EN = 0;
    36cc:	0004a023          	sw	zero,0(s1)
    usart_priv->cb_event   = NULL;
    36d0:	00042423          	sw	zero,8(s0)
    int32_t  ret = drv_usi_uninitialize(usart_priv->idx);
    36d4:	ef2ff0ef          	jal	ra,2dc6 <drv_usi_uninitialize>

    if (ret < 0) {
        return ERR_USART(DRV_ERROR_PARAMETER);
    }

    return 0;
    36d8:	4781                	li	a5,0
    if (ret < 0) {
    36da:	00055663          	bgez	a0,36e6 <drv_usi_usart_uninitialize+0x36>
        return ERR_USART(DRV_ERROR_PARAMETER);
    36de:	810207b7          	lui	a5,0x81020
    36e2:	08478793          	addi	a5,a5,132 # 81020084 <__heap_end+0x60ff0084>
}
    36e6:	40a2                	lw	ra,8(sp)
    36e8:	4412                	lw	s0,4(sp)
    36ea:	4482                	lw	s1,0(sp)
    36ec:	853e                	mv	a0,a5
    36ee:	0131                	addi	sp,sp,12
    36f0:	8082                	ret
        return ERR_USART(DRV_ERROR_PARAMETER);
    36f2:	810207b7          	lui	a5,0x81020
    36f6:	08478793          	addi	a5,a5,132 # 81020084 <__heap_end+0x60ff0084>
}
    36fa:	853e                	mv	a0,a5
    36fc:	8082                	ret

000036fe <drv_usi_usart_config>:
                             uint32_t baud,
                             usart_mode_e mode,
                             usart_parity_e parity,
                             usart_stop_bits_e stopbits,
                             usart_data_bits_e bits)
{
    36fe:	1111                	addi	sp,sp,-28
    3700:	c826                	sw	s1,16(sp)
    int32_t ret;
    wj_usi_usart_priv_t *usart_priv = handle;
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    3702:	4104                	lw	s1,0(a0)
{
    3704:	ca22                	sw	s0,20(sp)
    3706:	cc06                	sw	ra,24(sp)

    addr->USI_EN = 0x0;
    3708:	0004a023          	sw	zero,0(s1)
{
    370c:	842a                	mv	s0,a0
    370e:	c632                	sw	a2,12(sp)
    3710:	c436                	sw	a3,8(sp)
    3712:	c03a                	sw	a4,0(sp)
    3714:	c23e                	sw	a5,4(sp)
    /* control the data_bit of the usart*/
    ret = drv_usi_usart_config_baudrate(handle, baud);
    3716:	3129                	jal	3320 <drv_usi_usart_config_baudrate>

    if (ret < 0) {
    3718:	02054b63          	bltz	a0,374e <drv_usi_usart_config+0x50>
        return ret;
    }

    /* control the parity of the usart*/
    ret = drv_usi_usart_config_parity(handle, parity);
    371c:	46a2                	lw	a3,8(sp)
    371e:	8522                	mv	a0,s0
    3720:	85b6                	mv	a1,a3
    3722:	319d                	jal	3388 <drv_usi_usart_config_parity>

    if (ret < 0) {
    3724:	02054563          	bltz	a0,374e <drv_usi_usart_config+0x50>
        return ret;
    }

    /* control mode of the usart*/
    ret = drv_usi_usart_config_mode(handle, mode);
    3728:	4632                	lw	a2,12(sp)
    372a:	8522                	mv	a0,s0
    372c:	85b2                	mv	a1,a2
    372e:	3915                	jal	3362 <drv_usi_usart_config_mode>

    if (ret < 0) {
    3730:	00054f63          	bltz	a0,374e <drv_usi_usart_config+0x50>
        return ret;
    }

    /* control the stopbit of the usart*/
    ret = drv_usi_usart_config_stopbits(handle, stopbits);
    3734:	4582                	lw	a1,0(sp)
    3736:	8522                	mv	a0,s0
    3738:	3945                	jal	33e8 <drv_usi_usart_config_stopbits>

    if (ret < 0) {
    373a:	00054a63          	bltz	a0,374e <drv_usi_usart_config+0x50>
        return ret;
    }

    ret = drv_usi_usart_config_databits(handle, bits);
    373e:	4592                	lw	a1,4(sp)
    3740:	8522                	mv	a0,s0
    3742:	3311                	jal	3446 <drv_usi_usart_config_databits>

    if (ret < 0) {
    3744:	00054563          	bltz	a0,374e <drv_usi_usart_config+0x50>
        return ret;
    }

    addr->USI_EN = 0xf;
    3748:	47bd                	li	a5,15
    374a:	c09c                	sw	a5,0(s1)
    return 0;
    374c:	4501                	li	a0,0
}
    374e:	40e2                	lw	ra,24(sp)
    3750:	4452                	lw	s0,20(sp)
    3752:	44c2                	lw	s1,16(sp)
    3754:	0171                	addi	sp,sp,28
    3756:	8082                	ret

00003758 <drv_usi_usart_send>:
  \param[in]   num   Number of data items to send
  \return      error code
*/
int32_t drv_usi_usart_send(usart_handle_t handle, const void *data, uint32_t num)
{
    USART_NULL_PARAM_CHK(handle);
    3758:	cd05                	beqz	a0,3790 <drv_usi_usart_send+0x38>
    USART_NULL_PARAM_CHK(data);
    375a:	c99d                	beqz	a1,3790 <drv_usi_usart_send+0x38>

    if (num == 0) {
    375c:	ca15                	beqz	a2,3790 <drv_usi_usart_send+0x38>
        return ERR_USART(DRV_ERROR_PARAMETER);
    }

    wj_usi_usart_priv_t *usart_priv = handle;
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    375e:	411c                	lw	a5,0(a0)

    usart_priv->tx_buf = (uint8_t *)data;
    usart_priv->tx_total_num = num;
    usart_priv->tx_cnt = 0;
    3760:	02052023          	sw	zero,32(a0)
    usart_priv->tx_busy = 1;
    3764:	4705                	li	a4,1
    3766:	d158                	sw	a4,36(a0)
    usart_priv->last_tx_num = 0;

    /* enable the interrupt*/
    addr->USI_INTR_UNMASK   |= USI_INT_TX_EMPTY;
    3768:	4ff8                	lw	a4,92(a5)
    usart_priv->tx_buf = (uint8_t *)data;
    376a:	cd0c                	sw	a1,24(a0)
    usart_priv->tx_total_num = num;
    376c:	c910                	sw	a2,16(a0)
    usart_priv->last_tx_num = 0;
    376e:	02052623          	sw	zero,44(a0)
    addr->USI_INTR_UNMASK   |= USI_INT_TX_EMPTY;
    3772:	00276713          	ori	a4,a4,2
    3776:	cff8                	sw	a4,92(a5)
    addr->USI_INTR_EN       |= USI_INT_TX_EMPTY;
    3778:	4bb8                	lw	a4,80(a5)
    addr->USI_INTR_EN       &= ~USI_INT_UART_STOP;

    addr->USI_EN = 0xf;
    return 0;
    377a:	4501                	li	a0,0
    addr->USI_INTR_EN       |= USI_INT_TX_EMPTY;
    377c:	00276713          	ori	a4,a4,2
    3780:	cbb8                	sw	a4,80(a5)
    addr->USI_INTR_EN       &= ~USI_INT_UART_STOP;
    3782:	4bb8                	lw	a4,80(a5)
    3784:	bff77713          	andi	a4,a4,-1025
    3788:	cbb8                	sw	a4,80(a5)
    addr->USI_EN = 0xf;
    378a:	473d                	li	a4,15
    378c:	c398                	sw	a4,0(a5)
    return 0;
    378e:	8082                	ret
    USART_NULL_PARAM_CHK(handle);
    3790:	81020537          	lui	a0,0x81020
    3794:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
}
    3798:	8082                	ret

0000379a <drv_usi_usart_receive>:
  \param[in]   num   Number of data items to receive
  \return      error code
*/
int32_t drv_usi_usart_receive(usart_handle_t handle, void *data, uint32_t num)
{
    USART_NULL_PARAM_CHK(handle);
    379a:	cd01                	beqz	a0,37b2 <drv_usi_usart_receive+0x18>
    USART_NULL_PARAM_CHK(data);
    379c:	c999                	beqz	a1,37b2 <drv_usi_usart_receive+0x18>

    wj_usi_usart_priv_t *usart_priv = handle;

    usart_priv->rx_buf = (uint8_t *)data;   // Save receive buffer usart
    usart_priv->rx_total_num = num;         // Save number of data to be received
    usart_priv->rx_cnt = 0;
    379e:	00052e23          	sw	zero,28(a0)
    usart_priv->rx_busy = 1;
    37a2:	4785                	li	a5,1
    usart_priv->rx_buf = (uint8_t *)data;   // Save receive buffer usart
    37a4:	c94c                	sw	a1,20(a0)
    usart_priv->rx_total_num = num;         // Save number of data to be received
    37a6:	c550                	sw	a2,12(a0)
    usart_priv->rx_busy = 1;
    37a8:	d51c                	sw	a5,40(a0)
    usart_priv->last_rx_num = 0;
    37aa:	02052823          	sw	zero,48(a0)
    return 0;
    37ae:	4501                	li	a0,0
    37b0:	8082                	ret
    USART_NULL_PARAM_CHK(handle);
    37b2:	81020537          	lui	a0,0x81020
    37b6:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
}
    37ba:	8082                	ret

000037bc <drv_usi_usart_receive_query>:
  \param[in]   num   Number of data items to receive
  \return      fifo data num to receive
*/
int32_t drv_usi_usart_receive_query(usart_handle_t handle, void *data, uint32_t num)
{
    USART_NULL_PARAM_CHK(handle);
    37bc:	c10d                	beqz	a0,37de <drv_usi_usart_receive_query+0x22>
    USART_NULL_PARAM_CHK(data);
    37be:	c185                	beqz	a1,37de <drv_usi_usart_receive_query+0x22>

    wj_usi_usart_priv_t *usart_priv = handle;
    wj_usi_reg_t *addr = (wj_usi_reg_t *)(usart_priv->base);
    37c0:	4118                	lw	a4,0(a0)
    int32_t recv_num = 0;
    37c2:	4501                	li	a0,0
    uint8_t *dest = (uint8_t *)data;

    while ((addr->USI_FIFO_STA & 0x4) == 0) {
    37c4:	475c                	lw	a5,12(a4)
    37c6:	8b91                	andi	a5,a5,4
    37c8:	c391                	beqz	a5,37cc <drv_usi_usart_receive_query+0x10>
    37ca:	8082                	ret
        *dest++ = addr->USI_TX_RX_FIFO;
    37cc:	4714                	lw	a3,8(a4)
    37ce:	00a587b3          	add	a5,a1,a0
        recv_num++;
    37d2:	0505                	addi	a0,a0,1
        *dest++ = addr->USI_TX_RX_FIFO;
    37d4:	00d78023          	sb	a3,0(a5)

        if (recv_num >= num) {
    37d8:	fec566e3          	bltu	a0,a2,37c4 <drv_usi_usart_receive_query+0x8>
            break;
        }
    }

    return recv_num;
}
    37dc:	8082                	ret
    USART_NULL_PARAM_CHK(handle);
    37de:	81020537          	lui	a0,0x81020
    37e2:	08450513          	addi	a0,a0,132 # 81020084 <__heap_end+0x60ff0084>
    37e6:	bfdd                	j	37dc <drv_usi_usart_receive_query+0x20>

000037e8 <drv_usi_usart_get_status>:
  \brief       Get USART status.
  \param[in]   handle  usart handle to operate.
  \return      USART status \ref usart_status_t
*/
usart_status_t drv_usi_usart_get_status(usart_handle_t handle)
{
    37e8:	1171                	addi	sp,sp,-4
    if (handle == NULL) {
    37ea:	c919                	beqz	a0,3800 <drv_usi_usart_get_status+0x18>
    }

    usart_status_t usart_status = {0};
    wj_usi_usart_priv_t *usart_priv = handle;

    usart_status.tx_busy = usart_priv->tx_busy;
    37ec:	515c                	lw	a5,36(a0)
    usart_status.rx_busy = usart_priv->rx_busy;
    37ee:	5518                	lw	a4,40(a0)
    usart_status.tx_enable  = 1;
    usart_status.rx_enable  = 1;

    return usart_status;
    37f0:	0017f513          	andi	a0,a5,1
    37f4:	00177793          	andi	a5,a4,1
    37f8:	0786                	slli	a5,a5,0x1
    37fa:	8d5d                	or	a0,a0,a5
    37fc:	18056513          	ori	a0,a0,384
}
    3800:	0111                	addi	sp,sp,4
    3802:	8082                	ret

00003804 <csi_usart_putchar>:
  \param[in]   ch  the input character
  \return      error code
*/
int32_t csi_usart_putchar(usart_handle_t handle, uint8_t ch)
{
    return drv_usi_usart_putchar(handle, ch);
    3804:	bd39                	j	3622 <drv_usi_usart_putchar>

00003806 <csi_usart_initialize>:
  \param[in]   cb_event  Pointer to \ref usart_event_cb_t
  \return      return usart handle if success
*/
usart_handle_t csi_usart_initialize(int32_t idx, usart_event_cb_t cb_event)
{
    return drv_usi_usart_initialize(idx, cb_event);
    3806:	b52d                	j	3630 <drv_usi_usart_initialize>

00003808 <csi_usart_uninitialize>:
  \param[in]   handle  usart handle to operate.
  \return      error code
*/
int32_t csi_usart_uninitialize(usart_handle_t handle)
{
    return drv_usi_usart_uninitialize(handle);
    3808:	b565                	j	36b0 <drv_usi_usart_uninitialize>

0000380a <csi_usart_config>:
                         usart_mode_e mode,
                         usart_parity_e parity,
                         usart_stop_bits_e stopbits,
                         usart_data_bits_e bits)
{
    return drv_usi_usart_config(handle, baud, mode, parity, stopbits, bits);
    380a:	bdd5                	j	36fe <drv_usi_usart_config>

0000380c <csi_usart_send>:
  \param[in]   num   Number of data items to send
  \return      error code
*/
int32_t csi_usart_send(usart_handle_t handle, const void *data, uint32_t num)
{
    return drv_usi_usart_send(handle, data, num);
    380c:	b7b1                	j	3758 <drv_usi_usart_send>

0000380e <csi_usart_receive>:
  \param[in]   num   Number of data items to receive
  \return      error code
*/
int32_t csi_usart_receive(usart_handle_t handle, void *data, uint32_t num)
{
    return drv_usi_usart_receive(handle, data, num);
    380e:	b771                	j	379a <drv_usi_usart_receive>

00003810 <csi_usart_receive_query>:
  \param[in]   num   Number of data items to receive
  \return      receive fifo data num
*/
int32_t csi_usart_receive_query(usart_handle_t handle, void *data, uint32_t num)
{
    return drv_usi_usart_receive_query(handle, data, num);
    3810:	b775                	j	37bc <drv_usi_usart_receive_query>

00003812 <csi_usart_get_status>:
  \param[in]   handle  usart handle to operate.
  \return      USART status \ref usart_status_t
*/
usart_status_t csi_usart_get_status(usart_handle_t handle)
{
    return drv_usi_usart_get_status(handle);
    3812:	bfd9                	j	37e8 <drv_usi_usart_get_status>

00003814 <target_get_timer>:
    return CONFIG_TIMER_NUM;
}

int32_t target_get_timer(int32_t idx, uint32_t *base, uint32_t *irq, void **handler)
{
    if (idx >= target_get_timer_count()) {
    3814:	47bd                	li	a5,15
    3816:	04a7c163          	blt	a5,a0,3858 <target_get_timer+0x44>
        return -1;
    }

    if (base != NULL) {
    381a:	c991                	beqz	a1,382e <target_get_timer+0x1a>
        *base = sg_timer_config[idx].base;
    381c:	47b1                	li	a5,12
    381e:	02f50733          	mul	a4,a0,a5
    3822:	6799                	lui	a5,0x6
    3824:	bd078793          	addi	a5,a5,-1072 # 5bd0 <sg_timer_config>
    3828:	97ba                	add	a5,a5,a4
    382a:	439c                	lw	a5,0(a5)
    382c:	c19c                	sw	a5,0(a1)
    }

    if (irq != NULL) {
    382e:	ca11                	beqz	a2,3842 <target_get_timer+0x2e>
        *irq = sg_timer_config[idx].irq;
    3830:	47b1                	li	a5,12
    3832:	02f50733          	mul	a4,a0,a5
    3836:	6799                	lui	a5,0x6
    3838:	bd078793          	addi	a5,a5,-1072 # 5bd0 <sg_timer_config>
    383c:	97ba                	add	a5,a5,a4
    383e:	43dc                	lw	a5,4(a5)
    3840:	c21c                	sw	a5,0(a2)
    }

    if (handler != NULL) {
    3842:	ce81                	beqz	a3,385a <target_get_timer+0x46>
        *handler = sg_timer_config[idx].handler;
    3844:	47b1                	li	a5,12
    3846:	02f50733          	mul	a4,a0,a5
    384a:	6799                	lui	a5,0x6
    384c:	bd078793          	addi	a5,a5,-1072 # 5bd0 <sg_timer_config>
    3850:	97ba                	add	a5,a5,a4
    3852:	479c                	lw	a5,8(a5)
    3854:	c29c                	sw	a5,0(a3)
    3856:	8082                	ret
        return -1;
    3858:	557d                	li	a0,-1
    }

    return idx;
}
    385a:	8082                	ret

0000385c <target_usi_init>:
    {WJ_USI2_BASE, USI2_IRQn, USI2_IRQHandler},
};

int32_t target_usi_init(int32_t idx, uint32_t *base, uint32_t *irq)
{
    if (idx >= CONFIG_USI_NUM) {
    385c:	4789                	li	a5,2
    385e:	02a7c763          	blt	a5,a0,388c <target_usi_init+0x30>
        return -1;
    }

    if (base != NULL) {
    3862:	c991                	beqz	a1,3876 <target_usi_init+0x1a>
        *base = sg_usi_config[idx].base;
    3864:	47b1                	li	a5,12
    3866:	02f50733          	mul	a4,a0,a5
    386a:	6799                	lui	a5,0x6
    386c:	c9078793          	addi	a5,a5,-880 # 5c90 <sg_usi_config>
    3870:	97ba                	add	a5,a5,a4
    3872:	439c                	lw	a5,0(a5)
    3874:	c19c                	sw	a5,0(a1)
    }

    if (irq != NULL) {
    3876:	ce01                	beqz	a2,388e <target_usi_init+0x32>
        *irq = sg_usi_config[idx].irq;
    3878:	47b1                	li	a5,12
    387a:	02f50733          	mul	a4,a0,a5
    387e:	6799                	lui	a5,0x6
    3880:	c9078793          	addi	a5,a5,-880 # 5c90 <sg_usi_config>
    3884:	97ba                	add	a5,a5,a4
    3886:	43dc                	lw	a5,4(a5)
    3888:	c21c                	sw	a5,0(a2)
    388a:	8082                	ret
        return -1;
    388c:	557d                	li	a0,-1
    }

    return idx;
}
    388e:	8082                	ret

00003890 <target_usi_usart_init>:

int32_t target_usi_usart_init(int32_t idx, uint32_t *base, uint32_t *irq, void **handler)
{
    if (idx >= CONFIG_USI_SPI_NUM) {
    3890:	4789                	li	a5,2
    3892:	04a7c163          	blt	a5,a0,38d4 <target_usi_usart_init+0x44>
        return -1;
    }

    if (base != NULL) {
    3896:	c991                	beqz	a1,38aa <target_usi_usart_init+0x1a>
        *base = sg_usi_config[idx].base;
    3898:	47b1                	li	a5,12
    389a:	02f50733          	mul	a4,a0,a5
    389e:	6799                	lui	a5,0x6
    38a0:	c9078793          	addi	a5,a5,-880 # 5c90 <sg_usi_config>
    38a4:	97ba                	add	a5,a5,a4
    38a6:	439c                	lw	a5,0(a5)
    38a8:	c19c                	sw	a5,0(a1)
    }

    if (irq != NULL) {
    38aa:	ca11                	beqz	a2,38be <target_usi_usart_init+0x2e>
        *irq = sg_usi_config[idx].irq;
    38ac:	47b1                	li	a5,12
    38ae:	02f50733          	mul	a4,a0,a5
    38b2:	6799                	lui	a5,0x6
    38b4:	c9078793          	addi	a5,a5,-880 # 5c90 <sg_usi_config>
    38b8:	97ba                	add	a5,a5,a4
    38ba:	43dc                	lw	a5,4(a5)
    38bc:	c21c                	sw	a5,0(a2)
    }

    if (handler != NULL) {
    38be:	ce81                	beqz	a3,38d6 <target_usi_usart_init+0x46>
        *handler = sg_usi_config[idx].handler;
    38c0:	47b1                	li	a5,12
    38c2:	02f50733          	mul	a4,a0,a5
    38c6:	6799                	lui	a5,0x6
    38c8:	c9078793          	addi	a5,a5,-880 # 5c90 <sg_usi_config>
    38cc:	97ba                	add	a5,a5,a4
    38ce:	479c                	lw	a5,8(a5)
    38d0:	c29c                	sw	a5,0(a3)
    38d2:	8082                	ret
        return -1;
    38d4:	557d                	li	a0,-1
    }

    return idx;
}
    38d6:	8082                	ret

000038d8 <csi_gpio_pin_write>:
  \param[in]   value     the value to be set
  \return      error code
*/
int32_t csi_gpio_pin_write(gpio_pin_handle_t handle, bool value)
{
    GPIO_NULL_PARAM_CHK(handle);
    38d8:	cd05                	beqz	a0,3910 <csi_gpio_pin_write+0x38>

    wj_oip_gpio_pin_priv_t *gpio_pin_priv = handle;

    /* convert portidx to port handle */
    wj_oip_gpio_priv_t *port_handle = &gpio_handle[gpio_pin_priv->portidx];
    38da:	00054703          	lbu	a4,0(a0)

    uint8_t offset = gpio_pin_priv->idx;
    uint32_t port_value = value << offset;

    port_handle->value = port_value;
    38de:	46f1                	li	a3,28
    uint32_t port_value = value << offset;
    38e0:	00154603          	lbu	a2,1(a0)
    port_handle->value = port_value;
    38e4:	02d706b3          	mul	a3,a4,a3
    38e8:	20002737          	lui	a4,0x20002
    38ec:	ab470713          	addi	a4,a4,-1356 # 20001ab4 <gpio_handle>
    uint32_t port_value = value << offset;
    38f0:	00c595b3          	sll	a1,a1,a2
    gpio_write(port_handle, (1 << offset));
    38f4:	4785                	li	a5,1
    38f6:	00c797b3          	sll	a5,a5,a2
    value &= ~(mask);
    38fa:	fff7c793          	not	a5,a5

    return 0;
    38fe:	4501                	li	a0,0
    port_handle->value = port_value;
    3900:	9736                	add	a4,a4,a3
    3902:	cf0c                	sw	a1,24(a4)
    wj_oip_gpio_reg_t *gpio_reg = (wj_oip_gpio_reg_t *)(gpio_priv->base);
    3904:	4318                	lw	a4,0(a4)
    uint32_t value = gpio_reg->SWPORT_DR;
    3906:	4314                	lw	a3,0(a4)
    value &= ~(mask);
    3908:	8ff5                	and	a5,a5,a3
    value |= gpio_priv->value;
    390a:	8ddd                	or	a1,a1,a5
    gpio_reg->SWPORT_DR = value;
    390c:	c30c                	sw	a1,0(a4)
    return 0;
    390e:	8082                	ret
    GPIO_NULL_PARAM_CHK(handle);
    3910:	81010537          	lui	a0,0x81010
    3914:	08450513          	addi	a0,a0,132 # 81010084 <__heap_end+0x60fe0084>

}
    3918:	8082                	ret

0000391a <wj_oip_timer_irqhandler>:
}

void wj_oip_timer_irqhandler(int idx)
{
    wj_oip_timer_priv_t *timer_priv = &timer_instance[idx];
    timer_priv->timeout_flag = 1;
    391a:	47e1                	li	a5,24
    391c:	02f50733          	mul	a4,a0,a5
    3920:	200027b7          	lui	a5,0x20002
    3924:	ad078793          	addi	a5,a5,-1328 # 20001ad0 <timer_instance>
    3928:	97ba                	add	a5,a5,a4
    392a:	4705                	li	a4,1
    392c:	cbd8                	sw	a4,20(a5)

    wj_oip_timer_reg_t *addr = (wj_oip_timer_reg_t *)(timer_priv->base);
    392e:	43d8                	lw	a4,4(a5)

    addr->TxEOI;

    if (timer_priv->cb_event) {
    3930:	00c7a303          	lw	t1,12(a5)
    addr->TxEOI;
    3934:	4758                	lw	a4,12(a4)
    if (timer_priv->cb_event) {
    3936:	00030463          	beqz	t1,393e <wj_oip_timer_irqhandler+0x24>
        return timer_priv->cb_event(idx, TIMER_EVENT_TIMEOUT);
    393a:	4581                	li	a1,0
    393c:	8302                	jr	t1
    }

}
    393e:	8082                	ret

00003940 <csi_timer_initialize>:
  \param[in]   idx  instance timer index
  \param[in]   cb_event  Pointer to \ref timer_event_cb_t
  \return      pointer to timer instance
*/
timer_handle_t csi_timer_initialize(int32_t idx, timer_event_cb_t cb_event)
{
    3940:	1111                	addi	sp,sp,-28
    3942:	cc06                	sw	ra,24(sp)
    3944:	ca22                	sw	s0,20(sp)
    3946:	c826                	sw	s1,16(sp)
    if (idx < 0 || idx >= CONFIG_TIMER_NUM) {
    3948:	47bd                	li	a5,15
    394a:	00a7f963          	bgeu	a5,a0,395c <csi_timer_initialize+0x1c>
        return NULL;
    394e:	4401                	li	s0,0
        drv_irq_register(timer_priv->irq, handler);
        drv_irq_enable(timer_priv->irq);
    }

    return (timer_handle_t)timer_priv;
}
    3950:	8522                	mv	a0,s0
    3952:	40e2                	lw	ra,24(sp)
    3954:	4452                	lw	s0,20(sp)
    3956:	44c2                	lw	s1,16(sp)
    3958:	0171                	addi	sp,sp,28
    395a:	8082                	ret
    395c:	84ae                	mv	s1,a1
    int32_t real_idx = target_get_timer(idx, &base, &irq, &handler);
    395e:	0074                	addi	a3,sp,12
    3960:	0030                	addi	a2,sp,8
    3962:	004c                	addi	a1,sp,4
    3964:	c02a                	sw	a0,0(sp)
    uint32_t base = 0u;
    3966:	c202                	sw	zero,4(sp)
    uint32_t irq = 0u;
    3968:	c402                	sw	zero,8(sp)
    int32_t real_idx = target_get_timer(idx, &base, &irq, &handler);
    396a:	356d                	jal	3814 <target_get_timer>
    if (real_idx != idx) {
    396c:	4702                	lw	a4,0(sp)
    396e:	fea710e3          	bne	a4,a0,394e <csi_timer_initialize+0xe>
    wj_oip_timer_priv_t *timer_priv = &timer_instance[idx];
    3972:	4461                	li	s0,24
    3974:	028707b3          	mul	a5,a4,s0
    3978:	20002437          	lui	s0,0x20002
    397c:	ad040413          	addi	s0,s0,-1328 # 20001ad0 <timer_instance>
    timer_priv->irq  = irq;
    3980:	46a2                	lw	a3,8(sp)
    timer_priv->timeout = WJ_OIP_TIMER_INIT_DEFAULT_VALUE;
    3982:	4501                	li	a0,0
    wj_oip_timer_priv_t *timer_priv = &timer_instance[idx];
    3984:	943e                	add	s0,s0,a5
    timer_priv->base = base;
    3986:	4792                	lw	a5,4(sp)
    timer_priv->irq  = irq;
    3988:	c414                	sw	a3,8(s0)
    timer_priv->idx = idx;
    398a:	00e40023          	sb	a4,0(s0)
    timer_priv->base = base;
    398e:	c05c                	sw	a5,4(s0)
    3990:	c03e                	sw	a5,0(sp)
    timer_priv->timeout = WJ_OIP_TIMER_INIT_DEFAULT_VALUE;
    3992:	2ae9                	jal	3b6c <drv_get_timer_freq>
    3994:	577d                	li	a4,-1
    3996:	02a75533          	divu	a0,a4,a0
    399a:	000f4737          	lui	a4,0xf4
    399e:	24070713          	addi	a4,a4,576 # f4240 <__ctor_end__+0xed918>
    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_ENABLE;
    39a2:	4782                	lw	a5,0(sp)
    timer_priv->timeout = WJ_OIP_TIMER_INIT_DEFAULT_VALUE;
    39a4:	02e50533          	mul	a0,a0,a4
    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_ENABLE;
    39a8:	4798                	lw	a4,8(a5)
    39aa:	01e77693          	andi	a3,a4,30
    39ae:	0087c703          	lbu	a4,8(a5)
    39b2:	9b01                	andi	a4,a4,-32
    39b4:	8f55                	or	a4,a4,a3
    timer_priv->timeout = WJ_OIP_TIMER_INIT_DEFAULT_VALUE;
    39b6:	c808                	sw	a0,16(s0)
    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_ENABLE;
    39b8:	00e78423          	sb	a4,8(a5)
    addr->TxControl |= WJ_OIP_TIMER_TXCONTROL_INTMASK;
    39bc:	4798                	lw	a4,8(a5)
    39be:	8b7d                	andi	a4,a4,31
    39c0:	00476693          	ori	a3,a4,4
    39c4:	0087c703          	lbu	a4,8(a5)
    39c8:	9b01                	andi	a4,a4,-32
    39ca:	8f55                	or	a4,a4,a3
    39cc:	00e78423          	sb	a4,8(a5)
    timer_priv->cb_event = cb_event;
    39d0:	c444                	sw	s1,12(s0)
    if (cb_event != NULL) {
    39d2:	dcbd                	beqz	s1,3950 <csi_timer_initialize+0x10>
        drv_irq_register(timer_priv->irq, handler);
    39d4:	4408                	lw	a0,8(s0)
    39d6:	45b2                	lw	a1,12(sp)
    39d8:	b0cff0ef          	jal	ra,2ce4 <drv_irq_register>
        drv_irq_enable(timer_priv->irq);
    39dc:	4408                	lw	a0,8(s0)
    39de:	ad6ff0ef          	jal	ra,2cb4 <drv_irq_enable>
    39e2:	b7bd                	j	3950 <csi_timer_initialize+0x10>

000039e4 <csi_timer_config>:
  \param[in]   mode      \ref timer_mode_e
  \return      error code
*/
int32_t csi_timer_config(timer_handle_t handle, timer_mode_e mode)
{
    TIMER_NULL_PARAM_CHK(handle);
    39e4:	c511                	beqz	a0,39f0 <csi_timer_config+0xc>

    wj_oip_timer_priv_t *timer_priv = handle;
    wj_oip_timer_reg_t *addr = (wj_oip_timer_reg_t *)(timer_priv->base);
    39e6:	415c                	lw	a5,4(a0)

    switch (mode) {
    39e8:	c989                	beqz	a1,39fa <csi_timer_config+0x16>
    39ea:	4705                	li	a4,1
    39ec:	02e58c63          	beq	a1,a4,3a24 <csi_timer_config+0x40>
    TIMER_NULL_PARAM_CHK(handle);
    39f0:	81070537          	lui	a0,0x81070
    39f4:	08450513          	addi	a0,a0,132 # 81070084 <__heap_end+0x61040084>
    }

    addr->TxControl |= (WJ_OIP_TIMER_TXCONTROL_TRIGGER);

    return 0;
}
    39f8:	8082                	ret
            addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_MODE;
    39fa:	4798                	lw	a4,8(a5)
    39fc:	01d77693          	andi	a3,a4,29
            addr->TxControl |= WJ_OIP_TIMER_TXCONTROL_MODE;
    3a00:	0087c703          	lbu	a4,8(a5)
    return 0;
    3a04:	4501                	li	a0,0
            addr->TxControl |= WJ_OIP_TIMER_TXCONTROL_MODE;
    3a06:	9b01                	andi	a4,a4,-32
    3a08:	8f55                	or	a4,a4,a3
    3a0a:	00e78423          	sb	a4,8(a5)
    addr->TxControl |= (WJ_OIP_TIMER_TXCONTROL_TRIGGER);
    3a0e:	4798                	lw	a4,8(a5)
    3a10:	8b7d                	andi	a4,a4,31
    3a12:	01076693          	ori	a3,a4,16
    3a16:	0087c703          	lbu	a4,8(a5)
    3a1a:	9b01                	andi	a4,a4,-32
    3a1c:	8f55                	or	a4,a4,a3
    3a1e:	00e78423          	sb	a4,8(a5)
    return 0;
    3a22:	8082                	ret
            addr->TxControl |= WJ_OIP_TIMER_TXCONTROL_MODE;
    3a24:	4798                	lw	a4,8(a5)
    3a26:	8b7d                	andi	a4,a4,31
    3a28:	00276693          	ori	a3,a4,2
    3a2c:	bfd1                	j	3a00 <csi_timer_config+0x1c>

00003a2e <csi_timer_set_timeout>:
  \param[in]   timeout the timeout value in microseconds(us).
  \return      error code
*/
int32_t csi_timer_set_timeout(timer_handle_t handle, uint32_t timeout)
{
    TIMER_NULL_PARAM_CHK(handle);
    3a2e:	c501                	beqz	a0,3a36 <csi_timer_set_timeout+0x8>

    wj_oip_timer_priv_t *timer_priv = handle;
    timer_priv->timeout = timeout;
    3a30:	c90c                	sw	a1,16(a0)
    return 0;
    3a32:	4501                	li	a0,0
    3a34:	8082                	ret
    TIMER_NULL_PARAM_CHK(handle);
    3a36:	81070537          	lui	a0,0x81070
    3a3a:	08450513          	addi	a0,a0,132 # 81070084 <__heap_end+0x61040084>
}
    3a3e:	8082                	ret

00003a40 <csi_timer_start>:
  \return      error code
*/

int32_t csi_timer_start(timer_handle_t handle)
{
    TIMER_NULL_PARAM_CHK(handle);
    3a40:	c545                	beqz	a0,3ae8 <csi_timer_start+0xa8>
{
    3a42:	1141                	addi	sp,sp,-16
    3a44:	c606                	sw	ra,12(sp)
    3a46:	c422                	sw	s0,8(sp)
    3a48:	c226                	sw	s1,4(sp)
    3a4a:	842a                	mv	s0,a0

    wj_oip_timer_priv_t *timer_priv = handle;

    timer_priv->timeout_flag = 0;
    3a4c:	00052a23          	sw	zero,20(a0)

    uint32_t min_us = drv_get_timer_freq(timer_priv->idx) / 1000000;
    3a50:	00054503          	lbu	a0,0(a0)
    3a54:	000f44b7          	lui	s1,0xf4
    3a58:	24048493          	addi	s1,s1,576 # f4240 <__ctor_end__+0xed918>
    3a5c:	2a01                	jal	3b6c <drv_get_timer_freq>
    3a5e:	029544b3          	div	s1,a0,s1
    uint32_t load;

    if (((timer_priv->timeout * drv_get_timer_freq(timer_priv->idx)) / 1000000) > 0xffffffff) {
    3a62:	00044503          	lbu	a0,0(s0)
    3a66:	2219                	jal	3b6c <drv_get_timer_freq>
        return ERR_TIMER(DRV_ERROR_PARAMETER);
    }

    if (min_us) {
    3a68:	481c                	lw	a5,16(s0)
        load = (uint32_t)(timer_priv->timeout * min_us);
    3a6a:	02f48533          	mul	a0,s1,a5
    if (min_us) {
    3a6e:	e08d                	bnez	s1,3a90 <csi_timer_start+0x50>
    } else {
        load = (uint32_t)(((uint64_t)(timer_priv->timeout) * drv_get_timer_freq(timer_priv->idx)) / 1000000);
    3a70:	00044503          	lbu	a0,0(s0)
    3a74:	c03e                	sw	a5,0(sp)
    3a76:	28dd                	jal	3b6c <drv_get_timer_freq>
    3a78:	4782                	lw	a5,0(sp)
    3a7a:	000f4637          	lui	a2,0xf4
    3a7e:	24060613          	addi	a2,a2,576 # f4240 <__ctor_end__+0xed918>
    3a82:	4681                	li	a3,0
    3a84:	02f525b3          	mulhsu	a1,a0,a5
    3a88:	02a78533          	mul	a0,a5,a0
    3a8c:	f68fc0ef          	jal	ra,1f4 <__udivdi3>
    }

    wj_oip_timer_reg_t *addr = (wj_oip_timer_reg_t *)(timer_priv->base);

    if (timer_priv->timeout == 0) {
    3a90:	4818                	lw	a4,16(s0)
    wj_oip_timer_reg_t *addr = (wj_oip_timer_reg_t *)(timer_priv->base);
    3a92:	405c                	lw	a5,4(s0)
    if (timer_priv->timeout == 0) {
    3a94:	e729                	bnez	a4,3ade <csi_timer_start+0x9e>
        addr->TxLoadCount = 0xffffffff;                           /* load time(us) */
    } else {
        if ((addr->TxControl | 0x2) == 0x2) {
            addr->TxLoadCount = 0xffffffff;                           /* load time(us) */
    3a96:	577d                	li	a4,-1
    3a98:	c398                	sw	a4,0(a5)
        } else {
            addr->TxLoadCount = load;                           /* load time(us) */
        }
    }

    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_ENABLE;      /* disable the timer */
    3a9a:	4798                	lw	a4,8(a5)
    }

#endif

    return 0;
}
    3a9c:	40b2                	lw	ra,12(sp)
    3a9e:	4422                	lw	s0,8(sp)
    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_ENABLE;      /* disable the timer */
    3aa0:	01e77693          	andi	a3,a4,30
    3aa4:	0087c703          	lbu	a4,8(a5)
}
    3aa8:	4492                	lw	s1,4(sp)
    return 0;
    3aaa:	4501                	li	a0,0
    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_ENABLE;      /* disable the timer */
    3aac:	9b01                	andi	a4,a4,-32
    3aae:	8f55                	or	a4,a4,a3
    3ab0:	00e78423          	sb	a4,8(a5)
    addr->TxControl |= WJ_OIP_TIMER_TXCONTROL_ENABLE;       /* enable the corresponding timer */
    3ab4:	4798                	lw	a4,8(a5)
    3ab6:	8b7d                	andi	a4,a4,31
    3ab8:	00176693          	ori	a3,a4,1
    3abc:	0087c703          	lbu	a4,8(a5)
    3ac0:	9b01                	andi	a4,a4,-32
    3ac2:	8f55                	or	a4,a4,a3
    3ac4:	00e78423          	sb	a4,8(a5)
    addr->TxControl &= ~WJ_OIP_TIMER_TXCONTROL_INTMASK;     /* enable interrupt */
    3ac8:	4798                	lw	a4,8(a5)
    3aca:	01b77693          	andi	a3,a4,27
    3ace:	0087c703          	lbu	a4,8(a5)
    3ad2:	9b01                	andi	a4,a4,-32
    3ad4:	8f55                	or	a4,a4,a3
    3ad6:	00e78423          	sb	a4,8(a5)
}
    3ada:	0141                	addi	sp,sp,16
    3adc:	8082                	ret
        if ((addr->TxControl | 0x2) == 0x2) {
    3ade:	4798                	lw	a4,8(a5)
    3ae0:	8b75                	andi	a4,a4,29
    3ae2:	db55                	beqz	a4,3a96 <csi_timer_start+0x56>
            addr->TxLoadCount = load;                           /* load time(us) */
    3ae4:	c388                	sw	a0,0(a5)
    3ae6:	bf55                	j	3a9a <csi_timer_start+0x5a>
    TIMER_NULL_PARAM_CHK(handle);
    3ae8:	81070537          	lui	a0,0x81070
    3aec:	08450513          	addi	a0,a0,132 # 81070084 <__heap_end+0x61040084>
}
    3af0:	8082                	ret

00003af2 <csi_timer_get_current_value>:
  \param[out]   value     timer current value
  \return      error code
*/
int32_t csi_timer_get_current_value(timer_handle_t handle, uint32_t *value)
{
    TIMER_NULL_PARAM_CHK(handle);
    3af2:	c519                	beqz	a0,3b00 <csi_timer_get_current_value+0xe>
    TIMER_NULL_PARAM_CHK(value);
    3af4:	c591                	beqz	a1,3b00 <csi_timer_get_current_value+0xe>

    wj_oip_timer_priv_t *timer_priv = handle;
    wj_oip_timer_reg_t *addr = (wj_oip_timer_reg_t *)(timer_priv->base);
    3af6:	415c                	lw	a5,4(a0)

    *value = addr->TxCurrentValue;
    return 0;
    3af8:	4501                	li	a0,0
    *value = addr->TxCurrentValue;
    3afa:	43dc                	lw	a5,4(a5)
    3afc:	c19c                	sw	a5,0(a1)
    return 0;
    3afe:	8082                	ret
    TIMER_NULL_PARAM_CHK(handle);
    3b00:	81070537          	lui	a0,0x81070
    3b04:	08450513          	addi	a0,a0,132 # 81070084 <__heap_end+0x61040084>
}
    3b08:	8082                	ret

00003b0a <TIM0_IRQHandler>:
}

ATTRIBUTE_ISR void TIM0_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(0);
    3b0a:	4501                	li	a0,0
    3b0c:	b539                	j	391a <wj_oip_timer_irqhandler>

00003b0e <TIM1_IRQHandler>:
}

ATTRIBUTE_ISR void TIM1_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(1);
    3b0e:	4505                	li	a0,1
    3b10:	b529                	j	391a <wj_oip_timer_irqhandler>

00003b12 <TIM2_IRQHandler>:
    CSI_INTRPT_EXIT();
}
ATTRIBUTE_ISR void TIM2_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(2);
    3b12:	4509                	li	a0,2
    3b14:	b519                	j	391a <wj_oip_timer_irqhandler>

00003b16 <TIM3_IRQHandler>:
}

ATTRIBUTE_ISR void TIM3_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(3);
    3b16:	450d                	li	a0,3
    3b18:	b509                	j	391a <wj_oip_timer_irqhandler>

00003b1a <TIM4_IRQHandler>:
}

ATTRIBUTE_ISR void TIM4_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(4);
    3b1a:	4511                	li	a0,4
    3b1c:	bbfd                	j	391a <wj_oip_timer_irqhandler>

00003b1e <TIM5_IRQHandler>:
}

ATTRIBUTE_ISR void TIM5_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(5);
    3b1e:	4515                	li	a0,5
    3b20:	bbed                	j	391a <wj_oip_timer_irqhandler>

00003b22 <TIM6_IRQHandler>:
}

ATTRIBUTE_ISR void TIM6_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(6);
    3b22:	4519                	li	a0,6
    3b24:	bbdd                	j	391a <wj_oip_timer_irqhandler>

00003b26 <TIM7_IRQHandler>:
}

ATTRIBUTE_ISR void TIM7_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(7);
    3b26:	451d                	li	a0,7
    3b28:	bbcd                	j	391a <wj_oip_timer_irqhandler>

00003b2a <TIM8_IRQHandler>:
}

ATTRIBUTE_ISR void TIM8_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(8);
    3b2a:	4521                	li	a0,8
    3b2c:	b3fd                	j	391a <wj_oip_timer_irqhandler>

00003b2e <TIM9_IRQHandler>:
}

ATTRIBUTE_ISR void TIM9_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(9);
    3b2e:	4525                	li	a0,9
    3b30:	b3ed                	j	391a <wj_oip_timer_irqhandler>

00003b32 <TIM10_IRQHandler>:
}

ATTRIBUTE_ISR void TIM10_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(10);
    3b32:	4529                	li	a0,10
    3b34:	b3dd                	j	391a <wj_oip_timer_irqhandler>

00003b36 <TIM11_IRQHandler>:
}

ATTRIBUTE_ISR void TIM11_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(11);
    3b36:	452d                	li	a0,11
    3b38:	b3cd                	j	391a <wj_oip_timer_irqhandler>

00003b3a <TIM12_IRQHandler>:
}

ATTRIBUTE_ISR void TIM12_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(12);
    3b3a:	4531                	li	a0,12
    3b3c:	bbf9                	j	391a <wj_oip_timer_irqhandler>

00003b3e <TIM13_IRQHandler>:
}

ATTRIBUTE_ISR void TIM13_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(13);
    3b3e:	4535                	li	a0,13
    3b40:	bbe9                	j	391a <wj_oip_timer_irqhandler>

00003b42 <TIM14_IRQHandler>:
}

ATTRIBUTE_ISR void TIM14_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(14);
    3b42:	4539                	li	a0,14
    3b44:	bbd9                	j	391a <wj_oip_timer_irqhandler>

00003b46 <TIM15_IRQHandler>:
}

ATTRIBUTE_ISR void TIM15_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_oip_timer_irqhandler(15);
    3b46:	453d                	li	a0,15
    3b48:	bbc9                	j	391a <wj_oip_timer_irqhandler>

00003b4a <USI0_IRQHandler>:
}

ATTRIBUTE_ISR void USI0_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_usi_irqhandler(0);
    3b4a:	4501                	li	a0,0
    3b4c:	a0eff06f          	j	2d5a <wj_usi_irqhandler>

00003b50 <USI1_IRQHandler>:
}

ATTRIBUTE_ISR void USI1_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_usi_irqhandler(1);
    3b50:	4505                	li	a0,1
    3b52:	a08ff06f          	j	2d5a <wj_usi_irqhandler>

00003b56 <USI2_IRQHandler>:
}

ATTRIBUTE_ISR void USI2_IRQHandler(void)
{
    CSI_INTRPT_ENTER();
    wj_usi_irqhandler(2);
    3b56:	4509                	li	a0,2
    3b58:	a02ff06f          	j	2d5a <wj_usi_irqhandler>

00003b5c <drv_pinmux_config>:
#include <drv_gpio.h>

int32_t drv_pinmux_config(pin_name_e pin, pin_func_e pin_func)
{
    return 0;
}
    3b5c:	4501                	li	a0,0
    3b5e:	8082                	ret

00003b60 <drv_get_usi_freq>:
    3b60:	fe81a503          	lw	a0,-24(gp) # 2000065c <g_system_clock>
    3b64:	8082                	ret

00003b66 <drv_get_sys_freq>:
}

int32_t drv_get_sys_freq(void)
{
    return g_system_clock;
}
    3b66:	fe81a503          	lw	a0,-24(gp) # 2000065c <g_system_clock>
    3b6a:	8082                	ret

00003b6c <drv_get_timer_freq>:
    3b6c:	fe81a503          	lw	a0,-24(gp) # 2000065c <g_system_clock>
    3b70:	8082                	ret

00003b72 <SystemInit>:
  */
void SystemInit(void)
{
    int i;

    CLIC->CLICCFG = 0x4UL;
    3b72:	e000e7b7          	lui	a5,0xe000e
{
    3b76:	1151                	addi	sp,sp,-12
    CLIC->CLICCFG = 0x4UL;
    3b78:	10078793          	addi	a5,a5,256 # e000e100 <__heap_end+0xbffde100>
    3b7c:	6705                	lui	a4,0x1
{
    3b7e:	c222                	sw	s0,4(sp)
    CLIC->CLICCFG = 0x4UL;
    3b80:	973e                	add	a4,a4,a5
    3b82:	4691                	li	a3,4
{
    3b84:	c406                	sw	ra,8(sp)
    CLIC->CLICCFG = 0x4UL;
    3b86:	b0d70023          	sb	a3,-1280(a4) # b00 <__adddf3+0x24a>

    for (i = 0; i < 12; i++) {
        CLIC->INTIP[i] = 0;
    3b8a:	e000e437          	lui	s0,0xe000e
    for (i = 0; i < 12; i++) {
    3b8e:	4701                	li	a4,0
    3b90:	46b1                	li	a3,12
        CLIC->INTIP[i] = 0;
    3b92:	00e78633          	add	a2,a5,a4
    3b96:	00060023          	sb	zero,0(a2)
    for (i = 0; i < 12; i++) {
    3b9a:	0705                	addi	a4,a4,1
    3b9c:	fed71be3          	bne	a4,a3,3b92 <SystemInit+0x20>
    }

    drv_irq_enable(Machine_Software_IRQn);
    3ba0:	450d                	li	a0,3
    3ba2:	912ff0ef          	jal	ra,2cb4 <drv_irq_enable>
  \details Enables IRQ interrupts by setting the IE-bit in the PSR.
           Can only be executed in Privileged modes.
 */
__ALWAYS_STATIC_INLINE void __enable_irq(void)
{
    __ASM volatile("csrs mstatus, 8");
    3ba6:	30046073          	csrsi	mstatus,8
    csi_coret_config(drv_get_sys_freq() / CONFIG_SYSTICK_HZ, CORET_IRQn);    //10ms
    3baa:	3f75                	jal	3b66 <drv_get_sys_freq>
    3bac:	06400793          	li	a5,100
    3bb0:	02f54533          	div	a0,a0,a5
           function <b>SysTick_Config</b> is not included. In this case, the file <b><i>device</i>.h</b>
           must contain a vendor-specific implementation of this function.
 */
__STATIC_INLINE uint32_t csi_coret_config(uint32_t ticks, int32_t IRQn)
{
    if ((ticks - 1UL) > CORET_LOAD_RELOAD_Msk) {
    3bb4:	010007b7          	lui	a5,0x1000
    3bb8:	157d                	addi	a0,a0,-1
    3bba:	00f57763          	bgeu	a0,a5,3bc8 <SystemInit+0x56>
        return (1UL);                                                   /* Reload value impossible */
    }

    CORET->LOAD = (uint32_t)(ticks - 1UL);                              /* set reload register */
    3bbe:	c848                	sw	a0,20(s0)
    CORET->VAL  = 0UL;                                                  /* Load the CORET Counter Value */
    3bc0:	00042c23          	sw	zero,24(s0) # e000e018 <__heap_end+0xbffde018>
    CORET->CTRL = CORET_CTRL_CLKSOURCE_Msk |
    3bc4:	479d                	li	a5,7
    3bc6:	c81c                	sw	a5,16(s0)
#ifdef CONFIG_KERNEL_NONE
    _system_init_for_baremetal();
#else
    _system_init_for_kernel();
#endif
}
    3bc8:	4412                	lw	s0,4(sp)
    3bca:	40a2                	lw	ra,8(sp)
    3bcc:	0131                	addi	sp,sp,12
    mm_heap_initialize();
    3bce:	ac4d                	j	3e80 <mm_heap_initialize>

00003bd0 <trap_c>:
#include <csi_core.h>

void (*trap_c_callback)(void);

void trap_c(uint32_t *regs)
{
    3bd0:	1131                	addi	sp,sp,-20
    3bd2:	c622                	sw	s0,12(sp)
    3bd4:	c806                	sw	ra,16(sp)
    3bd6:	c426                	sw	s1,8(sp)
    3bd8:	842a                	mv	s0,a0
 */
__ALWAYS_STATIC_INLINE uint32_t __get_MCAUSE(void)
{
    uint32_t result;

    __ASM volatile("csrr %0, mcause" : "=r"(result));
    3bda:	342025f3          	csrr	a1,mcause
    int i;
    uint32_t vec = 0;

    vec = __get_MCAUSE() & 0x3FF;

    printf("CPU Exception: NO.%d", vec);
    3bde:	00006537          	lui	a0,0x6
    3be2:	3ff5f593          	andi	a1,a1,1023
    3be6:	cb450513          	addi	a0,a0,-844 # 5cb4 <sg_usi_config+0x24>
    3bea:	160010ef          	jal	ra,4d4a <printf>
    printf("\n");
    3bee:	4529                	li	a0,10
    3bf0:	272010ef          	jal	ra,4e62 <putchar>

    for (i = 0; i < 15; i++) {
    3bf4:	84a2                	mv	s1,s0
    3bf6:	4781                	li	a5,0
        printf("x%d: %08x\t", i + 1, regs[i]);
    3bf8:	4090                	lw	a2,0(s1)
    3bfa:	00178713          	addi	a4,a5,1 # 1000001 <__ctor_end__+0xff96d9>
    3bfe:	c23e                	sw	a5,4(sp)
    3c00:	000067b7          	lui	a5,0x6
    3c04:	ccc78513          	addi	a0,a5,-820 # 5ccc <sg_usi_config+0x3c>
    3c08:	85ba                	mv	a1,a4
    3c0a:	c03a                	sw	a4,0(sp)
    3c0c:	13e010ef          	jal	ra,4d4a <printf>

        if ((i % 4) == 3) {
    3c10:	4792                	lw	a5,4(sp)
    3c12:	468d                	li	a3,3
    3c14:	8b8d                	andi	a5,a5,3
    3c16:	00d79563          	bne	a5,a3,3c20 <trap_c+0x50>
            printf("\n");
    3c1a:	4529                	li	a0,10
    3c1c:	246010ef          	jal	ra,4e62 <putchar>
    3c20:	4782                	lw	a5,0(sp)
    for (i = 0; i < 15; i++) {
    3c22:	46bd                	li	a3,15
    3c24:	0491                	addi	s1,s1,4
    3c26:	fcd799e3          	bne	a5,a3,3bf8 <trap_c+0x28>
        }
    }

    printf("\n");
    3c2a:	4529                	li	a0,10
    3c2c:	236010ef          	jal	ra,4e62 <putchar>
    printf("mepc   : %08x\n", regs[15]);
    3c30:	5c4c                	lw	a1,60(s0)
    3c32:	00006537          	lui	a0,0x6
    3c36:	cd850513          	addi	a0,a0,-808 # 5cd8 <sg_usi_config+0x48>
    3c3a:	110010ef          	jal	ra,4d4a <printf>
    printf("mstatus: %08x\n", regs[16]);
    3c3e:	402c                	lw	a1,64(s0)
    3c40:	00006537          	lui	a0,0x6
    3c44:	ce850513          	addi	a0,a0,-792 # 5ce8 <sg_usi_config+0x58>
    3c48:	102010ef          	jal	ra,4d4a <printf>

    if (trap_c_callback) {
    3c4c:	200027b7          	lui	a5,0x20002
    3c50:	d307a783          	lw	a5,-720(a5) # 20001d30 <trap_c_callback>
    3c54:	c391                	beqz	a5,3c58 <trap_c+0x88>
        trap_c_callback();
    3c56:	9782                	jalr	a5
    }

    while (1);
    3c58:	a001                	j	3c58 <trap_c+0x88>

00003c5a <timer_cb_fun>:
static unsigned int Timer_LoopCount;    /* Count unit is 10 seconds */
static uint8_t timer_count_rise = 0;    /*1: timer cont increasing, 0: timer cont diminishing*/

static void timer_cb_fun(int32_t idx, timer_event_e event)
{
    if (TIMER_EVENT_TIMEOUT == event) {
    3c5a:	e981                	bnez	a1,3c6a <timer_cb_fun+0x10>
        Timer_LoopCount++;
    3c5c:	200027b7          	lui	a5,0x20002
    3c60:	c5878793          	addi	a5,a5,-936 # 20001c58 <Timer_LoopCount>
    3c64:	4398                	lw	a4,0(a5)
    3c66:	0705                	addi	a4,a4,1
    3c68:	c398                	sw	a4,0(a5)
    }
}
    3c6a:	8082                	ret

00003c6c <clock_timer_init>:

    return (unsigned long long)(Timer_LoopCount + 1) * (TIMER_LOADCOUNT + 1) - cv - 1;
}

int clock_timer_init(void)
{
    3c6c:	1131                	addi	sp,sp,-20
    if (CLOCK_GETTIME_USE_TIMER_ID > CONFIG_TIMER_NUM) {
        return EPERM;
    }

    uint32_t timer_loadtimer;
    timer_handle = csi_timer_initialize(CLOCK_GETTIME_USE_TIMER_ID, timer_cb_fun);
    3c6e:	000045b7          	lui	a1,0x4
{
    3c72:	c622                	sw	s0,12(sp)
    timer_handle = csi_timer_initialize(CLOCK_GETTIME_USE_TIMER_ID, timer_cb_fun);
    3c74:	c5a58593          	addi	a1,a1,-934 # 3c5a <timer_cb_fun>
    3c78:	4501                	li	a0,0
    3c7a:	20002437          	lui	s0,0x20002
{
    3c7e:	c806                	sw	ra,16(sp)
    3c80:	c426                	sw	s1,8(sp)
    timer_handle = csi_timer_initialize(CLOCK_GETTIME_USE_TIMER_ID, timer_cb_fun);
    3c82:	c6040413          	addi	s0,s0,-928 # 20001c60 <timer_handle>
    3c86:	396d                	jal	3940 <csi_timer_initialize>
    3c88:	c008                	sw	a0,0(s0)

    if (timer_handle == NULL) {
    3c8a:	e901                	bnez	a0,3c9a <clock_timer_init+0x2e>
        return -1;
    3c8c:	54fd                	li	s1,-1
    if (cv2 > cv1) {
        timer_count_rise = 1;
    }

    return 0;
}
    3c8e:	40c2                	lw	ra,16(sp)
    3c90:	4432                	lw	s0,12(sp)
    3c92:	8526                	mv	a0,s1
    3c94:	44a2                	lw	s1,8(sp)
    3c96:	0151                	addi	sp,sp,20
    3c98:	8082                	ret
    APB_FREQ = drv_get_timer_freq(CLOCK_GETTIME_USE_TIMER_ID);
    3c9a:	4501                	li	a0,0
    3c9c:	3dc1                	jal	3b6c <drv_get_timer_freq>
    3c9e:	200027b7          	lui	a5,0x20002
    3ca2:	c4a7a823          	sw	a0,-944(a5) # 20001c50 <APB_FREQ>
    TIMER_LOADCOUNT = timer_loadtimer * (APB_FREQ / MILLION);
    3ca6:	000f47b7          	lui	a5,0xf4
    3caa:	24078793          	addi	a5,a5,576 # f4240 <__ctor_end__+0xed918>
    3cae:	02f55533          	divu	a0,a0,a5
    3cb2:	009894b7          	lui	s1,0x989
    3cb6:	68048493          	addi	s1,s1,1664 # 989680 <__ctor_end__+0x982d58>
    3cba:	200027b7          	lui	a5,0x20002
    int ret = csi_timer_config(timer_handle, TIMER_MODE_RELOAD);
    3cbe:	4585                	li	a1,1
    TIMER_LOADCOUNT = timer_loadtimer * (APB_FREQ / MILLION);
    3cc0:	02950533          	mul	a0,a0,s1
    3cc4:	c4a7aa23          	sw	a0,-940(a5) # 20001c54 <TIMER_LOADCOUNT>
    int ret = csi_timer_config(timer_handle, TIMER_MODE_RELOAD);
    3cc8:	4008                	lw	a0,0(s0)
    3cca:	3b29                	jal	39e4 <csi_timer_config>
    if (ret != 0) {
    3ccc:	f161                	bnez	a0,3c8c <clock_timer_init+0x20>
    ret = csi_timer_set_timeout(timer_handle, timer_loadtimer);
    3cce:	4008                	lw	a0,0(s0)
    3cd0:	85a6                	mv	a1,s1
    3cd2:	3bb1                	jal	3a2e <csi_timer_set_timeout>
    3cd4:	84aa                	mv	s1,a0
    if (ret != 0) {
    3cd6:	f95d                	bnez	a0,3c8c <clock_timer_init+0x20>
    csi_timer_get_current_value(timer_handle, &cv1);
    3cd8:	4008                	lw	a0,0(s0)
    3cda:	858a                	mv	a1,sp
    3cdc:	3d19                	jal	3af2 <csi_timer_get_current_value>
    csi_timer_get_current_value(timer_handle, &cv2);
    3cde:	4008                	lw	a0,0(s0)
    3ce0:	004c                	addi	a1,sp,4
    3ce2:	3d01                	jal	3af2 <csi_timer_get_current_value>
    if (cv2 > cv1) {
    3ce4:	4712                	lw	a4,4(sp)
    3ce6:	4782                	lw	a5,0(sp)
    3ce8:	fae7f3e3          	bgeu	a5,a4,3c8e <clock_timer_init+0x22>
        timer_count_rise = 1;
    3cec:	200027b7          	lui	a5,0x20002
    3cf0:	4705                	li	a4,1
    3cf2:	c4e78e23          	sb	a4,-932(a5) # 20001c5c <timer_count_rise>
    3cf6:	bf61                	j	3c8e <clock_timer_init+0x22>

00003cf8 <clock_timer_start>:
}

int clock_timer_start(void)
{
    int ret = -1;
    Timer_LoopCount = 0;
    3cf8:	200027b7          	lui	a5,0x20002
    3cfc:	c407ac23          	sw	zero,-936(a5) # 20001c58 <Timer_LoopCount>

    ret = csi_timer_start(timer_handle);
    3d00:	200027b7          	lui	a5,0x20002
    3d04:	c607a503          	lw	a0,-928(a5) # 20001c60 <timer_handle>
{
    3d08:	1151                	addi	sp,sp,-12
    3d0a:	c406                	sw	ra,8(sp)
    ret = csi_timer_start(timer_handle);
    3d0c:	3b15                	jal	3a40 <csi_timer_start>
        (ts_begin.tv_sec * 1000000000 + ts_begin.tv_nsec);
    printf("clock_gettime() timing deviation is +%llu ns\n", error_margin_ns);
#endif

    return 0;
}
    3d0e:	40a2                	lw	ra,8(sp)
    if (ret != 0) {
    3d10:	00a03533          	snez	a0,a0
}
    3d14:	40a00533          	neg	a0,a0
    3d18:	0131                	addi	sp,sp,12
    3d1a:	8082                	ret

00003d1c <_init>:
extern int __ctor_start__;

typedef void (*func_ptr) (void);

CPP_WEAK void _init(void)
{
    3d1c:	1151                	addi	sp,sp,-12
    3d1e:	c222                	sw	s0,4(sp)
    3d20:	c026                	sw	s1,0(sp)
    3d22:	641d                	lui	s0,0x7
    func_ptr *p;
    for (p = (func_ptr *)&__ctor_end__ - 1; p >= (func_ptr *)&__ctor_start__; p--) {
    3d24:	649d                	lui	s1,0x7
{
    3d26:	c406                	sw	ra,8(sp)
    3d28:	92840413          	addi	s0,s0,-1752 # 6928 <__ctor_end__>
    for (p = (func_ptr *)&__ctor_end__ - 1; p >= (func_ptr *)&__ctor_start__; p--) {
    3d2c:	92848493          	addi	s1,s1,-1752 # 6928 <__ctor_end__>
    3d30:	1471                	addi	s0,s0,-4
    3d32:	00947763          	bgeu	s0,s1,3d40 <_init+0x24>
        (*p) ();
    }
}
    3d36:	40a2                	lw	ra,8(sp)
    3d38:	4412                	lw	s0,4(sp)
    3d3a:	4482                	lw	s1,0(sp)
    3d3c:	0131                	addi	sp,sp,12
    3d3e:	8082                	ret
        (*p) ();
    3d40:	401c                	lw	a5,0(s0)
    3d42:	9782                	jalr	a5
    3d44:	b7f5                	j	3d30 <_init+0x14>

00003d46 <_fini>:

CPP_WEAK void _fini(void)
{
    3d46:	1151                	addi	sp,sp,-12
    3d48:	c222                	sw	s0,4(sp)
    3d4a:	c026                	sw	s1,0(sp)
    func_ptr *p;
    for (p = (func_ptr *)&__ctor_end__; p <= (func_ptr *)&__dtor_end__ - 1; p++) {
    3d4c:	641d                	lui	s0,0x7
    3d4e:	649d                	lui	s1,0x7
{
    3d50:	c406                	sw	ra,8(sp)
    for (p = (func_ptr *)&__ctor_end__; p <= (func_ptr *)&__dtor_end__ - 1; p++) {
    3d52:	92840413          	addi	s0,s0,-1752 # 6928 <__ctor_end__>
    3d56:	92448493          	addi	s1,s1,-1756 # 6924 <pad_line+0x72c>
    3d5a:	0084f763          	bgeu	s1,s0,3d68 <_fini+0x22>
        (*p) ();
    }
}
    3d5e:	40a2                	lw	ra,8(sp)
    3d60:	4412                	lw	s0,4(sp)
    3d62:	4482                	lw	s1,0(sp)
    3d64:	0131                	addi	sp,sp,12
    3d66:	8082                	ret
        (*p) ();
    3d68:	401c                	lw	a5,0(s0)
    for (p = (func_ptr *)&__ctor_end__; p <= (func_ptr *)&__dtor_end__ - 1; p++) {
    3d6a:	0411                	addi	s0,s0,4
        (*p) ();
    3d6c:	9782                	jalr	a5
    3d6e:	b7f5                	j	3d5a <_fini+0x14>

00003d70 <fputc>:
{
    return 0;
}

int fputc(int ch, FILE *stream)
{
    3d70:	1151                	addi	sp,sp,-12
    (void)stream;

    if (console_handle == NULL) {
    3d72:	200027b7          	lui	a5,0x20002
{
    3d76:	c026                	sw	s1,0(sp)
    3d78:	84aa                	mv	s1,a0
    if (console_handle == NULL) {
    3d7a:	c647a503          	lw	a0,-924(a5) # 20001c64 <console_handle>
{
    3d7e:	c406                	sw	ra,8(sp)
    3d80:	c222                	sw	s0,4(sp)
    if (console_handle == NULL) {
    3d82:	c115                	beqz	a0,3da6 <fputc+0x36>
    3d84:	c6478413          	addi	s0,a5,-924
        return -1;
    }

    if (ch == '\n') {
    3d88:	47a9                	li	a5,10
    3d8a:	00f49463          	bne	s1,a5,3d92 <fputc+0x22>
        csi_usart_putchar(console_handle, '\r');
    3d8e:	45b5                	li	a1,13
    3d90:	3c95                	jal	3804 <csi_usart_putchar>
    }

    csi_usart_putchar(console_handle, ch);
    3d92:	4008                	lw	a0,0(s0)
    3d94:	0ff4f593          	andi	a1,s1,255
    3d98:	34b5                	jal	3804 <csi_usart_putchar>

    return 0;
    3d9a:	4501                	li	a0,0
}
    3d9c:	40a2                	lw	ra,8(sp)
    3d9e:	4412                	lw	s0,4(sp)
    3da0:	4482                	lw	s1,0(sp)
    3da2:	0131                	addi	sp,sp,12
    3da4:	8082                	ret
        return -1;
    3da6:	557d                	li	a0,-1
    3da8:	bfd5                	j	3d9c <fputc+0x2c>

00003daa <os_critical_enter>:
#ifndef CONFIG_KERNEL_NONE
    csi_kernel_sched_suspend();
#endif

    return 0;
}
    3daa:	4501                	li	a0,0
    3dac:	8082                	ret

00003dae <os_critical_exit>:
    3dae:	4501                	li	a0,0
    3db0:	8082                	ret

00003db2 <mm_addfreechunk>:
 *   the mm semaphore
 *
 ****************************************************************************/

void mm_addfreechunk(struct mm_heap_s *heap, struct mm_freenode_s *node)
{
    3db2:	1141                	addi	sp,sp,-16
    3db4:	c422                	sw	s0,8(sp)
    3db6:	842a                	mv	s0,a0
  struct mm_freenode_s *next;
  struct mm_freenode_s *prev;

  /* Convert the size to a nodelist index */

  int ndx = mm_size2ndx(node->size);
    3db8:	4188                	lw	a0,0(a1)
    3dba:	c02e                	sw	a1,0(sp)
{
    3dbc:	c606                	sw	ra,12(sp)
  int ndx = mm_size2ndx(node->size);
    3dbe:	20d5                	jal	3ea2 <mm_size2ndx>

  /* Now put the new node int the next */

  for (prev = &heap->mm_nodelist[ndx], next = heap->mm_nodelist[ndx].flink;
    3dc0:	0505                	addi	a0,a0,1
    3dc2:	0512                	slli	a0,a0,0x4
    3dc4:	00850713          	addi	a4,a0,8
    3dc8:	9722                	add	a4,a4,s0
    3dca:	942a                	add	s0,s0,a0
    3dcc:	481c                	lw	a5,16(s0)
    3dce:	4582                	lw	a1,0(sp)
    3dd0:	c791                	beqz	a5,3ddc <mm_addfreechunk+0x2a>
       next && next->size && next->size < node->size;
    3dd2:	4394                	lw	a3,0(a5)
    3dd4:	c681                	beqz	a3,3ddc <mm_addfreechunk+0x2a>
    3dd6:	4190                	lw	a2,0(a1)
    3dd8:	00c6eb63          	bltu	a3,a2,3dee <mm_addfreechunk+0x3c>
       prev = next, next = next->flink);

  /* Does it go in mid next or at the end? */

  prev->flink = node;
    3ddc:	c70c                	sw	a1,8(a4)
  node->blink = prev;
    3dde:	c5d8                	sw	a4,12(a1)
  node->flink = next;
    3de0:	c59c                	sw	a5,8(a1)

  if (next)
    3de2:	c391                	beqz	a5,3de6 <mm_addfreechunk+0x34>
    {
      /* The new node goes between prev and next */

      next->blink = node;
    3de4:	c7cc                	sw	a1,12(a5)
    }
}
    3de6:	40b2                	lw	ra,12(sp)
    3de8:	4422                	lw	s0,8(sp)
    3dea:	0141                	addi	sp,sp,16
    3dec:	8082                	ret
       prev = next, next = next->flink);
    3dee:	873e                	mv	a4,a5
    3df0:	479c                	lw	a5,8(a5)
    3df2:	bff9                	j	3dd0 <mm_addfreechunk+0x1e>

00003df4 <mm_addregion>:

  /* Adjust the provide heap start and size so that they are both aligned
   * with the MM_MIN_CHUNK size.
   */

  heapbase = MM_ALIGN_UP((uintptr_t)heapstart);
    3df4:	00f58713          	addi	a4,a1,15
  heapend  = MM_ALIGN_DOWN((uintptr_t)heapstart + (uintptr_t)heapsize);
    3df8:	962e                	add	a2,a2,a1

  //mlldbg("Region %d: base=%p size=%u\n", IDX+1, heapstart, heapsize);

  /* Add the size of this region to the total size of the heap */

  heap->mm_heapsize += heapsize;
    3dfa:	454c                	lw	a1,12(a0)
  heapbase = MM_ALIGN_UP((uintptr_t)heapstart);
    3dfc:	9b41                	andi	a4,a4,-16
  heapend  = MM_ALIGN_DOWN((uintptr_t)heapstart + (uintptr_t)heapsize);
    3dfe:	9a41                	andi	a2,a2,-16
  heapsize = heapend - heapbase;
    3e00:	40e607b3          	sub	a5,a2,a4
  heap->mm_heapsize += heapsize;
    3e04:	95be                	add	a1,a1,a5
    3e06:	c54c                	sw	a1,12(a0)
   *
   * And create one free node between the guard nodes that contains
   * all available memory.
   */

  heap->mm_heapstart[IDX]            = (struct mm_allocnode_s *)heapbase;
    3e08:	c918                	sw	a4,16(a0)
  heap->mm_heapstart[IDX]->size      = SIZEOF_MM_ALLOCNODE;
    3e0a:	4321                	li	t1,8
  heap->mm_heapstart[IDX]->preceding = MM_ALLOC_BIT;
    3e0c:	800002b7          	lui	t0,0x80000

  node                        = (struct mm_freenode_s *)(heapbase + SIZEOF_MM_ALLOCNODE);
  node->size                  = heapsize - 2*SIZEOF_MM_ALLOCNODE;
    3e10:	17c1                	addi	a5,a5,-16
    3e12:	c71c                	sw	a5,8(a4)
  heap->mm_heapstart[IDX]->size      = SIZEOF_MM_ALLOCNODE;
    3e14:	00672023          	sw	t1,0(a4)
  heap->mm_heapstart[IDX]->preceding = MM_ALLOC_BIT;
    3e18:	00572223          	sw	t0,4(a4)
  node                        = (struct mm_freenode_s *)(heapbase + SIZEOF_MM_ALLOCNODE);
    3e1c:	00870593          	addi	a1,a4,8
  node->preceding             = SIZEOF_MM_ALLOCNODE;
    3e20:	0065a223          	sw	t1,4(a1)

  heap->mm_heapend[IDX]              = (struct mm_allocnode_s *)(heapend - SIZEOF_MM_ALLOCNODE);
    3e24:	ff860713          	addi	a4,a2,-8
    3e28:	c958                	sw	a4,20(a0)
  heap->mm_heapend[IDX]->size        = SIZEOF_MM_ALLOCNODE;
  heap->mm_heapend[IDX]->preceding   = node->size | MM_ALLOC_BIT;
    3e2a:	0057e7b3          	or	a5,a5,t0
  heap->mm_heapend[IDX]->size        = SIZEOF_MM_ALLOCNODE;
    3e2e:	fe662c23          	sw	t1,-8(a2)
  heap->mm_heapend[IDX]->preceding   = node->size | MM_ALLOC_BIT;
    3e32:	fef62e23          	sw	a5,-4(a2)
  heap->mm_nregions++;
#endif

  /* Add the single, large free node to the nodelist */

  mm_addfreechunk(heap, node);
    3e36:	bfb5                	j	3db2 <mm_addfreechunk>

00003e38 <mm_initialize>:
 *
 ****************************************************************************/

void mm_initialize(struct mm_heap_s *heap, void *heapstart,
                   size_t heapsize)
{
    3e38:	1141                	addi	sp,sp,-16
    3e3a:	c422                	sw	s0,8(sp)
    3e3c:	c226                	sw	s1,4(sp)
    3e3e:	c606                	sw	ra,12(sp)
    3e40:	842a                	mv	s0,a0
    3e42:	c032                	sw	a2,0(sp)
  //CHECK_FREENODE_SIZE;
#endif

  /* Set up global variables */

  heap->mm_heapsize = 0;
    3e44:	00052623          	sw	zero,12(a0)
{
    3e48:	84ae                	mv	s1,a1
  heap->mm_nregions = 0;
#endif

  /* Initialize the node array */

  memset(heap->mm_nodelist, 0, sizeof(struct mm_freenode_s) * MM_NNODES);
    3e4a:	13000613          	li	a2,304
    3e4e:	4581                	li	a1,0
    3e50:	0561                	addi	a0,a0,24
    3e52:	caefe0ef          	jal	ra,2300 <memset>
  for (i = 1; i < MM_NNODES; i++)
    3e56:	4702                	lw	a4,0(sp)
    3e58:	02840793          	addi	a5,s0,40
    3e5c:	14840693          	addi	a3,s0,328
    {
      heap->mm_nodelist[i-1].flink = &heap->mm_nodelist[i];
      heap->mm_nodelist[i].blink   = &heap->mm_nodelist[i-1];
    3e60:	ff078613          	addi	a2,a5,-16
      heap->mm_nodelist[i-1].flink = &heap->mm_nodelist[i];
    3e64:	fef7ac23          	sw	a5,-8(a5)
      heap->mm_nodelist[i].blink   = &heap->mm_nodelist[i-1];
    3e68:	c7d0                	sw	a2,12(a5)
    3e6a:	07c1                	addi	a5,a5,16
  for (i = 1; i < MM_NNODES; i++)
    3e6c:	fed79ae3          	bne	a5,a3,3e60 <mm_initialize+0x28>

  mm_seminitialize(heap);

  /* Add the initial region of memory to the heap */

  mm_addregion(heap, heapstart, heapsize);
    3e70:	8522                	mv	a0,s0
}
    3e72:	4422                	lw	s0,8(sp)
    3e74:	40b2                	lw	ra,12(sp)
  mm_addregion(heap, heapstart, heapsize);
    3e76:	85a6                	mv	a1,s1
}
    3e78:	4492                	lw	s1,4(sp)
  mm_addregion(heap, heapstart, heapsize);
    3e7a:	863a                	mv	a2,a4
}
    3e7c:	0141                	addi	sp,sp,16
  mm_addregion(heap, heapstart, heapsize);
    3e7e:	bf9d                	j	3df4 <mm_addregion>

00003e80 <mm_heap_initialize>:

void mm_heap_initialize(void)
{
    mm_initialize(&g_mmheap, &__heap_start, (uint32_t)(&__heap_end) - (uint32_t)(&__heap_start));
    3e80:	200025b7          	lui	a1,0x20002
    3e84:	200307b7          	lui	a5,0x20030
    3e88:	e8058613          	addi	a2,a1,-384 # 20001e80 <__bss_end__>
    3e8c:	00078793          	mv	a5,a5
    3e90:	20002537          	lui	a0,0x20002
    3e94:	40c78633          	sub	a2,a5,a2
    3e98:	e8058593          	addi	a1,a1,-384
    3e9c:	d3450513          	addi	a0,a0,-716 # 20001d34 <g_mmheap>
    3ea0:	bf61                	j	3e38 <mm_initialize>

00003ea2 <mm_size2ndx>:

int mm_size2ndx(size_t size)
{
  int ndx = 0;

  if (size >= MM_MAX_CHUNK)
    3ea2:	004007b7          	lui	a5,0x400
    3ea6:	00f57c63          	bgeu	a0,a5,3ebe <mm_size2ndx+0x1c>
    {
       return MM_NNODES-1;
    }

  size >>= MM_MIN_SHIFT;
    3eaa:	00455793          	srli	a5,a0,0x4
  while (size > 1)
    3eae:	4705                	li	a4,1
  int ndx = 0;
    3eb0:	4501                	li	a0,0
  while (size > 1)
    3eb2:	00f76363          	bltu	a4,a5,3eb8 <mm_size2ndx+0x16>
    3eb6:	8082                	ret
    {
      ndx++;
    3eb8:	0505                	addi	a0,a0,1
      size >>= 1;
    3eba:	8385                	srli	a5,a5,0x1
    3ebc:	bfdd                	j	3eb2 <mm_size2ndx+0x10>
       return MM_NNODES-1;
    3ebe:	4549                	li	a0,18
    }

  return ndx;
}
    3ec0:	8082                	ret

00003ec2 <usart_send_sync>:

    return 0;
}

static int32_t usart_send_sync(usart_handle_t usart, const void *data, uint32_t num)
{
    3ec2:	1151                	addi	sp,sp,-12
    3ec4:	c222                	sw	s0,4(sp)
    int time_out = 0x7ffff;
    3ec6:	00080437          	lui	s0,0x80
{
    3eca:	c026                	sw	s1,0(sp)
    3ecc:	c406                	sw	ra,8(sp)
    3ece:	84aa                	mv	s1,a0
    int time_out = 0x7ffff;
    3ed0:	147d                	addi	s0,s0,-1
    usart_status_t status;

    csi_usart_send(usart, data, num);
    3ed2:	93bff0ef          	jal	ra,380c <csi_usart_send>

    while (time_out) {
        time_out--;
        status = csi_usart_get_status(usart);
    3ed6:	8526                	mv	a0,s1
    3ed8:	93bff0ef          	jal	ra,3812 <csi_usart_get_status>

        if (!status.tx_busy) {
    3edc:	8905                	andi	a0,a0,1
        time_out--;
    3ede:	147d                	addi	s0,s0,-1
        if (!status.tx_busy) {
    3ee0:	c901                	beqz	a0,3ef0 <usart_send_sync+0x2e>
    while (time_out) {
    3ee2:	f875                	bnez	s0,3ed6 <usart_send_sync+0x14>
            break;
        }
    }

    if (0 == time_out) {
        return -1;
    3ee4:	557d                	li	a0,-1
    }

    return 0;
}
    3ee6:	40a2                	lw	ra,8(sp)
    3ee8:	4412                	lw	s0,4(sp)
    3eea:	4482                	lw	s1,0(sp)
    3eec:	0131                	addi	sp,sp,12
    3eee:	8082                	ret
    if (0 == time_out) {
    3ef0:	00143513          	seqz	a0,s0
    3ef4:	40a00533          	neg	a0,a0
    3ef8:	b7fd                	j	3ee6 <usart_send_sync+0x24>

00003efa <usart_send_async>:
    rx_async_flag = 0;
    return 0;
}

static int32_t usart_send_async(usart_handle_t usart, const void *data, uint32_t num)
{
    3efa:	1151                	addi	sp,sp,-12
    3efc:	c222                	sw	s0,4(sp)
    int time_out = 0x7ffff;
    tx_async_flag = 0;
    3efe:	20002437          	lui	s0,0x20002
    3f02:	c6e40793          	addi	a5,s0,-914 # 20001c6e <tx_async_flag>
{
    3f06:	c406                	sw	ra,8(sp)
    tx_async_flag = 0;
    3f08:	00078023          	sb	zero,0(a5) # 400000 <__ctor_end__+0x3f96d8>

    csi_usart_send(usart, data, num);
    3f0c:	901ff0ef          	jal	ra,380c <csi_usart_send>
    int time_out = 0x7ffff;
    3f10:	000807b7          	lui	a5,0x80
    3f14:	17fd                	addi	a5,a5,-1
    3f16:	c6e40413          	addi	s0,s0,-914

    while (time_out) {
        time_out--;

        if (tx_async_flag == 1) {
    3f1a:	4705                	li	a4,1
    3f1c:	00044683          	lbu	a3,0(s0)
        time_out--;
    3f20:	17fd                	addi	a5,a5,-1
        if (tx_async_flag == 1) {
    3f22:	00d70863          	beq	a4,a3,3f32 <usart_send_async+0x38>
    while (time_out) {
    3f26:	fbfd                	bnez	a5,3f1c <usart_send_async+0x22>
            break;
        }
    }

    if (0 == time_out) {
        return -1;
    3f28:	557d                	li	a0,-1
    }

    tx_async_flag = 0;
    return 0;
}
    3f2a:	40a2                	lw	ra,8(sp)
    3f2c:	4412                	lw	s0,4(sp)
    3f2e:	0131                	addi	sp,sp,12
    3f30:	8082                	ret
        return -1;
    3f32:	557d                	li	a0,-1
    if (0 == time_out) {
    3f34:	dbfd                	beqz	a5,3f2a <usart_send_async+0x30>
    tx_async_flag = 0;
    3f36:	00040023          	sb	zero,0(s0)
    return 0;
    3f3a:	4501                	li	a0,0
    3f3c:	b7fd                	j	3f2a <usart_send_async+0x30>

00003f3e <usart_event_cb>:

static void usart_event_cb(int32_t idx, uint32_t event)
{
    uint8_t g_data[15];

    switch (event) {
    3f3e:	4785                	li	a5,1
    3f40:	00f58c63          	beq	a1,a5,3f58 <usart_event_cb+0x1a>
    3f44:	c589                	beqz	a1,3f4e <usart_event_cb+0x10>
    3f46:	47b9                	li	a5,14
    3f48:	00f58d63          	beq	a1,a5,3f62 <usart_event_cb+0x24>
    3f4c:	8082                	ret
        case USART_EVENT_SEND_COMPLETE:
            tx_async_flag = 1;
    3f4e:	20002737          	lui	a4,0x20002
    3f52:	c6f70723          	sb	a5,-914(a4) # 20001c6e <tx_async_flag>
            break;
    3f56:	8082                	ret

        case USART_EVENT_RECEIVE_COMPLETE:
            rx_async_flag = 1;
    3f58:	200027b7          	lui	a5,0x20002
    3f5c:	c6b78623          	sb	a1,-916(a5) # 20001c6c <rx_async_flag>
            break;
    3f60:	8082                	ret

        case USART_EVENT_RECEIVED:
            csi_usart_receive_query(g_usart_handle, g_data, 15);
    3f62:	200027b7          	lui	a5,0x20002
    3f66:	c687a503          	lw	a0,-920(a5) # 20001c68 <g_usart_handle>
{
    3f6a:	1111                	addi	sp,sp,-28
            csi_usart_receive_query(g_usart_handle, g_data, 15);
    3f6c:	858a                	mv	a1,sp
    3f6e:	463d                	li	a2,15
{
    3f70:	cc06                	sw	ra,24(sp)
            csi_usart_receive_query(g_usart_handle, g_data, 15);
    3f72:	89fff0ef          	jal	ra,3810 <csi_usart_receive_query>

        default:
            break;
    }
}
    3f76:	40e2                	lw	ra,24(sp)
    3f78:	0171                	addi	sp,sp,28
    3f7a:	8082                	ret

00003f7c <usart_event_cb_query>:

static void usart_event_cb_query(int32_t idx, uint32_t event)
{
    3f7c:	1111                	addi	sp,sp,-28
    3f7e:	c826                	sw	s1,16(sp)
    3f80:	cc06                	sw	ra,24(sp)
    3f82:	ca22                	sw	s0,20(sp)
    3f84:	4485                	li	s1,1
    3f86:	02958063          	beq	a1,s1,3fa6 <usart_event_cb_query+0x2a>
    3f8a:	c989                	beqz	a1,3f9c <usart_event_cb_query+0x20>
    3f8c:	47b9                	li	a5,14
    3f8e:	02f58163          	beq	a1,a5,3fb0 <usart_event_cb_query+0x34>
            break;

        default:
            break;
    }
}
    3f92:	40e2                	lw	ra,24(sp)
    3f94:	4452                	lw	s0,20(sp)
    3f96:	44c2                	lw	s1,16(sp)
    3f98:	0171                	addi	sp,sp,28
    3f9a:	8082                	ret
            tx_async_flag = 1;
    3f9c:	200027b7          	lui	a5,0x20002
    3fa0:	c6978723          	sb	s1,-914(a5) # 20001c6e <tx_async_flag>
            break;
    3fa4:	b7fd                	j	3f92 <usart_event_cb_query+0x16>
            rx_async_flag = 1;
    3fa6:	200027b7          	lui	a5,0x20002
    3faa:	c6b78623          	sb	a1,-916(a5) # 20001c6c <rx_async_flag>
            break;
    3fae:	b7d5                	j	3f92 <usart_event_cb_query+0x16>
            ret = csi_usart_receive_query(g_usart_handle, g_data, 15);
    3fb0:	20002437          	lui	s0,0x20002
    3fb4:	c6840413          	addi	s0,s0,-920 # 20001c68 <g_usart_handle>
    3fb8:	4008                	lw	a0,0(s0)
    3fba:	463d                	li	a2,15
    3fbc:	858a                	mv	a1,sp
    3fbe:	853ff0ef          	jal	ra,3810 <csi_usart_receive_query>
            csi_usart_send(g_usart_handle, g_data, ret);
    3fc2:	0ff57613          	andi	a2,a0,255
    3fc6:	4008                	lw	a0,0(s0)
    3fc8:	858a                	mv	a1,sp
    3fca:	843ff0ef          	jal	ra,380c <csi_usart_send>
            rx_trigger_flag = 1;
    3fce:	200027b7          	lui	a5,0x20002
    3fd2:	c69786a3          	sb	s1,-915(a5) # 20001c6d <rx_trigger_flag>
}
    3fd6:	bf75                	j	3f92 <usart_event_cb_query+0x16>

00003fd8 <usart_wait_reply>:

    return 2;
}

static int32_t usart_wait_reply(usart_handle_t usart)
{
    3fd8:	fd810113          	addi	sp,sp,-40
    3fdc:	ce26                	sw	s1,28(sp)
    3fde:	d206                	sw	ra,36(sp)
    3fe0:	d022                	sw	s0,32(sp)
    3fe2:	84aa                	mv	s1,a0
    volatile uint32_t i;
    uint8_t ch;
    char answer[20];

    for (i = 0; i < 20; i++) {
    3fe4:	c202                	sw	zero,4(sp)
    3fe6:	474d                	li	a4,19
    3fe8:	4792                	lw	a5,4(sp)
    3fea:	02f77863          	bgeu	a4,a5,401a <usart_wait_reply+0x42>
        answer[i] = '\0';
    }

    i = 0;
    3fee:	c202                	sw	zero,4(sp)

    while (i < 20) {
    3ff0:	4712                	lw	a4,4(sp)
    3ff2:	47cd                	li	a5,19
    3ff4:	04e7ea63          	bltu	a5,a4,4048 <usart_wait_reply+0x70>
    csi_usart_receive(usart, data, num);
    3ff8:	4605                	li	a2,1
    3ffa:	00310593          	addi	a1,sp,3
    3ffe:	8526                	mv	a0,s1
    int time_out = 0x7ffff;
    4000:	00080437          	lui	s0,0x80
    csi_usart_receive(usart, data, num);
    4004:	80bff0ef          	jal	ra,380e <csi_usart_receive>
    int time_out = 0x7ffff;
    4008:	147d                	addi	s0,s0,-1
        status = csi_usart_get_status(usart);
    400a:	8526                	mv	a0,s1
    400c:	807ff0ef          	jal	ra,3812 <csi_usart_get_status>
        if (!status.rx_busy) {
    4010:	8909                	andi	a0,a0,2
        time_out--;
    4012:	147d                	addi	s0,s0,-1
        if (!status.rx_busy) {
    4014:	cd01                	beqz	a0,402c <usart_wait_reply+0x54>
    while (time_out) {
    4016:	f875                	bnez	s0,400a <usart_wait_reply+0x32>
    4018:	bfe1                	j	3ff0 <usart_wait_reply+0x18>
        answer[i] = '\0';
    401a:	4792                	lw	a5,4(sp)
    401c:	0874                	addi	a3,sp,28
    401e:	97b6                	add	a5,a5,a3
    4020:	fe078623          	sb	zero,-20(a5)
    for (i = 0; i < 20; i++) {
    4024:	4792                	lw	a5,4(sp)
    4026:	0785                	addi	a5,a5,1
    4028:	c23e                	sw	a5,4(sp)
    402a:	bf7d                	j	3fe8 <usart_wait_reply+0x10>
    if (0 == time_out) {
    402c:	d071                	beqz	s0,3ff0 <usart_wait_reply+0x18>
        if (0 == usart_receive_sync(usart, &ch, 1)) {
            if (ch == '\n' || ch == '\r') {
    402e:	00314783          	lbu	a5,3(sp)
    4032:	4729                	li	a4,10
    4034:	00e78563          	beq	a5,a4,403e <usart_wait_reply+0x66>
    4038:	4735                	li	a4,13
    403a:	04e79463          	bne	a5,a4,4082 <usart_wait_reply+0xaa>

                answer[i] = '\0';
    403e:	4792                	lw	a5,4(sp)
    4040:	0878                	addi	a4,sp,28
    4042:	97ba                	add	a5,a5,a4
    4044:	fe078623          	sb	zero,-20(a5)
                usart_send_sync(usart, &ch, 1);
            }
        }
    }

    if ((i == 1) && (answer[0] == 'y')) {
    4048:	4712                	lw	a4,4(sp)
    404a:	4785                	li	a5,1
    404c:	00f71963          	bne	a4,a5,405e <usart_wait_reply+0x86>
    4050:	00814703          	lbu	a4,8(sp)
    4054:	07900793          	li	a5,121
        return 1;
    4058:	4505                	li	a0,1
    if ((i == 1) && (answer[0] == 'y')) {
    405a:	00f70e63          	beq	a4,a5,4076 <usart_wait_reply+0x9e>
    } else if ((i == 1) && (answer[0] == 'n')) {
    405e:	4712                	lw	a4,4(sp)
    4060:	4785                	li	a5,1
        return 0;
    }

    return 2;
    4062:	4509                	li	a0,2
    } else if ((i == 1) && (answer[0] == 'n')) {
    4064:	00f71963          	bne	a4,a5,4076 <usart_wait_reply+0x9e>
    4068:	00814703          	lbu	a4,8(sp)
    406c:	06e00793          	li	a5,110
    4070:	00f71363          	bne	a4,a5,4076 <usart_wait_reply+0x9e>
        return 0;
    4074:	4501                	li	a0,0
}
    4076:	5092                	lw	ra,36(sp)
    4078:	5402                	lw	s0,32(sp)
    407a:	44f2                	lw	s1,28(sp)
    407c:	02810113          	addi	sp,sp,40
    4080:	8082                	ret
            if (ch == 127 || ch == '\b') {
    4082:	07f00713          	li	a4,127
    4086:	00e78563          	beq	a5,a4,4090 <usart_wait_reply+0xb8>
    408a:	4721                	li	a4,8
    408c:	00e79d63          	bne	a5,a4,40a6 <usart_wait_reply+0xce>
                if (i > 0) {
    4090:	4792                	lw	a5,4(sp)
    4092:	dfb9                	beqz	a5,3ff0 <usart_wait_reply+0x18>
                    i--;
    4094:	4792                	lw	a5,4(sp)
    4096:	17fd                	addi	a5,a5,-1
    4098:	c23e                	sw	a5,4(sp)
                usart_send_sync(usart, &ch, 1);
    409a:	4605                	li	a2,1
    409c:	00310593          	addi	a1,sp,3
    40a0:	8526                	mv	a0,s1
    40a2:	3505                	jal	3ec2 <usart_send_sync>
    40a4:	b7b1                	j	3ff0 <usart_wait_reply+0x18>
                answer[i++] = ch;
    40a6:	4712                	lw	a4,4(sp)
    40a8:	00170693          	addi	a3,a4,1
    40ac:	c236                	sw	a3,4(sp)
    40ae:	0874                	addi	a3,sp,28
    40b0:	9736                	add	a4,a4,a3
    40b2:	fef70623          	sb	a5,-20(a4)
    40b6:	b7d5                	j	409a <usart_wait_reply+0xc2>

000040b8 <_usart_test>:

    return 0;
}

int _usart_test(usart_handle_t usart, int32_t uart_idx)
{
    40b8:	fb810113          	addi	sp,sp,-72
    40bc:	c02a                	sw	a0,0(sp)
    uint32_t get;
    //char input_char1,input_char2;
    int32_t ret;

    printf("Testing usart...");
    40be:	00006537          	lui	a0,0x6
    40c2:	cf850513          	addi	a0,a0,-776 # 5cf8 <sg_usi_config+0x68>
{
    40c6:	c286                	sw	ra,68(sp)
    40c8:	c82e                	sw	a1,16(sp)
    40ca:	c0a2                	sw	s0,64(sp)
    40cc:	de26                	sw	s1,60(sp)
    printf("Testing usart...");
    40ce:	47d000ef          	jal	ra,4d4a <printf>
    printf("Default configure: Baudrate --- 115200,");
    40d2:	00006537          	lui	a0,0x6
    40d6:	d0c50513          	addi	a0,a0,-756 # 5d0c <sg_usi_config+0x7c>
    40da:	471000ef          	jal	ra,4d4a <printf>
    printf("Parity --- NONE,");
    40de:	00006537          	lui	a0,0x6
    40e2:	d3450513          	addi	a0,a0,-716 # 5d34 <sg_usi_config+0xa4>
    40e6:	465000ef          	jal	ra,4d4a <printf>
    printf(" Wordsize --- 8. ");
    40ea:	00006537          	lui	a0,0x6
    40ee:	d4850513          	addi	a0,a0,-696 # 5d48 <sg_usi_config+0xb8>
    40f2:	459000ef          	jal	ra,4d4a <printf>
    printf("\n- - -USART ready? [y] \n");
    40f6:	00006537          	lui	a0,0x6
    40fa:	d5c50513          	addi	a0,a0,-676 # 5d5c <sg_usi_config+0xcc>
    40fe:	579000ef          	jal	ra,4e76 <puts>
        get = usart_wait_reply(usart);

        if ((get == 1)) {
			printf("\n 'yyyyyyyyyyyy'");
            break;
        } else {
    4102:	00006437          	lui	s0,0x6
        get = usart_wait_reply(usart);
    4106:	4502                	lw	a0,0(sp)
    4108:	3dc1                	jal	3fd8 <usart_wait_reply>
        if ((get == 1)) {
    410a:	4785                	li	a5,1
    410c:	00f50763          	beq	a0,a5,411a <_usart_test+0x62>
        } else {
    4110:	d7440513          	addi	a0,s0,-652 # 5d74 <sg_usi_config+0xe4>
    4114:	437000ef          	jal	ra,4d4a <printf>
        get = usart_wait_reply(usart);
    4118:	b7fd                	j	4106 <_usart_test+0x4e>
    printf("\n\n\t- - - Testing usart mode...\n");
    411a:	00006537          	lui	a0,0x6
    411e:	d8850513          	addi	a0,a0,-632 # 5d88 <sg_usi_config+0xf8>
    4122:	555000ef          	jal	ra,4e76 <puts>
    printf("\r\t(sync mode ): Output is---\n\t\t");
    4126:	00006537          	lui	a0,0x6
    412a:	da850513          	addi	a0,a0,-600 # 5da8 <sg_usi_config+0x118>
    412e:	41d000ef          	jal	ra,4d4a <printf>
    usart_send_sync(usart, data, sizeof(data));
    4132:	4502                	lw	a0,0(sp)
    4134:	fec18593          	addi	a1,gp,-20 # 20000660 <data>
    4138:	4649                	li	a2,18
    413a:	3361                	jal	3ec2 <usart_send_sync>
    printf("- - - [y/n] ");
    413c:	00006537          	lui	a0,0x6
    4140:	dc850513          	addi	a0,a0,-568 # 5dc8 <sg_usi_config+0x138>
    4144:	407000ef          	jal	ra,4d4a <printf>
    4148:	c222                	sw	s0,4(sp)
            printf("\n\tPlease enter 'y' or 'n'   ");
    414a:	00006437          	lui	s0,0x6
        get = usart_wait_reply(usart);
    414e:	4502                	lw	a0,0(sp)
    4150:	3561                	jal	3fd8 <usart_wait_reply>
        if ((get == 1) || (get == 0)) {
    4152:	4785                	li	a5,1
    4154:	00a7f763          	bgeu	a5,a0,4162 <_usart_test+0xaa>
            printf("\n\tPlease enter 'y' or 'n'   ");
    4158:	dd840513          	addi	a0,s0,-552 # 5dd8 <sg_usi_config+0x148>
    415c:	3ef000ef          	jal	ra,4d4a <printf>
    4160:	b7fd                	j	414e <_usart_test+0x96>
    if (get == 1) {
    4162:	0af51063          	bne	a0,a5,4202 <_usart_test+0x14a>
        printf("\t- - -PASS\n");
    4166:	00006437          	lui	s0,0x6
    416a:	df840513          	addi	a0,s0,-520 # 5df8 <sg_usi_config+0x168>
    416e:	509000ef          	jal	ra,4e76 <puts>
    uint32_t pB[] = {9600, 19200, 14400, 38400, 57600, 115200};
    4172:	6595                	lui	a1,0x5
    4174:	4661                	li	a2,24
    4176:	70058593          	addi	a1,a1,1792 # 5700 <__srodata>
    417a:	1048                	addi	a0,sp,36
    417c:	860fe0ef          	jal	ra,21dc <memcpy>
    printf("\n\t- - - Test usart baudrate.\n");
    4180:	00006537          	lui	a0,0x6
    4184:	e0450513          	addi	a0,a0,-508 # 5e04 <sg_usi_config+0x174>
    4188:	4ef000ef          	jal	ra,4e76 <puts>
    for (baudrate = 0; baudrate < sizeof(pB) / 4; baudrate++) {
    418c:	1044                	addi	s1,sp,36
    418e:	c422                	sw	s0,8(sp)
        printf("\tBaudrate is %d? [y] ", pB[baudrate]);
    4190:	4080                	lw	s0,0(s1)
    4192:	000067b7          	lui	a5,0x6
    4196:	e5478513          	addi	a0,a5,-428 # 5e54 <sg_usi_config+0x1c4>
    419a:	85a2                	mv	a1,s0
    419c:	3af000ef          	jal	ra,4d4a <printf>
        ret = csi_usart_config(usart, pB[baudrate], USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    41a0:	4502                	lw	a0,0(sp)
    41a2:	85a2                	mv	a1,s0
    41a4:	478d                	li	a5,3
    41a6:	4701                	li	a4,0
    41a8:	4681                	li	a3,0
    41aa:	4601                	li	a2,0
    41ac:	e5eff0ef          	jal	ra,380a <csi_usart_config>
    41b0:	85aa                	mv	a1,a0
        if (ret < 0) {
    41b2:	06054c63          	bltz	a0,422a <_usart_test+0x172>
            printf("\n\tPlease enter 'y'   ");
    41b6:	00006437          	lui	s0,0x6
        while (usart_wait_reply(usart) != 1) {
    41ba:	4502                	lw	a0,0(sp)
    41bc:	3d31                	jal	3fd8 <usart_wait_reply>
    41be:	4785                	li	a5,1
    41c0:	08f51163          	bne	a0,a5,4242 <_usart_test+0x18a>
        printf(" :Output is ---\n\t\t");
    41c4:	00006437          	lui	s0,0x6
    41c8:	ea040513          	addi	a0,s0,-352 # 5ea0 <sg_usi_config+0x210>
    41cc:	37f000ef          	jal	ra,4d4a <printf>
        usart_send_sync(usart, data, sizeof(data));
    41d0:	4792                	lw	a5,4(sp)
    41d2:	4502                	lw	a0,0(sp)
    41d4:	4649                	li	a2,18
    41d6:	fec18593          	addi	a1,gp,-20 # 20000660 <data>
    41da:	31e5                	jal	3ec2 <usart_send_sync>
        printf("- - -[y/n] ");
    41dc:	00006537          	lui	a0,0x6
    41e0:	eb450513          	addi	a0,a0,-332 # 5eb4 <sg_usi_config+0x224>
    41e4:	367000ef          	jal	ra,4d4a <printf>
    41e8:	c622                	sw	s0,12(sp)
                printf("\n\tPlease enter 'y' or 'n'   ");
    41ea:	00006437          	lui	s0,0x6
            get = usart_wait_reply(usart);
    41ee:	4502                	lw	a0,0(sp)
    41f0:	33e5                	jal	3fd8 <usart_wait_reply>
            if (get == 1 || get == 0) {
    41f2:	4785                	li	a5,1
    41f4:	04a7fc63          	bgeu	a5,a0,424c <_usart_test+0x194>
                printf("\n\tPlease enter 'y' or 'n'   ");
    41f8:	dd840513          	addi	a0,s0,-552 # 5dd8 <sg_usi_config+0x148>
    41fc:	34f000ef          	jal	ra,4d4a <printf>
    4200:	b7fd                	j	41ee <_usart_test+0x136>
        printf("\t- - -FAILURE\n");
    4202:	00006537          	lui	a0,0x6
    4206:	e2450513          	addi	a0,a0,-476 # 5e24 <sg_usi_config+0x194>
    420a:	46d000ef          	jal	ra,4e76 <puts>
        }
    }

    ret = usart_test_sync_mode(usart);

    if (ret < 0) {
    420e:	00006537          	lui	a0,0x6
    4212:	55fd                	li	a1,-1
    4214:	e3450513          	addi	a0,a0,-460 # 5e34 <sg_usi_config+0x1a4>
        return -1;
    }

    ret = usart_test_trigger_event(usart, uart_idx);

    if (ret < 0) {
    4218:	333000ef          	jal	ra,4d4a <printf>
        printf("usart_test_trigger error %x\n", ret);
    421c:	557d                	li	a0,-1
        return -1;
    }

    return 0;
    421e:	4096                	lw	ra,68(sp)
    4220:	4406                	lw	s0,64(sp)
    4222:	54f2                	lw	s1,60(sp)
    4224:	04810113          	addi	sp,sp,72
    4228:	8082                	ret
            printf("csi_usart_config error %x\n", ret);
    422a:	00006537          	lui	a0,0x6
    422e:	e6c50513          	addi	a0,a0,-404 # 5e6c <sg_usi_config+0x1dc>
    4232:	319000ef          	jal	ra,4d4a <printf>
    if (ret < 0) {
    4236:	00006537          	lui	a0,0x6
    423a:	55fd                	li	a1,-1
    423c:	f0850513          	addi	a0,a0,-248 # 5f08 <sg_usi_config+0x278>
    4240:	bfe1                	j	4218 <_usart_test+0x160>
            printf("\n\tPlease enter 'y'   ");
    4242:	e8840513          	addi	a0,s0,-376
    4246:	305000ef          	jal	ra,4d4a <printf>
    424a:	bf85                	j	41ba <_usart_test+0x102>
        if (get == 1) {
    424c:	08f51263          	bne	a0,a5,42d0 <_usart_test+0x218>
            printf("\t- - -PASS\n");
    4250:	47a2                	lw	a5,8(sp)
    4252:	0491                	addi	s1,s1,4
    4254:	df878513          	addi	a0,a5,-520
    4258:	41f000ef          	jal	ra,4e76 <puts>
    for (baudrate = 0; baudrate < sizeof(pB) / 4; baudrate++) {
    425c:	187c                	addi	a5,sp,60
    425e:	f2f499e3          	bne	s1,a5,4190 <_usart_test+0xd8>
    printf("\n\t- - - Test usart parity. (Parity: 0 --- ODD, 1 --- EVEN, 2 --- NONE)\n");
    4262:	00006537          	lui	a0,0x6
    4266:	ec050513          	addi	a0,a0,-320 # 5ec0 <sg_usi_config+0x230>
    426a:	40d000ef          	jal	ra,4e76 <puts>
    for (parity = 0; parity < 5; parity++) {
    426e:	4481                	li	s1,0
        switch (parity) {
    4270:	4785                	li	a5,1
    4272:	0af48663          	beq	s1,a5,431e <_usart_test+0x266>
    4276:	c4a5                	beqz	s1,42de <_usart_test+0x226>
    4278:	4789                	li	a5,2
    427a:	0af48d63          	beq	s1,a5,4334 <_usart_test+0x27c>
        if (parity == 3 || parity == 4) {
    427e:	ffd48793          	addi	a5,s1,-3
    4282:	4705                	li	a4,1
    4284:	0cf77f63          	bgeu	a4,a5,4362 <_usart_test+0x2aa>
            printf("\n\tPlease enter 'y'   ");
    4288:	00006437          	lui	s0,0x6
        while (usart_wait_reply(usart) != 1) {
    428c:	4502                	lw	a0,0(sp)
    428e:	33a9                	jal	3fd8 <usart_wait_reply>
    4290:	4785                	li	a5,1
    4292:	0af51c63          	bne	a0,a5,434a <_usart_test+0x292>
        printf(" :Output is ---\n\t\t");
    4296:	47b2                	lw	a5,12(sp)
                printf("\n\tPlease enter 'y' or 'n'   ");
    4298:	00006437          	lui	s0,0x6
        printf(" :Output is ---\n\t\t");
    429c:	ea078513          	addi	a0,a5,-352
    42a0:	2ab000ef          	jal	ra,4d4a <printf>
        usart_send_sync(usart, data, sizeof(data));
    42a4:	4792                	lw	a5,4(sp)
    42a6:	4502                	lw	a0,0(sp)
    42a8:	4649                	li	a2,18
    42aa:	fec18593          	addi	a1,gp,-20 # 20000660 <data>
    42ae:	3911                	jal	3ec2 <usart_send_sync>
        printf("- - -[y/n] ");
    42b0:	00006537          	lui	a0,0x6
    42b4:	eb450513          	addi	a0,a0,-332 # 5eb4 <sg_usi_config+0x224>
    42b8:	293000ef          	jal	ra,4d4a <printf>
            get = usart_wait_reply(usart);
    42bc:	4502                	lw	a0,0(sp)
    42be:	3b29                	jal	3fd8 <usart_wait_reply>
            if (get == 1 || get == 0) {
    42c0:	4785                	li	a5,1
    42c2:	08a7f963          	bgeu	a5,a0,4354 <_usart_test+0x29c>
                printf("\n\tPlease enter 'y' or 'n'   ");
    42c6:	dd840513          	addi	a0,s0,-552 # 5dd8 <sg_usi_config+0x148>
    42ca:	281000ef          	jal	ra,4d4a <printf>
    42ce:	b7fd                	j	42bc <_usart_test+0x204>
            printf("\t- - -FAILURE\n");
    42d0:	00006537          	lui	a0,0x6
    42d4:	e2450513          	addi	a0,a0,-476 # 5e24 <sg_usi_config+0x194>
    42d8:	39f000ef          	jal	ra,4e76 <puts>

    42dc:	bfa9                	j	4236 <_usart_test+0x17e>
                printf("\tParity is %d? [y] ", parity);
    42de:	000067b7          	lui	a5,0x6
    42e2:	f2878513          	addi	a0,a5,-216 # 5f28 <sg_usi_config+0x298>
    42e6:	4581                	li	a1,0
    42e8:	263000ef          	jal	ra,4d4a <printf>
                ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_ODD, USART_STOP_BITS_1, USART_DATA_BITS_8);
    42ec:	478d                	li	a5,3
    42ee:	4701                	li	a4,0
    42f0:	4689                	li	a3,2
                ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    42f2:	4502                	lw	a0,0(sp)
    42f4:	65f1                	lui	a1,0x1c
    42f6:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
    42fa:	4601                	li	a2,0
    42fc:	d0eff0ef          	jal	ra,380a <csi_usart_config>
    4300:	85aa                	mv	a1,a0
                if (ret < 0) {
    4302:	f6055ee3          	bgez	a0,427e <_usart_test+0x1c6>
                    printf("csi_usart_config error %x\n", ret);
    4306:	00006537          	lui	a0,0x6
    430a:	e6c50513          	addi	a0,a0,-404 # 5e6c <sg_usi_config+0x1dc>
    430e:	23d000ef          	jal	ra,4d4a <printf>
    if (ret < 0) {
    4312:	00006537          	lui	a0,0x6
    4316:	55fd                	li	a1,-1
    4318:	fac50513          	addi	a0,a0,-84 # 5fac <sg_usi_config+0x31c>
    431c:	bdf5                	j	4218 <_usart_test+0x160>
                printf("\tParity is %d? [y] ", parity);
    431e:	000067b7          	lui	a5,0x6
    4322:	f2878513          	addi	a0,a5,-216 # 5f28 <sg_usi_config+0x298>
    4326:	4585                	li	a1,1
    4328:	223000ef          	jal	ra,4d4a <printf>
                ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_EVEN, USART_STOP_BITS_1, USART_DATA_BITS_8);
    432c:	478d                	li	a5,3
    432e:	4701                	li	a4,0
    4330:	4685                	li	a3,1
    4332:	b7c1                	j	42f2 <_usart_test+0x23a>
                printf("\tParity is %d? [y] ", parity);
    4334:	000067b7          	lui	a5,0x6
    4338:	f2878513          	addi	a0,a5,-216 # 5f28 <sg_usi_config+0x298>
    433c:	4589                	li	a1,2
    433e:	20d000ef          	jal	ra,4d4a <printf>
                ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    4342:	478d                	li	a5,3
    4344:	4701                	li	a4,0
    4346:	4681                	li	a3,0
    4348:	b76d                	j	42f2 <_usart_test+0x23a>
            printf("\n\tPlease enter 'y'   ");
    434a:	e8840513          	addi	a0,s0,-376
    434e:	1fd000ef          	jal	ra,4d4a <printf>
    4352:	bf2d                	j	428c <_usart_test+0x1d4>
        if (get == 1) {
    4354:	0af51363          	bne	a0,a5,43fa <_usart_test+0x342>
            printf("\t- - -PASS\n");
    4358:	47a2                	lw	a5,8(sp)
    435a:	df878513          	addi	a0,a5,-520
    435e:	319000ef          	jal	ra,4e76 <puts>
    for (parity = 0; parity < 5; parity++) {
    4362:	0485                	addi	s1,s1,1
    4364:	4795                	li	a5,5
    4366:	f0f495e3          	bne	s1,a5,4270 <_usart_test+0x1b8>
    printf("\n\t- - - Test usart databits.\n");
    436a:	00006537          	lui	a0,0x6
    436e:	f3c50513          	addi	a0,a0,-196 # 5f3c <sg_usi_config+0x2ac>
    4372:	305000ef          	jal	ra,4e76 <puts>
    printf("\t0 --- WORD_SIZE_5,\n\t1 --- WORD_SIZE_6,\n"
    4376:	00006537          	lui	a0,0x6
    437a:	f5c50513          	addi	a0,a0,-164 # 5f5c <sg_usi_config+0x2cc>
    437e:	2f9000ef          	jal	ra,4e76 <puts>
    for (databits = 2; databits < 4; databits++) {
    4382:	4489                	li	s1,2
        printf("\tWordsize is %d? [y] ", databits);
    4384:	000067b7          	lui	a5,0x6
    4388:	fc878513          	addi	a0,a5,-56 # 5fc8 <sg_usi_config+0x338>
    438c:	85a6                	mv	a1,s1
    438e:	1bd000ef          	jal	ra,4d4a <printf>
        if (databits == 2) {
    4392:	4789                	li	a5,2
    4394:	06f49a63          	bne	s1,a5,4408 <_usart_test+0x350>
            ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_7);
    4398:	4502                	lw	a0,0(sp)
    439a:	65f1                	lui	a1,0x1c
    439c:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
    43a0:	4701                	li	a4,0
    43a2:	4681                	li	a3,0
    43a4:	4601                	li	a2,0
    43a6:	c64ff0ef          	jal	ra,380a <csi_usart_config>
    43aa:	85aa                	mv	a1,a0
            if (ret < 0) {
    43ac:	06054063          	bltz	a0,440c <_usart_test+0x354>
            printf("\n\tPlease enter 'y'   ");
    43b0:	00006437          	lui	s0,0x6
        while (usart_wait_reply(usart) != 1) {
    43b4:	4502                	lw	a0,0(sp)
    43b6:	310d                	jal	3fd8 <usart_wait_reply>
    43b8:	4785                	li	a5,1
    43ba:	06f51563          	bne	a0,a5,4424 <_usart_test+0x36c>
        printf(" :Output is ---\n\t\t");
    43be:	47b2                	lw	a5,12(sp)
        printf("- - -[y/n] ");
    43c0:	00006437          	lui	s0,0x6
        printf(" :Output is ---\n\t\t");
    43c4:	ea078513          	addi	a0,a5,-352
    43c8:	183000ef          	jal	ra,4d4a <printf>
        usart_send_sync(usart, data, sizeof(data));
    43cc:	4792                	lw	a5,4(sp)
    43ce:	4502                	lw	a0,0(sp)
    43d0:	4649                	li	a2,18
    43d2:	fec18593          	addi	a1,gp,-20 # 20000660 <data>
    43d6:	34f5                	jal	3ec2 <usart_send_sync>
        printf("- - -[y/n] ");
    43d8:	eb440513          	addi	a0,s0,-332 # 5eb4 <sg_usi_config+0x224>
    43dc:	16f000ef          	jal	ra,4d4a <printf>
    43e0:	ca22                	sw	s0,20(sp)
                printf("\n\tPlease enter 'y' or 'n'   ");
    43e2:	00006437          	lui	s0,0x6
            get = usart_wait_reply(usart);
    43e6:	4502                	lw	a0,0(sp)
    43e8:	3ec5                	jal	3fd8 <usart_wait_reply>
            if (get == 1 || get == 0) {
    43ea:	4785                	li	a5,1
    43ec:	04a7f163          	bgeu	a5,a0,442e <_usart_test+0x376>
                printf("\n\tPlease enter 'y' or 'n'   ");
    43f0:	dd840513          	addi	a0,s0,-552 # 5dd8 <sg_usi_config+0x148>
    43f4:	157000ef          	jal	ra,4d4a <printf>
    43f8:	b7fd                	j	43e6 <_usart_test+0x32e>
            printf("\t- - -FAILURE\n");
    43fa:	00006537          	lui	a0,0x6
    43fe:	e2450513          	addi	a0,a0,-476 # 5e24 <sg_usi_config+0x194>
    4402:	275000ef          	jal	ra,4e76 <puts>

    4406:	b731                	j	4312 <_usart_test+0x25a>
            ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    4408:	478d                	li	a5,3
    440a:	b779                	j	4398 <_usart_test+0x2e0>
                printf("csi_usart_config error %x\n", ret);
    440c:	00006537          	lui	a0,0x6
    4410:	e6c50513          	addi	a0,a0,-404 # 5e6c <sg_usi_config+0x1dc>
    4414:	137000ef          	jal	ra,4d4a <printf>
    if (ret < 0) {
    4418:	00006537          	lui	a0,0x6
    441c:	55fd                	li	a1,-1
    441e:	03450513          	addi	a0,a0,52 # 6034 <sg_usi_config+0x3a4>
    4422:	bbdd                	j	4218 <_usart_test+0x160>
            printf("\n\tPlease enter 'y'   ");
    4424:	e8840513          	addi	a0,s0,-376
    4428:	123000ef          	jal	ra,4d4a <printf>
    442c:	b761                	j	43b4 <_usart_test+0x2fc>
        if (get == 1) {
    442e:	00f51c63          	bne	a0,a5,4446 <_usart_test+0x38e>
            printf("\t- - -PASS\n");
    4432:	47a2                	lw	a5,8(sp)
    4434:	df878513          	addi	a0,a5,-520
    4438:	23f000ef          	jal	ra,4e76 <puts>
    443c:	478d                	li	a5,3
    for (databits = 2; databits < 4; databits++) {
    443e:	00f48b63          	beq	s1,a5,4454 <_usart_test+0x39c>
    4442:	448d                	li	s1,3
    4444:	b781                	j	4384 <_usart_test+0x2cc>
            printf("\t- - -FAILURE\n");
    4446:	00006537          	lui	a0,0x6
    444a:	e2450513          	addi	a0,a0,-476 # 5e24 <sg_usi_config+0x194>
    444e:	229000ef          	jal	ra,4e76 <puts>

    4452:	b7d9                	j	4418 <_usart_test+0x360>
    printf("\n\t- - - Test usart stopbits.\n");
    4454:	00006537          	lui	a0,0x6
    4458:	fe050513          	addi	a0,a0,-32 # 5fe0 <sg_usi_config+0x350>
    445c:	21b000ef          	jal	ra,4e76 <puts>
    printf("\t0 --- USART_STOP_BITS_1,\n\t1 --- USART_STOP_BITS_2,\n");
    4460:	00006537          	lui	a0,0x6
    4464:	00050513          	mv	a0,a0
    4468:	20f000ef          	jal	ra,4e76 <puts>
    for (stopbits = 0; stopbits < 2; stopbits++) {
    446c:	4481                	li	s1,0
        printf("\tstopbits is %d? [y] ", stopbits);
    446e:	000067b7          	lui	a5,0x6
    4472:	05478513          	addi	a0,a5,84 # 6054 <sg_usi_config+0x3c4>
    4476:	85a6                	mv	a1,s1
    4478:	0d3000ef          	jal	ra,4d4a <printf>
            ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    447c:	478d                	li	a5,3
            ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_2, USART_DATA_BITS_8);
    447e:	4705                	li	a4,1
        if (stopbits == 0) {
    4480:	e091                	bnez	s1,4484 <_usart_test+0x3cc>
            ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    4482:	4701                	li	a4,0
    4484:	4502                	lw	a0,0(sp)
    4486:	65f1                	lui	a1,0x1c
    4488:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
    448c:	4681                	li	a3,0
    448e:	4601                	li	a2,0
    4490:	b7aff0ef          	jal	ra,380a <csi_usart_config>
    4494:	85aa                	mv	a1,a0
            if (ret < 0) {
    4496:	04054663          	bltz	a0,44e2 <_usart_test+0x42a>
            printf("\n\tPlease enter 'y'   ");
    449a:	00006437          	lui	s0,0x6
        while (usart_wait_reply(usart) != 1) {
    449e:	4502                	lw	a0,0(sp)
    44a0:	3e25                	jal	3fd8 <usart_wait_reply>
    44a2:	4785                	li	a5,1
    44a4:	04f51b63          	bne	a0,a5,44fa <_usart_test+0x442>
        printf(" :Output is ---\n\t\t");
    44a8:	47b2                	lw	a5,12(sp)
                printf("\n\tPlease enter 'y' or 'n'   ");
    44aa:	00006437          	lui	s0,0x6
        printf(" :Output is ---\n\t\t");
    44ae:	ea078513          	addi	a0,a5,-352
    44b2:	099000ef          	jal	ra,4d4a <printf>
        usart_send_sync(usart, data, sizeof(data));
    44b6:	4792                	lw	a5,4(sp)
    44b8:	4502                	lw	a0,0(sp)
    44ba:	4649                	li	a2,18
    44bc:	fec18593          	addi	a1,gp,-20 # 20000660 <data>
    44c0:	a03ff0ef          	jal	ra,3ec2 <usart_send_sync>
        printf("- - -[y/n] ");
    44c4:	47d2                	lw	a5,20(sp)
    44c6:	eb478513          	addi	a0,a5,-332
    44ca:	081000ef          	jal	ra,4d4a <printf>
            get = usart_wait_reply(usart);
    44ce:	4502                	lw	a0,0(sp)
    44d0:	3621                	jal	3fd8 <usart_wait_reply>
            if (get == 1 || get == 0) {
    44d2:	4705                	li	a4,1
    44d4:	02a77863          	bgeu	a4,a0,4504 <_usart_test+0x44c>
                printf("\n\tPlease enter 'y' or 'n'   ");
    44d8:	dd840513          	addi	a0,s0,-552 # 5dd8 <sg_usi_config+0x148>
    44dc:	06f000ef          	jal	ra,4d4a <printf>
    44e0:	b7fd                	j	44ce <_usart_test+0x416>
                printf("csi_usart_config error %x\n", ret);
    44e2:	00006537          	lui	a0,0x6
    44e6:	e6c50513          	addi	a0,a0,-404 # 5e6c <sg_usi_config+0x1dc>
    44ea:	061000ef          	jal	ra,4d4a <printf>
    if (ret < 0) {
    44ee:	00006537          	lui	a0,0x6
    44f2:	55fd                	li	a1,-1
    44f4:	06c50513          	addi	a0,a0,108 # 606c <sg_usi_config+0x3dc>
    44f8:	b305                	j	4218 <_usart_test+0x160>
            printf("\n\tPlease enter 'y'   ");
    44fa:	e8840513          	addi	a0,s0,-376
    44fe:	04d000ef          	jal	ra,4d4a <printf>
    4502:	bf71                	j	449e <_usart_test+0x3e6>
        if (get == 1) {
    4504:	cc2a                	sw	a0,24(sp)
    4506:	00e51c63          	bne	a0,a4,451e <_usart_test+0x466>
            printf("\t- - -PASS\n");
    450a:	47a2                	lw	a5,8(sp)
    450c:	df878513          	addi	a0,a5,-520
    4510:	167000ef          	jal	ra,4e76 <puts>
    for (stopbits = 0; stopbits < 2; stopbits++) {
    4514:	47e2                	lw	a5,24(sp)
    4516:	00f48b63          	beq	s1,a5,452c <_usart_test+0x474>
    451a:	4485                	li	s1,1
    451c:	bf89                	j	446e <_usart_test+0x3b6>
            printf("\t- - -FAILURE\n");
    451e:	00006537          	lui	a0,0x6
    4522:	e2450513          	addi	a0,a0,-476 # 5e24 <sg_usi_config+0x194>
    4526:	151000ef          	jal	ra,4e76 <puts>

    452a:	b7d1                	j	44ee <_usart_test+0x436>
    csi_usart_uninitialize(usart);
    452c:	4502                	lw	a0,0(sp)
    452e:	adaff0ef          	jal	ra,3808 <csi_usart_uninitialize>
    usart = csi_usart_initialize(uart_idx, (usart_event_cb_t)usart_event_cb);
    4532:	4542                	lw	a0,16(sp)
    4534:	000045b7          	lui	a1,0x4
    4538:	f3e58593          	addi	a1,a1,-194 # 3f3e <usart_event_cb>
    453c:	acaff0ef          	jal	ra,3806 <csi_usart_initialize>
    4540:	842a                	mv	s0,a0
    if (usart == NULL) {
    4542:	e519                	bnez	a0,4550 <_usart_test+0x498>
    if (ret < 0) {
    4544:	00006537          	lui	a0,0x6
    4548:	55fd                	li	a1,-1
    454a:	0d850513          	addi	a0,a0,216 # 60d8 <sg_usi_config+0x448>
    454e:	b1e9                	j	4218 <_usart_test+0x160>
    ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    4550:	65f1                	lui	a1,0x1c
    g_usart_handle = usart;
    4552:	200024b7          	lui	s1,0x20002
    ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    4556:	478d                	li	a5,3
    4558:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
    455c:	4701                	li	a4,0
    455e:	4681                	li	a3,0
    4560:	4601                	li	a2,0
    g_usart_handle = usart;
    4562:	c6a4a423          	sw	a0,-920(s1) # 20001c68 <g_usart_handle>
    ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    4566:	aa4ff0ef          	jal	ra,380a <csi_usart_config>
    456a:	c6848793          	addi	a5,s1,-920
    456e:	c23e                	sw	a5,4(sp)
    4570:	85aa                	mv	a1,a0
    if (ret < 0) {
    4572:	00055963          	bgez	a0,4584 <_usart_test+0x4cc>
        printf("csi_usart_config error %x\n", ret);
    4576:	00006537          	lui	a0,0x6
    457a:	e6c50513          	addi	a0,a0,-404 # 5e6c <sg_usi_config+0x1dc>
    457e:	7cc000ef          	jal	ra,4d4a <printf>

    4582:	b7c9                	j	4544 <_usart_test+0x48c>
    printf("\r\t(async mode ): Output is---\n\t\t");
    4584:	00006537          	lui	a0,0x6
    4588:	08c50513          	addi	a0,a0,140 # 608c <sg_usi_config+0x3fc>
    458c:	7be000ef          	jal	ra,4d4a <printf>
    usart_send_async(usart, data, sizeof(data));
    4590:	4649                	li	a2,18
    4592:	fec18593          	addi	a1,gp,-20 # 20000660 <data>
    4596:	8522                	mv	a0,s0
    4598:	963ff0ef          	jal	ra,3efa <usart_send_async>
    printf("- - - [y/n] ");
    459c:	00006537          	lui	a0,0x6
    45a0:	dc850513          	addi	a0,a0,-568 # 5dc8 <sg_usi_config+0x138>
    45a4:	7a6000ef          	jal	ra,4d4a <printf>
    rx_async_flag = 0;
    45a8:	200024b7          	lui	s1,0x20002
    for (i = 0; i < 20; i++) {
    45ac:	d002                	sw	zero,32(sp)
    45ae:	474d                	li	a4,19
    45b0:	5782                	lw	a5,32(sp)
    45b2:	02f77e63          	bgeu	a4,a5,45ee <_usart_test+0x536>
    i = 0;
    45b6:	d002                	sw	zero,32(sp)
    while (i < 20) {
    45b8:	5702                	lw	a4,32(sp)
    45ba:	47cd                	li	a5,19
    45bc:	06e7e263          	bltu	a5,a4,4620 <_usart_test+0x568>
    rx_async_flag = 0;
    45c0:	c6c48793          	addi	a5,s1,-916 # 20001c6c <rx_async_flag>
    csi_usart_receive(usart, data, num);
    45c4:	4605                	li	a2,1
    45c6:	01f10593          	addi	a1,sp,31
    45ca:	8522                	mv	a0,s0
    rx_async_flag = 0;
    45cc:	00078023          	sb	zero,0(a5)
    csi_usart_receive(usart, data, num);
    45d0:	a3eff0ef          	jal	ra,380e <csi_usart_receive>
    int time_out = 0x7fffff;
    45d4:	008007b7          	lui	a5,0x800
    45d8:	17fd                	addi	a5,a5,-1
    45da:	c6c48713          	addi	a4,s1,-916
        if (rx_async_flag == 1) {
    45de:	4685                	li	a3,1
    45e0:	00074603          	lbu	a2,0(a4)
        time_out--;
    45e4:	17fd                	addi	a5,a5,-1
        if (rx_async_flag == 1) {
    45e6:	00c68d63          	beq	a3,a2,4600 <_usart_test+0x548>
    while (time_out) {
    45ea:	d7f9                	beqz	a5,45b8 <_usart_test+0x500>
    45ec:	bfd5                	j	45e0 <_usart_test+0x528>
        answer[i] = '\0';
    45ee:	5782                	lw	a5,32(sp)
    45f0:	1874                	addi	a3,sp,60
    45f2:	97b6                	add	a5,a5,a3
    45f4:	fe078423          	sb	zero,-24(a5) # 7fffe8 <__ctor_end__+0x7f96c0>
    for (i = 0; i < 20; i++) {
    45f8:	5782                	lw	a5,32(sp)
    45fa:	0785                	addi	a5,a5,1
    45fc:	d03e                	sw	a5,32(sp)
    45fe:	bf4d                	j	45b0 <_usart_test+0x4f8>
    if (0 == time_out) {
    4600:	dfc5                	beqz	a5,45b8 <_usart_test+0x500>
            if (ch == '\n' || ch == '\r') {
    4602:	01f14783          	lbu	a5,31(sp)
    rx_async_flag = 0;
    4606:	00070023          	sb	zero,0(a4)
            if (ch == '\n' || ch == '\r') {
    460a:	4729                	li	a4,10
    460c:	00e78563          	beq	a5,a4,4616 <_usart_test+0x55e>
    4610:	4735                	li	a4,13
    4612:	04e79f63          	bne	a5,a4,4670 <_usart_test+0x5b8>
                answer[i] = '\0';
    4616:	5782                	lw	a5,32(sp)
    4618:	1878                	addi	a4,sp,60
    461a:	97ba                	add	a5,a5,a4
    461c:	fe078423          	sb	zero,-24(a5)
    if ((i == 1) && (answer[0] == 'y')) {
    4620:	5702                	lw	a4,32(sp)
    4622:	4785                	li	a5,1
    4624:	08f71263          	bne	a4,a5,46a8 <_usart_test+0x5f0>
    4628:	02414703          	lbu	a4,36(sp)
    462c:	07900793          	li	a5,121
    4630:	06f71c63          	bne	a4,a5,46a8 <_usart_test+0x5f0>
        printf("\t- - -PASS\n");
    4634:	00006537          	lui	a0,0x6
    4638:	df850513          	addi	a0,a0,-520 # 5df8 <sg_usi_config+0x168>
    463c:	03b000ef          	jal	ra,4e76 <puts>
    printf("\r\t waitting uart  trigger event---\n\t\t");
    4640:	00006537          	lui	a0,0x6
    4644:	0b050513          	addi	a0,a0,176 # 60b0 <sg_usi_config+0x420>
    4648:	702000ef          	jal	ra,4d4a <printf>
    csi_usart_uninitialize(usart);
    464c:	4502                	lw	a0,0(sp)
    464e:	9baff0ef          	jal	ra,3808 <csi_usart_uninitialize>
    usart = csi_usart_initialize(uart_idx, (usart_event_cb_t)usart_event_cb_query);
    4652:	4542                	lw	a0,16(sp)
    4654:	000045b7          	lui	a1,0x4
    4658:	f7c58593          	addi	a1,a1,-132 # 3f7c <usart_event_cb_query>
    465c:	9aaff0ef          	jal	ra,3806 <csi_usart_initialize>
    4660:	842a                	mv	s0,a0
    if (usart == NULL) {
    4662:	e93d                	bnez	a0,46d8 <_usart_test+0x620>
    if (ret < 0) {
    4664:	00006537          	lui	a0,0x6
    4668:	55fd                	li	a1,-1
    466a:	13850513          	addi	a0,a0,312 # 6138 <sg_usi_config+0x4a8>
    466e:	b66d                	j	4218 <_usart_test+0x160>
            if (ch == 127 || ch == '\b') {
    4670:	07f00713          	li	a4,127
    4674:	00e78563          	beq	a5,a4,467e <_usart_test+0x5c6>
    4678:	4721                	li	a4,8
    467a:	00e79e63          	bne	a5,a4,4696 <_usart_test+0x5de>
                if (i > 0) {
    467e:	5782                	lw	a5,32(sp)
    4680:	df85                	beqz	a5,45b8 <_usart_test+0x500>
                    i--;
    4682:	5782                	lw	a5,32(sp)
    4684:	17fd                	addi	a5,a5,-1
    4686:	d03e                	sw	a5,32(sp)
                usart_send_async(usart, &ch, 1);
    4688:	4605                	li	a2,1
    468a:	01f10593          	addi	a1,sp,31
    468e:	8522                	mv	a0,s0
    4690:	86bff0ef          	jal	ra,3efa <usart_send_async>
    4694:	b715                	j	45b8 <_usart_test+0x500>
                answer[i++] = ch;
    4696:	5702                	lw	a4,32(sp)
    4698:	00170693          	addi	a3,a4,1
    469c:	d036                	sw	a3,32(sp)
    469e:	1874                	addi	a3,sp,60
    46a0:	9736                	add	a4,a4,a3
    46a2:	fef70423          	sb	a5,-24(a4)
    46a6:	b7cd                	j	4688 <_usart_test+0x5d0>
    } else if ((i == 1) && (answer[0] == 'n')) {
    46a8:	5702                	lw	a4,32(sp)
    46aa:	4785                	li	a5,1
    46ac:	00f71f63          	bne	a4,a5,46ca <_usart_test+0x612>
    46b0:	02414703          	lbu	a4,36(sp)
    46b4:	06e00793          	li	a5,110
    46b8:	00f71963          	bne	a4,a5,46ca <_usart_test+0x612>
        printf("\t- - -FAILURE\n");
    46bc:	00006537          	lui	a0,0x6
    46c0:	e2450513          	addi	a0,a0,-476 # 5e24 <sg_usi_config+0x194>
    46c4:	7b2000ef          	jal	ra,4e76 <puts>

    46c8:	bdb5                	j	4544 <_usart_test+0x48c>
            printf("\n\tPlease enter 'y' or 'n'   ");
    46ca:	00006537          	lui	a0,0x6
    46ce:	dd850513          	addi	a0,a0,-552 # 5dd8 <sg_usi_config+0x148>
    46d2:	678000ef          	jal	ra,4d4a <printf>
    46d6:	bdd9                	j	45ac <_usart_test+0x4f4>
    g_usart_handle = usart;
    46d8:	4792                	lw	a5,4(sp)
    ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    46da:	65f1                	lui	a1,0x1c
    46dc:	4701                	li	a4,0
    g_usart_handle = usart;
    46de:	c388                	sw	a0,0(a5)
    ret = csi_usart_config(usart, 115200, USART_MODE_ASYNCHRONOUS, USART_PARITY_NONE, USART_STOP_BITS_1, USART_DATA_BITS_8);
    46e0:	4681                	li	a3,0
    46e2:	478d                	li	a5,3
    46e4:	4601                	li	a2,0
    46e6:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
    46ea:	920ff0ef          	jal	ra,380a <csi_usart_config>
    if (ret != 0) {
    46ee:	f93d                	bnez	a0,4664 <_usart_test+0x5ac>
    rx_async_flag = 0;
    46f0:	200027b7          	lui	a5,0x20002
    46f4:	c6078623          	sb	zero,-916(a5) # 20001c6c <rx_async_flag>
    tx_async_flag = 0;
    46f8:	200027b7          	lui	a5,0x20002
    46fc:	c6e78713          	addi	a4,a5,-914 # 20001c6e <tx_async_flag>
    4700:	00070023          	sb	zero,0(a4)
    rx_trigger_flag = 0;
    4704:	20002737          	lui	a4,0x20002
    4708:	c6d70693          	addi	a3,a4,-915 # 20001c6d <rx_trigger_flag>
    470c:	00068023          	sb	zero,0(a3)
    while (rx_trigger_flag == 0);
    4710:	c6e78793          	addi	a5,a5,-914
    4714:	c6d70713          	addi	a4,a4,-915
    4718:	00074683          	lbu	a3,0(a4)
    471c:	0ff6f693          	andi	a3,a3,255
    4720:	dee5                	beqz	a3,4718 <_usart_test+0x660>
    while (tx_async_flag == 0);
    4722:	0007c703          	lbu	a4,0(a5)
    4726:	0ff77713          	andi	a4,a4,255
    472a:	df65                	beqz	a4,4722 <_usart_test+0x66a>
    printf("----- usart trigger PASS\n");
    472c:	00006537          	lui	a0,0x6
    4730:	0f850513          	addi	a0,a0,248 # 60f8 <sg_usi_config+0x468>
    4734:	742000ef          	jal	ra,4e76 <puts>
    ret = csi_usart_uninitialize(usart);
    4738:	8522                	mv	a0,s0
    473a:	8ceff0ef          	jal	ra,3808 <csi_usart_uninitialize>
    473e:	85aa                	mv	a1,a0

    4740:	4501                	li	a0,0
    if (ret < 0) {
    4742:	ac05dee3          	bgez	a1,421e <_usart_test+0x166>
        printf("csi_usart_uninitialize error %x\n", ret);
    4746:	00006537          	lui	a0,0x6
    474a:	11450513          	addi	a0,a0,276 # 6114 <sg_usi_config+0x484>
    474e:	5fc000ef          	jal	ra,4d4a <printf>

    4752:	bf09                	j	4664 <_usart_test+0x5ac>

00004754 <example_pin_usart_init>:
}

void example_pin_usart_init(void)
    4754:	1151                	addi	sp,sp,-12
{
    4756:	4581                	li	a1,0
    4758:	02000513          	li	a0,32
void example_pin_usart_init(void)
    475c:	c406                	sw	ra,8(sp)
{
    475e:	bfeff0ef          	jal	ra,3b5c <drv_pinmux_config>
    drv_pinmux_config(EXAMPLE_PIN_USART_TX, EXAMPLE_PIN_USART_TX_FUNC);
    drv_pinmux_config(EXAMPLE_PIN_USART_RX, EXAMPLE_PIN_USART_RX_FUNC);
    4762:	40a2                	lw	ra,8(sp)
    drv_pinmux_config(EXAMPLE_PIN_USART_TX, EXAMPLE_PIN_USART_TX_FUNC);
    4764:	4581                	li	a1,0
    4766:	02100513          	li	a0,33
    drv_pinmux_config(EXAMPLE_PIN_USART_RX, EXAMPLE_PIN_USART_RX_FUNC);
    476a:	0131                	addi	sp,sp,12
    drv_pinmux_config(EXAMPLE_PIN_USART_TX, EXAMPLE_PIN_USART_TX_FUNC);
    476c:	bf0ff06f          	j	3b5c <drv_pinmux_config>

00004770 <test_usart>:
}

int test_usart(int32_t uart_idx)
    4770:	1151                	addi	sp,sp,-12
{
    usart_handle_t p_csi_usart;
    int32_t ret;

    /* init the USART*/
    4772:	4581                	li	a1,0
int test_usart(int32_t uart_idx)
    4774:	c026                	sw	s1,0(sp)
    4776:	c406                	sw	ra,8(sp)
    4778:	c222                	sw	s0,4(sp)
    477a:	84aa                	mv	s1,a0
    /* init the USART*/
    477c:	88aff0ef          	jal	ra,3806 <csi_usart_initialize>
    p_csi_usart = csi_usart_initialize(uart_idx, NULL);

    4780:	ed01                	bnez	a0,4798 <test_usart+0x28>
    if (p_csi_usart == NULL) {
    4782:	00006537          	lui	a0,0x6
    4786:	16c50513          	addi	a0,a0,364 # 616c <sg_usi_config+0x4dc>
    478a:	25f5                	jal	4e76 <puts>
    }

    ret = _usart_test(p_csi_usart, uart_idx);

    if (ret < 0) {
        printf("_usart_test error %x\n", ret);
    478c:	557d                	li	a0,-1
        return -1;
    }

    printf("test_usart OK\n");

    return 0;
    478e:	40a2                	lw	ra,8(sp)
    4790:	4412                	lw	s0,4(sp)
    4792:	4482                	lw	s1,0(sp)
    4794:	0131                	addi	sp,sp,12
    4796:	8082                	ret
    4798:	842a                	mv	s0,a0

    479a:	3f6d                	jal	4754 <example_pin_usart_init>
    /* config the USART */
    479c:	65f1                	lui	a1,0x1c
    479e:	20058593          	addi	a1,a1,512 # 1c200 <__ctor_end__+0x158d8>
    47a2:	478d                	li	a5,3
    47a4:	4701                	li	a4,0
    47a6:	4681                	li	a3,0
    47a8:	4601                	li	a2,0
    47aa:	8522                	mv	a0,s0
    47ac:	85eff0ef          	jal	ra,380a <csi_usart_config>
    47b0:	85aa                	mv	a1,a0

    47b2:	00055863          	bgez	a0,47c2 <test_usart+0x52>
    if (ret < 0) {
    47b6:	00006537          	lui	a0,0x6
    47ba:	e6c50513          	addi	a0,a0,-404 # 5e6c <sg_usi_config+0x1dc>
    if (ret < 0) {
    47be:	2371                	jal	4d4a <printf>
    47c0:	b7f1                	j	478c <test_usart+0x1c>

    47c2:	85a6                	mv	a1,s1
    47c4:	8522                	mv	a0,s0
    47c6:	8f3ff0ef          	jal	ra,40b8 <_usart_test>
    47ca:	85aa                	mv	a1,a0

    47cc:	00055763          	bgez	a0,47da <test_usart+0x6a>
    if (ret < 0) {
    47d0:	00006537          	lui	a0,0x6
    47d4:	18850513          	addi	a0,a0,392 # 6188 <sg_usi_config+0x4f8>
    47d8:	b7dd                	j	47be <test_usart+0x4e>

    47da:	00006537          	lui	a0,0x6
    47de:	1a050513          	addi	a0,a0,416 # 61a0 <sg_usi_config+0x510>
    47e2:	2d51                	jal	4e76 <puts>

    47e4:	4501                	li	a0,0
    47e6:	b765                	j	478e <test_usart+0x1e>

000047e8 <example_usart>:
}


int example_usart(void)
    47e8:	1151                	addi	sp,sp,-12
{
    int ret;
    47ea:	4501                	li	a0,0
int example_usart(void)
    47ec:	c406                	sw	ra,8(sp)
    int ret;
    47ee:	3749                	jal	4770 <test_usart>
    ret = test_usart(EXAMPLE_USART_IDX);

    47f0:	4781                	li	a5,0
    47f2:	00055863          	bgez	a0,4802 <example_usart+0x1a>
    if (ret < 0) {
    47f6:	00006537          	lui	a0,0x6
    47fa:	15850513          	addi	a0,a0,344 # 6158 <sg_usi_config+0x4c8>
    47fe:	2da5                	jal	4e76 <puts>
        printf("test_usart failed\n");
    4800:	57fd                	li	a5,-1
        return -1;
    }

    return 0;
    4802:	40a2                	lw	ra,8(sp)
    4804:	853e                	mv	a0,a5
    4806:	0131                	addi	sp,sp,12
    4808:	8082                	ret

0000480a <main>:
}

int main(void)
{
    480a:	bff9                	j	47e8 <example_usart>

0000480c <copystring>:
    480c:	4781                	li	a5,0
    480e:	470d                	li	a4,3
    4810:	00b7c463          	blt	a5,a1,4818 <copystring+0xc>
    4814:	853e                	mv	a0,a5
    4816:	8082                	ret
    4818:	00f606b3          	add	a3,a2,a5
    481c:	0006c303          	lbu	t1,0(a3)
    4820:	00f506b3          	add	a3,a0,a5
    4824:	0785                	addi	a5,a5,1
    4826:	00668023          	sb	t1,0(a3)
    482a:	fee793e3          	bne	a5,a4,4810 <copystring+0x4>
    482e:	feb7d3e3          	bge	a5,a1,4814 <copystring+0x8>
    4832:	000501a3          	sb	zero,3(a0)
    4836:	4791                	li	a5,4
    4838:	bff1                	j	4814 <copystring+0x8>

0000483a <__dtostr>:
    483a:	fa810113          	addi	sp,sp,-88
    483e:	c8a2                	sw	s0,80(sp)
    4840:	ca86                	sw	ra,84(sp)
    4842:	c6a6                	sw	s1,76(sp)
    4844:	d02a                	sw	a0,32(sp)
    4846:	c42e                	sw	a1,8(sp)
    4848:	c032                	sw	a2,0(sp)
    484a:	8436                	mv	s0,a3
    484c:	d43a                	sw	a4,40(sp)
    484e:	c23e                	sw	a5,4(sp)
    4850:	2139                	jal	4c5e <__isinf>
    4852:	cd09                	beqz	a0,486c <__dtostr+0x32>
    4854:	00006637          	lui	a2,0x6
    4858:	1e860613          	addi	a2,a2,488 # 61e8 <sg_usi_config+0x558>
    485c:	85a2                	mv	a1,s0
    485e:	4446                	lw	s0,80(sp)
    4860:	4502                	lw	a0,0(sp)
    4862:	40d6                	lw	ra,84(sp)
    4864:	44b6                	lw	s1,76(sp)
    4866:	05810113          	addi	sp,sp,88
    486a:	b74d                	j	480c <copystring>
    486c:	5702                	lw	a4,32(sp)
    486e:	47a2                	lw	a5,8(sp)
    4870:	853a                	mv	a0,a4
    4872:	85be                	mv	a1,a5
    4874:	23b9                	jal	4dc2 <__isnan>
    4876:	d22a                	sw	a0,36(sp)
    4878:	c511                	beqz	a0,4884 <__dtostr+0x4a>
    487a:	00006637          	lui	a2,0x6
    487e:	1ec60613          	addi	a2,a2,492 # 61ec <sg_usi_config+0x55c>
    4882:	bfe9                	j	485c <__dtostr+0x22>
    4884:	5702                	lw	a4,32(sp)
    4886:	47a2                	lw	a5,8(sp)
    4888:	4601                	li	a2,0
    488a:	4681                	li	a3,0
    488c:	853a                	mv	a0,a4
    488e:	85be                	mv	a1,a5
    4890:	baffc0ef          	jal	ra,143e <__eqdf2>
    4894:	e925                	bnez	a0,4904 <__dtostr+0xca>
    4896:	4792                	lw	a5,4(sp)
    4898:	3a078e63          	beqz	a5,4c54 <__dtostr+0x41a>
    489c:	0789                	addi	a5,a5,2
    489e:	02f46163          	bltu	s0,a5,48c0 <__dtostr+0x86>
    48a2:	cb85                	beqz	a5,48d2 <__dtostr+0x98>
    48a4:	4722                	lw	a4,8(sp)
    48a6:	04075c63          	bgez	a4,48fe <__dtostr+0xc4>
    48aa:	4682                	lw	a3,0(sp)
    48ac:	02d00713          	li	a4,45
    48b0:	00e68023          	sb	a4,0(a3)
    48b4:	00178713          	addi	a4,a5,1
    48b8:	4785                	li	a5,1
    48ba:	03000693          	li	a3,48
    48be:	a801                	j	48ce <__dtostr+0x94>
    48c0:	47a1                	li	a5,8
    48c2:	b7cd                	j	48a4 <__dtostr+0x6a>
    48c4:	4602                	lw	a2,0(sp)
    48c6:	963e                	add	a2,a2,a5
    48c8:	00d60023          	sb	a3,0(a2)
    48cc:	0785                	addi	a5,a5,1
    48ce:	fee7ebe3          	bltu	a5,a4,48c4 <__dtostr+0x8a>
    48d2:	4702                	lw	a4,0(sp)
    48d4:	03000693          	li	a3,48
    48d8:	00074603          	lbu	a2,0(a4)
    48dc:	4705                	li	a4,1
    48de:	00d60363          	beq	a2,a3,48e4 <__dtostr+0xaa>
    48e2:	4709                	li	a4,2
    48e4:	4682                	lw	a3,0(sp)
    48e6:	9736                	add	a4,a4,a3
    48e8:	02e00693          	li	a3,46
    48ec:	00d70023          	sb	a3,0(a4)
    48f0:	4702                	lw	a4,0(sp)
    48f2:	00f704b3          	add	s1,a4,a5
    48f6:	00048023          	sb	zero,0(s1)
    48fa:	d23e                	sw	a5,36(sp)
    48fc:	ac69                	j	4b96 <__dtostr+0x35c>
    48fe:	873e                	mv	a4,a5
    4900:	4781                	li	a5,0
    4902:	bf65                	j	48ba <__dtostr+0x80>
    4904:	5702                	lw	a4,32(sp)
    4906:	47a2                	lw	a5,8(sp)
    4908:	4601                	li	a2,0
    490a:	4681                	li	a3,0
    490c:	853a                	mv	a0,a4
    490e:	85be                	mv	a1,a5
    4910:	c5dfc0ef          	jal	ra,156c <__ledf2>
    4914:	18055963          	bgez	a0,4aa6 <__dtostr+0x26c>
    4918:	47a2                	lw	a5,8(sp)
    491a:	4702                	lw	a4,0(sp)
    491c:	800002b7          	lui	t0,0x80000
    4920:	00f2c2b3          	xor	t0,t0,a5
    4924:	02d00793          	li	a5,45
    4928:	00f70023          	sb	a5,0(a4)
    492c:	147d                	addi	s0,s0,-1
    492e:	00170493          	addi	s1,a4,1
    4932:	000067b7          	lui	a5,0x6
    4936:	1b07a503          	lw	a0,432(a5) # 61b0 <sg_usi_config+0x520>
    493a:	1b47a583          	lw	a1,436(a5)
    493e:	000067b7          	lui	a5,0x6
    4942:	1c07a703          	lw	a4,448(a5) # 61c0 <sg_usi_config+0x530>
    4946:	1c47a783          	lw	a5,452(a5)
    494a:	4381                	li	t2,0
    494c:	4692                	lw	a3,4(sp)
    494e:	14d39f63          	bne	t2,a3,4aac <__dtostr+0x272>
    4952:	5702                	lw	a4,32(sp)
    4954:	862a                	mv	a2,a0
    4956:	86ae                	mv	a3,a1
    4958:	853a                	mv	a0,a4
    495a:	8596                	mv	a1,t0
    495c:	f5bfb0ef          	jal	ra,8b6 <__adddf3>
    4960:	000067b7          	lui	a5,0x6
    4964:	1c87a603          	lw	a2,456(a5) # 61c8 <sg_usi_config+0x538>
    4968:	1cc7a683          	lw	a3,460(a5)
    496c:	c82a                	sw	a0,16(sp)
    496e:	ca2e                	sw	a1,20(sp)
    4970:	bfdfc0ef          	jal	ra,156c <__ledf2>
    4974:	00055863          	bgez	a0,4984 <__dtostr+0x14a>
    4978:	03000793          	li	a5,48
    497c:	00f48023          	sb	a5,0(s1)
    4980:	147d                	addi	s0,s0,-1
    4982:	0485                	addi	s1,s1,1
    4984:	47a2                	lw	a5,8(sp)
    4986:	0147d513          	srli	a0,a5,0x14
    498a:	7ff57513          	andi	a0,a0,2047
    498e:	c0150513          	addi	a0,a0,-1023
    4992:	fa4fd0ef          	jal	ra,2136 <__floatsidf>
    4996:	000067b7          	lui	a5,0x6
    499a:	1d07a603          	lw	a2,464(a5) # 61d0 <sg_usi_config+0x540>
    499e:	1d47a683          	lw	a3,468(a5)
    49a2:	c7ffc0ef          	jal	ra,1620 <__muldf3>
    49a6:	f2cfd0ef          	jal	ra,20d2 <__fixdfsi>
    49aa:	00150793          	addi	a5,a0,1
    49ae:	cc3e                	sw	a5,24(sp)
    49b0:	22f05d63          	blez	a5,4bea <__dtostr+0x3b0>
    49b4:	000066b7          	lui	a3,0x6
    49b8:	1d86a603          	lw	a2,472(a3) # 61d8 <sg_usi_config+0x548>
    49bc:	82be                	mv	t0,a5
    49be:	1dc6a683          	lw	a3,476(a3)
    49c2:	000067b7          	lui	a5,0x6
    49c6:	1b87a703          	lw	a4,440(a5) # 61b8 <sg_usi_config+0x528>
    49ca:	1bc7a783          	lw	a5,444(a5)
    49ce:	43a9                	li	t2,10
    49d0:	d632                	sw	a2,44(sp)
    49d2:	d836                	sw	a3,48(sp)
    49d4:	0e53ea63          	bltu	t2,t0,4ac8 <__dtostr+0x28e>
    49d8:	000066b7          	lui	a3,0x6
    49dc:	1b86a603          	lw	a2,440(a3) # 61b8 <sg_usi_config+0x528>
    49e0:	1bc6a683          	lw	a3,444(a3)
    49e4:	4385                	li	t2,1
    49e6:	d632                	sw	a2,44(sp)
    49e8:	d836                	sw	a3,48(sp)
    49ea:	0e729c63          	bne	t0,t2,4ae2 <__dtostr+0x2a8>
    49ee:	4685                	li	a3,1
    49f0:	d636                	sw	a3,44(sp)
    49f2:	000066b7          	lui	a3,0x6
    49f6:	1e06a603          	lw	a2,480(a3) # 61e0 <sg_usi_config+0x550>
    49fa:	1e46a683          	lw	a3,484(a3)
    49fe:	da32                	sw	a2,52(sp)
    4a00:	dc36                	sw	a3,56(sp)
    4a02:	000066b7          	lui	a3,0x6
    4a06:	1b86a603          	lw	a2,440(a3) # 61b8 <sg_usi_config+0x528>
    4a0a:	1bc6a683          	lw	a3,444(a3)
    4a0e:	de32                	sw	a2,60(sp)
    4a10:	c0b6                	sw	a3,64(sp)
    4a12:	5652                	lw	a2,52(sp)
    4a14:	56e2                	lw	a3,56(sp)
    4a16:	853a                	mv	a0,a4
    4a18:	85be                	mv	a1,a5
    4a1a:	c2ba                	sw	a4,68(sp)
    4a1c:	c4be                	sw	a5,72(sp)
    4a1e:	a9bfc0ef          	jal	ra,14b8 <__gedf2>
    4a22:	4716                	lw	a4,68(sp)
    4a24:	47a6                	lw	a5,72(sp)
    4a26:	0ca04b63          	bgtz	a0,4afc <__dtostr+0x2c2>
    4a2a:	4682                	lw	a3,0(sp)
    4a2c:	00d49a63          	bne	s1,a3,4a40 <__dtostr+0x206>
    4a30:	16040363          	beqz	s0,4b96 <__dtostr+0x35c>
    4a34:	03000693          	li	a3,48
    4a38:	00d48023          	sb	a3,0(s1)
    4a3c:	147d                	addi	s0,s0,-1
    4a3e:	0485                	addi	s1,s1,1
    4a40:	4692                	lw	a3,4(sp)
    4a42:	ea81                	bnez	a3,4a52 <__dtostr+0x218>
    4a44:	4682                	lw	a3,0(sp)
    4a46:	5622                	lw	a2,40(sp)
    4a48:	40d486b3          	sub	a3,s1,a3
    4a4c:	0685                	addi	a3,a3,1
    4a4e:	04c6f663          	bgeu	a3,a2,4a9a <__dtostr+0x260>
    4a52:	14040263          	beqz	s0,4b96 <__dtostr+0x35c>
    4a56:	02e00693          	li	a3,46
    4a5a:	00d48023          	sb	a3,0(s1)
    4a5e:	4692                	lw	a3,4(sp)
    4a60:	147d                	addi	s0,s0,-1
    4a62:	00148293          	addi	t0,s1,1
    4a66:	ea81                	bnez	a3,4a76 <__dtostr+0x23c>
    4a68:	56a2                	lw	a3,40(sp)
    4a6a:	4602                	lw	a2,0(sp)
    4a6c:	0685                	addi	a3,a3,1
    4a6e:	40c28633          	sub	a2,t0,a2
    4a72:	8e91                	sub	a3,a3,a2
    4a74:	c236                	sw	a3,4(sp)
    4a76:	4692                	lw	a3,4(sp)
    4a78:	10d46f63          	bltu	s0,a3,4b96 <__dtostr+0x35c>
    4a7c:	8426                	mv	s0,s1
    4a7e:	94b6                	add	s1,s1,a3
    4a80:	000066b7          	lui	a3,0x6
    4a84:	1b86a603          	lw	a2,440(a3) # 61b8 <sg_usi_config+0x528>
    4a88:	1bc6a683          	lw	a3,444(a3)
    4a8c:	c432                	sw	a2,8(sp)
    4a8e:	c636                	sw	a3,12(sp)
    4a90:	16941463          	bne	s0,s1,4bf8 <__dtostr+0x3be>
    4a94:	4792                	lw	a5,4(sp)
    4a96:	00f284b3          	add	s1,t0,a5
    4a9a:	4782                	lw	a5,0(sp)
    4a9c:	00048023          	sb	zero,0(s1)
    4aa0:	40f487b3          	sub	a5,s1,a5
    4aa4:	bd99                	j	48fa <__dtostr+0xc0>
    4aa6:	4482                	lw	s1,0(sp)
    4aa8:	42a2                	lw	t0,8(sp)
    4aaa:	b561                	j	4932 <__dtostr+0xf8>
    4aac:	863a                	mv	a2,a4
    4aae:	86be                	mv	a3,a5
    4ab0:	d616                	sw	t0,44(sp)
    4ab2:	cc1e                	sw	t2,24(sp)
    4ab4:	c83a                	sw	a4,16(sp)
    4ab6:	ca3e                	sw	a5,20(sp)
    4ab8:	b69fc0ef          	jal	ra,1620 <__muldf3>
    4abc:	43e2                	lw	t2,24(sp)
    4abe:	52b2                	lw	t0,44(sp)
    4ac0:	4742                	lw	a4,16(sp)
    4ac2:	0385                	addi	t2,t2,1
    4ac4:	47d2                	lw	a5,20(sp)
    4ac6:	b559                	j	494c <__dtostr+0x112>
    4ac8:	5632                	lw	a2,44(sp)
    4aca:	56c2                	lw	a3,48(sp)
    4acc:	853a                	mv	a0,a4
    4ace:	85be                	mv	a1,a5
    4ad0:	da16                	sw	t0,52(sp)
    4ad2:	b4ffc0ef          	jal	ra,1620 <__muldf3>
    4ad6:	52d2                	lw	t0,52(sp)
    4ad8:	872a                	mv	a4,a0
    4ada:	87ae                	mv	a5,a1
    4adc:	12d9                	addi	t0,t0,-10
    4ade:	43a9                	li	t2,10
    4ae0:	bdd5                	j	49d4 <__dtostr+0x19a>
    4ae2:	5632                	lw	a2,44(sp)
    4ae4:	56c2                	lw	a3,48(sp)
    4ae6:	853a                	mv	a0,a4
    4ae8:	85be                	mv	a1,a5
    4aea:	da16                	sw	t0,52(sp)
    4aec:	b35fc0ef          	jal	ra,1620 <__muldf3>
    4af0:	52d2                	lw	t0,52(sp)
    4af2:	872a                	mv	a4,a0
    4af4:	87ae                	mv	a5,a1
    4af6:	12fd                	addi	t0,t0,-1
    4af8:	4385                	li	t2,1
    4afa:	bdc5                	j	49ea <__dtostr+0x1b0>
    4afc:	4542                	lw	a0,16(sp)
    4afe:	45d2                	lw	a1,20(sp)
    4b00:	863a                	mv	a2,a4
    4b02:	86be                	mv	a3,a5
    4b04:	c2ba                	sw	a4,68(sp)
    4b06:	c4be                	sw	a5,72(sp)
    4b08:	ba8fc0ef          	jal	ra,eb0 <__divdf3>
    4b0c:	dc6fd0ef          	jal	ra,20d2 <__fixdfsi>
    4b10:	5632                	lw	a2,44(sp)
    4b12:	0ff57693          	andi	a3,a0,255
    4b16:	4716                	lw	a4,68(sp)
    4b18:	47a6                	lw	a5,72(sp)
    4b1a:	c211                	beqz	a2,4b1e <__dtostr+0x2e4>
    4b1c:	ced5                	beqz	a3,4bd8 <__dtostr+0x39e>
    4b1e:	03068693          	addi	a3,a3,48
    4b22:	00d48023          	sb	a3,0(s1)
    4b26:	0485                	addi	s1,s1,1
    4b28:	ec35                	bnez	s0,4ba4 <__dtostr+0x36a>
    4b2a:	863a                	mv	a2,a4
    4b2c:	86be                	mv	a3,a5
    4b2e:	5702                	lw	a4,32(sp)
    4b30:	47a2                	lw	a5,8(sp)
    4b32:	853a                	mv	a0,a4
    4b34:	85be                	mv	a1,a5
    4b36:	b7afc0ef          	jal	ra,eb0 <__divdf3>
    4b3a:	4792                	lw	a5,4(sp)
    4b3c:	5722                	lw	a4,40(sp)
    4b3e:	4602                	lw	a2,0(sp)
    4b40:	4681                	li	a3,0
    4b42:	39e5                	jal	483a <__dtostr>
    4b44:	c929                	beqz	a0,4b96 <__dtostr+0x35c>
    4b46:	00a48333          	add	t1,s1,a0
    4b4a:	06500793          	li	a5,101
    4b4e:	00f30023          	sb	a5,0(t1)
    4b52:	00130493          	addi	s1,t1,1
    4b56:	fff54513          	not	a0,a0
    4b5a:	4711                	li	a4,4
    4b5c:	4685                	li	a3,1
    4b5e:	3e800793          	li	a5,1000
    4b62:	4629                	li	a2,10
    4b64:	45e2                	lw	a1,24(sp)
    4b66:	00f5d363          	bge	a1,a5,4b6c <__dtostr+0x332>
    4b6a:	e285                	bnez	a3,4b8a <__dtostr+0x350>
    4b6c:	c909                	beqz	a0,4b7e <__dtostr+0x344>
    4b6e:	46e2                	lw	a3,24(sp)
    4b70:	0485                	addi	s1,s1,1
    4b72:	02f6c6b3          	div	a3,a3,a5
    4b76:	03068693          	addi	a3,a3,48
    4b7a:	fed48fa3          	sb	a3,-1(s1)
    4b7e:	46e2                	lw	a3,24(sp)
    4b80:	157d                	addi	a0,a0,-1
    4b82:	02f6e6b3          	rem	a3,a3,a5
    4b86:	cc36                	sw	a3,24(sp)
    4b88:	4681                	li	a3,0
    4b8a:	177d                	addi	a4,a4,-1
    4b8c:	02c7c7b3          	div	a5,a5,a2
    4b90:	fb71                	bnez	a4,4b64 <__dtostr+0x32a>
    4b92:	f00514e3          	bnez	a0,4a9a <__dtostr+0x260>
    4b96:	40d6                	lw	ra,84(sp)
    4b98:	4446                	lw	s0,80(sp)
    4b9a:	5512                	lw	a0,36(sp)
    4b9c:	44b6                	lw	s1,76(sp)
    4b9e:	05810113          	addi	sp,sp,88
    4ba2:	8082                	ret
    4ba4:	0ff57513          	andi	a0,a0,255
    4ba8:	d63a                	sw	a4,44(sp)
    4baa:	d83e                	sw	a5,48(sp)
    4bac:	d8afd0ef          	jal	ra,2136 <__floatsidf>
    4bb0:	5732                	lw	a4,44(sp)
    4bb2:	57c2                	lw	a5,48(sp)
    4bb4:	147d                	addi	s0,s0,-1
    4bb6:	863a                	mv	a2,a4
    4bb8:	86be                	mv	a3,a5
    4bba:	c2ba                	sw	a4,68(sp)
    4bbc:	c4be                	sw	a5,72(sp)
    4bbe:	a63fc0ef          	jal	ra,1620 <__muldf3>
    4bc2:	862a                	mv	a2,a0
    4bc4:	86ae                	mv	a3,a1
    4bc6:	4542                	lw	a0,16(sp)
    4bc8:	45d2                	lw	a1,20(sp)
    4bca:	efbfc0ef          	jal	ra,1ac4 <__subdf3>
    4bce:	4716                	lw	a4,68(sp)
    4bd0:	47a6                	lw	a5,72(sp)
    4bd2:	c82a                	sw	a0,16(sp)
    4bd4:	ca2e                	sw	a1,20(sp)
    4bd6:	d602                	sw	zero,44(sp)
    4bd8:	5672                	lw	a2,60(sp)
    4bda:	4686                	lw	a3,64(sp)
    4bdc:	853a                	mv	a0,a4
    4bde:	85be                	mv	a1,a5
    4be0:	ad0fc0ef          	jal	ra,eb0 <__divdf3>
    4be4:	872a                	mv	a4,a0
    4be6:	87ae                	mv	a5,a1
    4be8:	b52d                	j	4a12 <__dtostr+0x1d8>
    4bea:	000067b7          	lui	a5,0x6
    4bee:	1c07a703          	lw	a4,448(a5) # 61c0 <sg_usi_config+0x530>
    4bf2:	1c47a783          	lw	a5,452(a5)
    4bf6:	bd15                	j	4a2a <__dtostr+0x1f0>
    4bf8:	4542                	lw	a0,16(sp)
    4bfa:	45d2                	lw	a1,20(sp)
    4bfc:	863a                	mv	a2,a4
    4bfe:	86be                	mv	a3,a5
    4c00:	d016                	sw	t0,32(sp)
    4c02:	cc3a                	sw	a4,24(sp)
    4c04:	ce3e                	sw	a5,28(sp)
    4c06:	aaafc0ef          	jal	ra,eb0 <__divdf3>
    4c0a:	cc8fd0ef          	jal	ra,20d2 <__fixdfsi>
    4c0e:	03050693          	addi	a3,a0,48
    4c12:	00d400a3          	sb	a3,1(s0)
    4c16:	0ff57513          	andi	a0,a0,255
    4c1a:	d1cfd0ef          	jal	ra,2136 <__floatsidf>
    4c1e:	4762                	lw	a4,24(sp)
    4c20:	47f2                	lw	a5,28(sp)
    4c22:	0405                	addi	s0,s0,1
    4c24:	863a                	mv	a2,a4
    4c26:	86be                	mv	a3,a5
    4c28:	9f9fc0ef          	jal	ra,1620 <__muldf3>
    4c2c:	862a                	mv	a2,a0
    4c2e:	86ae                	mv	a3,a1
    4c30:	4542                	lw	a0,16(sp)
    4c32:	45d2                	lw	a1,20(sp)
    4c34:	e91fc0ef          	jal	ra,1ac4 <__subdf3>
    4c38:	4762                	lw	a4,24(sp)
    4c3a:	47f2                	lw	a5,28(sp)
    4c3c:	4622                	lw	a2,8(sp)
    4c3e:	46b2                	lw	a3,12(sp)
    4c40:	c82a                	sw	a0,16(sp)
    4c42:	ca2e                	sw	a1,20(sp)
    4c44:	853a                	mv	a0,a4
    4c46:	85be                	mv	a1,a5
    4c48:	a68fc0ef          	jal	ra,eb0 <__divdf3>
    4c4c:	872a                	mv	a4,a0
    4c4e:	87ae                	mv	a5,a1
    4c50:	5282                	lw	t0,32(sp)
    4c52:	bd3d                	j	4a90 <__dtostr+0x256>
    4c54:	47a1                	li	a5,8
    4c56:	c40407e3          	beqz	s0,48a4 <__dtostr+0x6a>
    4c5a:	4785                	li	a5,1
    4c5c:	b1a1                	j	48a4 <__dtostr+0x6a>

00004c5e <__isinf>:
    4c5e:	e509                	bnez	a0,4c68 <__isinf+0xa>
    4c60:	7ff007b7          	lui	a5,0x7ff00
    4c64:	00f58b63          	beq	a1,a5,4c7a <__isinf+0x1c>
    4c68:	fff007b7          	lui	a5,0xfff00
    4c6c:	8fad                	xor	a5,a5,a1
    4c6e:	8d5d                	or	a0,a0,a5
    4c70:	00153513          	seqz	a0,a0
    4c74:	40a00533          	neg	a0,a0
    4c78:	8082                	ret
    4c7a:	4505                	li	a0,1
    4c7c:	8082                	ret

00004c7e <__lltostr>:
    4c7e:	fdc10113          	addi	sp,sp,-36
    4c82:	15fd                	addi	a1,a1,-1
    4c84:	ce22                	sw	s0,28(sp)
    4c86:	d006                	sw	ra,32(sp)
    4c88:	cc26                	sw	s1,24(sp)
    4c8a:	95aa                	add	a1,a1,a0
    4c8c:	00058023          	sb	zero,0(a1)
    4c90:	842a                	mv	s0,a0
    4c92:	82b2                	mv	t0,a2
    4c94:	8336                	mv	t1,a3
    4c96:	c709                	beqz	a4,4ca0 <__lltostr+0x22>
    4c98:	02400693          	li	a3,36
    4c9c:	00e6d363          	bge	a3,a4,4ca2 <__lltostr+0x24>
    4ca0:	4729                	li	a4,10
    4ca2:	0062e6b3          	or	a3,t0,t1
    4ca6:	4601                	li	a2,0
    4ca8:	e699                	bnez	a3,4cb6 <__lltostr+0x38>
    4caa:	03000693          	li	a3,48
    4cae:	fed58fa3          	sb	a3,-1(a1)
    4cb2:	4605                	li	a2,1
    4cb4:	15fd                	addi	a1,a1,-1
    4cb6:	02700693          	li	a3,39
    4cba:	c391                	beqz	a5,4cbe <__lltostr+0x40>
    4cbc:	469d                	li	a3,7
    4cbe:	0ff6f793          	andi	a5,a3,255
    4cc2:	c23e                	sw	a5,4(sp)
    4cc4:	00c587b3          	add	a5,a1,a2
    4cc8:	c43e                	sw	a5,8(sp)
    4cca:	41f75793          	srai	a5,a4,0x1f
    4cce:	84ae                	mv	s1,a1
    4cd0:	c03e                	sw	a5,0(sp)
    4cd2:	47a2                	lw	a5,8(sp)
    4cd4:	409786b3          	sub	a3,a5,s1
    4cd8:	00947563          	bgeu	s0,s1,4ce2 <__lltostr+0x64>
    4cdc:	0062e633          	or	a2,t0,t1
    4ce0:	e205                	bnez	a2,4d00 <__lltostr+0x82>
    4ce2:	00168613          	addi	a2,a3,1
    4ce6:	85a6                	mv	a1,s1
    4ce8:	8522                	mv	a0,s0
    4cea:	c036                	sw	a3,0(sp)
    4cec:	dcafd0ef          	jal	ra,22b6 <memmove>
    4cf0:	4682                	lw	a3,0(sp)
    4cf2:	5082                	lw	ra,32(sp)
    4cf4:	4472                	lw	s0,28(sp)
    4cf6:	44e2                	lw	s1,24(sp)
    4cf8:	8536                	mv	a0,a3
    4cfa:	02410113          	addi	sp,sp,36
    4cfe:	8082                	ret
    4d00:	4682                	lw	a3,0(sp)
    4d02:	863a                	mv	a2,a4
    4d04:	8516                	mv	a0,t0
    4d06:	859a                	mv	a1,t1
    4d08:	ca3a                	sw	a4,20(sp)
    4d0a:	c816                	sw	t0,16(sp)
    4d0c:	c61a                	sw	t1,12(sp)
    4d0e:	84ffb0ef          	jal	ra,55c <__umoddi3>
    4d12:	03050513          	addi	a0,a0,48
    4d16:	0ff57513          	andi	a0,a0,255
    4d1a:	03900793          	li	a5,57
    4d1e:	14fd                	addi	s1,s1,-1
    4d20:	4332                	lw	t1,12(sp)
    4d22:	42c2                	lw	t0,16(sp)
    4d24:	4752                	lw	a4,20(sp)
    4d26:	00a7ef63          	bltu	a5,a0,4d44 <__lltostr+0xc6>
    4d2a:	4682                	lw	a3,0(sp)
    4d2c:	00a48023          	sb	a0,0(s1)
    4d30:	863a                	mv	a2,a4
    4d32:	8516                	mv	a0,t0
    4d34:	859a                	mv	a1,t1
    4d36:	c63a                	sw	a4,12(sp)
    4d38:	cbcfb0ef          	jal	ra,1f4 <__udivdi3>
    4d3c:	82aa                	mv	t0,a0
    4d3e:	832e                	mv	t1,a1
    4d40:	4732                	lw	a4,12(sp)
    4d42:	bf41                	j	4cd2 <__lltostr+0x54>
    4d44:	4792                	lw	a5,4(sp)
    4d46:	953e                	add	a0,a0,a5
    4d48:	b7cd                	j	4d2a <__lltostr+0xac>

00004d4a <printf>:
    4d4a:	fdc10113          	addi	sp,sp,-36
    4d4e:	c82e                	sw	a1,16(sp)
    4d50:	080c                	addi	a1,sp,16
    4d52:	c606                	sw	ra,12(sp)
    4d54:	ca32                	sw	a2,20(sp)
    4d56:	cc36                	sw	a3,24(sp)
    4d58:	ce3a                	sw	a4,28(sp)
    4d5a:	d03e                	sw	a5,32(sp)
    4d5c:	c02e                	sw	a1,0(sp)
    4d5e:	2099                	jal	4da4 <vprintf>
    4d60:	40b2                	lw	ra,12(sp)
    4d62:	02410113          	addi	sp,sp,36
    4d66:	8082                	ret

00004d68 <putc>:
    4d68:	808ff06f          	j	3d70 <fputc>

00004d6c <__stdio_outs>:
    4d6c:	1151                	addi	sp,sp,-12
    4d6e:	c222                	sw	s0,4(sp)
    4d70:	c026                	sw	s1,0(sp)
    4d72:	842a                	mv	s0,a0
    4d74:	84ae                	mv	s1,a1
    4d76:	c406                	sw	ra,8(sp)
    4d78:	94a2                	add	s1,s1,s0
    4d7a:	830ff0ef          	jal	ra,3daa <os_critical_enter>
    4d7e:	00941a63          	bne	s0,s1,4d92 <__stdio_outs+0x26>
    4d82:	82cff0ef          	jal	ra,3dae <os_critical_exit>
    4d86:	40a2                	lw	ra,8(sp)
    4d88:	4412                	lw	s0,4(sp)
    4d8a:	4482                	lw	s1,0(sp)
    4d8c:	4505                	li	a0,1
    4d8e:	0131                	addi	sp,sp,12
    4d90:	8082                	ret
    4d92:	a4c1a703          	lw	a4,-1460(gp) # 200000c0 <_impure_ptr>
    4d96:	00044503          	lbu	a0,0(s0)
    4d9a:	0405                	addi	s0,s0,1
    4d9c:	470c                	lw	a1,8(a4)
    4d9e:	fd3fe0ef          	jal	ra,3d70 <fputc>
    4da2:	bff1                	j	4d7e <__stdio_outs+0x12>

00004da4 <vprintf>:
    4da4:	1131                	addi	sp,sp,-20
    4da6:	000057b7          	lui	a5,0x5
    4daa:	d6c78793          	addi	a5,a5,-660 # 4d6c <__stdio_outs>
    4dae:	862e                	mv	a2,a1
    4db0:	85aa                	mv	a1,a0
    4db2:	850a                	mv	a0,sp
    4db4:	c806                	sw	ra,16(sp)
    4db6:	c002                	sw	zero,0(sp)
    4db8:	c23e                	sw	a5,4(sp)
    4dba:	2291                	jal	4efe <__v_printf>
    4dbc:	40c2                	lw	ra,16(sp)
    4dbe:	0151                	addi	sp,sp,20
    4dc0:	8082                	ret

00004dc2 <__isnan>:
    4dc2:	fff80737          	lui	a4,0xfff80
    4dc6:	177d                	addi	a4,a4,-1
    4dc8:	8f6d                	and	a4,a4,a1
    4dca:	e509                	bnez	a0,4dd4 <__isnan+0x12>
    4dcc:	7ff007b7          	lui	a5,0x7ff00
    4dd0:	00f70963          	beq	a4,a5,4de2 <__isnan+0x20>
    4dd4:	fff807b7          	lui	a5,0xfff80
    4dd8:	8fad                	xor	a5,a5,a1
    4dda:	8fc9                	or	a5,a5,a0
    4ddc:	0017b513          	seqz	a0,a5
    4de0:	8082                	ret
    4de2:	4505                	li	a0,1
    4de4:	8082                	ret

00004de6 <__ltostr>:
    4de6:	1151                	addi	sp,sp,-12
    4de8:	15fd                	addi	a1,a1,-1
    4dea:	c406                	sw	ra,8(sp)
    4dec:	c222                	sw	s0,4(sp)
    4dee:	95aa                	add	a1,a1,a0
    4df0:	00058023          	sb	zero,0(a1)
    4df4:	fff68313          	addi	t1,a3,-1
    4df8:	02300793          	li	a5,35
    4dfc:	0067f363          	bgeu	a5,t1,4e02 <__ltostr+0x1c>
    4e00:	46a9                	li	a3,10
    4e02:	4781                	li	a5,0
    4e04:	e619                	bnez	a2,4e12 <__ltostr+0x2c>
    4e06:	03000793          	li	a5,48
    4e0a:	fef58fa3          	sb	a5,-1(a1)
    4e0e:	15fd                	addi	a1,a1,-1
    4e10:	4785                	li	a5,1
    4e12:	02700313          	li	t1,39
    4e16:	c311                	beqz	a4,4e1a <__ltostr+0x34>
    4e18:	431d                	li	t1,7
    4e1a:	0ff37713          	andi	a4,t1,255
    4e1e:	03900293          	li	t0,57
    4e22:	00f58333          	add	t1,a1,a5
    4e26:	40b30433          	sub	s0,t1,a1
    4e2a:	00b57363          	bgeu	a0,a1,4e30 <__ltostr+0x4a>
    4e2e:	ea11                	bnez	a2,4e42 <__ltostr+0x5c>
    4e30:	00140613          	addi	a2,s0,1
    4e34:	c82fd0ef          	jal	ra,22b6 <memmove>
    4e38:	8522                	mv	a0,s0
    4e3a:	40a2                	lw	ra,8(sp)
    4e3c:	4412                	lw	s0,4(sp)
    4e3e:	0131                	addi	sp,sp,12
    4e40:	8082                	ret
    4e42:	02d677b3          	remu	a5,a2,a3
    4e46:	15fd                	addi	a1,a1,-1
    4e48:	03078793          	addi	a5,a5,48 # fff80030 <__heap_end+0xdff50030>
    4e4c:	0ff7f793          	andi	a5,a5,255
    4e50:	00f2e763          	bltu	t0,a5,4e5e <__ltostr+0x78>
    4e54:	00f58023          	sb	a5,0(a1)
    4e58:	02d65633          	divu	a2,a2,a3
    4e5c:	b7e9                	j	4e26 <__ltostr+0x40>
    4e5e:	97ba                	add	a5,a5,a4
    4e60:	bfd5                	j	4e54 <__ltostr+0x6e>

00004e62 <putchar>:
    4e62:	a4c1a783          	lw	a5,-1460(gp) # 200000c0 <_impure_ptr>
    4e66:	1151                	addi	sp,sp,-12
    4e68:	c406                	sw	ra,8(sp)
    4e6a:	478c                	lw	a1,8(a5)
    4e6c:	3df5                	jal	4d68 <putc>
    4e6e:	40a2                	lw	ra,8(sp)
    4e70:	4501                	li	a0,0
    4e72:	0131                	addi	sp,sp,12
    4e74:	8082                	ret

00004e76 <puts>:
    4e76:	1151                	addi	sp,sp,-12
    4e78:	c222                	sw	s0,4(sp)
    4e7a:	c406                	sw	ra,8(sp)
    4e7c:	842a                	mv	s0,a0
    4e7e:	00044503          	lbu	a0,0(s0)
    4e82:	55fd                	li	a1,-1
    4e84:	e909                	bnez	a0,4e96 <puts+0x20>
    4e86:	4529                	li	a0,10
    4e88:	ee9fe0ef          	jal	ra,3d70 <fputc>
    4e8c:	40a2                	lw	ra,8(sp)
    4e8e:	4412                	lw	s0,4(sp)
    4e90:	4501                	li	a0,0
    4e92:	0131                	addi	sp,sp,12
    4e94:	8082                	ret
    4e96:	edbfe0ef          	jal	ra,3d70 <fputc>
    4e9a:	0405                	addi	s0,s0,1
    4e9c:	b7cd                	j	4e7e <puts+0x8>

00004e9e <write_pad>:
    4e9e:	1131                	addi	sp,sp,-20
    4ea0:	fd060613          	addi	a2,a2,-48
    4ea4:	c426                	sw	s1,8(sp)
    4ea6:	00163613          	seqz	a2,a2
    4eaa:	6499                	lui	s1,0x6
    4eac:	0612                	slli	a2,a2,0x4
    4eae:	1f848493          	addi	s1,s1,504 # 61f8 <pad_line>
    4eb2:	c622                	sw	s0,12(sp)
    4eb4:	c806                	sw	ra,16(sp)
    4eb6:	87aa                	mv	a5,a0
    4eb8:	872e                	mv	a4,a1
    4eba:	94b2                	add	s1,s1,a2
    4ebc:	842e                	mv	s0,a1
    4ebe:	463d                	li	a2,15
    4ec0:	408706b3          	sub	a3,a4,s0
    4ec4:	02864263          	blt	a2,s0,4ee8 <write_pad+0x4a>
    4ec8:	00805a63          	blez	s0,4edc <write_pad+0x3e>
    4ecc:	43d8                	lw	a4,4(a5)
    4ece:	4390                	lw	a2,0(a5)
    4ed0:	85a2                	mv	a1,s0
    4ed2:	8526                	mv	a0,s1
    4ed4:	c036                	sw	a3,0(sp)
    4ed6:	9702                	jalr	a4
    4ed8:	4682                	lw	a3,0(sp)
    4eda:	96a2                	add	a3,a3,s0
    4edc:	40c2                	lw	ra,16(sp)
    4ede:	4432                	lw	s0,12(sp)
    4ee0:	44a2                	lw	s1,8(sp)
    4ee2:	8536                	mv	a0,a3
    4ee4:	0151                	addi	sp,sp,20
    4ee6:	8082                	ret
    4ee8:	43d4                	lw	a3,4(a5)
    4eea:	4390                	lw	a2,0(a5)
    4eec:	45c1                	li	a1,16
    4eee:	8526                	mv	a0,s1
    4ef0:	c23a                	sw	a4,4(sp)
    4ef2:	c03e                	sw	a5,0(sp)
    4ef4:	9682                	jalr	a3
    4ef6:	1441                	addi	s0,s0,-16
    4ef8:	4712                	lw	a4,4(sp)
    4efa:	4782                	lw	a5,0(sp)
    4efc:	b7c9                	j	4ebe <write_pad+0x20>

00004efe <__v_printf>:
    4efe:	200027b7          	lui	a5,0x20002
    4f02:	e7c7a783          	lw	a5,-388(a5) # 20001e7c <errno>
    4f06:	f2810113          	addi	sp,sp,-216
    4f0a:	c9a2                	sw	s0,208(sp)
    4f0c:	c7a6                	sw	s1,204(sp)
    4f0e:	cb86                	sw	ra,212(sp)
    4f10:	84aa                	mv	s1,a0
    4f12:	c82e                	sw	a1,16(sp)
    4f14:	8432                	mv	s0,a2
    4f16:	d83e                	sw	a5,48(sp)
    4f18:	c202                	sw	zero,4(sp)
    4f1a:	47c2                	lw	a5,16(sp)
    4f1c:	0007c783          	lbu	a5,0(a5)
    4f20:	68078163          	beqz	a5,55a2 <__v_printf+0x6a4>
    4f24:	4701                	li	a4,0
    4f26:	02500613          	li	a2,37
    4f2a:	a011                	j	4f2e <__v_printf+0x30>
    4f2c:	0705                	addi	a4,a4,1
    4f2e:	47c2                	lw	a5,16(sp)
    4f30:	97ba                	add	a5,a5,a4
    4f32:	0007c683          	lbu	a3,0(a5)
    4f36:	74068963          	beqz	a3,5688 <__v_printf+0x78a>
    4f3a:	fec699e3          	bne	a3,a2,4f2c <__v_printf+0x2e>
    4f3e:	e319                	bnez	a4,4f44 <__v_printf+0x46>
    4f40:	47c2                	lw	a5,16(sp)
    4f42:	a01d                	j	4f68 <__v_printf+0x6a>
    4f44:	40d4                	lw	a3,4(s1)
    4f46:	4090                	lw	a2,0(s1)
    4f48:	4542                	lw	a0,16(sp)
    4f4a:	85ba                	mv	a1,a4
    4f4c:	c43e                	sw	a5,8(sp)
    4f4e:	c03a                	sw	a4,0(sp)
    4f50:	9682                	jalr	a3
    4f52:	4792                	lw	a5,4(sp)
    4f54:	4702                	lw	a4,0(sp)
    4f56:	97ba                	add	a5,a5,a4
    4f58:	c23e                	sw	a5,4(sp)
    4f5a:	47a2                	lw	a5,8(sp)
    4f5c:	02500713          	li	a4,37
    4f60:	0007c683          	lbu	a3,0(a5)
    4f64:	72e69563          	bne	a3,a4,568e <__v_printf+0x790>
    4f68:	00178513          	addi	a0,a5,1
    4f6c:	02000793          	li	a5,32
    4f70:	c002                	sw	zero,0(sp)
    4f72:	c602                	sw	zero,12(sp)
    4f74:	4701                	li	a4,0
    4f76:	ca02                	sw	zero,20(sp)
    4f78:	ce02                	sw	zero,28(sp)
    4f7a:	d602                	sw	zero,44(sp)
    4f7c:	d002                	sw	zero,32(sp)
    4f7e:	c402                	sw	zero,8(sp)
    4f80:	cc3e                	sw	a5,24(sp)
    4f82:	00054303          	lbu	t1,0(a0)
    4f86:	00150793          	addi	a5,a0,1
    4f8a:	c83e                	sw	a5,16(sp)
    4f8c:	046103a3          	sb	t1,71(sp)
    4f90:	06300793          	li	a5,99
    4f94:	1cf30963          	beq	t1,a5,5166 <__v_printf+0x268>
    4f98:	0c67e863          	bltu	a5,t1,5068 <__v_printf+0x16a>
    4f9c:	02d00793          	li	a5,45
    4fa0:	02f30363          	beq	t1,a5,4fc6 <__v_printf+0xc8>
    4fa4:	0667e263          	bltu	a5,t1,5008 <__v_printf+0x10a>
    4fa8:	02300793          	li	a5,35
    4fac:	02f30163          	beq	t1,a5,4fce <__v_printf+0xd0>
    4fb0:	0267e663          	bltu	a5,t1,4fdc <__v_printf+0xde>
    4fb4:	5e030563          	beqz	t1,559e <__v_printf+0x6a0>
    4fb8:	02000793          	li	a5,32
    4fbc:	f4f31fe3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    4fc0:	4785                	li	a5,1
    4fc2:	d63e                	sw	a5,44(sp)
    4fc4:	a019                	j	4fca <__v_printf+0xcc>
    4fc6:	4785                	li	a5,1
    4fc8:	d03e                	sw	a5,32(sp)
    4fca:	4542                	lw	a0,16(sp)
    4fcc:	bf5d                	j	4f82 <__v_printf+0x84>
    4fce:	0ff00793          	li	a5,255
    4fd2:	c43e                	sw	a5,8(sp)
    4fd4:	bfdd                	j	4fca <__v_printf+0xcc>
    4fd6:	4785                	li	a5,1
    4fd8:	ce3e                	sw	a5,28(sp)
    4fda:	bfc5                	j	4fca <__v_printf+0xcc>
    4fdc:	02a00793          	li	a5,42
    4fe0:	16f30163          	beq	t1,a5,5142 <__v_printf+0x244>
    4fe4:	02b00793          	li	a5,43
    4fe8:	fef307e3          	beq	t1,a5,4fd6 <__v_printf+0xd8>
    4fec:	02500793          	li	a5,37
    4ff0:	f2f315e3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    4ff4:	40dc                	lw	a5,4(s1)
    4ff6:	4090                	lw	a2,0(s1)
    4ff8:	4585                	li	a1,1
    4ffa:	04710513          	addi	a0,sp,71
    4ffe:	9782                	jalr	a5
    5000:	4792                	lw	a5,4(sp)
    5002:	0785                	addi	a5,a5,1
    5004:	c23e                	sw	a5,4(sp)
    5006:	bf11                	j	4f1a <__v_printf+0x1c>
    5008:	03900793          	li	a5,57
    500c:	0267ee63          	bltu	a5,t1,5048 <__v_printf+0x14a>
    5010:	03000793          	li	a5,48
    5014:	0ef37f63          	bgeu	t1,a5,5112 <__v_printf+0x214>
    5018:	02e00793          	li	a5,46
    501c:	eef31fe3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    5020:	00154683          	lbu	a3,1(a0)
    5024:	02a00793          	li	a5,42
    5028:	12f69163          	bne	a3,a5,514a <__v_printf+0x24c>
    502c:	4014                	lw	a3,0(s0)
    502e:	00440793          	addi	a5,s0,4
    5032:	c036                	sw	a3,0(sp)
    5034:	0006d363          	bgez	a3,503a <__v_printf+0x13c>
    5038:	c002                	sw	zero,0(sp)
    503a:	00250693          	addi	a3,a0,2
    503e:	c836                	sw	a3,16(sp)
    5040:	843e                	mv	s0,a5
    5042:	4785                	li	a5,1
    5044:	ca3e                	sw	a5,20(sp)
    5046:	b751                	j	4fca <__v_printf+0xcc>
    5048:	05800793          	li	a5,88
    504c:	2af30363          	beq	t1,a5,52f2 <__v_printf+0x3f4>
    5050:	06200793          	li	a5,98
    5054:	36f30463          	beq	t1,a5,53bc <__v_printf+0x4be>
    5058:	04c00793          	li	a5,76
    505c:	eaf31fe3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    5060:	0705                	addi	a4,a4,1
    5062:	0762                	slli	a4,a4,0x18
    5064:	8761                	srai	a4,a4,0x18
    5066:	a091                	j	50aa <__v_printf+0x1ac>
    5068:	06d00793          	li	a5,109
    506c:	10f30263          	beq	t1,a5,5170 <__v_printf+0x272>
    5070:	0267ef63          	bltu	a5,t1,50ae <__v_printf+0x1b0>
    5074:	06700793          	li	a5,103
    5078:	0067ef63          	bltu	a5,t1,5096 <__v_printf+0x198>
    507c:	06600793          	li	a5,102
    5080:	3ef37363          	bgeu	t1,a5,5466 <__v_printf+0x568>
    5084:	06400793          	li	a5,100
    5088:	e8f319e3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    508c:	4529                	li	a0,10
    508e:	4781                	li	a5,0
    5090:	4285                	li	t0,1
    5092:	4301                	li	t1,0
    5094:	ac51                	j	5328 <__v_printf+0x42a>
    5096:	06900793          	li	a5,105
    509a:	fef309e3          	beq	t1,a5,508c <__v_printf+0x18e>
    509e:	06f36663          	bltu	t1,a5,510a <__v_printf+0x20c>
    50a2:	06c00793          	li	a5,108
    50a6:	e6f31ae3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    50aa:	0705                	addi	a4,a4,1
    50ac:	a085                	j	510c <__v_printf+0x20e>
    50ae:	07300793          	li	a5,115
    50b2:	0ef30163          	beq	t1,a5,5194 <__v_printf+0x296>
    50b6:	0267ec63          	bltu	a5,t1,50ee <__v_printf+0x1f0>
    50ba:	07000793          	li	a5,112
    50be:	22f30363          	beq	t1,a5,52e4 <__v_printf+0x3e6>
    50c2:	07100793          	li	a5,113
    50c6:	f8f30de3          	beq	t1,a5,5060 <__v_printf+0x162>
    50ca:	06f00793          	li	a5,111
    50ce:	e4f316e3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    50d2:	47a2                	lw	a5,8(sp)
    50d4:	2e078863          	beqz	a5,53c4 <__v_printf+0x4c6>
    50d8:	03000793          	li	a5,48
    50dc:	04f106a3          	sb	a5,77(sp)
    50e0:	4785                	li	a5,1
    50e2:	c43e                	sw	a5,8(sp)
    50e4:	4521                	li	a0,8
    50e6:	4781                	li	a5,0
    50e8:	4281                	li	t0,0
    50ea:	4305                	li	t1,1
    50ec:	ac35                	j	5328 <__v_printf+0x42a>
    50ee:	07800793          	li	a5,120
    50f2:	2cf30363          	beq	t1,a5,53b8 <__v_printf+0x4ba>
    50f6:	07a00793          	li	a5,122
    50fa:	ecf308e3          	beq	t1,a5,4fca <__v_printf+0xcc>
    50fe:	07500793          	li	a5,117
    5102:	e0f31ce3          	bne	t1,a5,4f1a <__v_printf+0x1c>
    5106:	4529                	li	a0,10
    5108:	ac5d                	j	53be <__v_printf+0x4c0>
    510a:	177d                	addi	a4,a4,-1
    510c:	0762                	slli	a4,a4,0x18
    510e:	8761                	srai	a4,a4,0x18
    5110:	bd6d                	j	4fca <__v_printf+0xcc>
    5112:	47d2                	lw	a5,20(sp)
    5114:	c83a                	sw	a4,16(sp)
    5116:	48079463          	bnez	a5,559e <__v_printf+0x6a0>
    511a:	4629                	li	a2,10
    511c:	00ac                	addi	a1,sp,72
    511e:	91bfd0ef          	jal	ra,2a38 <strtoul>
    5122:	04714683          	lbu	a3,71(sp)
    5126:	c62a                	sw	a0,12(sp)
    5128:	03000793          	li	a5,48
    512c:	4742                	lw	a4,16(sp)
    512e:	00f69763          	bne	a3,a5,513c <__v_printf+0x23e>
    5132:	5782                	lw	a5,32(sp)
    5134:	e781                	bnez	a5,513c <__v_printf+0x23e>
    5136:	03000793          	li	a5,48
    513a:	cc3e                	sw	a5,24(sp)
    513c:	47a6                	lw	a5,72(sp)
    513e:	c83e                	sw	a5,16(sp)
    5140:	b569                	j	4fca <__v_printf+0xcc>
    5142:	401c                	lw	a5,0(s0)
    5144:	0411                	addi	s0,s0,4
    5146:	c63e                	sw	a5,12(sp)
    5148:	b549                	j	4fca <__v_printf+0xcc>
    514a:	4542                	lw	a0,16(sp)
    514c:	4629                	li	a2,10
    514e:	00ac                	addi	a1,sp,72
    5150:	ca3a                	sw	a4,20(sp)
    5152:	f76fd0ef          	jal	ra,28c8 <strtol>
    5156:	c02a                	sw	a0,0(sp)
    5158:	4752                	lw	a4,20(sp)
    515a:	00055363          	bgez	a0,5160 <__v_printf+0x262>
    515e:	c002                	sw	zero,0(sp)
    5160:	47a6                	lw	a5,72(sp)
    5162:	c83e                	sw	a5,16(sp)
    5164:	bdf9                	j	5042 <__v_printf+0x144>
    5166:	401c                	lw	a5,0(s0)
    5168:	0411                	addi	s0,s0,4
    516a:	04f103a3          	sb	a5,71(sp)
    516e:	b559                	j	4ff4 <__v_printf+0xf6>
    5170:	5542                	lw	a0,48(sp)
    5172:	dc2fd0ef          	jal	ra,2734 <strerror>
    5176:	c4aa                	sw	a0,72(sp)
    5178:	c42a                	sw	a0,8(sp)
    517a:	dd6fd0ef          	jal	ra,2750 <strlen>
    517e:	4722                	lw	a4,8(sp)
    5180:	40d4                	lw	a3,4(s1)
    5182:	4090                	lw	a2,0(s1)
    5184:	85aa                	mv	a1,a0
    5186:	c02a                	sw	a0,0(sp)
    5188:	853a                	mv	a0,a4
    518a:	9682                	jalr	a3
    518c:	4712                	lw	a4,4(sp)
    518e:	4782                	lw	a5,0(sp)
    5190:	97ba                	add	a5,a5,a4
    5192:	bd8d                	j	5004 <__v_printf+0x106>
    5194:	4018                	lw	a4,0(s0)
    5196:	00440793          	addi	a5,s0,4
    519a:	c721                	beqz	a4,51e2 <__v_printf+0x2e4>
    519c:	c4ba                	sw	a4,72(sp)
    519e:	4526                	lw	a0,72(sp)
    51a0:	cc3e                	sw	a5,24(sp)
    51a2:	daefd0ef          	jal	ra,2750 <strlen>
    51a6:	4752                	lw	a4,20(sp)
    51a8:	832a                	mv	t1,a0
    51aa:	47e2                	lw	a5,24(sp)
    51ac:	c321                	beqz	a4,51ec <__v_printf+0x2ee>
    51ae:	4702                	lw	a4,0(sp)
    51b0:	00a77363          	bgeu	a4,a0,51b6 <__v_printf+0x2b8>
    51b4:	833a                	mv	t1,a4
    51b6:	843e                	mv	s0,a5
    51b8:	c002                	sw	zero,0(sp)
    51ba:	ca02                	sw	zero,20(sp)
    51bc:	4281                	li	t0,0
    51be:	02000793          	li	a5,32
    51c2:	cc3e                	sw	a5,24(sp)
    51c4:	47b2                	lw	a5,12(sp)
    51c6:	4702                	lw	a4,0(sp)
    51c8:	46a6                	lw	a3,72(sp)
    51ca:	8fd9                	or	a5,a5,a4
    51cc:	e785                	bnez	a5,51f4 <__v_printf+0x2f6>
    51ce:	40dc                	lw	a5,4(s1)
    51d0:	4090                	lw	a2,0(s1)
    51d2:	859a                	mv	a1,t1
    51d4:	8536                	mv	a0,a3
    51d6:	c01a                	sw	t1,0(sp)
    51d8:	9782                	jalr	a5
    51da:	4792                	lw	a5,4(sp)
    51dc:	4302                	lw	t1,0(sp)
    51de:	979a                	add	a5,a5,t1
    51e0:	b515                	j	5004 <__v_printf+0x106>
    51e2:	00006737          	lui	a4,0x6
    51e6:	1f070713          	addi	a4,a4,496 # 61f0 <sg_usi_config+0x560>
    51ea:	bf4d                	j	519c <__v_printf+0x29e>
    51ec:	843e                	mv	s0,a5
    51ee:	4281                	li	t0,0
    51f0:	c002                	sw	zero,0(sp)
    51f2:	b7f1                	j	51be <__v_printf+0x2c0>
    51f4:	44029163          	bnez	t0,5636 <__v_printf+0x738>
    51f8:	47a2                	lw	a5,8(sp)
    51fa:	3a078b63          	beqz	a5,55b0 <__v_printf+0x6b2>
    51fe:	47a2                	lw	a5,8(sp)
    5200:	00f68733          	add	a4,a3,a5
    5204:	c4ba                	sw	a4,72(sp)
    5206:	4732                	lw	a4,12(sp)
    5208:	40f30333          	sub	t1,t1,a5
    520c:	8f1d                	sub	a4,a4,a5
    520e:	c63a                	sw	a4,12(sp)
    5210:	5702                	lw	a4,32(sp)
    5212:	3a070363          	beqz	a4,55b8 <__v_printf+0x6ba>
    5216:	40d8                	lw	a4,4(s1)
    5218:	4090                	lw	a2,0(s1)
    521a:	85be                	mv	a1,a5
    521c:	8536                	mv	a0,a3
    521e:	ca1a                	sw	t1,20(sp)
    5220:	c43e                	sw	a5,8(sp)
    5222:	9702                	jalr	a4
    5224:	4712                	lw	a4,4(sp)
    5226:	47a2                	lw	a5,8(sp)
    5228:	4352                	lw	t1,20(sp)
    522a:	97ba                	add	a5,a5,a4
    522c:	c23e                	sw	a5,4(sp)
    522e:	4782                	lw	a5,0(sp)
    5230:	03000613          	li	a2,48
    5234:	8526                	mv	a0,s1
    5236:	406785b3          	sub	a1,a5,t1
    523a:	ca1a                	sw	t1,20(sp)
    523c:	c63ff0ef          	jal	ra,4e9e <write_pad>
    5240:	4792                	lw	a5,4(sp)
    5242:	4352                	lw	t1,20(sp)
    5244:	40d8                	lw	a4,4(s1)
    5246:	97aa                	add	a5,a5,a0
    5248:	4090                	lw	a2,0(s1)
    524a:	4526                	lw	a0,72(sp)
    524c:	859a                	mv	a1,t1
    524e:	c43e                	sw	a5,8(sp)
    5250:	c21a                	sw	t1,4(sp)
    5252:	9702                	jalr	a4
    5254:	4312                	lw	t1,4(sp)
    5256:	47a2                	lw	a5,8(sp)
    5258:	4582                	lw	a1,0(sp)
    525a:	979a                	add	a5,a5,t1
    525c:	0065f363          	bgeu	a1,t1,5262 <__v_printf+0x364>
    5260:	859a                	mv	a1,t1
    5262:	c03e                	sw	a5,0(sp)
    5264:	47b2                	lw	a5,12(sp)
    5266:	02000613          	li	a2,32
    526a:	8526                	mv	a0,s1
    526c:	40b785b3          	sub	a1,a5,a1
    5270:	c2fff0ef          	jal	ra,4e9e <write_pad>
    5274:	4782                	lw	a5,0(sp)
    5276:	97aa                	add	a5,a5,a0
    5278:	b371                	j	5004 <__v_printf+0x106>
    527a:	c78d                	beqz	a5,52a4 <__v_printf+0x3a6>
    527c:	4662                	lw	a2,24(sp)
    527e:	03000713          	li	a4,48
    5282:	02e61163          	bne	a2,a4,52a4 <__v_printf+0x3a6>
    5286:	40d8                	lw	a4,4(s1)
    5288:	4090                	lw	a2,0(s1)
    528a:	85be                	mv	a1,a5
    528c:	8536                	mv	a0,a3
    528e:	ca1a                	sw	t1,20(sp)
    5290:	c43e                	sw	a5,8(sp)
    5292:	c036                	sw	a3,0(sp)
    5294:	9702                	jalr	a4
    5296:	4712                	lw	a4,4(sp)
    5298:	47a2                	lw	a5,8(sp)
    529a:	4352                	lw	t1,20(sp)
    529c:	4682                	lw	a3,0(sp)
    529e:	97ba                	add	a5,a5,a4
    52a0:	c23e                	sw	a5,4(sp)
    52a2:	4781                	li	a5,0
    52a4:	c43e                	sw	a5,8(sp)
    52a6:	47b2                	lw	a5,12(sp)
    52a8:	4662                	lw	a2,24(sp)
    52aa:	8526                	mv	a0,s1
    52ac:	406785b3          	sub	a1,a5,t1
    52b0:	c01a                	sw	t1,0(sp)
    52b2:	ca36                	sw	a3,20(sp)
    52b4:	bebff0ef          	jal	ra,4e9e <write_pad>
    52b8:	4792                	lw	a5,4(sp)
    52ba:	4302                	lw	t1,0(sp)
    52bc:	00a78733          	add	a4,a5,a0
    52c0:	47a2                	lw	a5,8(sp)
    52c2:	34078e63          	beqz	a5,561e <__v_printf+0x720>
    52c6:	46d2                	lw	a3,20(sp)
    52c8:	0044a383          	lw	t2,4(s1)
    52cc:	4090                	lw	a2,0(s1)
    52ce:	85be                	mv	a1,a5
    52d0:	8536                	mv	a0,a3
    52d2:	c41a                	sw	t1,8(sp)
    52d4:	c23a                	sw	a4,4(sp)
    52d6:	c03e                	sw	a5,0(sp)
    52d8:	9382                	jalr	t2
    52da:	4782                	lw	a5,0(sp)
    52dc:	4712                	lw	a4,4(sp)
    52de:	4322                	lw	t1,8(sp)
    52e0:	973e                	add	a4,a4,a5
    52e2:	ae35                	j	561e <__v_printf+0x720>
    52e4:	07800793          	li	a5,120
    52e8:	04f103a3          	sb	a5,71(sp)
    52ec:	4789                	li	a5,2
    52ee:	4705                	li	a4,1
    52f0:	c43e                	sw	a5,8(sp)
    52f2:	04714783          	lbu	a5,71(sp)
    52f6:	fa878793          	addi	a5,a5,-88
    52fa:	0017b793          	seqz	a5,a5
    52fe:	46a2                	lw	a3,8(sp)
    5300:	4301                	li	t1,0
    5302:	ce81                	beqz	a3,531a <__v_printf+0x41c>
    5304:	03000693          	li	a3,48
    5308:	04d106a3          	sb	a3,77(sp)
    530c:	04714683          	lbu	a3,71(sp)
    5310:	4309                	li	t1,2
    5312:	04d10723          	sb	a3,78(sp)
    5316:	4689                	li	a3,2
    5318:	c436                	sw	a3,8(sp)
    531a:	46b2                	lw	a3,12(sp)
    531c:	4602                	lw	a2,0(sp)
    531e:	00c6f363          	bgeu	a3,a2,5324 <__v_printf+0x426>
    5322:	c632                	sw	a2,12(sp)
    5324:	4541                	li	a0,16
    5326:	4281                	li	t0,0
    5328:	04d10693          	addi	a3,sp,77
    532c:	c4b6                	sw	a3,72(sp)
    532e:	0ae05a63          	blez	a4,53e2 <__v_printf+0x4e4>
    5332:	4685                	li	a3,1
    5334:	08d70c63          	beq	a4,a3,53cc <__v_printf+0x4ce>
    5338:	00840393          	addi	t2,s0,8
    533c:	4010                	lw	a2,0(s0)
    533e:	4054                	lw	a3,4(s0)
    5340:	4581                	li	a1,0
    5342:	08028c63          	beqz	t0,53da <__v_printf+0x4dc>
    5346:	0006da63          	bgez	a3,535a <__v_printf+0x45c>
    534a:	40c00633          	neg	a2,a2
    534e:	00c03733          	snez	a4,a2
    5352:	40d006b3          	neg	a3,a3
    5356:	8e99                	sub	a3,a3,a4
    5358:	4289                	li	t0,2
    535a:	872a                	mv	a4,a0
    535c:	04d10513          	addi	a0,sp,77
    5360:	951a                	add	a0,a0,t1
    5362:	07b00593          	li	a1,123
    5366:	da1e                	sw	t2,52(sp)
    5368:	d416                	sw	t0,40(sp)
    536a:	d21a                	sw	t1,36(sp)
    536c:	913ff0ef          	jal	ra,4c7e <__lltostr>
    5370:	53d2                	lw	t2,52(sp)
    5372:	5312                	lw	t1,36(sp)
    5374:	52a2                	lw	t0,40(sp)
    5376:	841e                	mv	s0,t2
    5378:	4752                	lw	a4,20(sp)
    537a:	47a6                	lw	a5,72(sp)
    537c:	cb5d                	beqz	a4,5432 <__v_printf+0x534>
    537e:	4705                	li	a4,1
    5380:	0ae51963          	bne	a0,a4,5432 <__v_printf+0x534>
    5384:	00678733          	add	a4,a5,t1
    5388:	00074683          	lbu	a3,0(a4)
    538c:	03000713          	li	a4,48
    5390:	0ae69163          	bne	a3,a4,5432 <__v_printf+0x534>
    5394:	4702                	lw	a4,0(sp)
    5396:	c345                	beqz	a4,5436 <__v_printf+0x538>
    5398:	4722                	lw	a4,8(sp)
    539a:	c319                	beqz	a4,53a0 <__v_printf+0x4a2>
    539c:	c402                	sw	zero,8(sp)
    539e:	4301                	li	t1,0
    53a0:	4709                	li	a4,2
    53a2:	08e29d63          	bne	t0,a4,543c <__v_printf+0x53e>
    53a6:	fff78713          	addi	a4,a5,-1
    53aa:	c4ba                	sw	a4,72(sp)
    53ac:	02d00713          	li	a4,45
    53b0:	fee78fa3          	sb	a4,-1(a5)
    53b4:	0305                	addi	t1,t1,1
    53b6:	b539                	j	51c4 <__v_printf+0x2c6>
    53b8:	4781                	li	a5,0
    53ba:	b791                	j	52fe <__v_printf+0x400>
    53bc:	4509                	li	a0,2
    53be:	4781                	li	a5,0
    53c0:	4281                	li	t0,0
    53c2:	b9c1                	j	5092 <__v_printf+0x194>
    53c4:	4781                	li	a5,0
    53c6:	4281                	li	t0,0
    53c8:	4521                	li	a0,8
    53ca:	b1e1                	j	5092 <__v_printf+0x194>
    53cc:	00440393          	addi	t2,s0,4
    53d0:	400c                	lw	a1,0(s0)
    53d2:	00029d63          	bnez	t0,53ec <__v_printf+0x4ee>
    53d6:	4601                	li	a2,0
    53d8:	4681                	li	a3,0
    53da:	4405                	li	s0,1
    53dc:	f6e44fe3          	blt	s0,a4,535a <__v_printf+0x45c>
    53e0:	a01d                	j	5406 <__v_printf+0x508>
    53e2:	00440393          	addi	t2,s0,4
    53e6:	400c                	lw	a1,0(s0)
    53e8:	00028863          	beqz	t0,53f8 <__v_printf+0x4fa>
    53ec:	4285                	li	t0,1
    53ee:	0005d563          	bgez	a1,53f8 <__v_printf+0x4fa>
    53f2:	40b005b3          	neg	a1,a1
    53f6:	4289                	li	t0,2
    53f8:	fc075fe3          	bgez	a4,53d6 <__v_printf+0x4d8>
    53fc:	56fd                	li	a3,-1
    53fe:	02d70763          	beq	a4,a3,542c <__v_printf+0x52e>
    5402:	0ff5f593          	andi	a1,a1,255
    5406:	873e                	mv	a4,a5
    5408:	04d10793          	addi	a5,sp,77
    540c:	86aa                	mv	a3,a0
    540e:	862e                	mv	a2,a1
    5410:	00678533          	add	a0,a5,t1
    5414:	07b00593          	li	a1,123
    5418:	da1e                	sw	t2,52(sp)
    541a:	d416                	sw	t0,40(sp)
    541c:	d21a                	sw	t1,36(sp)
    541e:	9c9ff0ef          	jal	ra,4de6 <__ltostr>
    5422:	53d2                	lw	t2,52(sp)
    5424:	52a2                	lw	t0,40(sp)
    5426:	5312                	lw	t1,36(sp)
    5428:	841e                	mv	s0,t2
    542a:	b7b9                	j	5378 <__v_printf+0x47a>
    542c:	05c2                	slli	a1,a1,0x10
    542e:	81c1                	srli	a1,a1,0x10
    5430:	bfd9                	j	5406 <__v_printf+0x508>
    5432:	932a                	add	t1,t1,a0
    5434:	b7b5                	j	53a0 <__v_printf+0x4a2>
    5436:	4301                	li	t1,0
    5438:	c402                	sw	zero,8(sp)
    543a:	b79d                	j	53a0 <__v_printf+0x4a2>
    543c:	d80284e3          	beqz	t0,51c4 <__v_printf+0x2c6>
    5440:	4772                	lw	a4,28(sp)
    5442:	ef19                	bnez	a4,5460 <__v_printf+0x562>
    5444:	5732                	lw	a4,44(sp)
    5446:	4281                	li	t0,0
    5448:	d6070ee3          	beqz	a4,51c4 <__v_printf+0x2c6>
    544c:	02000713          	li	a4,32
    5450:	fff78693          	addi	a3,a5,-1
    5454:	c4b6                	sw	a3,72(sp)
    5456:	fee78fa3          	sb	a4,-1(a5)
    545a:	0305                	addi	t1,t1,1
    545c:	4285                	li	t0,1
    545e:	b39d                	j	51c4 <__v_printf+0x2c6>
    5460:	02b00713          	li	a4,43
    5464:	b7f5                	j	5450 <__v_printf+0x552>
    5466:	00840793          	addi	a5,s0,8
    546a:	da3e                	sw	a5,52(sp)
    546c:	401c                	lw	a5,0(s0)
    546e:	d23e                	sw	a5,36(sp)
    5470:	405c                	lw	a5,4(s0)
    5472:	d43e                	sw	a5,40(sp)
    5474:	04d10793          	addi	a5,sp,77
    5478:	c4be                	sw	a5,72(sp)
    547a:	47b2                	lw	a5,12(sp)
    547c:	e399                	bnez	a5,5482 <__v_printf+0x584>
    547e:	4785                	li	a5,1
    5480:	c63e                	sw	a5,12(sp)
    5482:	47d2                	lw	a5,20(sp)
    5484:	e399                	bnez	a5,548a <__v_printf+0x58c>
    5486:	4799                	li	a5,6
    5488:	c03e                	sw	a5,0(sp)
    548a:	42f2                	lw	t0,28(sp)
    548c:	00029e63          	bnez	t0,54a8 <__v_printf+0x5aa>
    5490:	5712                	lw	a4,36(sp)
    5492:	57a2                	lw	a5,40(sp)
    5494:	4601                	li	a2,0
    5496:	4681                	li	a3,0
    5498:	853a                	mv	a0,a4
    549a:	85be                	mv	a1,a5
    549c:	dc1a                	sw	t1,56(sp)
    549e:	8cefc0ef          	jal	ra,156c <__ledf2>
    54a2:	5362                	lw	t1,56(sp)
    54a4:	01f55293          	srli	t0,a0,0x1f
    54a8:	5412                	lw	s0,36(sp)
    54aa:	53a2                	lw	t2,40(sp)
    54ac:	4782                	lw	a5,0(sp)
    54ae:	4732                	lw	a4,12(sp)
    54b0:	07f00693          	li	a3,127
    54b4:	04d10613          	addi	a2,sp,77
    54b8:	8522                	mv	a0,s0
    54ba:	859e                	mv	a1,t2
    54bc:	c096                	sw	t0,64(sp)
    54be:	dc1a                	sw	t1,56(sp)
    54c0:	b7aff0ef          	jal	ra,483a <__dtostr>
    54c4:	47d2                	lw	a5,20(sp)
    54c6:	de2a                	sw	a0,60(sp)
    54c8:	5362                	lw	t1,56(sp)
    54ca:	4286                	lw	t0,64(sp)
    54cc:	cb95                	beqz	a5,5500 <__v_printf+0x602>
    54ce:	4426                	lw	s0,72(sp)
    54d0:	02e00593          	li	a1,46
    54d4:	dc16                	sw	t0,56(sp)
    54d6:	8522                	mv	a0,s0
    54d8:	ca1a                	sw	t1,20(sp)
    54da:	ed1fc0ef          	jal	ra,23aa <strchr>
    54de:	4352                	lw	t1,20(sp)
    54e0:	52e2                	lw	t0,56(sp)
    54e2:	57f2                	lw	a5,60(sp)
    54e4:	cd49                	beqz	a0,557e <__v_printf+0x680>
    54e6:	4782                	lw	a5,0(sp)
    54e8:	e399                	bnez	a5,54ee <__v_printf+0x5f0>
    54ea:	47a2                	lw	a5,8(sp)
    54ec:	cb81                	beqz	a5,54fc <__v_printf+0x5fe>
    54ee:	0505                	addi	a0,a0,1
    54f0:	4782                	lw	a5,0(sp)
    54f2:	c789                	beqz	a5,54fc <__v_printf+0x5fe>
    54f4:	0505                	addi	a0,a0,1
    54f6:	00054783          	lbu	a5,0(a0)
    54fa:	efb5                	bnez	a5,5576 <__v_printf+0x678>
    54fc:	00050023          	sb	zero,0(a0)
    5500:	06700793          	li	a5,103
    5504:	04f31a63          	bne	t1,a5,5558 <__v_printf+0x65a>
    5508:	4526                	lw	a0,72(sp)
    550a:	02e00593          	li	a1,46
    550e:	c416                	sw	t0,8(sp)
    5510:	e9bfc0ef          	jal	ra,23aa <strchr>
    5514:	842a                	mv	s0,a0
    5516:	42a2                	lw	t0,8(sp)
    5518:	c121                	beqz	a0,5558 <__v_printf+0x65a>
    551a:	06500593          	li	a1,101
    551e:	e8dfc0ef          	jal	ra,23aa <strchr>
    5522:	42a2                	lw	t0,8(sp)
    5524:	85aa                	mv	a1,a0
    5526:	00044783          	lbu	a5,0(s0)
    552a:	e7b5                	bnez	a5,5596 <__v_printf+0x698>
    552c:	c191                	beqz	a1,5530 <__v_printf+0x632>
    552e:	842e                	mv	s0,a1
    5530:	03000693          	li	a3,48
    5534:	fff44703          	lbu	a4,-1(s0)
    5538:	fff40513          	addi	a0,s0,-1
    553c:	04d70f63          	beq	a4,a3,559a <__v_printf+0x69c>
    5540:	02e00693          	li	a3,46
    5544:	00d70363          	beq	a4,a3,554a <__v_printf+0x64c>
    5548:	8522                	mv	a0,s0
    554a:	00050023          	sb	zero,0(a0)
    554e:	c589                	beqz	a1,5558 <__v_printf+0x65a>
    5550:	c416                	sw	t0,8(sp)
    5552:	e73fc0ef          	jal	ra,23c4 <strcpy>
    5556:	42a2                	lw	t0,8(sp)
    5558:	47f2                	lw	a5,28(sp)
    555a:	10079263          	bnez	a5,565e <__v_printf+0x760>
    555e:	57b2                	lw	a5,44(sp)
    5560:	e3e5                	bnez	a5,5640 <__v_printf+0x742>
    5562:	4526                	lw	a0,72(sp)
    5564:	ce16                	sw	t0,28(sp)
    5566:	9eafd0ef          	jal	ra,2750 <strlen>
    556a:	832a                	mv	t1,a0
    556c:	5452                	lw	s0,52(sp)
    556e:	ca02                	sw	zero,20(sp)
    5570:	c402                	sw	zero,8(sp)
    5572:	42f2                	lw	t0,28(sp)
    5574:	b981                	j	51c4 <__v_printf+0x2c6>
    5576:	4782                	lw	a5,0(sp)
    5578:	17fd                	addi	a5,a5,-1
    557a:	c03e                	sw	a5,0(sp)
    557c:	bf95                	j	54f0 <__v_printf+0x5f2>
    557e:	4722                	lw	a4,8(sp)
    5580:	d341                	beqz	a4,5500 <__v_printf+0x602>
    5582:	943e                	add	s0,s0,a5
    5584:	02e00713          	li	a4,46
    5588:	00e40023          	sb	a4,0(s0)
    558c:	4526                	lw	a0,72(sp)
    558e:	953e                	add	a0,a0,a5
    5590:	000500a3          	sb	zero,1(a0)
    5594:	b7b5                	j	5500 <__v_printf+0x602>
    5596:	0405                	addi	s0,s0,1
    5598:	b779                	j	5526 <__v_printf+0x628>
    559a:	842a                	mv	s0,a0
    559c:	bf61                	j	5534 <__v_printf+0x636>
    559e:	57fd                	li	a5,-1
    55a0:	c23e                	sw	a5,4(sp)
    55a2:	40de                	lw	ra,212(sp)
    55a4:	444e                	lw	s0,208(sp)
    55a6:	4512                	lw	a0,4(sp)
    55a8:	44be                	lw	s1,204(sp)
    55aa:	0d810113          	addi	sp,sp,216
    55ae:	8082                	ret
    55b0:	5782                	lw	a5,32(sp)
    55b2:	c6079ee3          	bnez	a5,522e <__v_printf+0x330>
    55b6:	4781                	li	a5,0
    55b8:	4752                	lw	a4,20(sp)
    55ba:	cc0700e3          	beqz	a4,527a <__v_printf+0x37c>
    55be:	4582                	lw	a1,0(sp)
    55c0:	0065f363          	bgeu	a1,t1,55c6 <__v_printf+0x6c8>
    55c4:	859a                	mv	a1,t1
    55c6:	ca3e                	sw	a5,20(sp)
    55c8:	47b2                	lw	a5,12(sp)
    55ca:	02000613          	li	a2,32
    55ce:	8526                	mv	a0,s1
    55d0:	40b785b3          	sub	a1,a5,a1
    55d4:	c41a                	sw	t1,8(sp)
    55d6:	cc36                	sw	a3,24(sp)
    55d8:	8c7ff0ef          	jal	ra,4e9e <write_pad>
    55dc:	4792                	lw	a5,4(sp)
    55de:	4322                	lw	t1,8(sp)
    55e0:	00f50733          	add	a4,a0,a5
    55e4:	47d2                	lw	a5,20(sp)
    55e6:	cf99                	beqz	a5,5604 <__v_printf+0x706>
    55e8:	46e2                	lw	a3,24(sp)
    55ea:	0044a383          	lw	t2,4(s1)
    55ee:	4090                	lw	a2,0(s1)
    55f0:	85be                	mv	a1,a5
    55f2:	8536                	mv	a0,a3
    55f4:	c61a                	sw	t1,12(sp)
    55f6:	c43a                	sw	a4,8(sp)
    55f8:	c23e                	sw	a5,4(sp)
    55fa:	9382                	jalr	t2
    55fc:	4792                	lw	a5,4(sp)
    55fe:	4722                	lw	a4,8(sp)
    5600:	4332                	lw	t1,12(sp)
    5602:	973e                	add	a4,a4,a5
    5604:	4782                	lw	a5,0(sp)
    5606:	03000613          	li	a2,48
    560a:	8526                	mv	a0,s1
    560c:	406785b3          	sub	a1,a5,t1
    5610:	c23a                	sw	a4,4(sp)
    5612:	c01a                	sw	t1,0(sp)
    5614:	88bff0ef          	jal	ra,4e9e <write_pad>
    5618:	4712                	lw	a4,4(sp)
    561a:	4302                	lw	t1,0(sp)
    561c:	972a                	add	a4,a4,a0
    561e:	40dc                	lw	a5,4(s1)
    5620:	4090                	lw	a2,0(s1)
    5622:	4526                	lw	a0,72(sp)
    5624:	859a                	mv	a1,t1
    5626:	c23a                	sw	a4,4(sp)
    5628:	c01a                	sw	t1,0(sp)
    562a:	9782                	jalr	a5
    562c:	4302                	lw	t1,0(sp)
    562e:	4712                	lw	a4,4(sp)
    5630:	006707b3          	add	a5,a4,t1
    5634:	bac1                	j	5004 <__v_printf+0x106>
    5636:	47a2                	lw	a5,8(sp)
    5638:	bc0793e3          	bnez	a5,51fe <__v_printf+0x300>
    563c:	4785                	li	a5,1
    563e:	b6c9                	j	5200 <__v_printf+0x302>
    5640:	57a2                	lw	a5,40(sp)
    5642:	5712                	lw	a4,36(sp)
    5644:	4601                	li	a2,0
    5646:	85be                	mv	a1,a5
    5648:	4681                	li	a3,0
    564a:	853a                	mv	a0,a4
    564c:	c416                	sw	t0,8(sp)
    564e:	e6bfb0ef          	jal	ra,14b8 <__gedf2>
    5652:	02000793          	li	a5,32
    5656:	42a2                	lw	t0,8(sp)
    5658:	02055163          	bgez	a0,567a <__v_printf+0x77c>
    565c:	b719                	j	5562 <__v_printf+0x664>
    565e:	5712                	lw	a4,36(sp)
    5660:	57a2                	lw	a5,40(sp)
    5662:	4601                	li	a2,0
    5664:	4681                	li	a3,0
    5666:	853a                	mv	a0,a4
    5668:	85be                	mv	a1,a5
    566a:	c416                	sw	t0,8(sp)
    566c:	e4dfb0ef          	jal	ra,14b8 <__gedf2>
    5670:	42a2                	lw	t0,8(sp)
    5672:	ee0548e3          	bltz	a0,5562 <__v_printf+0x664>
    5676:	02b00793          	li	a5,43
    567a:	4726                	lw	a4,72(sp)
    567c:	fff70693          	addi	a3,a4,-1
    5680:	c4b6                	sw	a3,72(sp)
    5682:	fef70fa3          	sb	a5,-1(a4)
    5686:	bdf1                	j	5562 <__v_printf+0x664>
    5688:	8a071ee3          	bnez	a4,4f44 <__v_printf+0x46>
    568c:	47c2                	lw	a5,16(sp)
    568e:	c83e                	sw	a5,16(sp)
    5690:	b069                	j	4f1a <__v_printf+0x1c>
	...
