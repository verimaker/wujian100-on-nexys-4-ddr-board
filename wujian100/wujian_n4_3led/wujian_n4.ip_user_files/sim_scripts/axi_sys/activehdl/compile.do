vlib work
vlib activehdl

vlib activehdl/xil_defaultlib
vlib activehdl/xpm
vlib activehdl/ahblite_axi_bridge_v3_0_14
vlib activehdl/gigantic_mux
vlib activehdl/xlconcat_v2_1_3
vlib activehdl/generic_baseblocks_v2_1_0
vlib activehdl/fifo_generator_v13_2_4
vlib activehdl/axi_data_fifo_v2_1_18
vlib activehdl/axi_infrastructure_v1_1_0
vlib activehdl/axi_register_slice_v2_1_19
vlib activehdl/axi_protocol_converter_v2_1_19

vmap xil_defaultlib activehdl/xil_defaultlib
vmap xpm activehdl/xpm
vmap ahblite_axi_bridge_v3_0_14 activehdl/ahblite_axi_bridge_v3_0_14
vmap gigantic_mux activehdl/gigantic_mux
vmap xlconcat_v2_1_3 activehdl/xlconcat_v2_1_3
vmap generic_baseblocks_v2_1_0 activehdl/generic_baseblocks_v2_1_0
vmap fifo_generator_v13_2_4 activehdl/fifo_generator_v13_2_4
vmap axi_data_fifo_v2_1_18 activehdl/axi_data_fifo_v2_1_18
vmap axi_infrastructure_v1_1_0 activehdl/axi_infrastructure_v1_1_0
vmap axi_register_slice_v2_1_19 activehdl/axi_register_slice_v2_1_19
vmap axi_protocol_converter_v2_1_19 activehdl/axi_protocol_converter_v2_1_19

vlog -work xil_defaultlib  -sv2k12 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work ahblite_axi_bridge_v3_0_14 -93 \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/2efd/hdl/ahblite_axi_bridge_v3_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -93 \
"../../../bd/axi_sys/ip/axi_sys_ahblite_axi_bridge_0_0/sim/axi_sys_ahblite_axi_bridge_0_0.vhd" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ipshared/ed48/hdl/wujian_myip_v1_0_S00_AXI.v" \
"../../../bd/axi_sys/ipshared/ed48/hdl/wujian_myip_v1_0.v" \
"../../../bd/axi_sys/ip/axi_sys_wujian_myip_0_1/sim/axi_sys_wujian_myip_0_1.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_0/sim/bd_521e_ila_lib_0.v" \

vlog -work gigantic_mux  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/d322/hdl/gigantic_mux_v1_0_cntr.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_1/bd_521e_g_inst_0_gigantic_mux.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_1/sim/bd_521e_g_inst_0.v" \

vlog -work xlconcat_v2_1_3  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_2/sim/bd_521e_slot_0_aw_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_3/sim/bd_521e_slot_0_w_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_4/sim/bd_521e_slot_0_b_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_5/sim/bd_521e_slot_0_ar_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_6/sim/bd_521e_slot_0_r_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/sim/bd_521e.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/sim/axi_sys_system_ila_0_0.v" \

vlog -work generic_baseblocks_v2_1_0  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_4 -93 \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_4  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_18  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_infrastructure_v1_1_0  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_19  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work axi_protocol_converter_v2_1_19  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib  -v2k5 "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ip/axi_sys_auto_pc_0/sim/axi_sys_auto_pc_0.v" \
"../../../bd/axi_sys/sim/axi_sys.v" \

vlog -work xil_defaultlib \
"glbl.v"

