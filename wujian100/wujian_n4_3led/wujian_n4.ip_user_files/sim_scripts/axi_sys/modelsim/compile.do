vlib modelsim_lib/work
vlib modelsim_lib/msim

vlib modelsim_lib/msim/xil_defaultlib
vlib modelsim_lib/msim/xpm
vlib modelsim_lib/msim/ahblite_axi_bridge_v3_0_14
vlib modelsim_lib/msim/gigantic_mux
vlib modelsim_lib/msim/xlconcat_v2_1_3
vlib modelsim_lib/msim/generic_baseblocks_v2_1_0
vlib modelsim_lib/msim/fifo_generator_v13_2_4
vlib modelsim_lib/msim/axi_data_fifo_v2_1_18
vlib modelsim_lib/msim/axi_infrastructure_v1_1_0
vlib modelsim_lib/msim/axi_register_slice_v2_1_19
vlib modelsim_lib/msim/axi_protocol_converter_v2_1_19

vmap xil_defaultlib modelsim_lib/msim/xil_defaultlib
vmap xpm modelsim_lib/msim/xpm
vmap ahblite_axi_bridge_v3_0_14 modelsim_lib/msim/ahblite_axi_bridge_v3_0_14
vmap gigantic_mux modelsim_lib/msim/gigantic_mux
vmap xlconcat_v2_1_3 modelsim_lib/msim/xlconcat_v2_1_3
vmap generic_baseblocks_v2_1_0 modelsim_lib/msim/generic_baseblocks_v2_1_0
vmap fifo_generator_v13_2_4 modelsim_lib/msim/fifo_generator_v13_2_4
vmap axi_data_fifo_v2_1_18 modelsim_lib/msim/axi_data_fifo_v2_1_18
vmap axi_infrastructure_v1_1_0 modelsim_lib/msim/axi_infrastructure_v1_1_0
vmap axi_register_slice_v2_1_19 modelsim_lib/msim/axi_register_slice_v2_1_19
vmap axi_protocol_converter_v2_1_19 modelsim_lib/msim/axi_protocol_converter_v2_1_19

vlog -work xil_defaultlib -64 -incr -sv "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_cdc/hdl/xpm_cdc.sv" \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_memory/hdl/xpm_memory.sv" \

vcom -work xpm -64 -93 \
"C:/Xilinx/Vivado/2019.1/data/ip/xpm/xpm_VCOMP.vhd" \

vcom -work ahblite_axi_bridge_v3_0_14 -64 -93 \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/2efd/hdl/ahblite_axi_bridge_v3_0_vh_rfs.vhd" \

vcom -work xil_defaultlib -64 -93 \
"../../../bd/axi_sys/ip/axi_sys_ahblite_axi_bridge_0_0/sim/axi_sys_ahblite_axi_bridge_0_0.vhd" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ipshared/ed48/hdl/wujian_myip_v1_0_S00_AXI.v" \
"../../../bd/axi_sys/ipshared/ed48/hdl/wujian_myip_v1_0.v" \
"../../../bd/axi_sys/ip/axi_sys_wujian_myip_0_1/sim/axi_sys_wujian_myip_0_1.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_0/sim/bd_521e_ila_lib_0.v" \

vlog -work gigantic_mux -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/d322/hdl/gigantic_mux_v1_0_cntr.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_1/bd_521e_g_inst_0_gigantic_mux.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_1/sim/bd_521e_g_inst_0.v" \

vlog -work xlconcat_v2_1_3 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/442e/hdl/xlconcat_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_2/sim/bd_521e_slot_0_aw_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_3/sim/bd_521e_slot_0_w_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_4/sim/bd_521e_slot_0_b_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_5/sim/bd_521e_slot_0_ar_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/ip/ip_6/sim/bd_521e_slot_0_r_0.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/bd_0/sim/bd_521e.v" \
"../../../bd/axi_sys/ip/axi_sys_system_ila_0_0/sim/axi_sys_system_ila_0_0.v" \

vlog -work generic_baseblocks_v2_1_0 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/b752/hdl/generic_baseblocks_v2_1_vl_rfs.v" \

vlog -work fifo_generator_v13_2_4 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1f5a/simulation/fifo_generator_vlog_beh.v" \

vcom -work fifo_generator_v13_2_4 -64 -93 \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.vhd" \

vlog -work fifo_generator_v13_2_4 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1f5a/hdl/fifo_generator_v13_2_rfs.v" \

vlog -work axi_data_fifo_v2_1_18 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/5b9c/hdl/axi_data_fifo_v2_1_vl_rfs.v" \

vlog -work axi_infrastructure_v1_1_0 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl/axi_infrastructure_v1_1_vl_rfs.v" \

vlog -work axi_register_slice_v2_1_19 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/4d88/hdl/axi_register_slice_v2_1_vl_rfs.v" \

vlog -work axi_protocol_converter_v2_1_19 -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/c83a/hdl/axi_protocol_converter_v2_1_vl_rfs.v" \

vlog -work xil_defaultlib -64 -incr "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/ec67/hdl" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/1b7e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/122e/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/6887/hdl/verilog" "+incdir+../../../../wujian_n4.srcs/sources_1/bd/axi_sys/ipshared/9623/hdl/verilog" \
"../../../bd/axi_sys/ip/axi_sys_auto_pc_0/sim/axi_sys_auto_pc_0.v" \
"../../../bd/axi_sys/sim/axi_sys.v" \

vlog -work xil_defaultlib \
"glbl.v"

