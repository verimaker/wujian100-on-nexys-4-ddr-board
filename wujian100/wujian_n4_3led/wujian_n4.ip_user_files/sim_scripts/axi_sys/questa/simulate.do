onbreak {quit -f}
onerror {quit -f}

vsim -t 1ps -lib xil_defaultlib axi_sys_opt

do {wave.do}

view wave
view structure
view signals

do {axi_sys.udo}

run -all

quit -force
