//Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
//--------------------------------------------------------------------------------
//Tool Version: Vivado v.2019.1 (win64) Build 2552052 Fri May 24 14:49:42 MDT 2019
//Date        : Fri Mar  5 22:00:03 2021
//Host        : DESKTOP-ID6TDBA running 64-bit major release  (build 9200)
//Command     : generate_target axi_sys_wrapper.bd
//Design      : axi_sys_wrapper
//Purpose     : IP block netlist
//--------------------------------------------------------------------------------
`timescale 1 ps / 1 ps

module axi_sys_wrapper
   (myio,
    s_ahb_haddr,
    s_ahb_hburst,
    s_ahb_hclk,
    s_ahb_hprot,
    s_ahb_hrdata,
    s_ahb_hready_in,
    s_ahb_hready_out,
    s_ahb_hresetn,
    s_ahb_hresp,
    s_ahb_hsize,
    s_ahb_htrans,
    s_ahb_hwdata,
    s_ahb_hwrite,
    s_ahb_sel);
  output [31:0]myio;
  input [31:0]s_ahb_haddr;
  input [2:0]s_ahb_hburst;
  input s_ahb_hclk;
  input [3:0]s_ahb_hprot;
  output [31:0]s_ahb_hrdata;
  input s_ahb_hready_in;
  output s_ahb_hready_out;
  input s_ahb_hresetn;
  output s_ahb_hresp;
  input [2:0]s_ahb_hsize;
  input [1:0]s_ahb_htrans;
  input [31:0]s_ahb_hwdata;
  input s_ahb_hwrite;
  input s_ahb_sel;

  wire [31:0]myio;
  wire [31:0]s_ahb_haddr;
  wire [2:0]s_ahb_hburst;
  wire s_ahb_hclk;
  wire [3:0]s_ahb_hprot;
  wire [31:0]s_ahb_hrdata;
  wire s_ahb_hready_in;
  wire s_ahb_hready_out;
  wire s_ahb_hresetn;
  wire s_ahb_hresp;
  wire [2:0]s_ahb_hsize;
  wire [1:0]s_ahb_htrans;
  wire [31:0]s_ahb_hwdata;
  wire s_ahb_hwrite;
  wire s_ahb_sel;

  axi_sys axi_sys_i
       (.myio(myio),
        .s_ahb_haddr(s_ahb_haddr),
        .s_ahb_hburst(s_ahb_hburst),
        .s_ahb_hclk(s_ahb_hclk),
        .s_ahb_hprot(s_ahb_hprot),
        .s_ahb_hrdata(s_ahb_hrdata),
        .s_ahb_hready_in(s_ahb_hready_in),
        .s_ahb_hready_out(s_ahb_hready_out),
        .s_ahb_hresetn(s_ahb_hresetn),
        .s_ahb_hresp(s_ahb_hresp),
        .s_ahb_hsize(s_ahb_hsize),
        .s_ahb_htrans(s_ahb_htrans),
        .s_ahb_hwdata(s_ahb_hwdata),
        .s_ahb_hwrite(s_ahb_hwrite),
        .s_ahb_sel(s_ahb_sel));
endmodule
