`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 2021/03/05 11:42:31
// Design Name: 
// Module Name: myio_top
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module myio_top(
        myio,
        haddr,
        hclk,
        hprot,
        hrdata,
        hready,
        hresp,
        hrst_b,
        hsel,
        hsize,
        htrans,
        hwdata,
        hwrite,
        hburst,
        intr
);


output  [31:0]  myio;
input   [31:0]  haddr;        
input           hclk;         
input   [3 :0]  hprot;        
input           hrst_b;       
input           hsel;         
input   [2 :0]  hsize;        
input   [1 :0]  htrans;       
input   [31:0]  hwdata;       
input           hwrite;       
output  [31:0]  hrdata;       
output          hready;  
output  [1 :0]  hresp;    
output		intr; 
input  [2:0] hburst;

wire  [31:0]  myio;

wire  [31:0]  hrdata;       
wire          hready;  
wire  [1 :0]  hresp;    
wire          intr;
wire   hclk;
wire   hsel;
wire   [31:0]  haddr;    
wire   [3:0]    hprot;     
wire   [1:0]     htrans;    
wire   [2:0]  hsize;     
wire   hwrite;
wire   [2:0]  hburst;    
wire   [31:0]  hwdata;   
wire   hready_in; 

assign hready_in = hready;
assign hresp[1] = 1'b0;

axi_sys_wrapper  u_axi_sys_wrapper (
    .s_ahb_haddr             ( haddr        ),
    .s_ahb_hburst            ( hburst       ),
    .s_ahb_hclk              ( hclk         ),
    .s_ahb_hprot             ( hprot        ),
    .s_ahb_hready_in         ( hready_in    ),
    .s_ahb_hresetn           ( hrst_b      ),
    .s_ahb_hsize             ( hsize        ),
    .s_ahb_htrans            ( htrans       ),
    .s_ahb_hwdata            ( hwdata       ),
    .s_ahb_hwrite            ( hwrite       ),
    .s_ahb_sel               ( hsel         ),

    .myio                    ( myio               ),
    .s_ahb_hrdata            ( hrdata       ),
    .s_ahb_hready_out        ( hready   ),
    .s_ahb_hresp             ( hresp[0] )
);



endmodule
