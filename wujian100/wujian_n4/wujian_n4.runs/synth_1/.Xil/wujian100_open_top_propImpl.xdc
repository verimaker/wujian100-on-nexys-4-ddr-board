set_property SRC_FILE_INFO {cfile:D:/Project/Part-timeJob/YXpaper/ali/2021/HelloT-Head/prj/wujian100/wujian_n4/wujian_n4.srcs/constrs_1/imports/constrs_1/imports/xdc/XC7A200T3B.xdc rfile:../../../wujian_n4.srcs/constrs_1/imports/constrs_1/imports/xdc/XC7A200T3B.xdc id:1} [current_design]
set_property src_info {type:XDC file:1 line:91 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN E3 [get_ports PIN_EHS]
set_property src_info {type:XDC file:1 line:93 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C12 [get_ports PAD_MCURST]
set_property src_info {type:XDC file:1 line:107 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN C17    [get_ports PAD_JTAG_TCLK]
set_property src_info {type:XDC file:1 line:108 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN D18    [get_ports PAD_JTAG_TMS]
set_property src_info {type:XDC file:1 line:189 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN H17    [get_ports PAD_GPIO_0]
set_property src_info {type:XDC file:1 line:190 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN K15    [get_ports PAD_GPIO_1]
set_property src_info {type:XDC file:1 line:191 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN J13    [get_ports PAD_GPIO_2]
set_property src_info {type:XDC file:1 line:192 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN N14    [get_ports PAD_GPIO_3]
set_property src_info {type:XDC file:1 line:193 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R18    [get_ports PAD_GPIO_4]
set_property src_info {type:XDC file:1 line:194 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V17    [get_ports PAD_GPIO_5]
set_property src_info {type:XDC file:1 line:195 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U17    [get_ports PAD_GPIO_6]
set_property src_info {type:XDC file:1 line:196 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U16    [get_ports PAD_GPIO_7]
set_property src_info {type:XDC file:1 line:197 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V16    [get_ports PAD_GPIO_8]
set_property src_info {type:XDC file:1 line:198 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN T15    [get_ports PAD_GPIO_9]
set_property src_info {type:XDC file:1 line:199 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN U14    [get_ports PAD_GPIO_10]
set_property src_info {type:XDC file:1 line:200 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN T16    [get_ports PAD_GPIO_11]
set_property src_info {type:XDC file:1 line:201 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V15    [get_ports PAD_GPIO_12]
set_property src_info {type:XDC file:1 line:202 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V14    [get_ports PAD_GPIO_13]
set_property src_info {type:XDC file:1 line:203 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V12    [get_ports PAD_GPIO_14]
set_property src_info {type:XDC file:1 line:204 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN V11    [get_ports PAD_GPIO_15]
set_property src_info {type:XDC file:1 line:374 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN G16   [get_ports PAD_USI0_SCLK ]
set_property src_info {type:XDC file:1 line:375 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN F16   [get_ports PAD_USI0_SD0 ]
set_property src_info {type:XDC file:1 line:411 export:INPUT save:INPUT read:READ} [current_design]
set_property PACKAGE_PIN R12       [get_ports POUT_EHS]
set_property src_info {type:XDC file:1 line:433 export:INPUT save:INPUT read:READ} [current_design]
set_false_path -from [get_pins u_clk_wiz_0/inst/mmcm_adv_inst/CLKOUT0] -to [get_pins x_cpu_top/CPU/x_cr_tcipif_top/x_cr_coretim_top/refclk_ff1_reg/D]
